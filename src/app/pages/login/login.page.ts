import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { LoginService } from './login.service';
import { StorageService } from 'src/app/services/storage.service';
import { GlobalService } from 'src/app/services/global.service';
import { Router } from '@angular/router';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { LoadingController, AlertController } from '@ionic/angular';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Market } from '@ionic-native/market/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  passwordType: any;

  isAccountIDVerified: boolean;
  companyCode: any;
  isUpdated = true;

  togglePasswordType() {
    this.passwordType = this.passwordType || 'password';
    this.passwordType = this.passwordType === 'password' ? 'text' : 'password';
  }

  data: any = {};

  currentYear: any = new Date().getFullYear();

  constructor(private service: LoginService,
    private storage: StorageService,
    private global: GlobalService,
    private router: Router,
    private loadingController: LoadingController,
    private googlePlus: GooglePlus,
    private alertController: AlertController,
    private appVersion: AppVersion,
    private market: Market,
    private backgroundMode: BackgroundMode
  ) { }

  ionViewWillEnter() {
    this.loadingController.create(
      {
        message: 'Please wait...'
      }
    ).then(loader => {
      loader.present();
      let info = '';
      this.appVersion.getVersionNumber().then(
        version => {
          info = version;
          this.service.getMobileAppVersion('/getbrandsetting').subscribe(
            response => {
              if (response[0]['mobileAppVersion'] === info) {
                loader.dismiss();
                this.loggingInApp();
              } else {
                this.isUpdated = false;
                this.alertController.create({
                  header: 'New Update !!',
                  subHeader: 'Required',
                  message: 'A new version is released, Please upgrade to the latest version to continue using this app.',
                  buttons: [{
                    text: 'Update',
                    handler: () => {
                      this.backgroundMode.disable();
                      this.market.open('com.smartdocs.tcl');
                      navigator['app'].exit();
                    }
                  }],
                  backdropDismiss: false
                }).then(alert => {
                  loader.dismiss();
                  alert.present();
                });
              }
            },
            error => {
              loader.dismiss();
              this.isUpdated = false;
              this.global.displayToastMessage('Error Occured while fetching device version.');
            });
        },
        error => {
          loader.dismiss();
          this.global.displayToastMessage('Unable to fetch Version of the application.');
        });
    })
  }

  loggingInApp() {
    this.storage.getUser().then(storageUser => {
      if (!(storageUser === null)) {
        this.loadingController.create(
          {
            message: 'Please wait...',
            spinner: 'circular'
          }
        ).then(loader => {
          loader.present();
          sessionStorage.setItem('accessToken', storageUser.accessToken)
          this.service.getAccountDetails().subscribe((user: any) => {
            user['accessToken'] = sessionStorage.getItem('accessToken');
            this.service.saveUserData(user);
            this.global.displayToastMessage('You have successfully logged in.');
            if (user.authorities) {
              loader.dismiss();
              this.router.navigate(['/main/inbox']);
              this.global.displayToastMessage("You've logged in successfully.");
            }
          }, error => {
            loader.dismiss();
            this.global.displayToastMessage('Session time out, Please login again');
          });
        });
      }
    });
  }

  ionViewDidLeave() {
    this.data = {};
    this.isAccountIDVerified = false;
  }

  ngOnInit() { }

  googleSignIn() {
    this.googlePlus.login(this.global.googleSignInConfiguration).then(result => {
      console.log(result);
      this.loadingController.create(
        {
          message: 'Please wait while you are getting logged in.'
        }
      ).then(loader => {
        loader.present();
        this.service.getAuthenticationTokenFromGoogleSignIn(result.accessToken).subscribe((token: any) => {
          if (token.id_token) {
            sessionStorage.setItem('accessToken', token.id_token);
            this.service.getAccountDetails().subscribe((user: any) => {
              user['accessToken'] = token.id_token;
              this.service.saveUserData(user);
              this.global.isGoogleSignIn = true;
              this.global.displayToastMessage('You have successfully logged in.');
              if (user.authorities) {
                loader.dismiss();
                this.router.navigate(['/main/inbox']);
                this.global.displayToastMessage("You've logged in successfully.");
              }
            }, error => {
              loader.dismiss();
              this.global.clearUser();
              this.global.displayToastMessage('Error Occurred While Fetching User Data. Please try after some time');
            });
          } else {
            this.global.displayToastMessage('ID Token Missing From Response');
            loader.dismiss();
          }
        }, error => {
          loader.dismiss();
          this.global.displayToastMessage('Unable To Get JWT Token');
        });
      });
    }).catch(error => {
      this.global.displayToastMessage('Some problem occurred while signing using Google Account');
    });
  }

  googleSignOut() {
    this.googlePlus.logout().then(result => this.global.displayToastMessage('Logout Success')).catch(error => this.global.displayToastMessage('Logout Failed'));
  }

  onSubmit(form) {
    this.loadingController.create(
      {
        message: 'Please wait while you are getting logged in...',
      }
    ).then(loader => {
      loader.present();
      this.service.authenticateUser(this.data)
        .subscribe((response: any) => {
          sessionStorage.setItem('accessToken', response.id_token);
          this.service.getAccountDetails().subscribe((user: any) => {
            user['accessToken'] = response.id_token;
            this.service.saveUserData(user);
            this.global.displayToastMessage('You have successfully logged in.');
            if (user.authorities) {
              loader.dismiss();
              this.router.navigate(['/main/inbox']);
              this.global.displayToastMessage("You've logged in successfully.");
              form.reset();
            }
          }, error => {
            loader.dismiss();
            this.global.clearUser();
            this.global.displayToastMessage('Error Occurred While Fetching User Data. Please try after some time');
          });
        }, (error: HttpErrorResponse) => {
          loader.dismiss();
          if (error.status === 401 || error.status === 400) {
            this.global.displayToastMessage('Username or Password do not match!');
          } else if (error.status === 522) {
            this.global.displayToastMessage('Connection Time-Out. Please try after some time.')
          } else {
            this.global.displayToastMessage('Error Occured While Logging In. Please try after some time.');
            this.global.clearUser();
          }
        });
    });
  }

  onKeyPress(keyCode, form) {
    if (keyCode === 13) {
      if (this.data.username && this.data.password) {
        this.onSubmit(form);
      }
    }
  }

}
