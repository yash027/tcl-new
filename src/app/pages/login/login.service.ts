import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { StorageService } from 'src/app/services/storage.service';
import { GlobalService } from 'src/app/services/global.service';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    constructor(private http: HttpService, private storage: StorageService, private global: GlobalService) { }

    authenticateUser(userDetails: any) {
        userDetails['rememberMe'] = 'true';
        const url = '/authenticate';
        return this.http.call_POST(url, userDetails);
    }

    authenticateIfIOS(userDetails) {
        userDetails['rememberMe'] = 'true';
        const url = '/authenticate';
        return this.http.callWithUserAgent(url, userDetails);
    }

    getAuthenticationTokenFromGoogleSignIn(idToken) {
        // return this.http.newGoogleLogin(accessToken, refreshToken, expiresIn);
        return this.http.googleLoginFromBackend(idToken);
    }

    getAccountDetails() {
        const url = '/account';
        return this.http.call_GET(url);
    }

    getCompanyDetails(companyCode: any) {
        return this.http.getCompanyCode(companyCode);
    }

    saveUserData(user) {
        this.storage.saveUser(user);
        this.global.userDetails = user;
        localStorage.setItem('role', user.authorities);
        localStorage.setItem('url', user.url);
    }

    removeUserData() {
        this.storage.deleteUser();
        localStorage.clear();
    }

    getMobileAppVersion(url) {
        return this.http.call_GET(url);
    }
}
