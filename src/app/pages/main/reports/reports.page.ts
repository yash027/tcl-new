import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.page.html',
  styleUrls: ['./reports.page.scss'],
})
export class ReportsPage {

  reports: any = [];

  requests: any = [
    {
      imgText: 'Store Goods Issue',
      title: 'Store Goods Issue',
      routeUrl: 'goods-issue',
      toShow: true
    }
  ]

  constructor(
    public global: GlobalService
    ) { }

  ionViewWillEnter() {
    this.global.showBottomTabs();
  }

  ionViewWillLeave() {
    this.global.hideBottomTabs();
  }
}
