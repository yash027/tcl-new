import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportsPage } from './reports.page';
import { AuthGuardService } from 'src/app/guards/auth.guard';
import { GoodsIssueReportService } from './goods-issue-reports/goods-issue-report.service';

const routes: Routes = [
  {
    path: '',
    component: ReportsPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'goods-issue',
    loadChildren: () => import('./goods-issue-reports/goods-issue-reports.module').then( m => m.GoodsIssueReportsPageModule),
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportsPageRoutingModule {}
