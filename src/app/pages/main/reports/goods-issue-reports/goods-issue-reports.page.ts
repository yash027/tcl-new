import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/services/global.service';
import { Router, ActivatedRoute } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';
import { LoadingController, ModalController } from '@ionic/angular';
import { GoodsIssueReportService } from './goods-issue-report.service';
import { GIFilterModulePage } from 'src/app/pages/modals/gifilter-module/gifilter-module.page';

@Component({
  selector: 'app-goods-issue-reports',
  templateUrl: './goods-issue-reports.page.html',
  styleUrls: ['./goods-issue-reports.page.scss'],
})
export class GoodsIssueReportsPage implements OnInit {

  searchParams: any = {
    creator: '',
    requestType: ["GI_PROCESS"],
    requestId: '',
    createdDate: '',
    reservationNumber: '',
    workflowStatus: '',
    movementType: '',
    movementTypeDescription: '',
    plant: '',
    plantDescription: '',
    department: '',
    departmentDescription: '',
    isFiltered: false
  }
  reports: any = [];
  isLoadedFirstTime: boolean = true;
  isDataLoading: boolean = false;

  constructor(
    public global: GlobalService,
    public service: GoodsIssueReportService,
    private router: Router,
    private storage: StorageService,
    private activatedRoute: ActivatedRoute,
    private loadingController: LoadingController,
    private modalController: ModalController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.loadingController.create(
      {
        message: 'Please wait while reports are getting loaded...'
      }
    ).then(loader => {
      loader.present();
      this.storage.getUser().then(user => {
        this.searchParams.creator = user.login;
        this.service.getReports(this.searchParams).subscribe((response: any) => {
          loader.dismiss();
          this.isLoadedFirstTime = false;
          if (response.length > 0) {
            this.reports = response;
          }
        }, error => {
          this.global.displayToastMessage('Error occurred while loading reports, Please try after some time.');
          this.isLoadedFirstTime = false;
          loader.dismiss();
        });
      });
    })
    this.global.hideBottomTabs();
  }

  onBack() {
    this.router.navigate(['/main/reports']);
  }

  fixCurrentUserBrackets(currentUser) {
    if(currentUser) {
      currentUser = currentUser.replace(/[\[\]']+/g,'');
    }
    return currentUser;
  }

  applyFilter(value) {
    this.isDataLoading = true;
    this.searchParams.requestId = value;
    this.service.getReports(this.searchParams).subscribe(response => {
      this.reports = response;
      this.isDataLoading = false;
    }, error => {
      this.global.displayToastMessage('Error occurred while fetching data. Please try after some time');
      this.isDataLoading = false;
    });
  }

  calculateTotalUsers(workitem) {
    let count = 0;
    for (let key in workitem.approvalMatrix) {
      if (key.includes('_name')) {
        count++;
      }
    }
    return count;
  }

  getWorkitemStatus(workitem) {
    let status = '';
    switch(workitem.currentstep) {
      case '&START' :
        status = 'At Initiator';
        break;
      case '&END' :
        if(this.global.getLogTextBasedOnActivity(workitem.logs[workitem.logs.length-1].activityText) === 'Cancelled')
        status = 'Cancelled';
        else 
        status = 'Sent to SAP';
        break;
      default :
        status = 'At Approver';
        break;
    }
    return status;
  }

  isFiltered() {
    let decision = false;
    for(let key in this.searchParams) {
      if(key === 'movementType' && this.searchParams[key] !== '') {
        decision = true;
        break;
      } else if(key === 'createdDate' && this.searchParams[key] !== '') {
        decision = true;
        break;
      } else if(key === 'plant' && this.searchParams[key] !== '') {
        decision = true;
        break;
      } else if(key === 'reservationNumber' && this.searchParams[key] !== '') {
        decision = true;
        break;
      } else if(key === 'department' && this.searchParams[key] !== '') {
        decision = true;
        break;
      } else if(key === 'workflowStatus' && this.searchParams[key] !== '') {
        decision = true;
        break;
      }
    }
    return decision;
  }

  clearFilters() {
    for(let key in this.searchParams) {
      if(key === 'movementType') {
        this.searchParams[key] = '';
        this.searchParams['movementTypeDescription'] = '';
      }
      if(key === 'createdDate') {
        this.searchParams[key] = '';
      }
      if(key === 'plant') {
        this.searchParams[key] = '';
        this.searchParams['plantDescription'] = '';
      }
      if(key === 'reservationNumber') {
        this.searchParams[key] = '';
      }
      if(key === 'department') {
        this.searchParams[key] = '';
        this.searchParams['departmentDescription'] = '';
      }
      if(key === 'workflowStatus') {
        this.searchParams[key] = '';
      }
    }
    this.isDataLoading = true;
    this.service.getReports(this.searchParams).subscribe(response => {
      this.reports = response;
      this.isDataLoading = false;
    }, error => {
      this.isDataLoading = false;
      this.global.displayToastMessage('Error occurred while fetching data. Please try after some time');
    });
    this.searchParams.isFiltered = false;
  }

  openFilterModal() {
    this.modalController.create(
      {
        component: GIFilterModulePage,
        componentProps: this.searchParams
      }
    ).then(modal => {
      modal.present();
      modal.onDidDismiss().then(data => {
        data = data.data;
        if(data) {
          this.searchParams = data.data;
          this.loadingController.create(
            {
              message: 'Please Wait...'
            }
          ).then(loader => {
            loader.present();
            let records = [];
            this.service.getReports(this.searchParams).subscribe((response: any) => {
              this.searchParams.isFiltered = this.isFiltered();
              if(this.searchParams.workflowStatus !== '') {
                records = response.filter(element => element.logs[element.logs.length-1].activityText === this.searchParams.workflowStatus);
                if(this.searchParams.workflowStatus === 'Approve') {
                  let indexes = [];
                  records.forEach((workItem, index) => {
                    if((workItem.currentstep === '4' || workItem.currentstep === '5' || workItem.currentstep === '&END') && workItem.sapDocId !== null) {
                      indexes.push(index);
                    }
                  });
                  while(indexes.length) {
                    records.splice(indexes.pop(), 1);
                  }
                  // indexes.forEach(indexNo => records.splice(indexNo, 1));
                  this.reports = records;
                  console.log(this.reports);
                }
                loader.dismiss();
              } else {
                this.reports = response;
                loader.dismiss();
              }
            }, error => {
              this.global.displayToastMessage('Error occurred while fetching data from server. Please try again after some time.');
              loader.dismiss();
            });
          });
        }
      })
    })
  }

  ionViewDidLeave() {
    this.searchParams = {
      creator: '',
      "requestType": ["GI_PROCESS"]
    }
    this.reports.length = 0;
  }

}
