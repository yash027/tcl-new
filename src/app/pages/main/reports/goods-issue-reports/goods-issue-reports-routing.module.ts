import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GoodsIssueReportsPage } from './goods-issue-reports.page';
import { AuthGuardService } from 'src/app/guards/auth.guard';
import { GoodsIssueReportDetailsService } from './report-details-page/goods-issue-report-details.service';

const routes: Routes = [
  {
    path: '',
    component: GoodsIssueReportsPage
  },
  {
    path: ':requestId',
    loadChildren: () => import('./report-details-page/report-details-page.module').then( m => m.ReportDetailsPagePageModule),
    canActivate: [AuthGuardService],
    resolve: {data: GoodsIssueReportDetailsService}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GoodsIssueReportsPageRoutingModule {}
