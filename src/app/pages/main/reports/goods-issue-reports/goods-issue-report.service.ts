import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { StorageService } from 'src/app/services/storage.service';
import { GlobalService } from 'src/app/services/global.service';

@Injectable({
    providedIn: 'root'
})
export class GoodsIssueReportService {

    constructor(
        private http: HttpService,
        private global: GlobalService
    ) {

    }

    getRequestStatus(request) {
        let text = '';
        if(request.header.WORKFLOW_COMPLETED) {
            text = 'Completed';
        } else if(request.logs.length > 0) {
            if(request.logs[request.logs.length - 1].activityText === 'Cancel') {
                text = 'Cancelled';
            } else {
                text = 'In-Progress';
            }
        } else {
            text = 'In-Progress';
        }
        return text;
    }

    getReports(body) {
        let submitBody = {
            requestType: body.requestType,
            creator: body.creator,
            headerFields: {}
        }
        if(body.reservationNumber) submitBody.headerFields['RSNUM'] = body.reservationNumber;
        if(body.plant) submitBody.headerFields['WERKS'] = body.plant;
        if(body.plantDescription) submitBody.headerFields['PLANT_DES'] = body.plantDescription;
        if(body.movementType) submitBody.headerFields['BWART'] = body.movementType;
        if(body.movementTypeDescription) submitBody.headerFields['MVT_DES'] = body.movementTypeDescription;
        if(body.createdDate) submitBody.headerFields['RSDAT'] = this.global.getSAPDate(body.createdDate);
        if(body.department) submitBody.headerFields['DEPT_ID'] = body.department;
        if(body.departmentDescription) submitBody.headerFields['DEPT_NAME'] = body.departmentDescription;
        return this.http.call_POST('/requestSearch?page=0&size=100', submitBody);
    }

}