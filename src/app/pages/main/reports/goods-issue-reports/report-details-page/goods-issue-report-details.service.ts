import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { ActivatedRouteSnapshot } from '@angular/router';

@Injectable(
    {
        providedIn: 'root'
    }
)
export class GoodsIssueReportDetailsService {
    
    constructor(
        private http: HttpService
    ) {}

    resolve(route: ActivatedRouteSnapshot) {
        return this.getRequestIdDetails(route.params.requestId);
    }

    getRequestIdDetails(requestId) {
        const url = '/requests/inbox/requests/' + requestId;
        return this.http.call_GET(url);
      }
}