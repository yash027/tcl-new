import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportDetailsPagePage } from './report-details-page.page';

const routes: Routes = [
  {
    path: '',
    component: ReportDetailsPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportDetailsPagePageRoutingModule {}
