import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, ActionSheetController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-report-details-page',
  templateUrl: './report-details-page.page.html',
  styleUrls: ['./report-details-page.page.scss'],
})
export class ReportDetailsPagePage implements OnInit {

  @ViewChild('slides', { static: false }) slider: IonSlides;

  segment = 0;

  slideOpts = {
    allowTouchMove: false,
    autoHeight: true
  };

  requestData: any = {
    header: {},
    attachments: [],
    logs: []
  };

  attachments: any = {
    images: [],
    files: []
  };

  userList: any = [];

  selectedUser: any;

  currentStatus: any;
  currentStatusCssClass: any;

  constructor(private route: ActivatedRoute,
    public global: GlobalService,
    private actionSheetController: ActionSheetController,
    private alertController: AlertController) { }

  ionViewWillEnter() {
    this.route.data.subscribe(data => {
      if (data.data) {
        this.requestData = data.data;
        this.currentStatus = this.global.getLogTextBasedOnActivity(this.requestData.logs[this.requestData.logs.length - 1].activityText);
        this.currentStatusCssClass = this.global.getColorCssClass(this.currentStatus);
      }
    });
  }

  ngOnInit() { }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  showNoStockItemAlert(lineItem) {
    let msg = 'For material ' + lineItem.lineItems.MAT_DES + ' (Quantity ' + parseInt(lineItem.lineItems.ERFMG) + ') cannot be procedded for Goods Issue because of No Stock availiblity.';
    this.alertController.create(
      {
        header: 'Message',
        message: msg,
        buttons: [
          {
            text: 'Ok',
            role: 'ok'
          }
        ]
      }
    ).then(alert => alert.present());
  }

  onViewFile(file) {
    this.actionSheetController.create(
      {
        header: 'Options',
        buttons: [
          {
            text: 'Preview Attachment',
            handler: () => {
              if (file.url) {
                this.global.downloadFile(this.requestData.requestId, file);
              } else {
                this.global.displayToastMessage('File URL Missing for Preview');
              }
            }
          }
        ]
      }
    ).then(actionSheet => actionSheet.present());
  }

}
