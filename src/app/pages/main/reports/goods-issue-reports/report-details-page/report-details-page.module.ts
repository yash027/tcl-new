import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportDetailsPagePageRoutingModule } from './report-details-page-routing.module';

import { ReportDetailsPagePage } from './report-details-page.page';
import { MatExpansionModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatExpansionModule,
    ReportDetailsPagePageRoutingModule
  ],
  declarations: [ReportDetailsPagePage]
})
export class ReportDetailsPagePageModule {}
