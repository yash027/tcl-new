import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GoodsIssueReportsPageRoutingModule } from './goods-issue-reports-routing.module';

import { GoodsIssueReportsPage } from './goods-issue-reports.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GoodsIssueReportsPageRoutingModule
  ],
  declarations: [GoodsIssueReportsPage]
})
export class GoodsIssueReportsPageModule {}
