import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TimesheetClaimPageRoutingModule } from './timesheet-claim-routing.module';

import { TimesheetClaimPage } from './timesheet-claim.page';
import { TimesheetClaimService } from './timesheet-claim.service';
import { MatExpansionModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatExpansionModule,
    TimesheetClaimPageRoutingModule
  ],
  declarations: [TimesheetClaimPage]
})
export class TimesheetClaimPageModule {}
