import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TimesheetClaimPage } from './timesheet-claim.page';

const routes: Routes = [
  {
    path: '',
    component: TimesheetClaimPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TimesheetClaimPageRoutingModule {}
