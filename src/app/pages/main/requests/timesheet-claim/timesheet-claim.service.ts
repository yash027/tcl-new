import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { StorageService } from 'src/app/services/storage.service';
import { AlertController } from '@ionic/angular';
import { GlobalService } from 'src/app/services/global.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class TimesheetClaimService {

    vendorDetails: any = {};
    contractorDetails: any = {};
    contractorName: any;
  
    constructor(private http: HttpService, 
                private storage: StorageService, 
                private alertController: AlertController,
                private global: GlobalService,
                private router: Router,
                private httpClient: HttpClient) { }
  
    async resolve() {
      this.storage.getUser().then( user => {
        this.contractorName = user.firstName + ' ' + user.lastName;
        this.getVendorDetails(user.login).subscribe( (response: any) => {
          this.vendorDetails = response[0];
          if(!this.vendorDetails.LIFNR){
            this.alertDialog('Vendor');
          }
        }, error => {
          this.global.displayToastMessage('Some problem occured, please login again');
          this.global.gotoLogin();
        });
        this.getContractorDetails(user.login).subscribe( (response: any) => {
          this.contractorDetails = response[0];
          if(!this.contractorDetails.contractorCode){
            this.alertDialog('Contractor');
          }
        }, error => {
          this.global.displayToastMessage('Some problem occured, please login again');
          this.global.gotoLogin();
        });
      });
    }
  
    getEngineersList() {
      const url = '/getdynamicinstace/department?page=0&size=2000';
      return this.http.call_GET(url);
    }
  
    getVendorDetails(loginId) {
      return this.http.call_POST('/vendor/SDOCS_PRTL_ID/' + loginId + '/getlistdatabysearch', null);
    }
  
    getContractorDetails(loginId) {
      return this.http.call_POST('/contractorCodes/SDOCS_PRTL_ID/' + loginId + '/getlistdatabysearch', null);
    }
  
    getTimesheetData(contractorCode, claimingDate, pageNumber) {
      const url = 'https://tatachemicalsdev.smartdocsnow.com/smartportal-server/api/attendanceRunTimeTable/getlistdatabyvalues?listvar=CONT_CODE%2CDATE%2CINIT_FLG&values='+contractorCode+'%2C'+claimingDate+'%2CX&page='+pageNumber+'&size=10';
      return this.httpClient.get(url, { observe: 'response'});
    }
  
    getTaskGroups() {
      const url = '/taskGroup/getlistdatabyMultiplevalues?listvar=PO_TYPE&values=PS2%2CPS4';
      return this.http.call_GET(url);
    }
  
    onSubmitTimesheetClaim(data, attachments, images) {
      const form = new FormData();
      if(attachments) {
        for (let key in attachments) {
          if (attachments[key]) {
            form.append('files', attachments[key]);
          }
        }
      }
      images.forEach(element => {
        form.append('files', element.image, element.name);
      });
      form.append('requestData', JSON.stringify(data));
      return this.http.call_POST('/multiplerequests/tcl?actionValue=submit', form);
    }
  
    alertDialog(sender) {
      this.alertController.create({
        header: 'Notification',
        message: 'This Portal User is not mapped to any ' + sender + '. Please update.',
        buttons: [
          {
            text: 'Ok',
            handler: () => this.router.navigate(['/tabs/requests'])
          }
        ]
      }).then( alert => alert.present());
    }

}