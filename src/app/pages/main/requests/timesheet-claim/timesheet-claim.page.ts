import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { TimesheetClaimService } from './timesheet-claim.service';
import { Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { LoadingController, ModalController, AlertController } from '@ionic/angular';

import { Plugins, Capacitor, CameraSource, CameraResultType } from '@capacitor/core';

import { SearchTasklistPage } from 'src/app/pages/modals/search-tasklist/search-tasklist.page';
import { SearchEngineerPage } from 'src/app/pages/modals/search-engineer/search-engineer.page';

@Component({
  selector: 'app-timesheet-claim',
  templateUrl: './timesheet-claim.page.html',
  styleUrls: ['./timesheet-claim.page.scss'],
})
export class TimesheetClaimPage implements OnInit {

  engineersList: any = [];
  taskList: any = [];
  attachments: any = [];
  images: any = [];

  createDate: any;
  pageNumber: any = 0;
  totalCount: any;
  lineItemLoading: boolean = false;

  isSubmitDisabled: boolean = false;
  totalAmount: any = 0;

  timesheetClaimData: any = {
    docType: 'time_claim',
    description: '',
    header: []
  }

  lineItems: any = [];

  constructor(private service: TimesheetClaimService,
              private router: Router,
              private cdRef: ChangeDetectorRef,
              private global: GlobalService,
              private loadingController: LoadingController,
              private modalController: ModalController,
              private alertController: AlertController) { }

  ionViewWillEnter() {
    
    this.service.getEngineersList().subscribe( response => {
      this.engineersList = response;
    }, error => {
      this.global.displayToastMessage('Some problem occured, please login again');
      this.global.gotoLogin();
    }); 
    this.service.getTaskGroups().subscribe( response => {
      this.taskList = response;
    }, error => {
      this.global.displayToastMessage('Some problem occured, please login again');
      this.global.gotoLogin();
    });
  }

  ionViewDidLeave() {
    
  }

  ngOnInit() {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  onEngineerSelect(lineItem) {
    this.loadingController.create(
      {
        message: 'Please wait...',
      }
    ).then( loader => {
      loader.present();
      this.modalController.create(
        {
          component: SearchEngineerPage,
          componentProps: {
            data: this.engineersList
          }
        }
      ).then( modal => {
        loader.dismiss();
        modal.present();

        modal.onDidDismiss().then( (data: any) => {
          if (data.data) {
            data = data.data;
            lineItem['EMP_ENG'] = data.EMP_ENG;
            lineItem['EMP_ENG_NAME'] = data.EMP_ENG_NAME;
            lineItem['DEPT'] = data.DEPT;
            lineItem['DEPT_DES'] = data.DEPT_DES;
          }
        });
      });
    });
  }

  onTaskGroupSelect(lineItem) {
    this.loadingController.create(
      {
        message: 'Please wait...',
      }
    ).then( loader => {
      loader.present();
      this.modalController.create(
        {
          component: SearchTasklistPage,
          componentProps: {
            data: this.taskList,
            field: 'taskGroup' 
          }
        }
      ).then( modal => {
        loader.dismiss();
        modal.present();

        modal.onDidDismiss().then( (data: any) => {
          if (data.data) {
            data = data.data;
            lineItem['TG_CODE'] = data.TG_CODE;
            lineItem['TG_DES'] = data.TG_DES;
          }
        });
      });
    });
  }

  onDateChange(date, dateChange?) {
    this.lineItemLoading = true;
    if (dateChange) {
      this.lineItems = [];
      this.totalCount = 0;
      this.pageNumber = 0;
    } else {
      this.pageNumber++;
    }
    this.service.getTimesheetData(this.service.contractorDetails.contractorCode, this.global.getSAPDate(date), this.pageNumber)
    .subscribe( (response: any) => {
      this.totalCount = response.headers.get('x-total-count');
      if (response.body.length > 0) {
        response.body.forEach(element => {
          element['SOR_FLAG'] = true;
          element['INCENTIVE'] = 0;
          element.NET_TIME = Number(element.NET_TIME);
          element.OVER_TIME = Number(element.OVER_TIME);
          element.SKILL_RATE = Number(element.SKILL_RATE);
          element.OT_RATE = Number(element.OT_RATE);
          element['AMOUNT'] = (element.NET_TIME * element.SKILL_RATE) + (element.OVER_TIME * element.OT_RATE) + element.INCENTIVE;
          this.lineItems.push(element);
        });
        this.lineItemLoading = false;
      } else {
        this.global.displayToastMessage('No Line Items');
        this.lineItemLoading = false;
      }
    }, error => {
      this.global.displayToastMessage('Some problem occurred while getching line items for given date.');
      this.lineItemLoading = false;
    });
  }

  chooseFiles() {
    document.getElementById('fileUpload').click();
  }

  onSelectFile(event) {
    const formData = event.target.files;
    for (let key in formData) {
      if (key !== 'length' && key !== 'item') {
        this.attachments.push(formData[key]);
      }
    }
  }

  onRemoveAttachment(index) {
    this.attachments.splice(index, 1);
  }

  onTakeImage() {
    if (!Capacitor.isPluginAvailable('Camera')) {
      this.global.displayToastMessage(
        'Unable To Open Camera');
      return;
    }
    Plugins.Camera.getPhoto({
      quality: 60,
      source: CameraSource.Camera,
      height: 400,
      width: 300,
      correctOrientation: true,
      resultType: CameraResultType.DataUrl
    })
      .then(image => {
        const blobImg = this.global.dataURItoBlob(image.dataUrl);
        const img = {
          name: 'Image-' + new Date().toISOString(),
          image: blobImg
        }
        this.images.push(img);
      })
      .catch(error => {
        return false;
      });
  }

  onRemoveImage(index) {
    this.images.splice(index, 1);
  }

  getBytesConversion(bytes) {
    const size = this.global.getBytesConversion(bytes, 2);
    return size;
  }

  onSORChange(lineItem) {
    this.calculateTotal(lineItem);
  }

  calculateTotal(lineItem) {
    lineItem.AMOUNT = (lineItem.NET_TIME * lineItem.SKILL_RATE) + (lineItem.OVER_TIME * lineItem.OT_RATE) + lineItem.INCENTIVE;
    if (lineItem.SOR_FLAG) {
      this.totalAmount = this.totalAmount - lineItem.AMOUNT;
    } else {
      this.totalAmount = this.totalAmount + lineItem.AMOUNT;
    }
  }

  getLineItemAmount(lineItem) {
    lineItem.AMOUNT = (lineItem.NET_TIME * lineItem.SKILL_RATE) + (lineItem.OVER_TIME * lineItem.OT_RATE) + lineItem.INCENTIVE;
    this.totalAmount = 0;
    this.lineItems.forEach(element => {
      if (!element.SOR_FLAG) {
        this.totalAmount += Number(element.AMOUNT);
      }
    });
  }

  onSubmit() {
    this.alertController.create(
      {
        message: 'Add Description',
        backdropDismiss: false,
        inputs: [
          {
            placeholder: 'Add Comments',
            type: 'text',
            name: 'tsDescription'
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'Cancel'
          },
          {
            text: 'Submit',
            handler: (data) => {
              this.loadingController.create(
                {
                  message: 'Please wait while your request is getting initiated...'
                }
              ).then ( loader => {
                loader.present();
                if(data.tsDescription === '') {
                  this.global.displayToastMessage('You must enter description to submit request.');
                  this.onSubmit();
                } else {
                  this.timesheetClaimData.description = data.tsDescription;
                  this.lineItems.forEach( (element, index) => {
                    if(element.PRESENT === 'AB') {
                      this.lineItems.splice(index, 1);
                    } else {
                      let header = {};
                      header['CR_DATE'] = this.global.getSAPDate(this.createDate);
                      if (element.SOR_FLAG !== true) {
                        header['EMP_ENG'] = element.EMP_ENG;
                        header['EMP_ENG_NAME'] = element.EMP_ENG_NAME;
                        header['DEPT'] = element.DEPT;
                        header['DEPT_DES'] = element.DEPT_DES;
                        header['TG_CODE'] = element.TG_CODE;
                        header['TG_DES'] = element.TG_DES;
                      } else {
                        header['DEPT'] = element.DEPT_CODE;
                      }
                      header['CONT_NAME'] = this.service.contractorName;
                      header['CONT_ID'] = this.service.vendorDetails.LIFNR;
                      header['EMP_CODE'] = element.EMP_CODE;
                      header['DEPT_CODE'] = element.DEPT_CODE;
                      header['DESIGNATION'] = element.DESIGNATION;
                      header['PRESENT'] = element.PRESENT;
                      header['NET_TIME'] = element.NET_TIME;
                      header['OVER_TIME'] = element.OVER_TIME;
                      header['AMOUNT'] = element.AMOUNT;
                      header['SHIFT'] = element.SHIFT;
                      header['SKILL_RATE'] = element.SKILL_RATE;
                      if (element.OT_RATE) {
                        header['OT_RATE'] = element.OT_RATE
                      }
                      header['SOR_FLAG'] = '' + element.SOR_FLAG;
                      header['INCENTIVE'] = element.INCENTIVE;
                      this.timesheetClaimData.header.push(header);
                    }
                  });
                  this.service.onSubmitTimesheetClaim(this.timesheetClaimData, this.attachments, this.images).subscribe( response => {
                    loader.dismiss();
                    this.alertController.create(
                      {
                        header: 'Message',
                        message: 'You have successfully initiated request',
                        buttons: [
                          {
                            text: 'Ok',
                            handler: () => {
                              this.router.navigate(['/main/requests']);
                            }
                          }
                        ]
                      }
                    ).then( successAlert => successAlert.present());
                  }, error => {
                    loader.dismiss();
                    this.global.displayToastMessage('Some problem occurred while initiation request.');
                  });
                }
              });
            }
          }
        ]
      }
    ).then( alert => alert.present());
  }

}
