import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SorClaimPage } from './sor-claim.page';

const routes: Routes = [
  {
    path: '',
    component: SorClaimPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SorClaimPageRoutingModule {}
