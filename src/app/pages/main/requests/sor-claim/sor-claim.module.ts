import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SorClaimPageRoutingModule } from './sor-claim-routing.module';

import { SorClaimPage } from './sor-claim.page';
import { SorClaimService } from './sor-claim.service';
import { MatExpansionModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatExpansionModule,
    SorClaimPageRoutingModule
  ],
  declarations: [SorClaimPage],
  providers: [SorClaimService]
})
export class SorClaimPageModule {}
