import { Component, OnInit } from '@angular/core';
import { SorClaimService } from './sor-claim.service';
import { GlobalService } from 'src/app/services/global.service';
import { ModalController, LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Capacitor, Plugins, CameraSource, CameraResultType } from '@capacitor/core';

import { SearchTasklistPage } from 'src/app/pages/modals/search-tasklist/search-tasklist.page';
import { SearchEngineerPage } from 'src/app/pages/modals/search-engineer/search-engineer.page';

@Component({
  selector: 'app-sor-claim',
  templateUrl: './sor-claim.page.html',
  styleUrls: ['./sor-claim.page.scss'],
})
export class SorClaimPage implements OnInit {

  engineersList: any = [];
  taskList: any = [];

  engineerInfo: any = {};
  createDate: any = new Date().toISOString();
  maxDate: any = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
  attachments: any = [];
  images: any = [];

  totalAmount: any;

  sorData: any = {
    docType: 'sor_claim',
    description: '',
    header: []
  };

  isHeaderAvailable: boolean = false;

  constructor(private service: SorClaimService,
              private global: GlobalService,
              private modalController: ModalController,
              private loadingController: LoadingController,
              private router: Router,
              private alertController: AlertController) { }

  ionViewWillEnter() {
    this.service.getContractorName();
    this.getEngineersList();
    this.service.getVendorCode();
  }

  ngOnInit() {
  }

  ionViewDidLeave() {
    this.sorData = {
      docType: 'sor_claim',
      description: '',
      header: []
    }
    this.engineerInfo = {};
    this.totalAmount = undefined;
  }

  selectEngineer() {
    if (this.sorData.header.length > 0) {
      this.alertController.create(
        {
          header: 'Important!',
          message: 'Please remove all the line items first to change engineer.',
          buttons: [
            {
              text: 'Ok',
              role: 'ok'
            }
          ]
        }
      ).then( alert => alert.present() );
    } else {
      this.modalController.create(
        {
          component: SearchEngineerPage,
          componentProps: {
            data: this.engineersList
          }
        }
      ).then( modal => {
        modal.present();

        modal.onDidDismiss().then( selected => {
          if (selected.data) {
            this.engineerInfo = selected.data;
          }
        });
      });
    }
  }

  getEngineersList() {
    this.service.getEngineersList().subscribe( response => {
      this.engineersList = response;
    }, error => {
      this.global.displayToastMessage('Error Occured While Fetching List Of Engineers');
    });
  }

  getCurrentDate() {
    const date = new Date().toISOString();
    return date.substring(0, 10);
  }

  chooseFiles() {
    document.getElementById('fileUpload').click();
  }

  onSelectFile(event) {
    const formData = event.target.files;
    for (let key in formData) {
      if (key !== 'length' && key !== 'item') {
        this.attachments.push(formData[key]);
      }
    }
  }

  onRemoveAttachment(index) {
    this.attachments.splice(index, 1);
  }

  getBytesConversion(bytes) {
    const size = this.global.getBytesConversion(bytes, 2);
    return size;
  }

  selectTaskGroup(lineItem) {
    if (this.engineerInfo.EMP_ENG) {
      this.loadingController.create(
        {
          message: 'Please wait...',
        }
      ).then( loader => {
        loader.present();
          let ps1Data;
          let ps3Data;
          this.service.getTaskGroups('PS1', this.service.vendorDetails[0].LIFNR, this.engineerInfo.EMP_ENG).subscribe( ps1Response => {
            ps1Data = ps1Response;
            this.service.getTaskGroups('PS3', this.service.vendorDetails[0].LIFNR, this.engineerInfo.EMP_ENG).subscribe( ps3Response => {
              ps3Data = ps3Response;
              this.taskList = ps1Data.concat(ps3Data);
              this.modalController.create({
                component: SearchTasklistPage,
                componentProps: { data: this.taskList, field: 'taskGroup' }
              }).then( modal => {
                loader.dismiss();
                modal.present();

                modal.onDidDismiss().then( (data: any) => {
                  if (data.data) {
                    data = data.data;
                    lineItem['TG_CODE'] = data.TG_CODE;
                    lineItem['TG_DES'] = data.TG_DES;
                  }
                });
              });
            }, error => {
              this.global.displayToastMessage('Error while loading PS3 task list.');
              loader.dismiss();
            });
          }, error => {
            this.global.displayToastMessage('Error while loading PS1 task list.');
            loader.dismiss();
          });
      });
    } else {
      this.global.displayToastMessage('Please select engineer first.');
    }
  }

  selectSOR(lineItem) {
    this.loadingController.create(
      {
        message: 'Please wait...',
      }
    ).then( loader => {
      loader.present();
      this.service.getSOR().subscribe( response => {
        this.modalController.create(
          {
            component: SearchTasklistPage,
            componentProps: { data: response, field: 'sor' }
          }
        ).then( modal => {
          loader.dismiss();
          modal.present();

          modal.onDidDismiss().then( data => {
            if (data.data) {
              data = data.data;
              Object.assign(lineItem, data);
              delete lineItem._id;
            }
          });
        });
      }, error => {
        this.global.displayToastMessage('Some problem occurred while getting SORs');
        loader.dismiss();
      });
    });
  }

  addLineItem() {
    this.sorData.header.push({});
  }

  removeLineItem(index) {
    this.sorData.header.splice(index, 1);
  }

  onTakeImage() {
    if (!Capacitor.isPluginAvailable('Camera')) {
      this.global.displayToastMessage(
        'Unable To Open Camera');
      return;
    }
    Plugins.Camera.getPhoto({
      quality: 60,
      source: CameraSource.Camera,
      height: 400,
      width: 300,
      correctOrientation: true,
      resultType: CameraResultType.DataUrl
    })
      .then(image => {
        const blobImg = this.global.dataURItoBlob(image.dataUrl);
        const img = {
          name: 'Image-' + new Date().toISOString(),
          image: blobImg
        }
        this.images.push(img);
      })
      .catch(error => {
        return false;
      });
  }

  onRemoveImage(index) {
    this.images.splice(index, 1);
  }

  changeOnUnit(value, lineItem) {
    // tslint:disable-next-line: radix
    value = parseInt(value);
    // tslint:disable-next-line: radix
    lineItem.AMOUNT = (parseInt(lineItem.PRICE) * value).toFixed(2);
    if (lineItem.AMOUNT === 'NaN') {
      lineItem.AMOUNT = 0;
    }
  }

  calculateTotal() {
    this.totalAmount = 0;
    this.sorData.header.forEach(element => {
      this.totalAmount += Number(element.AMOUNT);
    });
  }

  submitData(backdrop?) {
    if(!backdrop){
      this.sorData.header.forEach(element => {
        element['CONT_ID'] = this.service.vendorDetails[0].LIFNR;
        element['CONT_NAME'] = this.service.contractorFullName;
        element['CR_DATE'] = this.global.getSAPDate(this.createDate);
        element['CMPL_DATE'] = this.global.getSAPDate(element.CMPL_DATE);
        element['DEPT'] = this.engineerInfo.DEPT;
        element['DEPT_DES'] = this.engineerInfo.DEPT_DES;
        element['EMP_ENG'] = this.engineerInfo.EMP_ENG;
        element['EMP_ENG_NAME'] = this.engineerInfo.EMP_ENG_NAME;
      });
    }
    this.alertController.create(
      {
        message: 'Add Description',
        backdropDismiss: false,
        inputs: [
          {
            type: 'text',
            name: 'requestComment',
            placeholder: 'Add Comments'
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel'
          },
          {
            text: 'Submit',
            handler: (data) => {
              if (data.requestComment) {
                this.loadingController.create(
                  {
                    message: 'Please wait while your request is getting initiated...'
                  }
                ).then( loader => {
                  loader.present();
                  this.sorData.description = data.requestComment;
                  this.service.initiateRequest(this.sorData, this.attachments, this.images)
                   .subscribe(response => {
                    loader.dismiss();
                    this.alertController.create({message: 'You have succesfully initiated the request', buttons: [{text: 'Ok', handler: () => {
                      this.router.navigate(['/main/requests']);
                    }}]}).then( successAlert => successAlert.present());
                  }, error => {
                    loader.dismiss();
                    this.global.displayToastMessage('Some problem occured while initiating your request');
                  });
                })
              } else {
                this.alertController.create({message: 'You must add a comment to initiate request', buttons: [{text: 'Ok', handler: () => {this.submitData(true)}}]})
                .then(alert => alert.present());
              }
            }
          },
        ]
      }
    ).then( alert => alert.present() );
  }


}
