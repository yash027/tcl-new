import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { StorageService } from 'src/app/services/storage.service';
import { GlobalService } from 'src/app/services/global.service';

@Injectable()
export class SorClaimService {

    vendorDetails: any = {};
    contractorFullName: any;

    constructor(private http: HttpService, private storage: StorageService, private global: GlobalService) { }

    getEngineersList() {
        const url = '/getdynamicinstace/department?page=0&size=2000';
        return this.http.call_GET(url);
    }

    getVendorCode() {
        let loginId;
        let url;
        this.storage.getUser().then(user => {
            loginId = user.login;
            url = '/vendor/SDOCS_PRTL_ID/' + loginId + '/getlistdatabysearch';
            this.http.call_POST(url, null).subscribe(response => {
                this.vendorDetails = response;
                console.log(this.vendorDetails);
            }, error => {
                this.global.displayToastMessage('Some Problem Occured While Retrieving Vendor Detauils');
            });
        }).catch(error => {
            this.global.displayToastMessage('Some Problem Occured, Please Login Again');
            this.global.gotoLogin();
        });
    }

    getTaskGroups(poType, contractorCode, engineerCode) {
        // tslint:disable-next-line: max-line-length
        const url = '/taskGroup/getlistdatabyvalues?listvar=PO_TYPE%2CLIFNR%2CENG_CODE&values=' + poType + '%2C' + contractorCode + '%2C' + engineerCode;
        return this.http.call_GET(url);
    }

    getSOR() {
        const url = '/getdynamicinstace/sor?page=0&size=500';
        return this.http.call_GET(url);
    }

    getContractorName() {
        this.storage.getUser().then(user => {
            this.contractorFullName = (user.firstName + ' ' + user.lastName);
        });
    }

    initiateRequest(data, attachments, images) {
        const formData = new FormData();
        for (let key in attachments) {
            if (attachments[key]) {
                formData.append('files', attachments[key]);
            }
        }
        images.forEach(element => {
            formData.append('files', element.image, element.name);
        });
        formData.append('requestData', JSON.stringify(data));
        const url = '/multiplerequests?&actionValue=submit';
        return this.http.call_POST(url, formData);
    }

}