import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Injectable(
    {
        providedIn: 'root'
    }
)
export class RequestService {

    constructor(private http: HttpService) {
    }

    resolve() {
        return this.getInitiatableRequestList();
    }

    getInitiatableRequestList() {
        return this.http.call_GET('/requests/initiate?page=0&size=100');
    }

}