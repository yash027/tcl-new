import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/services/global.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.page.html',
  styleUrls: ['./requests.page.scss'],
})
export class RequestsPage implements OnInit {

  externalUser: boolean;

  requests: any = [];

  constructor(
    public global: GlobalService,
    private activatedRoute: ActivatedRoute
    ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.activatedRoute.data.subscribe(data => {
      data = data.data;
      // Removing Goods Issue from the initiate API to avoid multiple GI Tile
      data = data.filter(element => 
        element.description !== 'Goods Issue'
      );
      this.requests = data;
      // Adding Goods Issue Tile as default to make it available for all users
      this.addGoodsIssueTile();
    });
    this.global.showBottomTabs();
  }

  addGoodsIssueTile() {
    this.requests.unshift(
      {
       docType: 'Goods Issue',
       description: 'Store Goods Issue',
       id: 'gi_process' 
      }
    )
  }

  ionViewWillLeave() {
    this.global.hideBottomTabs();
  }

}
