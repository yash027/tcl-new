import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RequestsPage } from './requests.page';
import { TimesheetClaimService } from './timesheet-claim/timesheet-claim.service';
import { AuthGuardService } from 'src/app/guards/auth.guard';
import { RequestService } from './requests.service';

const routes: Routes = [
  {
    path: '',
    component: RequestsPage,
    canActivate: [AuthGuardService],
    resolve: { data: RequestService }
  },
  {
    path: 'sor_claim',
    loadChildren: () => import('./sor-claim/sor-claim.module').then( m => m.SorClaimPageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'time_claim',
    loadChildren: () => import('./timesheet-claim/timesheet-claim.module').then( m => m.TimesheetClaimPageModule),
    resolve: { data: TimesheetClaimService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'gi_process',
    loadChildren: () => import('./goods-issue/goods-issue.module').then( m => m.GoodsIssuePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RequestsPageRoutingModule {}
