
import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { GlobalService } from 'src/app/services/global.service';
import { IonSlides, ModalController, ActionSheetController, AlertController, LoadingController, PopoverController } from '@ionic/angular';
import { GoodsIssueService } from './goods-issue.service';
import { SearchHelpPage } from 'src/app/pages/modals/search-help/search-help.page';
import { Capacitor, Plugins, CameraSource, CameraResultType } from '@capacitor/core';
import { Router, ActivatedRoute } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';
import { ReservationSearchPage } from 'src/app/pages/modals/reservation-search/reservation-search.page';
import { MaterialSearchHelpPage } from 'src/app/pages/modals/material-search-help/material-search-help.page';
import { LineItemPlantSearchPage } from 'src/app/pages/modals/line-item-plant-search/line-item-plant-search.page';
import { MovementTypeSearchPage } from 'src/app/pages/modals/movement-type-search/movement-type-search.page';
import { DepartmentSearchPage } from 'src/app/pages/modals/department-search/department-search.page';
import { StockCheckModalPage } from 'src/app/pages/modals/stock-check-modal/stock-check-modal.page';

@Component({
  selector: 'app-goods-issue',
  templateUrl: './goods-issue.page.html',
  styleUrls: ['./goods-issue.page.scss'],
})
export class GoodsIssuePage implements OnInit {

  @ViewChild('slides', { static: false }) slider: IonSlides;

  segment = 0;

  slideOpts = {
    allowTouchMove: false,
    autoHeight: true
  };

  header: any = {};

  lineItems: any = [];

  lineItemsInCaseOfReservationSync: any = [];

  recipient: any = {};

  reservationDate: any;

  isOpenReservationEditable = {
    department: false,
    costCenter: false,
    orderNumber: false
  };

  attachments: any = {
    images: [],
    files: []
  }

  comments: any;

  currentDate: any;
  aheadDate: any;

  isFirstLineItem: boolean = false;

  reservationNumber: any;
  isOpenReservationSelected = false;

  // In case of mvt type 301 and 302
  storageLocations: any = [];

  constructor(
    public global: GlobalService,
    public service: GoodsIssueService,
    private modalController: ModalController,
    private actionSheetController: ActionSheetController,
    private alertController: AlertController,
    private router: Router,
    private storage: StorageService,
    private loadingController: LoadingController,
    private activatedRoute: ActivatedRoute,
    private popoverController: PopoverController,
    private zone: NgZone
  ) {
    this.global.hideBottomTabs();
    this.service.lineItemSubject.subscribe(lineItemData => {
      if (lineItemData !== 0) {
        if (this.isFirstLineItem) {
          if (lineItemData.from === 'lineItemToMain' && lineItemData.data.id >= 0) {
            this.lineItems[lineItemData.data.id] = lineItemData.data.lineItem;
          } else if (lineItemData.from === 'lineItemToMain') {
            this.lineItems.push(lineItemData.data);
            if (this.header.BWART === '221') {
              // this.checkForStockAvailiblity();
            }
          }
        }
      }
    });
  }

  getRecipientDetails() {
    this.storage.getUser().then(user => {
      this.zone.run(() => {
        this.recipient = user;
        this.service.userDetails = user;
      });
    });
  }

  onEditLineItem(lineItem) {
    let data = {
      from: 'mainToLineItem',
      data: { ...lineItem }
    }
    this.service.lineItemSubject.next(data);
    this.router.navigate(['/main/requests/gi_process/line-item']);
  }

  openStockCheckModal(lineItem) {
    let stockFetchingData = {
      materialNumber: lineItem.lineItem.MATNR,
      materialDescription: lineItem.lineItem.MAT_DES,
      uom: lineItem.lineItem.MEINS,
      plant: lineItem.lineItem.WERKS
    }

    this.modalController.create(
      {
        component: StockCheckModalPage,
        componentProps: {
          data: stockFetchingData
        }
      }
    ).then(modal => {
      modal.present();
    });
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getRecipientDetails();
    this.setDatepicker();
  }

  checkForm(value) {
    if (value) {
      this.global.displayToastMessage('Please add all the required header details');
    }
  }

  toShowInternalOrders() {
    let decision = true;
    switch (this.header.WERKS) {
      case 'MKAJ':
      case 'MMSA':
      case 'MMGD':
        decision = false;
        break;
      default:
        decision = true;
        break;
    }
    return decision;
  }

  onBack() {
    this.header = {};
    this.attachments = {
      images: [],
      files: []
    };
    this.service.isReservationSync = false;
    for (let i = 0; i < this.lineItems.length; i++) {
      this.lineItems.splice(i, 1);
    }
    this.resetValues();
    this.router.navigate(['/main/requests']);
  }

  onAddLineItem() {
    if (this.header.WERKS) {
      if (this.isFirstLineItem === false) {
        this.isFirstLineItem = true;
      }
      this.router.navigate(['line-item'], { relativeTo: this.activatedRoute });
    } else {
      this.global.displayToastMessage('Select Plant first to add line item');
    }
  }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  onSlide(slideMode) {
    (slideMode === 'next') ? this.segment++ : this.segment--;
    this.segmentChanged();
  }

  onRemoveLineItem(id) {
    this.alertController.create(
      {
        header: 'Message',
        message: 'Are you sure you want to delete this line item?',
        buttons: [
          {
            text: 'No',
            role: 'cancel'
          },
          {
            text: 'Yes',
            handler: () => {
              this.lineItems.splice(id, 1);
            }
          }
        ]
      }
    ).then(alert => alert.present());
  }

  openMovementTypeSearchHelp(value) {
    if (!this.isOpenReservationSelected || this.isOpenReservationSelected == undefined) {
      if (this.header.WERKS) {
        if (this.checkForMvtOrPlantOrProfitCenter('MovementTypes')) {
          if (this.checkForReservationSyncValue(value)) {
            this.modalController.create(
              {
                component: MovementTypeSearchPage,
                componentProps: {
                  data: this.header.WERKS
                }
              }
            ).then(modal => {
              modal.present();
              modal.onDidDismiss().then(data => {
                data = data.data;
                if (data !== undefined) {
                  this.setDataToHeader(data);
                }
              });
            });
          }
        }
      } else {
        this.global.displayToastMessage('Please select Plant first');
      }
    } else this.global.displayToastMessage('Data can not be changed while proceeding with Auto Reservation.');
  }

  openDepartmentSearchHelp(value) {
    if (!this.isOpenReservationSelected || this.isOpenReservationSelected == undefined) {
      if (this.header.WERKS) {
        if (this.checkForReservationSyncValue(value)) {
          this.modalController.create(
            {
              component: DepartmentSearchPage,
              componentProps: {
                data: this.header.WERKS
              }
            }
          ).then(modal => {
            modal.present();
            modal.onDidDismiss().then(data => {
              data = data.data;
              if (data !== undefined) {
                this.setDataToHeader(data);
              }
            });
          });
        }
      } else {
        this.global.displayToastMessage('Please select Plant first');
      }
    } else if (!this.header.DEPT_NAME || this.header.DEPT_NAME == '') {
      if (this.isOpenReservationEditable.department == true) {
        this.modalController.create(
          {
            component: DepartmentSearchPage,
            componentProps: {
              data: this.header.WERKS
            }
          }
        ).then(modal => {
          modal.present();
          modal.onDidDismiss().then(data => {
            data = data.data;
            if (data !== undefined) {
              this.setDataToHeader(data);
            }
          });
        });
      } else this.global.displayToastMessage('Data can not be changed while proceeding with Auto Reservation.');
    } else if (this.isOpenReservationEditable.department == true) {
      this.modalController.create(
        {
          component: DepartmentSearchPage,
          componentProps: {
            data: this.header.WERKS
          }
        }
      ).then(modal => {
        modal.present();
        modal.onDidDismiss().then(data => {
          data = data.data;
          if (data !== undefined) {
            this.setDataToHeader(data);
          }
        });
      });
    } else this.global.displayToastMessage('Data can not be changed while proceeding with Auto Reservation.');

  }

  openDialogForAutoReservation(instanceType) {
    this.modalController.create(
      {
        component: SearchHelpPage,
        componentProps: {
          data: instanceType
        }
      }
    ).then(modal => {
      modal.present();
      modal.onDidDismiss().then(data => {
        data = data.data;
        if (data !== undefined) {
          this.setDataToHeader(data);
        }
      });
    });
  }

  openSearchHelp(instanceType, value) {
    if (!this.isOpenReservationSelected || this.isOpenReservationSelected == undefined) {
      if (this.checkForReservationSyncValue(value)) {
        if (this.checkForMvtOrPlantOrProfitCenter(instanceType)) {
          this.openDialogForAutoReservation(instanceType);
        }
      }
    } else {
      switch (instanceType) {
        case 'Plant':
          if (!this.header.PLANT_DES || this.header.PLANT_DES == '') {
            if (this.checkForMvtOrPlantOrProfitCenter(instanceType)) {
              this.openDialogForAutoReservation(instanceType);
            }
          } else this.global.displayToastMessage('Data can not be changed while proceeding with Auto Reservation.');
          break;
        case 'CostCenter':
          if (!this.header.KOSTL || this.header.KOSTL == '') {
            if (this.isOpenReservationEditable.costCenter == true) {
              if (this.checkForMvtOrPlantOrProfitCenter(instanceType)) {
                this.openDialogForAutoReservation(instanceType);
              }
            } else this.global.displayToastMessage('Data can not be changed while proceeding with Auto Reservation.');
          } else if (this.isOpenReservationEditable.costCenter == true) {
            if (this.checkForMvtOrPlantOrProfitCenter(instanceType)) {
              this.openDialogForAutoReservation(instanceType);
            }
          } else this.global.displayToastMessage('Data can not be changed while proceeding with Auto Reservation.');
          break;
      }
    }
  }

  checkForMvtOrPlantOrProfitCenter(instanceType) {
    let decision = true;
    if (instanceType === 'MovementTypes') {
      if (this.header.BWART && this.header.BWART !== '') {
        decision = false;
        this.alertController.create(
          {
            header: 'Confirmation',
            message: 'Change of Movement type clears all previous values relating to it, Are you sure you want to change the movement type?',
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel'
              },
              {
                text: 'Ok',
                handler: () => {
                  this.loadingController.create(
                    {
                      message: 'Please wait...'
                    }
                  ).then(
                    loader => {
                      loader.present();
                      for (let key in this.header) {
                        this.header[key] = '';
                      }
                      this.lineItems.length = 0;
                      loader.dismiss();
                    }
                  );
                }
              }
            ]
          }
        ).then(alert => alert.present());
      }
    } else if (instanceType === 'Plant') {
      if (this.header.WERKS && this.header.WERKS !== '') {
        decision = false;
        this.alertController.create(
          {
            header: 'Confirmation',
            message: 'Change of Plant clears all previous values relating to it, Are you sure you want to change the Plant?',
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel'
              },
              {
                text: 'Ok',
                handler: () => {
                  this.loadingController.create(
                    {
                      message: 'Please wait...'
                    }
                  ).then(
                    loader => {
                      loader.present();
                      for (let key in this.header) {
                        this.header[key] = '';
                      }
                      this.lineItems.length = 0;
                      loader.dismiss();
                    }
                  );
                }
              }
            ]
          }
        ).then(alert => alert.present());
      }
    } else if (instanceType === 'Profitcenter' && this.header.BWART === '502') {
      decision = false;
      this.global.displayToastMessage('Please select Cost Center first to get Profit Center');
    } else if (instanceType === 'CostCenter') {
      if (this.header.WERKS === 'MKAJ' || this.header.WERKS === 'MMGD' || this.header.WERKS === 'MMSA') {
        decision = false;
        this.global.displayToastMessage('Please select Department to get Cost Center');
      }
    }
    return decision;
  }

  openStorageLocationSearchHelp(instanceType, value) {
    if (this.checkForReservationSyncValue(value)) {
      if (this.header.BWART === '301' || this.header.BWART === '302') {
        if (this.header.UMWRK) {
          if (this.storageLocations.length > 0) {
            this.modalController.create(
              {
                component: LineItemPlantSearchPage,
                componentProps: { data: { data: this.storageLocations, selector: 'storage' } }
              }
            ).then(
              modal => {
                modal.present();
                modal.onDidDismiss().then(data => {
                  data = data.data;
                  if (data !== undefined) {
                    this.header['UMLGO'] = data.data.storageLocation;
                    this.header['RI_STRG_DES'] = data.data.storageLocationDescription;
                  }
                });
              }
            )
          } else {
            let plantType = this.header.BWART === '301' ? 'Receiving' : 'Issuing';
            this.global.displayToastMessage('No Storage Locations available for selected ' + plantType + ' Plant');
          }
        } else {
          let plantType = this.header.BWART === '301' ? 'Receiving' : 'Issuing';
          this.global.displayToastMessage('Please select ' + plantType + ' Plant first')
        }
      } else {
        this.openSearchHelp(instanceType, value)
      }
    }
  }

  openMasterSearchHelpDialogAfterValidate() {
    this.modalController.create(
      {
        component: MaterialSearchHelpPage,
        componentProps: { data: 'Order Number' }
      }
    ).then(modal => {
      modal.present();
      modal.onDidDismiss().then(data => {
        data = data.data;
        if (data !== undefined) {
          this.header = Object.assign(this.header, data.data);
        }
      })
    })
  }

  getOrderNumber(value) {
    if (!this.isOpenReservationSelected || this.isOpenReservationSelected == undefined) {
      if (this.checkForReservationSyncValue(value)) {
        this.openMasterSearchHelpDialogAfterValidate();
      }
    } else {
      if (!this.header.ORDER_DES || this.header.ORDER_DES == '') {
        if (this.isOpenReservationEditable.orderNumber == true) {
          this.openMasterSearchHelpDialogAfterValidate();
        } else this.global.displayToastMessage('Data can not be changed while proceeding with Auto Reservation.');
      } else if (this.isOpenReservationEditable.orderNumber == true) {
        this.openMasterSearchHelpDialogAfterValidate();
      } else this.global.displayToastMessage('Data can not be changed while proceeding with Auto Reservation.');
    }
  }

  checkForReservationSyncValue(value) {
    let decision: boolean;
    if (this.service.isReservationSync) {
      if (!value) {
        decision = true;
      } else {
        decision = false;
      }
    } else {
      decision = true;
    }
    return decision;
  }


  reservationSync() {
  //   this.modalController.create(
  //     {
  //       component: ReservationSearchPage,
  //       backdropDismiss: false
  //     },
  //   ).then(
  //     modal => {
  //       modal.present();
  //       modal.onDidDismiss().then(response => {
  //         if (response.data != undefined) {
  //           this.header = {};
  //           this.lineItems = [];
  //           let data = response.data;
  //           let selectedWorkItem: any = data.data;
  //           if (data) {
  //             data = data.data;
  //             this.modalController.create({
  //               component: ReservationLineItemsPage,
  //               componentProps: {
  //                 workitem: data
  //               },
  //               backdropDismiss: false
  //             }).then(modal => {
  //               modal.present();
  //               modal.onDidDismiss().then(response => {
  //                 if (response.data == undefined) {
  //                   this.reservationSync();
  //                 } else {
  //                   let selectedLineItems: any = response.data.data;
  //                   this.service.isReservationSync = true;
  //                   this.isOpenReservationSelected = true;
  //                   this.reservationNumber = selectedWorkItem.reservationNumber;
  //                   this.isFirstLineItem = true;
  //                   this.header = {
  //                     BWART: selectedWorkItem.mvtType,
  //                     MVT_DES: selectedWorkItem.mvtTypeDesciption,
  //                     PRCTR: selectedWorkItem.profitCenter,
  //                     KOSTL: selectedWorkItem.costCenter,
  //                     COST_DES: selectedWorkItem.costCenterDescrption,
  //                     SAKNR: selectedWorkItem.reservationItem[0].glAccount,
  //                     UMWRK: selectedWorkItem.receiveIssuePlant,
  //                     UMLGO: selectedWorkItem.receiveIssueStorage,
  //                     RSNUM: selectedWorkItem.reservationNumber,
  //                     WERKS: selectedWorkItem.reservationItem[0].plant,
  //                     PLANT_DES: selectedWorkItem.reservationItem[0].plantDescription,
  //                     DEPT_ID: selectedWorkItem.DEPID,
  //                     DEPT_NAME: selectedWorkItem.DEPDS,
  //                     AUFNR: selectedWorkItem.orderNumber,
  //                     ORDER_DES: selectedWorkItem.orderNumber
  //                   }
  //                   if (!selectedWorkItem.DEPID || selectedWorkItem.DEPID == '') this.isOpenReservationEditable.department = true;
  //                   if (!selectedWorkItem.costCenter || selectedWorkItem.costCenter == '') this.isOpenReservationEditable.costCenter = true;
  //                   if (!selectedWorkItem.orderNumber || selectedWorkItem.orderNumber == '') this.isOpenReservationEditable.orderNumber = true;
  //                   if (this.header.KOSTL) {
  //                     this.getProfitCenterAndBusinessArea(this.header.KOSTL);
  //                   }
  //                   selectedLineItems.forEach(lineItem => {
  //                     let unit;
  //                     if (parseFloat(lineItem.standardPrice) === 0) {
  //                       unit = parseFloat(lineItem.movingPrice.replace(',', ''));
  //                     } else {
  //                       unit = parseFloat(lineItem.standardPrice.replace(',', '.'));
  //                     }
  //                     this.lineItems.push({
  //                       MATNR: lineItem.materialCode,
  //                       MAT_DES: lineItem.materialDescription,
  //                       WERKS: lineItem.plant,
  //                       PLANT_DES: lineItem.plantDescription,
  //                       MEINS: lineItem.uom,
  //                       STORAGE_DES: lineItem.storageLocationDescription,
  //                       LGORT: lineItem.storageLocation,
  //                       ERFMG: parseFloat(lineItem.reservedQty),
  //                       BDMNG: parseFloat(lineItem.reservedQty) * unit,
  //                       unitPrice: unit,
  //                       CHARG: lineItem.batchNo
  //                     });
  //                   });
  //                 }
  //               });
  //             });
  //           }
  //         }
  //       })
  //     }

  //   )
  }

  setDataToHeader(data) {
    switch (data.instance) {
      case 'MovementTypes':
        this.header['BWART'] = data.data.BWART;
        this.header['MVT_DES'] = data.data.BTEXT;
        this.service.movementTypeinHeader = data.data.BWART;
        this.clearOtherFieldValues(this.header.BWART);
        break;
      case 'Departments':
        this.header['DEPT_ID'] = data.data.DEPID;
        this.header['DEPT_NAME'] = data.data.DEPDS;
        if (this.header.WERKS === 'MKAJ' || this.header.WERKS === 'MMGD' || this.header.WERKS === 'MMSA') {
          this.setDataToHeader(
            {
              instance: 'CostCenter',
              data: {
                KOSTL: this.header.DEPT_ID,
                LTEXT: this.header.DEPT_NAME
              }
            }
          )
        }
        break;
      case 'CostCenter':
        this.header['KOSTL'] = data.data.KOSTL;
        this.header['COST_DES'] = data.data.LTEXT;
        this.getProfitCenterAndBusinessArea(this.header.KOSTL);
        break;
      case 'Plant':
        this.header['WERKS'] = data.data.WERKS_D;
        this.header['PLANT_DES'] = data.data.NAME1;
        this.service.plantBasedMaterialValue = data.data.WERKS_D;
        break;
      case 'OrderTypes':
        this.header['AUART'] = data.data.AUART;
        this.header['ORDER_DES'] = data.data.TXT;
        break;
      case 'GLaccount':
        this.header['SAKNR'] = data.data.SAKNR;
        this.header['GLACNT_TXT'] = data.data.GLACNT_TXT;
        break;
      case 'Profitcenter':
        this.header['PRCTR'] = data.data.PRCTR;
        this.header['PRC_TXT'] = data.data.PRC_TXT;
        break;
      case 'Fundscenter':
        this.header['FISTL'] = data.data.FICTR;
        this.header['BESCHR'] = data.data.BESCHR;
        break;
      case 'Businessarea':
        this.header['GSBER'] = data.data.GSBER;
        this.header['GTEXT'] = data.data.GTEXT;
        break;
      case 'Commitmentitem':
        this.header['FIPOS'] = data.data.FIPEX;
        this.header['COMIT_ITEM_DES'] = data.data.COMIT_ITEM_DES;
        break;
      case 'ReceivingPlant':
        this.header['UMWRK'] = data.data.WERKS_D;
        this.header['RI_PLNT_DES'] = data.data.NAME1;
        this.getReceivingStorageLocationBasedOnSelectedPlant(this.header.UMWRK);
        break;
      case 'ReceivingStorageLocation':
        this.header['UMLGO'] = data.data.LGORT_D;
        this.header['RI_STRG_DES'] = data.data.LGOBE;
        break;
      case 'WBSElements':
        this.header['WBS_ELMNT'] = data.data.POSID;
        this.header['WBS_ELMNT_DES'] = data.data.WBS_DES;
        this.service.wbsElementInHeader = data.data.POSID;
        break;
      case 'Vendors':
        this.header['VENDOR_NUM'] = data.data.LIFNR;
        this.header['VENDOR_NAME'] = data.data.VENDOR_NAME;
        break;
      case 'WorkOrders':
        this.header['AUFNR'] = data.data.WRK_ORDER,
          this.header['ORDER_DES'] = data.data.WRK_ORDER_DES;
        break;
      default:
        alert('Nothing Selected');
        break;
    }
  }

  clearOtherFieldValues(mvtType) {

  }

  getProfitCenterAndBusinessArea(costCenter) {
    this.service.getDynamicInstanceData('Costdetails', ['KOSTL'], costCenter, 0, 100).subscribe(
      (response: any) => {
        if (response.Content.length > 0) {
          response = response.Content[0];
          if (response) {
            if (this.header.BWART === '201' || this.header.BWART === '202') {
              this.header['GSBER'] = (response.GSBER ? response.GSBER : '');
              this.header['GTEXT'] = (response.GSBER_TEXT ? response.GSBER_TEXT : '');
            } else if (this.header.BWART === '502') {
              this.header['PRCTR'] = (response.PRCTR ? response.PRCTR : '');
              this.header['PRC_TXT'] = (response.PRCTR_TEXT ? response.PRCTR_TEXT : '');
            }
          }
        } else {
          this.global.displayToastMessage('Unable to fetch Profit Center and Business Area From Cost Center');
        }
      }, error => this.global.displayToastMessage('Unable to fetch Profit Center and Business Area From Cost Center')
    );
  }

  getReceivingStorageLocationBasedOnSelectedPlant(plant) {
    this.header['UMLGO'] = '';
    this.header['RI_STRG_DES'] = '';
    this.loadingController.create(
      {
        message: 'Please wait while Receiving Storage Locations are getting loaded...'
      }
    ).then(loader => {
      loader.present();
      this.service.getDynamicInstanceData('PlantStorageLocations', ['WERKS_D'], plant, 0, 100).subscribe((response: any) => {
        this.storageLocations = response.Content;
        loader.dismiss();
      }, error => {
        loader.dismiss();
        this.global.displayToastMessage('Error occurred while loading Storage Locations');
      });
    });
  }

  uploadAnAttachment() {
    this.actionSheetController.create(
      {
        header: 'Options',
        cssClass: 'attachmentsAS',
        mode: 'md',
        buttons: [
          {
            text: 'Scan',
            icon: 'camera',
            cssClass: 'scanOptionAS',
            handler: () => {
              if (!Capacitor.isPluginAvailable('Camera')) {
                this.global.displayToastMessage(
                  'Unable To Open Camera');
                return;
              }
              Plugins.Camera.getPhoto({
                quality: 60,
                source: CameraSource.Camera,
                height: 400,
                width: 300,
                correctOrientation: true,
                resultType: CameraResultType.DataUrl
              })
                .then(image => {
                  const blobImg = this.global.dataURItoBlob(image.dataUrl);
                  const img = {
                    name: 'Image-' + new Date().toISOString(),
                    image: blobImg
                  }
                  this.attachments.images.push(img);
                  this.global.displayToastMessage('Image has uploaded successfully');
                })
                .catch(error => {
                  return false;
                });
            }
          },
          {
            text: 'Upload',
            icon: 'folder',
            cssClass: 'filesOptionAS',
            handler: () => {
              document.getElementById('fileUpload').click();
            }
          }
        ]
      }
    ).then(actionSheet => actionSheet.present());
  }

  onLineItemChecked(event, lineItem) {
    if (event.isChecked === 'yes') {
      this.lineItemsInCaseOfReservationSync.push(lineItem)
    } else {
      this.lineItemsInCaseOfReservationSync.splice(event.id, 1);
    }
  }

  onSelectFile(event) {
    const formData = event.target.files;
    for (let key in formData) {
      if (key !== 'length' && key !== 'item') {
        if (formData[key].size < 25000000) {
          this.attachments.files.push(formData[key]);
          this.global.displayToastMessage('File has attached successfully');
        } else {
          this.global.displayToastMessage('File size is more than 25MB');
        }
      }
    }
    (<HTMLInputElement>document.getElementById('fileUpload')).value = '';
  }

  onRemoveAttachment(index, type) {
    if (type === 'file') {
      this.attachments.files.splice(index, 1);
    } else {
      this.attachments.images.splice(index, 1);
    }
  }

  // Setting Pick A Date, upto how many years ahead user can select
  setDatepicker() {
    let yearsToAddCount = 50;
    this.currentDate = new Date().toISOString();
    this.aheadDate = new Date(new Date().setFullYear(new Date().getFullYear() + yearsToAddCount)).toISOString();
    this.reservationDate = this.currentDate;
  }

  resetValues() {
    this.service.plantBasedMaterialValue = '';
    this.service.movementTypeinHeader = '';
    this.service.isReservationSync = false;
  }

  checkForStockAvailiblity() {
    this.service.getDynamicInstanceData('Msprvalues', ['MATNR'], this.lineItems[this.lineItems.length - 1].MATNR, 0, 100).subscribe((response: any) => {
      if (response.Content.length > 0) {
        for (let i = 0; i < response.Content.length; i++) {
          if (response.Content[i].POSID === this.header.WBS_ELMNT && response.Content[i].MATNR === this.lineItems[this.lineItems.length - 1].MATNR) {
            this.lineItems[this.lineItems.length - 1]['isStockAvailable'] = true;
            break;
          } else {
            this.lineItems[this.lineItems.length - 1]['isStockAvailable'] = false;
          }
        }
      } else {
        this.lineItems[this.lineItems.length - 1]['isStockAvailable'] = false;
      }
    });
  }

  onSubmit() {
    // Uncomment this and last lines of this method to make comments mandatory
    // if (this.comments) {
    if (this.service.shouldGoForDoa({ header: this.header })) {
      let totalAmount = 0;
      this.lineItems.forEach(element => {
        totalAmount += parseFloat(element.BDMNG);
      });
      this.service.doaCheck(this.header, totalAmount).subscribe((response: any) => {
        if (response) {
          this.alertController.create(
            {
              header: 'Confirmation',
              message: 'Are you sure you want to create the reservation?',
              backdropDismiss: false,
              buttons: [
                {
                  text: 'Cancel',
                  role: 'cancel'
                },
                {
                  text: 'Yes',
                  handler: () => {
                    if (this.header.BWART === '221') {
                      if (this.lineItems.length > 0) {
                        let decision = true, lineItemIndexes = [];
                        for (let index = 0; index < this.lineItems.length; index++) {
                          if (this.lineItems[index].isStockAvailable === false) {
                            decision = false;
                            lineItemIndexes.push(index + 1);
                          }
                        }
                        if (decision) {
                          if (this.service.isReservationSync) {
                            this.submitRequest(true);
                          } else {
                            this.submitRequest(false);
                          }
                        } else {
                          let msg = 'Line item no. ' + lineItemIndexes.toString() + ' not associated with WBS Element.'
                          this.alertController.create(
                            {
                              header: 'Message',
                              message: msg,
                              backdropDismiss: false,
                              buttons: [
                                {
                                  text: 'Ok',
                                  role: 'cancel'
                                }
                              ]
                            }
                          ).then(alert => alert.present());
                        }
                      } else {
                        this.global.displayToastMessage('Atleast one line item is needed to initiate request.');
                      }
                    } else if (this.service.isReservationSync) {
                      if (this.lineItemsInCaseOfReservationSync.length > 0) {
                        this.submitRequest(true);
                      } else {
                        this.global.displayToastMessage('Atleast one line item needs to be selected for initiation');
                      }
                    } else {
                      if (this.lineItems.length > 0) {
                        this.submitRequest(false);
                      } else {
                        this.global.displayToastMessage('Atleast one line item is needed to initiate request.');
                      }
                    }
                  }
                }
              ]
            }
          ).then(alert => alert.present());
        } else {
          this.global.displayToastMessage('The Approval Matrix is not available for the Selected Department!');
        }
      }, error => this.global.displayToastMessage('The Approval Matrix is not available for the Selected Department!'));
    } else {
      this.alertController.create(
        {
          header: 'Confirmation',
          message: 'Are you sure you want to create the reservation?',
          backdropDismiss: false,
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel'
            },
            {
              text: 'Yes',
              handler: () => {
                if (this.header.BWART === '221') {
                  if (this.lineItems.length > 0) {
                    let decision = true, lineItemIndexes = [];
                    for (let index = 0; index < this.lineItems.length; index++) {
                      if (this.lineItems[index].isStockAvailable === false) {
                        decision = false;
                        lineItemIndexes.push(index + 1);
                      }
                    }
                    if (decision) {
                      if (this.service.isReservationSync) {
                        this.submitRequest(true);
                      } else {
                        this.submitRequest(false);
                      }
                    } else {
                      let msg = 'Line item no. ' + lineItemIndexes.toString() + ' not associated with WBS Element.'
                      this.alertController.create(
                        {
                          header: 'Message',
                          message: msg,
                          backdropDismiss: false,
                          buttons: [
                            {
                              text: 'Ok',
                              role: 'cancel'
                            }
                          ]
                        }
                      ).then(alert => alert.present());
                    }
                  } else {
                    this.global.displayToastMessage('Atleast one line item is needed to initiate request.');
                  }
                } else if (this.service.isReservationSync) {
                  if (this.lineItemsInCaseOfReservationSync.length > 0) {
                    this.submitRequest(true);
                  } else {
                    this.global.displayToastMessage('Atleast one line item needs to be selected for initiation');
                  }
                } else {
                  if (this.lineItems.length > 0) {
                    this.submitRequest(false);
                  } else {
                    this.global.displayToastMessage('Atleast one line item is needed to initiate request.');
                  }
                }
              }
            }
          ]
        }
      ).then(alert => alert.present());
    }
    // } else {
    //   this.global.displayToastMessage('Comments are required');
    // }
  }

  submitRequest(isReservationSync) {
    this.loadingController.create(
      {
        message: 'Please wait while its getting initiated'
      }
    ).then(loader => {
      loader.present();
      let totalAmount: any = 0;
      let finalSubmitData = {
        docType: 'GI_PROCESS',
        header: {
          RSDAT: this.global.getSAPDate(this.reservationDate), // Reservation Date
          ...this.header, // Attaching all header details
          RECP_ID: this.recipient.login, // Recipient ID
          RECP_NAME: this.recipient.firstName + ' ' + this.recipient.lastName // Recipient
        },
        lineItems: [],
        description: this.comments ? this.comments : ''
      }
      let lineItem;
      if (isReservationSync) lineItem = [...this.lineItemsInCaseOfReservationSync];
      else lineItem = [...this.lineItems];
      lineItem.forEach((element, index) => {
        totalAmount += parseFloat(element.BDMNG);
        if (finalSubmitData.header.BWART === '221' || finalSubmitData.header.BWART === '222') element['ITEM_FLAG'] = 'S';
        finalSubmitData.lineItems.push(
          {
            sno: index + 1,
            lineItems: element,
            subLineItems: {}
          }
        );
      });
      finalSubmitData.header['TOTAL_AMOUNT'] = parseFloat(totalAmount);
      this.service.initiateRequest(finalSubmitData, this.attachments.files, this.attachments.images).subscribe((response: any) => {
        this.alertController.create(
          {
            header: 'Message',
            subHeader: 'Request ID: ' + response.requestId,
            backdropDismiss: false,
            message: 'You have successfully initiated the Reservation Request',
            buttons: [
              {
                text: 'Ok',
                handler: () => {
                  loader.dismiss();

                  this.router.navigate(['/main/requests']);
                }
              }
            ]
          }
        ).then(successAlert => {
          loader.dismiss();
          successAlert.present();
        });
      }, error => {
        this.global.displayToastMessage('Some problem occurred while initiating request, please try after some time');
        loader.dismiss();
      });
    });
  }

}
