import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GoodsIssuePage } from './goods-issue.page';

const routes: Routes = [
  {
    path: '',
    component: GoodsIssuePage
  },  {
    path: 'line-item',
    loadChildren: () => import('./line-item/line-item.module').then( m => m.LineItemPageModule)
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GoodsIssuePageRoutingModule {}
