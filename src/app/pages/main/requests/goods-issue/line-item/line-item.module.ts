import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LineItemPageRoutingModule } from './line-item-routing.module';

import { LineItemPage } from './line-item.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LineItemPageRoutingModule
  ],
  declarations: [LineItemPage]
})
export class LineItemPageModule {}
