import { Component, OnInit } from '@angular/core';
import { GoodsIssueService } from '../goods-issue.service';
import { Router } from '@angular/router';
import { ModalController, LoadingController } from '@ionic/angular';
import { GlobalService } from 'src/app/services/global.service';
import { MaterialSearchHelpPage } from 'src/app/pages/modals/material-search-help/material-search-help.page';
import { LineItemPlantSearchPage } from 'src/app/pages/modals/line-item-plant-search/line-item-plant-search.page';
import { StockCheckModalPage } from 'src/app/pages/modals/stock-check-modal/stock-check-modal.page';
import { SearchHelpPage } from 'src/app/pages/modals/search-help/search-help.page';

@Component({
  selector: 'app-line-item',
  templateUrl: './line-item.page.html',
  styleUrls: ['./line-item.page.scss'],
})
export class LineItemPage implements OnInit {

  lineItem: any = {
    batch: []
  };
  lineItemId: any = undefined;

  maxLimitOfReserveQty: number;

  isBatchSelected: boolean = false;

  sortedSpecialStockData: any = [];
  specialStock: any = '';

  constructor(
    public service: GoodsIssueService,
    private router: Router,
    private modalController: ModalController,
    public global: GlobalService,
    private loadingController: LoadingController
  ) {
    this.global.hideBottomTabs();
    this.service.lineItemSubject.subscribe(lineItemData => {
      if (lineItemData !== 0) {
        if (lineItemData.from === 'mainToLineItem' && lineItemData.data.id >= 0) {
          this.lineItem = lineItemData.data.lineItem;
          this.maxLimitOfReserveQty = parseInt(this.lineItem.ERFMG);
          this.lineItemId = lineItemData.data.id;
        }
      }
    });
    this.getSpecialStockDetails();
  }

  ngOnInit() {
  }

  setSpecialStock(item) {
    this.specialStock = item.SOBKZ + '-' + item.SOTXT;
    this.lineItem['SOBKZ'] = item.SOBKZ;
    this.lineItem['SOTXT'] = item.SOTXT;
    if (this.lineItem.SOBKZ === 'NA') {
      if (this.lineItem.LIFNR) this.lineItem.LIFNR = '';
      if (this.lineItem.VENDOR_NAME) this.lineItem.VENDOR_NAME = '';
    }
  }

  getSpecialStockDetails() {
    this.loadingController.create(
      {
        message: 'Please wait...'
      }
    ).then(loader => {
      loader.present();
      this.service.getDynamicInstanceData('SpecialStockBasedOnMvtType', ['MovementType'], this.service.movementTypeinHeader, 0, 100).subscribe((response: any) => {
        if (response.Content.length > 0) {
          let specialStockDetailsBasedOnMovementType = response.Content[0];
          specialStockDetailsBasedOnMovementType.SpecialStock = specialStockDetailsBasedOnMovementType.SpecialStock.split(',');
          this.service.getDynamicInstanceData('SpecialStock', [], '', 0, 100).subscribe((specialStockDetails: any) => {
            this.sortedSpecialStockData = [];
            let defaultSelectedSpecialStock = specialStockDetails.filter((element) => {
              return element.SOBKZ === specialStockDetailsBasedOnMovementType.DefaultSelected;
            });
            if (defaultSelectedSpecialStock.length === 1) this.setSpecialStock(defaultSelectedSpecialStock[0]);
            specialStockDetailsBasedOnMovementType.SpecialStock.forEach(specialStockCode => {
              let filteredResult = specialStockDetails.filter(function (element) {
                return element.SOBKZ === specialStockCode;
              });
              if (filteredResult.length === 1) this.sortedSpecialStockData.push(filteredResult[0]);
            });
            loader.dismiss();
          }, error => {
            this.global.displayToastMessage('Problem occurred while fetching Special Stock Details, Please try after some time');
            loader.dismiss();
          });
        } else {
          this.global.displayToastMessage('Problem occurred while fetching Special Stock Details, Please try after some time');
          loader.dismiss();
        }
      }, error => {
        this.global.displayToastMessage('Problem occurred while fetching Special Stock Details, Please try after some time');
        loader.dismiss();
      });
    });
  };

  checkForReservationSyncValue(value) {
    let decision: boolean;
    if (this.service.isReservationSync) {
      if (value || value === undefined || value === "") {
        decision = true;
      } else {
        decision = false;
      }
    } else {
      decision = true;
    }
    return decision;
  }

  onReserveQtyChange() {
    if (this.service.isReservationSync) {
      let value = parseInt(this.lineItem.ERFMG);
      if (value > this.maxLimitOfReserveQty) {
        this.lineItem.ERFMG = this.maxLimitOfReserveQty;
        this.global.displayToastMessage('Cannot set Reserve Quantity more than its value');
      } else {
        this.lineItem.ERFMG = value;
      }
    }
    this.calculateReserveValue({ detail: { value: this.lineItem.ERFMG } });
  }

  calculateReserveValue(event) {
    let unit: any = parseFloat(event.detail.value).toFixed(3);
    this.lineItem.ERFMG = Math.abs(unit);
    this.lineItem.BDMNG = (this.lineItem.ERFMG * this.lineItem.unitPrice).toFixed(2);
  }

  itemTextValidation(event) {
    // this.lineItem.SGTXT = event.detail.value.replace(/[^A-Za-z]+/g, '');
  }

  openMaterialSearchHelp(value) {
    if (this.checkForReservationSyncValue(value)) {
      if (this.service.plantBasedMaterialValue !== '') this.global.displayToastMessage('Displaying Material based on Plant selected in header');
      this.modalController.create(
        {
          component: MaterialSearchHelpPage,
          componentProps: { data: 'Material' }
        }
      ).then(
        modal => {
          modal.present();
          modal.onDidDismiss().then(data => {
            data = data.data;
            if (data !== undefined) {
              this.clearForm();
              this.lineItem = Object.assign(this.lineItem, data.data);
              this.lineItem.plantDetails.filter(item => {
                if (item.plant === this.service.plantBasedMaterialValue) {
                  this.lineItem['WERKS'] = item.plant;
                  this.lineItem['PLANT_DES'] = item.plantDescription;
                }
              });
              this.lineItem['batch'] = [];
              this.sortStorageLocationsBasedOnPlant();
              this.getBatchDetails();
            }
          });
        }
      )
    }
  }

  sortStorageLocationsBasedOnPlant() {
    let sortedStorageLocations = [];
    if (this.lineItem.storage.length > 1) {
      this.service.getDynamicInstanceData('PlantStorageLocations', ['LGORT_D', 'WERKS_D'], this.service.plantBasedMaterialValue, 0, 500).subscribe((response: any) => {
        this.lineItem.storage.forEach(storage => {
          let element = response.Content.filter(plantStorageValue =>
            plantStorageValue.LGORT_D === storage.storageLocation && plantStorageValue.WERKS_D === this.service.plantBasedMaterialValue
          );
          if (element.length > 0) {
            sortedStorageLocations.push(storage);
          }
        });
        this.lineItem.storage = sortedStorageLocations;
        this.lineItem.LGORT = this.lineItem.storage[0].storageLocation;
        this.lineItem.STORAGE_DES = this.lineItem.storage[0].storageDescription;
      }, error => this.global.displayToastMessage('Unable to sort Storage Location based on Plant'));
    } else {
      if (this.lineItem.storage.length === 0) {
        this.lineItem.LGORT = '';
        this.lineItem.STORAGE_DES = '';
        this.global.displayToastMessage('Storage Location not available for selected material.');
      } else {
        this.lineItem.LGORT = this.lineItem.storage[0].storageLocation;
        this.lineItem.STORAGE_DES = this.lineItem.storage[0].storageDescription;
      }
    }
  }

  selectVendorCode(value) {
    if (this.checkForReservationSyncValue(value)) {
      this.modalController.create(
        {
          component: SearchHelpPage,
          componentProps: {
            data: 'VendorsGI'
          }
        }
      ).then(modal => {
        modal.present();
        modal.onDidDismiss().then(data => {
          data = data.data;
          if (data !== undefined) {
            let vendorDetails: any = data.data;
            this.lineItem['LIFNR'] = vendorDetails.LIFNR;
            this.lineItem['VENDOR_NAME'] = vendorDetails.VENDOR_NAME;
          }
        });
      });
    }
  }

  selectSpecialStock(value) {
    if (this.checkForReservationSyncValue(value)) {
      if (this.sortedSpecialStockData.length > 0) {
        this.modalController.create(
          {
            component: LineItemPlantSearchPage,
            componentProps: { data: { data: this.sortedSpecialStockData, selector: 'specialStock' } }
          }
        ).then(modal => {
          modal.present();
          modal.onDidDismiss().then(data => {
            data = data.data;
            if (data !== undefined) {
              this.setSpecialStock(data.data);
            }
          });
        })
      } else {
        this.global.displayToastMessage('Special stock data not fetched properly, please try after some time');
      }
    }
  }

  selectBatch(value) {
    if (this.lineItem.MATNR) {
      if (this.checkForReservationSyncValue(value)) {
        if (this.lineItem.batch.length > 0) {
          if (this.lineItem.batch.length > 1) {
            this.modalController.create(
              {
                component: LineItemPlantSearchPage,
                componentProps: { data: { data: this.lineItem.batch, selector: 'batch' } }
              }
            ).then(
              modal => {
                modal.present();
                modal.onDidDismiss().then(data => {
                  data = data.data;
                  if (data !== undefined) {
                    this.lineItem = Object.assign(this.lineItem, data.data);
                  }
                });
              }
            )
          } else {
            this.global.displayToastMessage('Only one Batch is available for selected Material');
          }
        } else {
          this.global.displayToastMessage('No Batch available for selected Material');
        }
      }
    } else {
      this.global.displayToastMessage('Select Material first to get Batch Details.');
    }
  }

  openPlantSearchHelp(value) {
    if (this.checkForReservationSyncValue(value)) {
      if (this.lineItem.plantDetails) {
        if (this.lineItem.plantDetails.length > 0) {
          this.modalController.create(
            {
              component: LineItemPlantSearchPage,
              componentProps: { data: { data: this.lineItem.plantDetails, selector: 'plant' } }
            }
          ).then(
            modal => {
              modal.present();
              modal.onDidDismiss().then(data => {
                data = data.data;
                if (data !== undefined) {
                  this.lineItem = Object.assign(this.lineItem, data.data);
                }
              });
            }
          )
        } else {
          this.global.displayToastMessage('No Plant is assigned for this Material');
        }
      } else {
        this.global.displayToastMessage('Select Material First to get Plant Details');
      }
    }
  }

  openStorageLocationSearchHelp(value) {
    if (this.checkForReservationSyncValue(value)) {
      if (this.lineItem.storage) {
        if (this.lineItem.storage.length > 1) {
          this.modalController.create(
            {
              component: LineItemPlantSearchPage,
              componentProps: { data: { data: this.lineItem.storage, selector: 'storageFromLineItem' } }
            }
          ).then(
            modal => {
              modal.present();
              modal.onDidDismiss().then((data: any) => {
                data = data.data.data;
                if (data !== undefined) {
                  this.lineItem.LGORT = data.storageLocation;
                  this.lineItem.STORAGE_DES = data.storageLocationDescription;
                }
              })
            }
          )
        } else {
          if (this.lineItem.storage.length > 0) this.global.displayToastMessage('Only one Storage Location is assigned for this Material');
          else this.global.displayToastMessage('No Storage Location is assigned for this Material');
        }
      } else {
        this.global.displayToastMessage('Select Material First to get Storage Location Details');
      }
    }
  }

  getBatchDetails() {
    this.service.getBatchData(this.service.plantBasedMaterialValue, this.lineItem.MATNR).subscribe((response: any) => {
      if (response.length > 0) {
        this.lineItem.batch = response;
        this.lineItem['CHARG'] = this.lineItem.batch[0].batchNumber;
        this.isBatchSelected = true;
      } else {
        this.global.displayToastMessage('No Batch available for this material.');
        this.isBatchSelected = false;
      }
    }), error => this.global.displayToastMessage('Unable to fetch batch details');
  }

  openStockCheckModal() {
    let stockFetchingData = {
      materialNumber: this.lineItem.MATNR,
      materialDescription: this.lineItem.MAT_DES,
      uom: this.lineItem.MEINS,
      plant: this.service.plantBasedMaterialValue
    }
    this.modalController.create(
      {
        component: StockCheckModalPage,
        componentProps: {
          data: stockFetchingData
        }
      }
    ).then(modal => {
      modal.present();
    });
  }

  submitLineItem(lineItemForm) {
    if (this.lineItem.ERFMG > 0) {
      if (this.lineItem.BDMNG >= 0) {
        delete this.lineItem.plantDetails;
        delete this.lineItem.storage;
        delete this.lineItem.batch;
        let data;
        if (this.lineItemId >= 0) {
          data = {
            from: 'lineItemToMain',
            data: {
              id: this.lineItemId,
              lineItem: { ...this.lineItem }
            }
          }
        } else {
          data = {
            from: 'lineItemToMain',
            data: { ...this.lineItem }
          }
        }
        this.service.lineItemSubject.next(data);
        lineItemForm.reset();
        this.lineItem['batch'] = [];
        this.router.navigate(['/main/requests/gi_process']);
      } else {
        this.global.displayToastMessage('Reserve Value must be greater than or equal to zero');
      }
    } else {
      this.global.displayToastMessage('Reserve Quantity cannot be zero or less than zero');
    }
  }

  clearForm() {
    if (this.lineItem.CHARG) this.lineItem['CHARG'] = '';
    if (this.lineItem.ABLAD) this.lineItem['ABLAD'] = '';
    if (this.lineItem.ERFMG) this.lineItem['ERFMG'] = '';
    if (this.lineItem.BDMNG) this.lineItem['BDMNG'] = '';
  }

}
