import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { BehaviorSubject } from 'rxjs';
import { StorageService } from 'src/app/services/storage.service';

@Injectable(
    {
        providedIn: 'root'
    }
)
export class GoodsIssueService {

    userDetails: any;
    isReservationSync: boolean = false;

    lineItemSubject: BehaviorSubject<any>;

    plantBasedMaterialValue: any = '';

    movementTypeinHeader: any = '';

    wbsElementInHeader: any = '';

    constructor(private httpService: HttpService, private storage: StorageService) {
        this.lineItemSubject = new BehaviorSubject<any>(0);
    }

    getDynamicInstanceData(instanceName, listVariables, values, page, size) {
        let url: string;
        if (values !== '') {
            url = '/' + instanceName + '/getlistdatabyorvalues?listvar=' + listVariables.join('%2C') + '&page=' + page + '&size=' + size + '&values=' + values;
        } else {
            url = '/getdynamicinstace/' + instanceName + '?page=' + page + ' &size=' + size;
        }
        return this.httpService.call_GET(url);
    }

    getMaterialData(searchParams, materialCode) {
        let size = 100;
        let page = 0;
        // Adding materialBlock empty in body to get the Non Blocked material from material Master
        let body = {
            'materailCode': materialCode,
            'materialDescription': searchParams,
            'materialBlock': ''
        }
        if (this.plantBasedMaterialValue !== '') {
            body['plantDetails.plant'] = this.plantBasedMaterialValue;
        }
        return this.httpService.call_POST('/materailSearch?page=' + page + '&size=' + size, body);
    }

    getOrderData(searchParams, companyCode) {
        let size = 100;
        let page = 0;
        let body = {
            'orderDescription': searchParams,
            'compCode': companyCode
        }
        return this.httpService.call_POST('/orderMasterSearch?page=' + page + '&size=' + size, body);
    }

    getBatchData(plant, material) {
        let size = 100;
        let page = 0;
        let body = {
            'plantBatch': plant,
            'materialNumber': material

        }
        return this.httpService.call_POST('/plantbatchSearch?page=' + page + '&size=' + size, body);
    }

    getReservationData(searchParams) {
        let size = 100;
        let page = 0;
        let mvtType = '261'; // Sending MVT Type as client said to show Reservation data only for 261
        let body = {
            'reservationNumber': searchParams,
            'goodsRecipient': this.userDetails.login,
            'mvtType': mvtType
        }
        return this.httpService.call_POST('/reservationMasterSearch?page=' + page + '&size=' + size, body);
    }

    getStockDetails(requestData: any)  {
        return this.httpService.call_POST_with_rfc('/stockcheck', requestData);
    }

    doaCheck(header, totalAmount) {
        let body = {
            "DEPT_ID": header.DEPT_ID, "WERKS": header.WERKS, "TOTAL_AMOUNT": totalAmount
        }
        return this.httpService.call_POST('/approval-matrices/doasearch/GI_PROCESS', body);
    }

    initiateRequest(data, attachments, images) {
        if (this.prePostApprovalDecider(data).isPostApprovalPlant) {
            return this.postApprovalSubmission(data, attachments, images);
        } else {
            return this.preApprovalSubmission(data, attachments, images);
        }
    }

    shouldGoForDoa(data) {
        let decision = true;
        switch (data.header.WERKS) {
            case 'TMPC':
            case 'TMCT':
            case 'TMSA':
            case 'TMCC':
            case 'TMPH':
            case 'TMRV':
            case 'TMFS':
            case 'TMSM':
            case 'TMST':
            case 'TMBC':
                if (data.header.BWART === '221' || data.header.BWART === '222') decision = false;
                break;
            case 'NELR':
                decision = true;
                break;
            default:
                decision = true;
                break;
        }
        return decision;
    }

    prePostApprovalDecider(data) {
        let decider = {
            isPreApprovalPlant: false,
            isPostApprovalPlant: false
        }
        switch (data.header.WERKS) {
            case 'MMGD':
            case 'MKAJ':
            case 'MMSA':
                decider.isPostApprovalPlant = true;
                break;
            default:
                decider.isPreApprovalPlant = true;
                break;
        }
        return decider;
    }

    preApprovalSubmission(data, attachments, images) {
        const formData = new FormData();
        for (let key in attachments) {
            if (attachments[key]) {
                formData.append('files', attachments[key]);
            }
        }
        images.forEach(element => {
            formData.append('files', element.image, element.name);
        });
        let url;
        if (this.shouldGoForDoa(data)) {
            url = '/requests?requestData=' + encodeURIComponent(JSON.stringify(data)) + '&action=submit';
        } else {
            data.lineItems.forEach(element => {
                element.lineItems['ITEM_FLAG'] = 'S';
            });
            url = '/requests/tcl?requestData=' + encodeURIComponent(JSON.stringify(data)) + '&action=submit';
        }
        return this.httpService.call_POST(url, formData);
    }

    postApprovalSubmission(data, attachments, images) {
        const formData = new FormData();
        for (let key in attachments) {
            if (attachments[key]) {
                formData.append('files', attachments[key]);
            }
        }
        images.forEach(element => {
            formData.append('files', element.image, element.name);
        });
        data.lineItems.forEach(element => {
            element.lineItems['ITEM_FLAG'] = 'S';
        });
        let url = '?requestData=' + encodeURIComponent(JSON.stringify(data));
        return this.httpService.call_POST('/requests/requestType/' + data.docType + '/submit/rebate' + url, formData);
    }

}