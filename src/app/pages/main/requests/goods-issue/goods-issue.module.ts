import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GoodsIssuePageRoutingModule } from './goods-issue-routing.module';

import { GoodsIssuePage } from './goods-issue.page';
import { UIComponentsModule } from 'src/app/components/uicomponents.module';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GoodsIssuePageRoutingModule,
    UIComponentsModule,
    NgSelectModule
  ],
  declarations: [GoodsIssuePage]
})
export class GoodsIssuePageModule {}
