import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Capacitor, Plugins, CameraSource, CameraResultType } from '@capacitor/core';
import { IonSlides, ActionSheetController, AlertController, LoadingController, ModalController } from '@ionic/angular';
import { CollaborateModalPage } from 'src/app/pages/modals/collaborate-modal/collaborate-modal.page';
import { ValidationModalPage } from 'src/app/pages/modals/validation-modal/validation-modal.page';
import { GlobalService } from 'src/app/services/global.service';
import { StorageService } from 'src/app/services/storage.service';
import { RebateInboxWorkitemService } from './rebate-inbox-workitem.service';

@Component({
  selector: 'app-rebate-inbox-workitem',
  templateUrl: './rebate-inbox-workitem.page.html',
  styleUrls: ['./rebate-inbox-workitem.page.scss'],
})
export class RebateInboxWorkitemPage {

  @ViewChild('slides', { static: false }) slider: IonSlides;

  segment = 0;
  login: any;

  slideOpts = {
    allowTouchMove: false,
    autoHeight: true
  };

  requestData: any = {
    header: {},
    attachments: [],
    logs: []
  };

  attachments: any = {
    images: [],
    files: []
  };

  userList: any = [];

  lastActivity: any;

  currentStatus: any;
  currentStatusCssClass: any;

  validating = false;
  isValidate = false;
  decision = false;

  popupData: any = [];
  price_duplicate: any;
  selectedLineitem: any;
  rebateShiptoParty: any = [];
  refWorkitemInfo: any;
  lastCollabUserName: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public service: RebateInboxWorkitemService,
    public global: GlobalService,
    private actionSheetController: ActionSheetController,
    private alertController: AlertController,
    private loadingController: LoadingController,
    private modalController: ModalController,
    private storage: StorageService) { }

  ionViewWillEnter() {
    this.global.hideBottomTabs();
    this.route.data.subscribe(response => {
      if (response.data) {
        this.requestData = response.data;
        if (this.requestData.logs) {
          this.currentStatus = this.global.getLogTextBasedOnActivity(this.requestData.logs[this.requestData.logs.length - 1].activityText);
          this.currentStatusCssClass = this.global.getColorCssClass(this.currentStatus);
          this.lastActivity = this.requestData.logs[this.requestData.logs.length - 1].endDate;
        }
        this.getShiptoParty();
        if (this.requestData.header.REF_WORKFLOW) this.getRequestIdLogs();
        this.isValidate = false;
      }
    });
  }

  onBack() {
    this.router.navigate(['/main/inbox/' + this.service.parentRequestId]);
  }

  //get Ref. workflow logs
  getRequestIdLogs() {
    this.global.getrequestsById(this.requestData.header.REF_WORKFLOW).subscribe((success) => {
      this.refWorkitemInfo = success;
    }, (error) => {
      this.global.displayToastMessage('Error Occured while fetching the Referenced request number.')
    }
    )
  }

  //get ship to party details
  getShiptoParty() {
    if (this.requestData.header.SHIP_TO == 'No' || this.requestData.header.SHIP_TO == 'N') {
      this.rebateShiptoParty = [];
      let obj_code = this.requestData.header.Ship_To_Code ? this.requestData.header.Ship_To_Code.split('~') : this.requestData.header.KUNNR.split('~');
      let obj_name = this.requestData.header.Ship_To_Desc.split('~');

      for (let k = 0; k < obj_code.length; k++) {
        let obj = { party_code: '', party_name: '' };
        obj.party_code = obj_code[k];
        obj.party_name = obj_name[k];
        this.rebateShiptoParty.push(obj);
      }
    }
  }

  //get ship to party name
  getShipToPartyname(code) {
    if (code && code != '') {
      let partyCode = this.requestData.header.Ship_To_Code ? this.requestData.header.Ship_To_Code.split('~') : this.requestData.header.KUNNR.split('~');
      let partyName = this.requestData.header.Ship_To_Desc.split('~');
      let STPname = '';
      for (let i = 0; i < partyCode.length; i++) {
        if (code == partyCode[i]) {
          STPname = partyName[i];
          break;
        }
      }
      return STPname;
    }
  }

  getAmount(amount) {
    return (parseFloat(amount)).toFixed(2);
  }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  uploadAnAttachment() {
    this.actionSheetController.create(
      {
        header: 'Options',
        cssClass: 'attachmentsAS',
        mode: 'md',
        buttons: [
          {
            text: 'Scan',
            icon: 'camera',
            cssClass: 'scanOptionAS',
            handler: () => {
              if (!Capacitor.isPluginAvailable('Camera')) {
                this.global.displayToastMessage(
                  'Unable To Open Camera');
                return;
              }
              Plugins.Camera.getPhoto({
                quality: 60,
                source: CameraSource.Camera,
                height: 400,
                width: 300,
                correctOrientation: true,
                resultType: CameraResultType.DataUrl
              })
                .then(image => {
                  const blobImg = this.global.dataURItoBlob(image.dataUrl);
                  const img = {
                    name: 'Image-' + new Date().toISOString(),
                    image: blobImg
                  }
                  this.attachments.images.push(img);
                  this.global.displayToastMessage('Image has uploaded successfully');
                })
                .catch(error => {
                  return false;
                });
            }
          },
          {
            text: 'Upload',
            icon: 'folder',
            cssClass: 'filesOptionAS',
            handler: () => {
              document.getElementById('fileUploadInput').click();
            }
          }
        ]
      }
    ).then(actionSheet => actionSheet.present());
  }

  onSelectFile(event) {
    const formData = event.target.files;
    for (let key in formData) {
      if (key !== 'length' && key !== 'item') {
        if (formData[key].size < 25000000) {
          this.attachments.files.push(formData[key]);
          this.global.displayToastMessage('File has attached successfully');
        } else {
          this.global.displayToastMessage('File size is more than 25MB');
        }
      }
    }
    (<HTMLInputElement>document.getElementById('fileUploadInput')).value = '';
  }

  onRemoveAttachment(index, type) {
    if (type === 'file') {
      this.attachments.files.splice(index, 1);
    } else {
      this.attachments.images.splice(index, 1);
    }
  }

  onViewFile(file) {
    this.actionSheetController.create(
      {
        header: 'Options',
        cssClass: 'attachmentsAS',
        buttons: [
          {
            text: 'Preview Attachment',
            icon: 'eye',
            cssClass: 'scanOptionAS',
            handler: () => {
              this.global.downloadFile(this.requestData.requestId, file);
            }
          }
        ]
      }
    ).then(actionSheet => actionSheet.present());
  }

  checkDuplicationOfRecords() {
    this.validating = true;
    this.popupData = [];

    let fromDate: any;
    let toDate: any;
    let new_data = [];

    fromDate = new Date(this.global.getValidationDate(this.requestData.header.DATAB));
    (fromDate.setDate(fromDate.getDate() + 1));
    fromDate = fromDate.toISOString();
    toDate = new Date(this.global.getValidationDate(this.requestData.header.DATBI));
    (toDate.setDate(toDate.getDate() + 1));
    toDate = toDate.toISOString();
    if (this.requestData.header.BONEM && (this.requestData.header.MATNR || this.requestData.header.MAKTL) && this.requestData.header.DATAB && this.requestData.header.DATBI) {
      this.service.getDuplicateData('requestSearch/tcl', ["PRC_PROCESS", "REB_PROCESS", "REB_DRCT_CM", "REB_DRCT_A_NOTE", "REB_CRE_MEMO"],
        this.requestData.header.BONEM, (this.requestData.header.MATNR || this.requestData.header.MAKTL), fromDate, toDate, 0, 1000).subscribe((response: any) => {
          if (response) {
            (response).forEach((item) => {
              if (item.header.KSCHL == 'ZSD2') {
                if (item.requestId != this.requestData.requestId) new_data.push(item);
              }
            });
            let vm = new Set(new_data);
            this.popupData = Array.from(vm);
          }
          this.isValidate = true;
          this.validating = false;
          this.showAvailableDataInDateRange();
        },
          (error) => {
            this.validating = false;
            this.global.displayToastMessage('Error !! Error Occured while getting the requests.');
          }
        )
    } else {
      this.validating = false;
      this.global.displayToastMessage('Error !! Payer, Material and date range are mandatory for validation.')
    }
  }

  async showAvailableDataInDateRange() {
    if (this.popupData.length > 0) {
      const modal = await this.modalController.create({
        component: ValidationModalPage,
        backdropDismiss: false,
        componentProps: { data: this.popupData, isLineitem: this.price_duplicate, selectedItem: this.selectedLineitem }
      });
      return await modal.present();
    } else alert('INFO !! No Duplicate Data Found.')
  }

  getActivity(activityText) {
    const activity = this.global.getActivityByList(activityText, this.requestData.activities);
    if (activityText === 'Collaborate' || activityText === 'CollaborateBack') {
      if (this.isValidate && this.isValidate === true) {
        this.onCollaborate(activity);
      } else {
        this.global.displayToastMessage('Perform "Validate" activity to proceed.');
      }
    } else {
      if (this.isValidate && this.isValidate === true) {
        this.submit(activity);
      } else {
        this.global.displayToastMessage('Perform "Validate" activity to proceed.');
      }
    }
  }

  onCollaborate(activity) {
    if (activity.buttonText === 'Collaborate') {
      this.actionSheetController.create(
        {
          header: 'Collaborate with',
          cssClass: 'scanOptionAS',
          buttons: [
            {
              text: 'User',
              icon: 'person',
              cssClass: 'userIconAS',
              handler: () => {
                this.modalController.create(
                  {
                    component: CollaborateModalPage,
                    componentProps: {
                      data: this.requestData
                    }
                  }
                ).then(
                  modal => {
                    modal.present();
                    modal.onDidDismiss().then((data: any) => {
                      data = data.data;
                      if (data && data.user !== '') {
                        this.doCollaboration(activity, data);
                      }
                    });
                  }
                );
              }
            }
          ]
        }
      ).then(alert => alert.present());
    } else {
      if (activity.buttonText === 'CollaborateBack') {
        this.global.getUserDetailsById(this.requestData.collaborateUser).subscribe((res: any) => {
          this.lastCollabUserName = res.firstName + " " + res.lastName;
          this.alertController.create(
            {
              header: 'Collaborate Back',
              message: 'User: ' + this.lastCollabUserName,
              inputs: [
                {
                  type: 'text',
                  placeholder: 'Enter Comments (Optional)',
                  name: 'comment'
                }
              ],
              buttons: [
                {
                  text: 'Cancel',
                  role: 'cancel'
                },
                {
                  text: 'Collaborate',
                  handler: (data) => {
                    if (data) {
                      data['user'] = this.requestData.collaborateUser;
                      this.doCollaboration(activity, data);
                    }
                  }
                }
              ]
            }
          ).then(alert => alert.present());
        });
      } else {
        this.modalController.create(
          {
            component: CollaborateModalPage,
            componentProps: {
              data: this.requestData
            }
          }
        ).then(
          modal => {
            modal.present();
            modal.onDidDismiss().then((data: any) => {
              data = data.data;
              if (data && data.user !== '') {
                this.doCollaboration(activity, data);
              }
            });
          }
        );
      }
    }
  }

  doCollaboration(activity, data) {
    this.loadingController.create(
      {
        message: 'Please wait while your request is getting collaborated...'
      }
    ).then(
      loader => {
        loader.present();

        const submitData = {
          activities: [activity],
          header: this.requestData.header,
          comment: data.comment,
          lineItems: this.requestData.lineItems,
        }
        let user = data.user;
        if (activity.description === 'CollaborateBack') user = 'null';
        this.service.submitForCollaborate(submitData, user, this.attachments.files, this.attachments.images)
          .subscribe(response => {
            loader.dismiss();
            this.alertController.create(
              {
                header: 'Message',
                backdropDismiss: false,
                message: this.global.generateSubmissionMessage(activity, (this.requestData.status === '5' ? this.lastCollabUserName : data.name)),
                buttons: [
                  {
                    text: 'Ok',
                    handler: () => {
                      loader.dismiss();
                      this.router.navigate(['/main/inbox']);
                    }
                  }
                ]
              }
            ).then(successAlert => {
              loader.dismiss();
              successAlert.present();
            });
          }, error => {
            loader.dismiss();
            this.global.displayToastMessage('Unable to collaborate request, Try after some time');
          });
      }
    )
  }

  submit(activity) {
    let inputs = [];
    if (this.requestData.requestType === 'REB_CRE_MEMO' || this.requestData.requestType === 'REB_DRCT_CM') {
      inputs = [
        {
          type: 'date',
          name: 'billDate',
          placeholder: 'Select Date',
          min: this.global.getMaxDate()
        },
        {
          type: 'text',
          name: 'comments',
          placeholder: 'Add Comments'
        }
      ]
    } else {
      inputs = [
        {
          type: 'text',
          name: 'comments',
          placeholder: 'Add Comments'
        }
      ]
    }
    this.alertController.create(
      {
        header: activity.description,
        subHeader: `Request ID: ${this.requestData.requestId} ${(this.requestData.requestType === 'REB_CRE_MEMO'
          || this.requestData.requestType === 'REB_DRCT_CM') ? '(Billing date is mandatory)' : ''}`,
        backdropDismiss: false,
        inputs: inputs,
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              // this.removeTemporarySelectionInCaseOfGoodsIssue('rejected');
            }
          },
          {
            text: 'Submit',
            handler: (data) => {
              if (this.requestData.requestType === 'REB_CRE_MEMO' || this.requestData.requestType === 'REB_DRCT_CM') {
                if (data.billDate && data.billDate != "") {
                  this.requestData.header.BILL_DATE = this.global.getSAPDate(data.billDate);
                  this.decision = true;
                } else {
                  this.decision = false;
                  this.global.displayToastMessage('Billing Date is mandatory.');
                }
              } else this.decision = true;

              if (this.decision) {
                const submitData = {
                  activities: [activity],
                  header: this.requestData.header,
                  comment: data.comments,
                  lineItems: this.requestData.lineItems || [],
                }
                this.loadingController.create(
                  {
                    message: 'Please wait while its getting initiated for ' + activity.buttonText
                  }
                ).then(loader => {
                  loader.present();
                  this.service.submitWorkItem(submitData, this.attachments.files, this.attachments.images).subscribe(response => {
                    let successMessage = '';
                    if (this.requestData.currentstep === '2' && this.requestData.sapDocId !== null) {
                      successMessage = 'Request submitted for Rebate Process.';
                    } else {
                      successMessage = this.global.generateSubmissionMessage(activity, (this.requestData.step === '5' ? this.requestData.collaborateUser : data.user));
                    }
                    this.alertController.create(
                      {
                        header: 'Message',
                        subHeader: 'Request ID: ' + this.requestData.requestId,
                        backdropDismiss: false,
                        message: successMessage,
                        buttons: [
                          {
                            text: 'Ok',
                            handler: () => {
                              loader.dismiss();
                              this.router.navigate(['/main/inbox']);
                            }
                          }
                        ]
                      }
                    ).then(successAlert => {
                      loader.dismiss();
                      successAlert.present();
                    });
                  }, error => {
                    loader.dismiss();
                    this.global.displayToastMessage('Some problem occured while initiating for ' + activity.description);
                    this.router.navigate(['/main/inbox']);
                  });
                });
              }
            }
          }
        ]
      }
    ).then(alert => alert.present());
  }
}
