import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RebateInboxWorkitemPageRoutingModule } from './rebate-inbox-workitem-routing.module';

import { RebateInboxWorkitemPage } from './rebate-inbox-workitem.page';
import { MatExpansionModule } from '@angular/material';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatExpansionModule,
    NgSelectModule,
    RebateInboxWorkitemPageRoutingModule
  ],
  declarations: [RebateInboxWorkitemPage]
})
export class RebateInboxWorkitemPageModule {}
