import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RebateInboxWorkitemPage } from './rebate-inbox-workitem.page';

const routes: Routes = [
  {
    path: '',
    component: RebateInboxWorkitemPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RebateInboxWorkitemPageRoutingModule {}
