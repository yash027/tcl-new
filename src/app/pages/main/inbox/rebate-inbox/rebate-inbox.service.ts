import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { GlobalService } from 'src/app/services/global.service';
import { ActivatedRouteSnapshot } from '@angular/router';

@Injectable({ providedIn: "root" })
export class RebateInboxService {

    requestType: any;

    constructor(private http: HttpService, private global: GlobalService) { }

    resolve(route: ActivatedRouteSnapshot) {
        if (route.routeConfig.path) {
            this.requestType = route.routeConfig.path;
            return this.getListData();
        } else {
            this.global.gotoLogin();
        }
    }

    getListData() {
        const url = '/requests/inbox/requests/' + this.requestType + '/list';
        return this.http.call_GET(url);
    }
}