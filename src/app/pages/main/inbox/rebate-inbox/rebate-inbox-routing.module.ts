import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/guards/auth.guard';
import { RebateInboxWorkitemService } from './rebate-inbox-workitem/rebate-inbox-workitem.service';

import { RebateInboxPage } from './rebate-inbox.page';

const routes: Routes = [
  {
    path: '',
    component: RebateInboxPage
  },
  {
    path: ':requestId',
    loadChildren: () => import('./rebate-inbox-workitem/rebate-inbox-workitem.module').then(m => m.RebateInboxWorkitemPageModule),
    resolve: { data: RebateInboxWorkitemService },
    canActivate: [AuthGuardService]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RebateInboxPageRoutingModule { }
