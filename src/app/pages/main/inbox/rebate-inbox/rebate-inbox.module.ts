import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RebateInboxPageRoutingModule } from './rebate-inbox-routing.module';

import { RebateInboxPage } from './rebate-inbox.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RebateInboxPageRoutingModule
  ],
  declarations: [RebateInboxPage]
})
export class RebateInboxPageModule {}
