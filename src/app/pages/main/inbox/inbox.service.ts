import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Injectable({providedIn: 'root'})
export class InboxService {

    constructor(private http: HttpService) { }

    resolve() {
        return this.getInboxRequests();
    }

    getInboxRequests() {
        const url = '/requests/inbox';
        return this.http.call_GET(url);
    }
}