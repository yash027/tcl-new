import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CapexBudgetInboxPageRoutingModule } from './capex-budget-inbox-routing.module';

import { CapexBudgetInboxPage } from './capex-budget-inbox.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CapexBudgetInboxPageRoutingModule
  ],
  declarations: [CapexBudgetInboxPage]
})
export class CapexBudgetInboxPageModule {}
