import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { HttpService } from 'src/app/services/http.service';

@Injectable({providedIn: 'root'})
export class CapexBudgetInboxService {

    requestType: any = '';

    constructor(
        private http: HttpService,
        private global: GlobalService
    ) {}

    resolve(route: ActivatedRouteSnapshot) {
        if (route.routeConfig.path) {
            this.requestType = route.routeConfig.path;
            return this.getListData();
        } else {
            this.global.gotoLogin();
        }
    }

    getListData() {
        const url = '/requests/inbox/requests/' + this.requestType + '/list?size=' + 100;
        return this.http.call_GET(url);
    }

    submitWorkItem(data) {
        const url = '/requests/inbox/requestType/' + this.requestType + '/requests/submit';
        const formData = new FormData();
        formData.append('requestData', JSON.stringify(data));
        return this.http.call_POST(url, formData);
    }

}