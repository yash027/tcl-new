import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/guards/auth.guard';
import { CapexBudgetInboxWorkitemService } from './capex-budget-inbox-workitem/capex-budget-inbox-workitem.service';

import { CapexBudgetInboxPage } from './capex-budget-inbox.page';

const routes: Routes = [
  {
    path: '',
    component: CapexBudgetInboxPage
  },
  {
    path: ':requestId',
    loadChildren: () => import('./capex-budget-inbox-workitem/capex-budget-inbox-workitem.module').then( m => m.CapexBudgetInboxWorkitemPageModule),
    resolve: { data: CapexBudgetInboxWorkitemService },
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CapexBudgetInboxPageRoutingModule {}
