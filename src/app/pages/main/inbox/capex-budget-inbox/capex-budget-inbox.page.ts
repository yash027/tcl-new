import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { GlobalService } from 'src/app/services/global.service';
import { CapexBudgetInboxService } from './capex-budget-inbox.service';

@Component({
  selector: 'app-capex-budget-inbox',
  templateUrl: './capex-budget-inbox.page.html',
  styleUrls: ['./capex-budget-inbox.page.scss'],
})
export class CapexBudgetInboxPage {

  requestData: any = {};
  selectedWorkItems: any = [];

  isAllSelected: boolean = false;
  selectAllClicked: boolean = false;
  selectOneClicked: boolean = false;

  bulkApprovalActivities = [{
    id: "5d1c602b9c73c510e0aae5b7",
    actvity: "510",
    description: "Approved",
    buttonText: "Approve",
    type: "SAP",
    url: "/approve",
    commentsRequired: true
  }, {
    id: "5d1c60469c73c510e0aae5b8",
    actvity: "511",
    description: "Rejected",
    buttonText: "Reject",
    type: "SAP",
    url: "/reject",
    commentsRequired: true
  }]

  constructor(public service: CapexBudgetInboxService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public global: GlobalService,
    private alertController: AlertController,
    private loadingController: LoadingController,
  ) { }

  ionViewWillEnter() {
    this.activatedRoute.data.subscribe(data => {
      if(data.data.content.length > 0) this.requestData = data.data.content[0];
      this.requestData.RequestList.forEach(element => {
        element['isSelected'] = false;
      });
    });
  }

  convertToLocalDate(date) {
    return new Date(date).toLocaleDateString();
  }

  onSelectWorkItem(request) {
    if (!this.selectAllClicked) {
      this.selectOneClicked = true;
      request.isSelected = !request.isSelected;
      if (request.isSelected) {
        this.selectedWorkItems.push(request);
      } else {
        this.selectedWorkItems = this.selectedWorkItems.filter(element => element.requestId !== request.requestId);
      }
      if (this.checkForAllSelected()) this.isAllSelected = true;
      else this.isAllSelected = false;
      setTimeout(() => this.selectOneClicked = false, 1000);
    }
  }

  checkForAllSelected() {
    let decision = true;
    for (let i = 0; i < this.requestData.RequestList.length; i++) {
      if (this.requestData.RequestList[i].isSelected === false) {
        decision = false;
        break;
      }
    }
    if (decision) this.isAllSelected = true;
    return decision;
  }

  onSelectAll(event) {
    if (!this.selectOneClicked) {
      this.selectAllClicked = true;
      if (event.detail.checked) {
        this.requestData.RequestList.forEach(element => {
          element.isSelected = true;
        });
        this.isAllSelected = true;
        this.addOrRemoveWorkItemsFromArray();
      } else {
        this.requestData.RequestList.forEach(element => {
          element.isSelected = false;
        });
        this.isAllSelected = false;
        this.selectedWorkItems = [];
      }
      setTimeout(() => this.selectAllClicked = false, 1000);
    }
  }

  addOrRemoveWorkItemsFromArray() {
    this.requestData.RequestList.forEach(element => {
      if (element.isSelected) {
        if (!this.checkIfWorkItemIsPresent(element)) this.selectedWorkItems.push(element);
      }
    });
  }

  checkIfWorkItemIsPresent(request) {
    let decision = false;
    let workitem = this.selectedWorkItems.find(element => element.requestId === request.requestId);
    if (workitem) decision = true;
    return decision;
  }

  ionViewDidLeave() {
    this.selectedWorkItems = [];
  }

  checkIfAnyCollaborateWorkItemExist() {
    let decision = false;
    let workitem = this.selectedWorkItems.find(element => element.status === '5');
    if (workitem) {
      decision = true;
    } else {
      decision = workitem.requestId
    };
    return decision;
  }

  submit(activity) {
    if (this.selectedWorkItems.length > 0) {
      this.alertController.create(
        {
          header: 'Confirmation',
          message: 'Are you sure you want to submit the selected workitem(s)?',
          inputs: [
            {
              type: 'text',
              name: 'comments',
              placeholder: 'Comments'
            }
          ],
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel'
            },
            {
              text: 'Ok',
              handler: (inputData) => {
                this.loadingController.create(
                  {
                    message: 'Please wait...'
                  }
                ).then(loader => {
                  loader.present();
                  let data = {
                    requestDetailsList: []
                  }
                  this.selectedWorkItems.forEach(element => {
                    let obj = {
                      activities: [(activity === 'Approve' ? this.bulkApprovalActivities[0] : this.bulkApprovalActivities[1])],
                      header: element.header,
                      lineItems: element.lineItems,
                      requestId: element.requestId,
                      comment: inputData.comments
                    }
                    data.requestDetailsList.push(obj);
                  });
                  this.service.submitWorkItem(data).subscribe(response => {
                    loader.dismiss();
                    this.alertController.create(
                      {
                        header: 'Message',
                        message: this.global.generateSubmissionMessage({ buttonText: activity }),
                        backdropDismiss: false,
                        buttons: [
                          {
                            text: 'Ok',
                            handler: () => {
                              data.requestDetailsList.forEach(element => {
                                this.requestData.RequestList = this.requestData.RequestList.filter(subElement => subElement.requestId !== element.requestId);
                              });
                              this.selectedWorkItems = [];
                              this.global.displayToastMessage('List Refreshed');
                            }
                          }
                        ]
                      }
                    ).then(alert => alert.present());
                  }, error => {
                    loader.dismiss();
                    this.global.displayToastMessage('Some problem occurred while submitting request, Please try after some time.');
                  })
                });
              }
            }
          ]
        }
      ).then(alert => {
        alert.present();
      });
    } else {
      this.global.displayToastMessage('Atleast one workitem needs to be selected for ' + activity);
    }
  }

}
