import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Capacitor, Plugins, CameraSource, CameraResultType } from '@capacitor/core';
import { ActionSheetController, AlertController, IonSlides, LoadingController, ModalController } from '@ionic/angular';
import { CollaborateModalPage } from 'src/app/pages/modals/collaborate-modal/collaborate-modal.page';
import { GlobalService } from 'src/app/services/global.service';
import { CapexBudgetInboxWorkitemService } from './capex-budget-inbox-workitem.service';

@Component({
  selector: 'app-capex-budget-inbox-workitem',
  templateUrl: './capex-budget-inbox-workitem.page.html',
  styleUrls: ['./capex-budget-inbox-workitem.page.scss'],
})
export class CapexBudgetInboxWorkitemPage implements OnInit {

  @ViewChild('slides', { static: false }) slider: IonSlides;

  segment = 0;
  login: any;

  slideOpts = {
    allowTouchMove: false,
    autoHeight: true
  };

  requestData: any = {
    header: {},
    attachments: [],
    logs: []
  };

  attachments: any = {
    images: [],
    files: []
  };
  currentStatus: any = '';
  currentStatusCssClass: any = '';

  constructor(
    public global: GlobalService,
    private route: ActivatedRoute,
    private router: Router,
    private alertController: AlertController,
    private loadingController: LoadingController,
    private modalController: ModalController,
    private actionSheetController: ActionSheetController,
    public service: CapexBudgetInboxWorkitemService
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.global.hideBottomTabs();
    this.route.data.subscribe(data => {
      if (data.data) {
        this.requestData = data.data;
      }
    });
    this.currentStatus = this.global.getLogTextBasedOnActivity(this.requestData.logs[this.requestData.logs.length - 1].activityText);
    this.currentStatusCssClass = this.global.getColorCssClass(this.currentStatus);
  }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  uploadAnAttachment() {
    this.actionSheetController.create(
      {
        header: 'Options',
        cssClass: 'attachmentsAS',
        mode: 'md',
        buttons: [
          {
            text: 'Scan',
            icon: 'camera',
            cssClass: 'scanOptionAS',
            handler: () => {
              if (!Capacitor.isPluginAvailable('Camera')) {
                this.global.displayToastMessage(
                  'Unable To Open Camera');
                return;
              }
              Plugins.Camera.getPhoto({
                quality: 60,
                source: CameraSource.Camera,
                height: 400,
                width: 300,
                correctOrientation: true,
                resultType: CameraResultType.DataUrl
              })
                .then(image => {
                  const blobImg = this.global.dataURItoBlob(image.dataUrl);
                  const img = {
                    name: 'Image-' + new Date().toISOString(),
                    image: blobImg
                  }
                  this.attachments.images.push(img);
                  this.global.displayToastMessage('Image has uploaded successfully');
                })
                .catch(error => {
                  return false;
                });
            }
          },
          {
            text: 'Upload',
            icon: 'folder',
            cssClass: 'filesOptionAS',
            handler: () => {
              document.getElementById('fileUploadInput').click();
            }
          }
        ]
      }
    ).then(actionSheet => actionSheet.present());
  }

  onSelectFile(event) {
    const formData = event.target.files;
    for (let key in formData) {
      if (key !== 'length' && key !== 'item') {
        if (formData[key].size < 25000000) {
          this.attachments.files.push(formData[key]);
          this.global.displayToastMessage('File has attached successfully');
        } else {
          this.global.displayToastMessage('File size is more than 25MB');
        }
      }
    }
    (<HTMLInputElement>document.getElementById('fileUploadInput')).value = '';
  }

  onRemoveAttachment(index, type) {
    if (type === 'file') {
      this.attachments.files.splice(index, 1);
    } else {
      this.attachments.images.splice(index, 1);
    }
  }

  onViewFile(file) {
    this.actionSheetController.create(
      {
        header: 'Options',
        cssClass: 'attachmentsAS',
        buttons: [
          {
            text: 'Preview Attachment',
            icon: 'eye',
            cssClass: 'scanOptionAS',
            handler: () => {
              this.global.downloadFile(this.requestData.requestId, file);
            }
          }
        ]
      }
    ).then(actionSheet => actionSheet.present());
  }

  onCollaborate(activityText) {
    const activity = this.global.getActivityByList(activityText, this.requestData.activities);
    if (this.requestData.status === '5') {
      this.alertController.create(
        {
          header: this.global.getLogTextBasedOnActivity(activity.description),
          backdropDismiss: false,
          inputs: [
            {
              type: 'text',
              name: 'comments',
              placeholder: 'Add Comments*'
            }
          ],
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel'
            },
            {
              text: 'Submit',
              handler: (data) => {
                if (data.comments) {
                  const submitData = {
                    activities: [activity],
                    header: this.requestData.header,
                    comment: data.comments,
                    lineItems: this.requestData.lineItems,
                  }
                  this.loadingController.create(
                    {
                      message: 'Please wait while its getting initiated for ' + activity.description
                    }
                  ).then(loader => {
                    loader.present();
                    this.service.submitForCollaborate(submitData, null, this.attachments.files, this.attachments.images)
                      .subscribe(response => {
                        loader.dismiss();
                        this.alertController.create(
                          {
                            header: 'Message',
                            subHeader: 'Request ID: ' + this.requestData.requestId,
                            backdropDismiss: false,
                            message: this.global.generateSubmissionMessage(activity, (this.requestData.status === '5' ? this.requestData.collaborateUser : data.user)),
                            buttons: [
                              {
                                text: 'Ok',
                                handler: () => {
                                  loader.dismiss();
                                  this.router.navigate(['/main/inbox']);
                                }
                              }
                            ]
                          }
                        ).then(successAlert => {
                          loader.dismiss();
                          successAlert.present();
                        });
                      }, error => {
                        loader.dismiss();
                        this.global.displayToastMessage('Unable to collaborate request, Try after some time');
                      });
                  });
                }
              }
            }
          ]
        }).then(alert => {
          alert.present();
        })
    } else {
      this.modalController.create(
        {
          component: CollaborateModalPage,
          componentProps: {
            data: this.requestData
          }
        }
      ).then(
        modal => {
          modal.present();
          modal.onDidDismiss().then((data: any) => {
            data = data.data;
            if (data && data.user !== '' && data.comment !== '') {
              this.loadingController.create(
                {
                  message: 'Please wait while its getting initiated for ' + activity.buttonText
                }
              ).then(
                loader => {
                  loader.present();
                  const submitData = {
                    activities: [activity],
                    header: this.requestData.header,
                    comment: data.comment,
                    lineItems: this.requestData.lineItems,
                  }
                  this.service.submitForCollaborate(submitData, data.user, this.attachments.files, this.attachments.images)
                    .subscribe(response => {
                      loader.dismiss();
                      this.alertController.create(
                        {
                          header: 'Message',
                          subHeader: 'Request ID: ' + this.requestData.requestId,
                          backdropDismiss: false,
                          message: this.global.generateSubmissionMessage(activity, (this.requestData.status === '5' ? this.requestData.collaborateUser : data.user)),
                          buttons: [
                            {
                              text: 'Ok',
                              handler: () => {
                                loader.dismiss();
                                this.router.navigate(['/main/inbox']);
                              }
                            }
                          ]
                        }
                      ).then(successAlert => {
                        loader.dismiss();
                        successAlert.present();
                      });
                    }, error => {
                      loader.dismiss();
                      this.global.displayToastMessage('Unable to collaborate request, Try after some time');
                    });
                }
              )
            }
          });
        }
      )
    }
  }

  submit(activity) {
    activity = this.global.getActivityByList(activity, this.requestData.activities);
    this.alertController.create(
      {
        header: activity.description,
        subHeader: 'Request ID: ' + this.requestData.requestId,
        backdropDismiss: false,
        inputs: [
          {
            type: 'text',
            name: 'comments',
            placeholder: 'Add Comments'
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel'
          },
          {
            text: 'Submit',
            handler: (data) => {
              const submitData = {
                activities: [activity],
                header: this.requestData.header,
                comment: data.comments,
                lineItems: this.requestData.lineItems,
              }
              this.loadingController.create(
                {
                  message: 'Please wait while its getting initiated for ' + activity.buttonText
                }
              ).then(loader => {
                loader.present();
                this.service.submitWorkItem(submitData, this.attachments.files, this.attachments.images).subscribe(response => {
                  this.alertController.create(
                    {
                      header: 'Message',
                      subHeader: 'Request ID: ' + this.requestData.requestId,
                      backdropDismiss: false,
                      message: this.global.generateSubmissionMessage(activity),
                      buttons: [
                        {
                          text: 'Ok',
                          handler: () => {
                            loader.dismiss();
                            this.router.navigate(['/main/inbox']);
                          }
                        }
                      ]
                    }
                  ).then(successAlert => {
                    loader.dismiss();
                    successAlert.present();
                  });
                }, error => {
                  loader.dismiss();
                  this.global.displayToastMessage('Some problem occured while initiating for ' + activity.description);
                  this.router.navigate(['/main/inbox']);
                });
              });
            }
          }
        ]
      }
    ).then(alert => alert.present());
  }

}
