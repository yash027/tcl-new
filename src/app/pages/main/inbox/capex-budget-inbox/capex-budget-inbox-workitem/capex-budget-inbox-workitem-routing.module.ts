import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CapexBudgetInboxWorkitemPage } from './capex-budget-inbox-workitem.page';

const routes: Routes = [
  {
    path: '',
    component: CapexBudgetInboxWorkitemPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CapexBudgetInboxWorkitemPageRoutingModule {}
