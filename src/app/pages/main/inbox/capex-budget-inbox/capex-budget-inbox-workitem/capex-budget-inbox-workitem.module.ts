import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CapexBudgetInboxWorkitemPageRoutingModule } from './capex-budget-inbox-workitem-routing.module';

import { CapexBudgetInboxWorkitemPage } from './capex-budget-inbox-workitem.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CapexBudgetInboxWorkitemPageRoutingModule
  ],
  declarations: [CapexBudgetInboxWorkitemPage]
})
export class CapexBudgetInboxWorkitemPageModule {}
