import { Component, ViewChild } from '@angular/core';
import { GoodsIssueInboxWorkitemService } from './goods-issue-inbox-workitem.service';
import { ActivatedRoute, ChildActivationStart, Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { ActionSheetController, AlertController, LoadingController, ModalController, IonSlides } from '@ionic/angular';
import { Capacitor, Plugins, CameraSource, CameraResultType } from '@capacitor/core';
import { StorageService } from 'src/app/services/storage.service';
import { CollaborateGoodsIssuePage } from 'src/app/pages/modals/collaborate-goods-issue/collaborate-goods-issue.page';
import { StockCheckModalPage } from 'src/app/pages/modals/stock-check-modal/stock-check-modal.page';
import { GoodsIssueService } from '../../../requests/goods-issue/goods-issue.service';
import { LineItemPlantSearchPage } from 'src/app/pages/modals/line-item-plant-search/line-item-plant-search.page';
import { SearchHelpPage } from 'src/app/pages/modals/search-help/search-help.page';

@Component({
  selector: 'app-goods-issue-inbox-workitem',
  templateUrl: './goods-issue-inbox-workitem.page.html',
  styleUrls: ['./goods-issue-inbox-workitem.page.scss'],
})
export class GoodsIssueInboxWorkitemPage {

  @ViewChild('slides', { static: false }) slider: IonSlides;

  segment = 0;
  login: any;
  lineItemChangeIconDecider = false;

  slideOpts = {
    allowTouchMove: false,
    autoHeight: true
  };

  requestData: any = {
    header: {},
    attachments: [],
    logs: [],
    selectionCriteria: {}
  };

  attachments: any = {
    images: [],
    files: []
  };

  userList: any = [];

  selectedUser: any;
  selectedGiGrSlipNo: any;

  movementTypeReason: any;

  currentStatus: any;
  currentStatusCssClass: any;

  deliveredTo: any = '';

  isSapUser: any = {
    isStoreIncharge: false,
    isStoreKeeper: false,
    isFinalInitiator: false
  }

  isCommentEdited: boolean = false;

  constructor(private route: ActivatedRoute,
    private router: Router,
    public service: GoodsIssueInboxWorkitemService,
    public global: GlobalService,
    private actionSheetController: ActionSheetController,
    private alertController: AlertController,
    private loadingController: LoadingController,
    private modalController: ModalController,
    private goodsIssueInitiationService: GoodsIssueService,
    private storage: StorageService) { }

  ionViewWillEnter() {
    this.global.hideBottomTabs();
    this.route.data.subscribe(data => {
      if (data.data) {
        this.requestData = data.data;
        this.showLineItemUpdateCheckIconDecider();
        this.requestData['selectionCriteria'] = {};
        // if (this.requestData.steps && Object.keys(this.requestData.steps).length === 0) this.detectFields();
        if (this.requestData.currentstep === '1' && this.requestData.selectionCriteria.checkboxSelection) {
          this.setLineItemsForSelectionForDOAUsers();
        } else if (this.requestData.sapDocId === null) {
          this.setStringValuesToBoolean();
        }
        this.requestData['prepostApprovalDecider'] = this.global.goodsIssuePrePostApprovalDecider(this.requestData);
        this.detectSAPUsers();
        this.setIssueQtyAsReserveQty();
        this.currentStatus = this.global.getWorkflowStatusForGoodsIssue(this.requestData, this.global.userDetails);
        this.currentStatusCssClass = this.global.getColorCssClass(this.global.getWorkflowStatusForGoodsIssue(this.requestData, this.global.userDetails));
        this.setFirstOptionForReasonForMVT();
        this.resetLineItemSelection();
        this.detectFields();
      }
    });
  }

  detectFields() {
    this.requestData.selectionCriteria = this.global.goodsIssueInputFieldsDecider(this.requestData);
  }

  setIssueQtyAsReserveQty() {
    if (this.requestData.selectionCriteria.issueQtySelection && this.requestData.status !== '5') {
      this.requestData.lineItems.forEach(element => {
        element.lineItems['MENGE'] = element.lineItems.ERFMG;
      });
    }
  }

  setInitiatorCommentsAtLastInitiatorLevel() {
    if (this.isSapUser.isFinalInitiator) {
      if (!this.requestData.header.INIT_COMMENT) {
        this.requestData.header['INIT_COMMENT'] = '';
      }
    }
  }

  resetLineItemSelection() {
    if (this.requestData.currentstep == '&START' && this.requestData.selectionCriteria.checkboxSelection) {
      this.requestData.lineItems.forEach(element => {
        if (element.lineItems.isSelected && element.lineItems.ITEM_FLAG) {
          element.lineItems.isSelected = true;
          element.lineItems.ITEM_FLAG = "";
        }
      });
    }
  }

  detectSAPUsers() {
    if (this.requestData.sapDocId !== null) {
      let currentUser = this.global.userDetails;
      for (let key in this.requestData.steps) {
        if (this.requestData.steps[key].users[0].toLowerCase() === currentUser.login) {
          switch (this.requestData.steps[key].stepDescription) {
            case 'StoreIncharge':
              this.isSapUser.isStoreIncharge = true;
              this.getStoreKeepersList();
              break;
            case 'Storekeeper':
              this.isSapUser.isStoreKeeper = true;
              break;
            case 'GIInitiator':
              this.isSapUser.isFinalInitiator = true;
              this.setInitiatorCommentsAtLastInitiatorLevel();
              break;
          }
          this.requestData['sapUsersForGoodsIssue'] = {
            isStoreIncharge: this.isSapUser.isStoreIncharge,
            isStoreKeeper: this.isSapUser.isStoreKeeper,
            isFinalInitiator: this.isSapUser.isFinalInitiator
          }
          this.detectFields();
        }
      }
    }
  }

  setFirstOptionForReasonForMVT() {
    if (this.requestData.header.BWART === '201') {
      this.movementTypeReason = '0001';
    }
  }

  onCommentEdited() {
    this.isCommentEdited = true;
  }

  setStringValuesToBoolean() {
    this.requestData.lineItems.forEach(element => {
      if (element.lineItems.isSelected === 'true') element.lineItems.isSelected = true;
      else if (element.lineItems.isSelected === 'false') element.lineItems.isSelected = false;
    });
  }

  showLineItemUpdateCheckIconDecider() {
    switch (this.requestData.header.WERKS) {
      case 'MMGD':
      case 'MMSA':
      case 'MKAJ':
        this.requestData['isEdited'] = false;
        if (this.requestData.header.isLineitemEdited && this.requestData.header.isLineitemEdited == 'true') this.requestData.isEdited = true;
        else {
          for (var i = 0; i < this.requestData.lineItems.length; i++) {
            if (this.requestData.lineItems[i].lineItems.isDataEdited && this.requestData.lineItems[i].lineItems.isDataEdited == 'true') {
              this.requestData.isEdited = true;
              break;
            }
          };
        }
        if (this.requestData.isEdited && this.requestData.currentstep !== '&START' && this.requestData.isEdited) this.lineItemChangeIconDecider = true;
        break;
      default:
        this.lineItemChangeIconDecider = false;
        break;
    }
  }

  getStoreKeepersList() {
    if (this.isSapUser.isStoreIncharge) {
      this.service.getStoreKeeperData(this.requestData.header.WERKS).subscribe((response: any) => {
        this.userList = response.Content;
      }, error => {
        this.global.displayToastMessage('Unable to fetch Store Keepers list');
      });
    }
  }

  showNoStockItemAlert(lineItem) {
    let msg = 'For material ' + lineItem.lineItems.MAT_DES + ' (Quantity ' + parseInt(lineItem.lineItems.ERFMG) + ') cannot be proceeded for Goods Issue because of No Stock availiblity.';
    this.alertController.create(
      {
        header: 'Message',
        message: msg,
        buttons: [
          {
            text: 'Ok',
            role: 'ok'
          }
        ]
      }
    ).then(alert => alert.present());
  }

  showNotSelectedLineItemAlert() {
    let msg = 'This line item material is unchecked by the approver or not to be considered for reservation';
    this.alertController.create(
      {
        header: 'Message',
        message: msg,
        buttons: [
          {
            text: 'Ok',
            role: 'ok'
          }
        ]
      }
    ).then(alert => alert.present());
  }

  setLineItemsForSelectionForDOAUsers() {
    this.requestData.lineItems.forEach(element => {
      element.lineItems['isSelected'] = true;
      element.lineItems['isSelectedRecently'] = true;
    });
  }

  onSelectLineItem(index) {
    if (this.requestData.lineItems[index].lineItems.ITEM_FLAG) {
      if (this.requestData.lineItems[index].lineItems.ITEM_FLAG !== 'D') {
        this.requestData.lineItems[index].lineItems.isSelected = !this.requestData.lineItems[index].lineItems.isSelected;
        this.requestData.lineItems[index].lineItems['isSelectedRecently'] = this.requestData.lineItems[index].lineItems.isSelected;
      }
    } else {
      this.requestData.lineItems[index].lineItems.isSelected = !this.requestData.lineItems[index].lineItems.isSelected;
      this.requestData.lineItems[index].lineItems['isSelectedRecently'] = this.requestData.lineItems[index].lineItems.isSelected;
    }
  }

  removeTemporarySelectionInCaseOfGoodsIssue(decision, data?) {
    if (decision === 'approved') {
      data.lineItems.forEach(lineItem => {
        if (lineItem.lineItems.isSelectedRecently === true || lineItem.lineItems.isSelectedRecently === false) {
          delete lineItem.lineItems.isSelectedRecently;
        }
      });
    } else if (decision === 'rejected') {
      if (this.requestData.header.isWorkflowCompleted) delete this.requestData.header.isWorkflowCompleted;
      this.requestData.lineItems.forEach(lineItem => {
        if (lineItem.lineItems.isSelectedRecently === false) {
          lineItem.lineItems.ITEM_FLAG = 'S';
          lineItem.lineItems.isSelected = false;
        }
      });
    }
  }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  uploadAnAttachment() {
    this.actionSheetController.create(
      {
        header: 'Options',
        cssClass: 'attachmentsAS',
        mode: 'md',
        buttons: [
          {
            text: 'Scan',
            icon: 'camera',
            cssClass: 'scanOptionAS',
            handler: () => {
              if (!Capacitor.isPluginAvailable('Camera')) {
                this.global.displayToastMessage(
                  'Unable To Open Camera');
                return;
              }
              Plugins.Camera.getPhoto({
                quality: 60,
                source: CameraSource.Camera,
                height: 400,
                width: 300,
                correctOrientation: true,
                resultType: CameraResultType.DataUrl
              })
                .then(image => {
                  const blobImg = this.global.dataURItoBlob(image.dataUrl);
                  const img = {
                    name: 'Image-' + new Date().toISOString(),
                    image: blobImg
                  }
                  this.attachments.images.push(img);
                  this.global.displayToastMessage('Image has uploaded successfully');
                })
                .catch(error => {
                  return false;
                });
            }
          },
          {
            text: 'Upload',
            icon: 'folder',
            cssClass: 'filesOptionAS',
            handler: () => {
              document.getElementById('fileUploadInput').click();
            }
          }
        ]
      }
    ).then(actionSheet => actionSheet.present());
  }

  onSelectFile(event) {
    const formData = event.target.files;
    for (let key in formData) {
      if (key !== 'length' && key !== 'item') {
        if (formData[key].size < 25000000) {
          this.attachments.files.push(formData[key]);
          this.global.displayToastMessage('File has attached successfully');
        } else {
          this.global.displayToastMessage('File size is more than 25MB');
        }
      }
    }
    (<HTMLInputElement>document.getElementById('fileUploadInput')).value = '';
  }

  onRemoveAttachment(index, type) {
    if (type === 'file') {
      this.attachments.files.splice(index, 1);
    } else {
      this.attachments.images.splice(index, 1);
    }
  }

  onRemoveLineItem(index) {
    if (this.requestData.lineItems.length > 1) {
      this.alertController.create(
        {
          header: 'Confirmation',
          message: 'Are you sure you want to remove this line item?',
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel'
            },
            {
              text: 'Remove',
              handler: () => {
                this.requestData.lineItems.splice(index, 1);
                this.global.displayToastMessage('Line Item removed.');
              }
            }
          ]
        }
      ).then(alert => alert.present());
    } else {
      this.global.displayToastMessage('Atleast one line item is required to continue this request.');
    }
  }

  onViewFile(file) {
    this.actionSheetController.create(
      {
        header: 'Options',
        cssClass: 'attachmentsAS',
        buttons: [
          {
            text: 'Preview Attachment',
            icon: 'eye',
            cssClass: 'scanOptionAS',
            handler: () => {
              this.global.downloadFile(this.requestData.requestId, file);
            }
          }
        ]
      }
    ).then(actionSheet => actionSheet.present());
  }

  onCheckLineItem(event, lineItem) {
    if (event.detail.checked) {
      lineItem.lineItems['KZEAR'] = 'X';
    } else {
      lineItem.lineItems['KZEAR'] = '';
    }
  }

  onEditQuantity(fieldName, lineItem) {
    if (fieldName === 'Reserve Quantity') {
      this.alertController.create(
        {
          header: 'Enter New Reserve Quantity',
          inputs: [
            {
              type: 'number',
              placeholder: 'Enter Reserve Qty.',
              name: 'qty'
            }
          ],
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel'
            },
            {
              text: 'Update',
              handler: (data) => {
                if (data.qty) {
                  if (!isNaN(data.qty)) {
                    lineItem.ERFMG = Math.abs(parseFloat(data.qty));
                    lineItem.BDMNG = (parseFloat(lineItem.ERFMG) * parseFloat(lineItem.unitPrice)).toFixed(2);
                  }
                }
              }
            }
          ]
        }
      ).then(alert => alert.present());
    } else {
      let input = [];
      let message = '';
      if (fieldName === 'Remarks') {
        input = [
          {
            type: 'text',
            placeholder: 'Write Here',
            name: 'qty',
            max: 25
          }
        ];
        message = 'Enter your remarks for quantity issued physically.';
      } else {
        input = [
          {
            type: 'number',
            placeholder: 'Enter New Quantity',
            name: 'qty'
          }
        ];
        if (fieldName === 'Issue Qty') {
          if (this.isSapUser.isStoreIncharge) message = 'By Store incharge physically.';
          else if (this.isSapUser.isStoreKeeper) message = 'By Store keeper physically.';
        }
      }
      this.alertController.create(
        {
          header: fieldName,
          message: message,
          inputs: input,
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel'
            },
            {
              text: 'Update',
              handler: (data) => {
                if (data.qty) {
                  if (fieldName === 'Remarks') {
                    lineItem.QTY_ISSUE = data.qty;
                  } else {
                    if (!isNaN(data.qty)) {
                      if (fieldName === 'Stock Before') {
                        data.qty = parseFloat(data.qty);
                        if (data.qty < 0) {
                          this.global.displayToastMessage('Stock Before quantity cannot be negative');
                        } else {
                          lineItem.STOCK_BFR = data.qty;
                        }
                      } else {
                        if (parseFloat(lineItem.ERFMG) < parseFloat(data.qty)) {
                          this.global.displayToastMessage('Issue Quantity must be less than or equal to Reserve Quantity');
                        } else {
                          lineItem.MENGE = data.qty;
                        }
                      }
                    }
                  }
                }
              }
            }
          ]
        }
      ).then(alert => alert.present());
    }
  }

  onStockCheck(lineItem) {
    let stockFetchingData = {
      materialNumber: lineItem.MATNR,
      materialDescription: lineItem.MAT_DES,
      uom: lineItem.MEINS,
      plant: this.requestData.header.WERKS
    }
    this.modalController.create(
      {
        component: StockCheckModalPage,
        componentProps: {
          data: stockFetchingData
        }
      }
    ).then(modal => {
      modal.present();
    });
  }

  onEditStorageLocation(lineItem) {
    this.loadingController.create(
      {
        message: 'Please wait...'
      }
    ).then(loader => {
      loader.present();
      this.service.getMaterialData(lineItem.MATNR, this.requestData.header.WERKS).subscribe((material: any) => {
        if(material.length > 0) {
          let storageLocations = material[0].storage;
          let sortedStorageLocations = [];
          this.goodsIssueInitiationService.getDynamicInstanceData('PlantStorageLocations', ['LGORT_D', 'WERKS_D'], this.requestData.header.WERKS, 0, 100).subscribe((response: any) => {
            storageLocations.forEach(storage => {
              let element = [];
              response.Content.forEach(plantStorageValue => {
                if (plantStorageValue.LGORT_D === storage.storageLocation && plantStorageValue.WERKS_D === this.requestData.header.WERKS) {
                  element.push(plantStorageValue);
                }
              });
              if (element.length > 0) {
                sortedStorageLocations.push(storage);
              }
            });
            if (sortedStorageLocations.length > 0) {
              loader.dismiss();
              if (sortedStorageLocations.length === 1) {
                this.global.displayToastMessage('Only one storage location is assigned for selected Material');
              } else {
                this.modalController.create(
                  {
                    component: LineItemPlantSearchPage,
                    componentProps: { data: { data: sortedStorageLocations, selector: 'storageFromLineItem' } }
                  }
                ).then(
                  modal => {
                    modal.present();
                    modal.onDidDismiss().then((data: any) => {
                      data = data.data.data;
                      if (data !== undefined) {
                        lineItem.LGORT = data.storageLocation;
                        lineItem.STORAGE_DES = data.storageLocationDescription;
                      }
                    })
                  }
                )
              }
            } else {
              this.global.displayToastMessage('Storage Locations not found, please try after some time');
            }
          }, error => {
            this.global.displayToastMessage('Storage location details not found. Please try after some time');
            loader.dismiss();
          });
        } else {
          this.global.displayToastMessage('Material details not found for storage location details. Please try after some time');  
          loader.dismiss();
        }
        loader.dismiss();
      }, error => {
        this.global.displayToastMessage('Problem occurred while fetching details. Please try after some time');
        loader.dismiss();
      });
    });
  }

  onEditBatch(lineItem) {
    this.loadingController.create(
      {
        message: 'Please wait...'
      }
    ).then(loader => {
      loader.present();
      this.goodsIssueInitiationService.getBatchData(this.requestData.header.WERKS, lineItem.MATNR).subscribe((response: any) => {
        if(response.length > 0) {
          if(response.length === 1) {
            this.global.displayToastMessage('Only one batch is associated with this material.');
            loader.dismiss();
          } else {
            this.modalController.create(
              {
                component: LineItemPlantSearchPage,
                componentProps: { data: { data: response, selector: 'batch' } }
              }
            ).then(
              modal => {
                modal.present();
                modal.onDidDismiss().then((data: any) => {
                  data = data.data.data;
                  if (data !== undefined) {
                    lineItem.CHARG = data.batchNumber;
                  }
                })
              }
            )
          }
        } else {
          if(this.requestData.header.BWART === '221') {
            this.goodsIssueInitiationService.getDynamicInstanceData('Msprvalues', ['MATNR'], lineItem.MATNR, 0, 100).subscribe((response: any) => {
              if(response.Content.length > 0) {
                response = response.Content;
                let sortedBatches = [];
                response.forEach(element => {
                  if(element.POSID === this.requestData.header.WBS_ELMNT && element.PLANT === this.requestData.header.WERKS) {
                    element['materialNumber'] = element.MATNR;
                    element['batchNumber'] = element.BATCH_NO;
                    sortedBatches.push(element);
                  }
                  if (sortedBatches.length > 0) {
                    if (sortedBatches.length > 1) {
                      this.modalController.create(
                        {
                          component: LineItemPlantSearchPage,
                          componentProps: { data: { data: sortedBatches, selector: 'batch' } }
                        }
                      ).then(
                        modal => {
                          modal.present();
                          modal.onDidDismiss().then((data: any) => {
                            data = data.data.data;
                            if (data !== undefined) {
                              lineItem.CHARG = data.batchNumber;
                            }
                          })
                        }
                      )
                    } else {
                      this.global.displayToastMessage('Only one batch is associated with this material');
                      loader.dismiss();
                    }
                  } else {
                    this.global.displayToastMessage('Only one batch is associated with this material');
                    loader.dismiss();
                  }
                });
              } else {
                this.global.displayToastMessage('Only one batch associated with this material');
                loader.dismiss();
              }
            }, error => {
              this.global.displayToastMessage('Problem occurred while fetching batch data');
              loader.dismiss();
            });
          } else {
            this.global.displayToastMessage('Only one batch is associated with this material');
            loader.dismiss();
          }
        }
      }, error => {
        this.global.displayToastMessage('Problem occurred while fetching batch details. Please try after some time');
        loader.dismiss();
      })
    });
  }

  setSpecialStock(item, lineItem) {
    lineItem.SOBKZ = item.SOBKZ;
    lineItem.SOTXT = item.SOTXT;
    if(lineItem.SOBKZ !== 'K') {
      lineItem.LIFNR = '';
      lineItem.VENDOR_NAME = '';
    }
  }

  onSpecialStockEdit(lineItem) {
    this.loadingController.create(
      {
        message: 'Please wait...'
      }
    ).then(loader => {
      loader.present();
      this.goodsIssueInitiationService.getDynamicInstanceData('SpecialStockBasedOnMvtType', ['MovementType'], this.requestData.header.BWART, 0, 100).subscribe((response: any) => {
        if (response.Content.length > 0) {
          let specialStockDetailsBasedOnMovementType = response.Content[0];
          specialStockDetailsBasedOnMovementType.SpecialStock = specialStockDetailsBasedOnMovementType.SpecialStock.split(',');
          let sortedSpecialStockData = [];
          this.goodsIssueInitiationService.getDynamicInstanceData('SpecialStock', [], '', 0, 100).subscribe((specialStockDetails: any) => {
            sortedSpecialStockData = [];
            specialStockDetailsBasedOnMovementType.SpecialStock.forEach(specialStockCode => {
              let filteredResult = specialStockDetails.filter(function (element) {
                return element.SOBKZ === specialStockCode;
              });
              if (filteredResult.length === 1) sortedSpecialStockData.push(filteredResult[0]);
            });
            console.log(sortedSpecialStockData);
            this.modalController.create(
              {
                component: LineItemPlantSearchPage,
                componentProps: { data: { data: sortedSpecialStockData, selector: 'specialStock' } }
              }
            ).then(modal => {
              modal.present();
              modal.onDidDismiss().then(data => {
                data = data.data;
                if (data !== undefined) {
                  this.setSpecialStock(data.data, lineItem);
                }
              });
            })
            loader.dismiss();
          }, error => {
            this.global.displayToastMessage('Problem occurred while fetching Special Stock Details, Please try after some time');
            loader.dismiss();
          });
        } else {
          this.global.displayToastMessage('Problem occurred while fetching Special Stock Details, Please try after some time');
          loader.dismiss();
        }
      }, error => {
        this.global.displayToastMessage('Problem occurred while fetching Special Stock Details, Please try after some time');
        loader.dismiss();
      });
    });
  }

  onVendorCodeEdit(lineItem) {
    this.modalController.create(
      {
        component: SearchHelpPage,
        componentProps: {
          data: 'VendorsGI'
        }
      }
    ).then(modal => {
      modal.present();
      modal.onDidDismiss().then(data => {
        data = data.data;
        if (data !== undefined) {
          let vendorDetails: any = data.data;
          lineItem['LIFNR'] = vendorDetails.LIFNR;
          lineItem['VENDOR_NAME'] = vendorDetails.VENDOR_NAME;
        }
      });
    });
  }

  getActivity(activityText) {
    const activity = this.global.getActivityByList(activityText, this.requestData.activities);
    if (activityText === 'Collaborate' || activityText === 'CollaborateBack') {
      this.onCollaborate(activity);
    } else {
      if (this.requestData.selectionCriteria.checkboxSelection && this.requestData.currentstep !== '&START') {
        let decision = false;
        this.requestData.lineItems.forEach(element => {
          if (element.lineItems.isSelected === true) {
            decision = true;
            element.lineItems['ITEM_FLAG'] = 'S';
          } else if (element.lineItems.isSelected === false) {
            element.lineItems['ITEM_FLAG'] = 'D';
          }
        });
        if (decision) {
          this.submit(activity);
        } else {
          this.global.displayToastMessage('Atleast one line item needs to be selected');
          this.removeTemporarySelectionInCaseOfGoodsIssue('rejected');
        }
      } else if (this.isSapUser.isStoreIncharge && this.requestData.status !== '5') {
        this.requestData.lineItems.forEach(element => {
          if (this.requestData.header.BWART === '201') {
            if (this.movementTypeReason) {
              element.lineItems['MOVE_REAS'] = this.movementTypeReason
            }
          }
        });
        if (this.requestData.selectionCriteria.storeKeeperSelection && this.requestData.selectionCriteria.grgiSlipSelection) {
          if (this.selectedUser) {
            if (this.selectedGiGrSlipNo) {
              this.requestData.header['STORE_KEEPER_ID'] = this.selectedUser.login;
              this.requestData.header['GR_GI_SLIP_NO'] = this.selectedGiGrSlipNo;
              this.submit(activity);
            } else {
              this.global.displayToastMessage('Please select GI/GR Slip No.');
            }
          } else {
            this.global.displayToastMessage('Please select Store Keeper ID');
          }
        } else {
          if (this.requestData.selectionCriteria.issueQtySelection && this.requestData.selectionCriteria.deliveredToSelection) {
            if (this.deliveredTo && this.deliveredTo.length > 0) {
              this.requestData.header['DELIVERED_TO'] = this.deliveredTo;
              let flag = true;
              this.requestData.lineItems.forEach((LineItem, index) => {
                if (LineItem.lineItems.STOCK_NOT_AVL !== 'X') {
                  if (parseFloat(LineItem.lineItems.MENGE) === 0) {
                    flag = false;
                  }
                }
              });
              if (flag === true) {
                this.submit(activity);
              } else {
                this.global.displayToastMessage('Issue Quantity is zero in line item.');
              }
            } else {
              this.alertController.create(
                {
                  header: 'Message',
                  message: 'Please enter "Delivered To" text to confirm the request',
                  buttons: [
                    {
                      text: 'Ok',
                      role: 'ok'
                    }
                  ]
                }
              ).then(alert => alert.present());
            }
          }
        }
      } else if (this.isSapUser.isFinalInitiator && this.requestData.status !== '5') {
        this.alertController.create(
          {
            header: 'Confirmation',
            message: 'This action cannot be undone.In case if you have not received complete qty. or facing any issues with delivery please use Collaborate option',
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel'
              },
              {
                text: 'Confirm',
                handler: () => {
                  this.requestData.header['WORKFLOW_COMPLETED'] = true;
                  this.submit(activity);
                }
              }
            ]
          }
        ).then(alert => alert.present());
      } else if (this.isSapUser.isStoreKeeper && this.requestData.status !== '5') {
        if (this.deliveredTo && this.deliveredTo.length > 0) {
          this.requestData.header['DELIVERED_TO'] = this.deliveredTo;
          let flag = true;
          this.requestData.lineItems.forEach((LineItem, index) => {
            if (LineItem.lineItems.STOCK_NOT_AVL !== 'X') {
              if (parseFloat(LineItem.lineItems.MENGE) === 0) {
                flag = false;
              }
            }
          });
          if (flag === true) {
            this.submit(activity);
          } else {
            this.global.displayToastMessage('Issue Quantity is zero in line item.');
          }
        } else {
          this.alertController.create(
            {
              header: 'Message',
              message: 'Please enter "Delivered To" text to confirm the request',
              buttons: [
                {
                  text: 'Ok',
                  role: 'ok'
                }
              ]
            }
          ).then(alert => alert.present());
        }
      } else {
        this.submit(activity);
      }
    }
  }

  onCollaborate(activity) {
    if (this.isSapUser.isFinalInitiator) {
      let secondOptionText = '';
      // Decides Collaboration in case of role change
      if (this.requestData.header.WERKS === 'MKAJ' || this.requestData.header.WERKS === 'MMSA' || this.requestData.header.WERKS === 'MMGD') {
        secondOptionText = 'Store Incharge';
      } else {
        secondOptionText = 'Store Keeper';
      }
      this.actionSheetController.create(
        {
          header: 'Collaborate with',
          cssClass: 'scanOptionAS',
          buttons: [
            {
              text: 'User',
              icon: 'person',
              cssClass: 'userIconAS',
              handler: () => {
                this.modalController.create(
                  {
                    component: CollaborateGoodsIssuePage,
                    componentProps: {
                      data: this.requestData
                    }
                  }
                ).then(
                  modal => {
                    modal.present();
                    modal.onDidDismiss().then((data: any) => {
                      data = data.data;
                      if (data && data.user !== '') {
                        this.doCollaboration(activity, data);
                      }
                    });
                  }
                );
              }
            },
            {
              text: secondOptionText,
              cssClass: 'filesOptionAS',
              icon: 'person',
              handler: () => {
                let step, user;
                const activity = this.global.getActivityByList('Collaborate', this.requestData.activities);
                for (let key in this.requestData.steps) {
                  if (secondOptionText.includes('Store Keeper')) {
                    if (this.requestData.steps[key].stepDescription === 'Storekeeper') {
                      step = this.requestData.steps[key];
                    }
                  } else {
                    if (this.requestData.steps[key].stepDescription === 'StoreIncharge') {
                      step = this.requestData.steps[key];
                    }
                  }
                }
                if (step.users[0]) {
                  user = step.users[0].toLowerCase();
                }
                this.alertController.create(
                  {
                    header: 'Collaborate with ' + secondOptionText,
                    message: 'ID: ' + user,
                    inputs: [
                      {
                        placeholder: 'Enter Comments (Optional)',
                        type: 'text',
                        name: 'comment',
                      }
                    ],
                    buttons: [
                      {
                        text: 'Cancel',
                        role: 'cancel'
                      },
                      {
                        text: 'Collaborate',
                        handler: (data) => {
                          if (data) {
                            data['user'] = user;
                            this.doCollaboration(activity, data);
                          }
                        }
                      }
                    ]
                  }
                ).then(alert => alert.present());
              }
            }
          ]
        }
      ).then(alert => alert.present());
    } else {
      if (activity.buttonText === 'CollaborateBack') {
        this.alertController.create(
          {
            header: 'Collaborate Back',
            message: 'User: ' + this.requestData.collaborateUser,
            inputs: [
              {
                type: 'text',
                placeholder: 'Enter Comments (Optional)',
                name: 'comment'
              }
            ],
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel'
              },
              {
                text: 'Collaborate',
                handler: (data) => {
                  if (data) {
                    data['user'] = this.requestData.collaborateUser;
                    this.doCollaboration(activity, data);
                  }
                }
              }
            ]
          }
        ).then(alert => alert.present());
      } else {
        this.modalController.create(
          {
            component: CollaborateGoodsIssuePage,
            componentProps: {
              data: this.requestData
            }
          }
        ).then(
          modal => {
            modal.present();
            modal.onDidDismiss().then((data: any) => {
              data = data.data;
              if (data && data.user !== '') {
                this.doCollaboration(activity, data);
              }
            });
          }
        );
      }
    }
  }

  doCollaboration(activity, data) {
    this.loadingController.create(
      {
        message: 'Please wait while your request is getting collaborated...'
      }
    ).then(
      loader => {
        loader.present();
        if (this.requestData.selectionCriteria.checkboxSelection && this.requestData.currentstep !== '&START') {
          this.requestData.lineItems.forEach(element => {
            if (element.lineItems.isSelected === true) {
              element.lineItems['ITEM_FLAG'] = 'S';
            } else if (element.lineItems.isSelected === false) {
              element.lineItems['ITEM_FLAG'] = 'D';
            }
          });
        }
        const submitData = {
          activities: [activity],
          header: this.requestData.header,
          comment: data.comment,
          lineItems: this.requestData.lineItems,
        }
        let user = data.user;
        if (activity.description === 'CollaborateBack') user = 'null';
        this.service.submitForCollaborate(submitData, user, this.attachments.files, this.attachments.images)
          .subscribe(response => {
            loader.dismiss();
            this.alertController.create(
              {
                header: 'Message',
                backdropDismiss: false,
                message: this.global.generateSubmissionMessage(activity, (this.requestData.step === '5' ? this.requestData.collaborateUser : data.user)),
                buttons: [
                  {
                    text: 'Ok',
                    handler: () => {
                      loader.dismiss();
                      this.router.navigate(['/main/inbox']);
                    }
                  }
                ]
              }
            ).then(successAlert => {
              loader.dismiss();
              successAlert.present();
            });
          }, error => {
            loader.dismiss();
            this.global.displayToastMessage('Unable to collaborate request, Try after some time');
          });
      }
    )
  }

  submit(activity) {
    this.alertController.create(
      {
        header: activity.description,
        subHeader: 'Request ID: ' + this.requestData.requestId,
        backdropDismiss: false,
        inputs: [
          {
            type: 'text',
            name: 'comments',
            placeholder: 'Add Comments',
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              this.removeTemporarySelectionInCaseOfGoodsIssue('rejected');
            }
          },
          {
            text: 'Submit',
            handler: (data) => {
              if (this.requestData.selectionCriteria.checkboxSelection && this.requestData.currentstep !== '&START') {
                this.requestData.lineItems.forEach(element => {
                  if (element.lineItems.isSelected === true) {
                    element.lineItems['ITEM_FLAG'] = 'S';
                  } else if (element.lineItems.isSelected === false) {
                    element.lineItems['ITEM_FLAG'] = 'D';
                  }
                });
              }
              const submitData = {
                activities: [activity],
                header: this.requestData.header,
                comment: data.comments,
                lineItems: this.requestData.lineItems,
              }
              this.removeTemporarySelectionInCaseOfGoodsIssue('approved', submitData);
              this.loadingController.create(
                {
                  message: 'Please wait while its getting initiated for ' + activity.buttonText
                }
              ).then(loader => {
                loader.present();
                this.service.submitWorkItem(submitData, this.attachments.files, this.attachments.images).subscribe(response => {
                  let successMessage = '';
                  if (this.isSapUser.StoreIncharge) {
                    successMessage = 'Request submitted for Goods Issue in SAP and further delivery';
                  } else {
                    successMessage = this.global.generateSubmissionMessage(activity, (this.requestData.step === '5' ? this.requestData.collaborateUser : data.user));
                  }
                  this.alertController.create(
                    {
                      header: 'Message',
                      subHeader: 'Request ID: ' + this.requestData.requestId,
                      backdropDismiss: false,
                      message: successMessage,
                      buttons: [
                        {
                          text: 'Ok',
                          handler: () => {
                            loader.dismiss();
                            this.router.navigate(['/main/inbox']);
                          }
                        }
                      ]
                    }
                  ).then(successAlert => {
                    loader.dismiss();
                    successAlert.present();
                  });
                }, error => {
                  loader.dismiss();
                  this.global.displayToastMessage('Some problem occured while initiating for ' + activity.description);
                  this.router.navigate(['/main/inbox']);
                });
              });
            }
          }
        ]
      }
    ).then(alert => alert.present());
  }

  saveRequest() {
    this.loadingController.create(
      {
        message: 'Please wait while your request is getting saved...'
      }
    ).then(loader => {
      loader.present();
      const body = {
        header: this.requestData.header,
        lineItems: this.requestData.lineItems
      }
      this.service.saveRequest(this.requestData.requestType, this.requestData.requestId, body).subscribe(response => {
        loader.dismiss();
        this.alertController.create(
          {
            header: 'Message',
            message: 'Your request has succesfully saved.',
            backdropDismiss: false,
            buttons: [
              {
                text: 'Ok',
                handler: () => {
                  // this.router.navigate(['/main/inbox']);
                  this.isCommentEdited = false;
                }
              }
            ]
          }
        ).then(alert => alert.present());
      }, error => {
        loader.dismiss();
        this.global.displayToastMessage('Unable to save the request.');
      });
    });
  }
}
