import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GoodsIssueInboxWorkitemPageRoutingModule } from './goods-issue-inbox-workitem-routing.module';

import { GoodsIssueInboxWorkitemPage } from './goods-issue-inbox-workitem.page';
import { MatExpansionModule } from '@angular/material';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatExpansionModule,
    NgSelectModule,
    GoodsIssueInboxWorkitemPageRoutingModule
  ],
  declarations: [GoodsIssueInboxWorkitemPage]
})
export class GoodsIssueInboxWorkitemPageModule {}
