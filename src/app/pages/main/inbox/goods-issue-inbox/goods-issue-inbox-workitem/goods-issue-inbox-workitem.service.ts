import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { ActivatedRouteSnapshot } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { GoodsIssueService } from '../../../requests/goods-issue/goods-issue.service';

@Injectable(
    {
        providedIn: 'root'
    }
)
export class GoodsIssueInboxWorkitemService {

    requestId: any;
    parentRequestId: any;
  
    constructor(private http: HttpService, private goodsIssueService: GoodsIssueService) { }
  
    resolve(route: ActivatedRouteSnapshot) {
      if (route.parent.routeConfig.path && route.params.requestId) {
        this.requestId = route.params.requestId;
        this.parentRequestId = route.parent.routeConfig.path;
        return this.getRequestIdDetails(route.parent.routeConfig.path, this.requestId);
      }
    }
  
    getRequestIdDetails(parent, requestId) {
      const url = '/requests/inbox/requestType/' + parent + '/requests/' + requestId;
      return this.http.call_GET(url);
    }

    getUserData() {
      return this.http.call_GET('/users?page=0&size=9000');
    }

    getStoreKeeperData(plantCode) {
      return this.goodsIssueService.getDynamicInstanceData('StoreKeepersList', ['plant'], plantCode, 0, 100);
    }

    getMaterialData(materialCode, plant) {
      let size = 100;
      let page = 0;
      // Adding materialBlock empty in body to get the Non Blocked material from material Master
      let body = {
          'materailCode': materialCode,
          'plantDetails.plant': plant,
          'materialBlock': ''
      }
      return this.http.call_POST('/materailSearch?page=' + page + '&size=' + size, body);
    }
  
    submitWorkItem(data, attachments, images) {
      const url = '/requests/inbox/requestType/' + this.parentRequestId + '/requests/' + this.requestId + '/submit';
      const formData = new FormData();
      for (let key in attachments) {
        formData.append('files', attachments[key]);
      }
      images.forEach(element => {
        formData.append('files', element.image, element.name);
      });
      formData.append('requestData', JSON.stringify(data));
      return this.http.call_POST(url, formData);
    }
  
    submitForCollaborate(request, user, attachments, images) {
      const formData = new FormData();
      for (let key in attachments) {
        formData.append('files', attachments[key]);
      }
      images.forEach(element => {
        formData.append('files', element.image, element.name);
      });
      const appUrl = '/collaborate?requestType=' + this.parentRequestId + '&requestID=' + this.requestId  + '&userName=' + user + '&requestData='+encodeURIComponent(JSON.stringify(request));
      return this.http.call_POST(appUrl, formData);
    }

    saveRequest(requestType, requestId, body) {
      const formData = new FormData();
      formData.append('requestData', JSON.stringify(body));
      return this.http.call_POST('/requests/inbox/requestType/' + requestType + '/requests/' + requestId + '/submit/draft', formData);
    }
    
}