import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GoodsIssueInboxWorkitemPage } from './goods-issue-inbox-workitem.page';

const routes: Routes = [
  {
    path: '',
    component: GoodsIssueInboxWorkitemPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GoodsIssueInboxWorkitemPageRoutingModule {}
