import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GoodsIssueInboxPage } from './goods-issue-inbox.page';
import { GoodsIssueInboxWorkitemService } from './goods-issue-inbox-workitem/goods-issue-inbox-workitem.service';
import { AuthGuardService } from 'src/app/guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: GoodsIssueInboxPage
  },
  {
    path: ':requestId',
    loadChildren: () => import('./goods-issue-inbox-workitem/goods-issue-inbox-workitem.module').then( m => m.GoodsIssueInboxWorkitemPageModule),
    resolve: { data: GoodsIssueInboxWorkitemService },
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GoodsIssueInboxPageRoutingModule {}
