import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GoodsIssueInboxPageRoutingModule } from './goods-issue-inbox-routing.module';

import { GoodsIssueInboxPage } from './goods-issue-inbox.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GoodsIssueInboxPageRoutingModule
  ],
  declarations: [GoodsIssueInboxPage]
})
export class GoodsIssueInboxPageModule {}
