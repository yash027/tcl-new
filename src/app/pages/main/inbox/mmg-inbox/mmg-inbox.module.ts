import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MmgInboxPageRoutingModule } from './mmg-inbox-routing.module';

import { MmgInboxPage } from './mmg-inbox.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MmgInboxPageRoutingModule
  ],
  declarations: [MmgInboxPage]
})
export class MmgInboxPageModule {}
