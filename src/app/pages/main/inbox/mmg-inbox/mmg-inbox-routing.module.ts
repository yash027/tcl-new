import { MmgWorkItemsService } from './mmg-workitems/mmg-workitems.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MmgInboxPage } from './mmg-inbox.page';

const routes: Routes = [
  {
    path: '',
    component: MmgInboxPage
  },
  {
    path: ':requestId',
    loadChildren: () => import('./mmg-workitems/mmg-workitems.module').then(m => m.MmgWorkitemsPageModule),
    resolve: { data: MmgWorkItemsService }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MmgInboxPageRoutingModule { }
