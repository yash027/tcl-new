import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class MmgWorkItemsService {

    requestId: any;
    parentRequestId: any;

    constructor(private http: HttpService) { }

    resolve(route: ActivatedRouteSnapshot) {
        if (route.parent.routeConfig.path && route.params.requestId) {
            this.requestId = route.params.requestId;
            this.parentRequestId = route.parent.routeConfig.path;
            return this.getRequestIdDetails(route.parent.routeConfig.path, this.requestId);
        }
    }

    fetchChangedDocumentHistory(requestId) {
        return this.http.call_GET('/changedocument/' + requestId);
    }

    getRequestIdDetails(parent, requestId) {
        const url = '/requests/inbox/requestType/' + parent + '/requests/' + requestId;
        return this.http.call_GET(url);
    }

    generateMaterialCode(url) {
        return this.http.call_POST(url);
    }

    submitWorkItem(data, attachments, images) {
        const url = '/requests/inbox/requestType/' + this.parentRequestId + '/requests/' + this.requestId + '/submit';
        const formData = new FormData();
        if (attachments.length > 0) {
            for (let key in attachments) {
                formData.append('files', attachments[key]);
            }
        }
        if (images.length > 0) {
            images.forEach(element => {
                formData.append('files', element.image, element.name);
            });
        }
        formData.append('requestData', JSON.stringify(data));
        return this.http.call_POST(url, formData);
    }

    submitForCollaborate(request, user, attachments, images) {
        const formData = new FormData();
        if (attachments.length > 0) {
            for (let key in attachments) {
                formData.append('files', attachments[key]);
            }
        }
        if (images.length > 0) {
            images.forEach(element => {
                formData.append('files', element.image, element.name);
            });
        }
        const appUrl = '/collaborate?requestType=' + this.parentRequestId + '&requestID=' + this.requestId + '&userName=' + user + '&requestData=' + encodeURIComponent(JSON.stringify(request));
        return this.http.call_POST(appUrl, formData);
    }

}