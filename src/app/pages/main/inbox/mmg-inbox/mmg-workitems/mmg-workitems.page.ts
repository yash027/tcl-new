import { ChangeDocumentHistoryPage } from './../../../../modals/change-document-history/change-document-history.page';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { ActionSheetController, AlertController, LoadingController, ModalController, IonSlides } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
import { CollaborateModalPage } from 'src/app/pages/modals/collaborate-modal/collaborate-modal.page';
import { LogsAndAttachmentsPage } from 'src/app/pages/modals/logs-and-attachments/logs-and-attachments.page';
import { Capacitor, CameraSource, CameraResultType, Plugins } from '@capacitor/core';
import { MmgWorkItemsService } from './mmg-workitems.service';

@Component({
  selector: 'app-mmg-workitems',
  templateUrl: './mmg-workitems.page.html',
  styleUrls: ['./mmg-workitems.page.scss'],
})
export class MmgWorkitemsPage implements OnInit {

  requestID: any;

  requestData: any = {
    header: {},
    logs: []
  };

  attachments: any = [[], []];

  segment = 0;
  login: any;

  slideOpts = {
    allowTouchMove: false,
    autoHeight: true
  };

  decision = false;
  isFinalLevel = false;
  showMaterial = true;
  disableGenerateButton = false;

  @ViewChild('slides', { static: false }) slider: IonSlides;

  constructor(private route: ActivatedRoute,
    private router: Router,
    public service: MmgWorkItemsService,
    public global: GlobalService,
    private actionsheet: ActionSheetController,
    private sanitizer: DomSanitizer,
    private alertController: AlertController,
    private loadingController: LoadingController,
    private modalController: ModalController,
  ) { }

  ionViewWillEnter() {
    this.global.hideBottomTabs();
    this.route.data.subscribe(data => {
      if (data.data) {
        this.requestData = data.data;
        this.checkMaterialStatus();
      }
    });
    this.checkRequestTypeForHeader();
  }

  ionViewWillLeave() {
    this.disableGenerateButton = false;
  }
  ngOnInit() {
  }

  checkForMark(data) {
    if (data == 'X') return true;
    else false;
  }

  checkMaterialStatus() {
    if (this.requestData.header.MTART == 'ROH' || this.requestData.header.MTART == 'VERP' || this.requestData.header.MTART == 'HALB'
      || this.requestData.header.MTART == 'ZHLB' || this.requestData.header.MTART == 'FERT' || this.requestData.header.MTART == 'ZFRT'
      || this.requestData.header.MTART == 'EPA') {
      switch (this.requestData.header.location) {
        case 'Mithapur':
        case 'Nellore':
        case 'Sri-Perumbdur':
        case 'Mambattu':
          this.showMaterial = false;
          break;
        default:
          this.showMaterial = true;
          break;
      }
    } else if (this.requestData.header.MTART == 'ZROH' || this.requestData.header.MTART == 'ZVRP' || this.requestData.header.MTART == 'ZRSA'
      || this.requestData.header.MTART == 'ZIBE' || this.requestData.header.MTART == 'ZNBW') {
      switch (this.requestData.header.location) {
        case 'Nellore':
          this.showMaterial = false;
          break;
        default:
          this.showMaterial = true;
          break;
      }
    } else this.showMaterial = true;
  }

  generateMaterialCode() {
    let appUrl;
    if (this.showMaterial == true) appUrl = `/getmaterialNumber/${this.requestData.header.level1}/${this.requestData.header.level2}/${this.requestData.header.level3}/${this.requestData.header.level4}`;
    else appUrl = `/getmaterialNumber/${this.requestData.header.MTART}/${this.requestData.header.location}`;
    this.service.generateMaterialCode(appUrl).subscribe((response: any) => {
      this.requestData.header.MATNR = response.data;
      if (response.data != '' && response.data != null) this.disableGenerateButton = true;
      else this.global.displayToastMessage('Unable to generate Material Code, mandatory details are not available.')
    },
      (error) => {
        this.global.displayToastMessage('Error while generating the material code. please try again later.');
      });
  }

  checkRequestTypeForHeader() {
    this.requestID = (this.requestData.requestId).toUpperCase();
  }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  onBack() {
    this.router.navigate(['/main/inbox/' + this.service.parentRequestId]);
  }

  getAttachmentUrl() {
    if (this.requestData.attachments[0].url) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(this.requestData.attachments[0].url);
    }
  }

  openActionSheet() {
    if (this.requestData.attachments.length > 0 && this.requestData.attachments[0].url !== null) {
      this.actionsheet.create(
        {
          header: 'Options',
          buttons: [
            {
              text: 'Preview',
              handler: () => {
                this.global.openDocumentInViewer(this.requestData.attachments[0].url);
              }
            },
            {
              text: 'Logs',
              handler: () => {
                this.modalController.create(
                  {
                    component: LogsAndAttachmentsPage,
                    componentProps: {
                      data: this.requestData
                    }
                  }
                ).then(modal => {
                  modal.present();
                });
              }
            }
          ]
        }
      ).then(actionSheet => actionSheet.present());
    } else {
      this.actionsheet.create(
        {
          header: 'Options',
          buttons: [
            {
              text: 'Logs',
              handler: () => {
                this.modalController.create(
                  {
                    component: LogsAndAttachmentsPage,
                    componentProps: {
                      data: this.requestData
                    }
                  }
                ).then(modal => {
                  modal.present();
                });
              }
            }
          ]
        }
      ).then(actionSheet => actionSheet.present());
    }

  }

  uploadAnAttachment() {
    this.actionsheet.create(
      {
        header: 'Options',
        cssClass: 'attachmentsAS',
        mode: 'md',
        buttons: [
          {
            text: 'Scan',
            icon: 'camera',
            cssClass: 'scanOptionAS',
            handler: () => {
              if (!Capacitor.isPluginAvailable('Camera')) {
                this.global.displayToastMessage(
                  'Unable To Open Camera');
                return;
              }
              Plugins.Camera.getPhoto({
                quality: 60,
                source: CameraSource.Camera,
                height: 400,
                width: 300,
                correctOrientation: true,
                resultType: CameraResultType.DataUrl
              })
                .then(image => {
                  const blobImg = this.global.dataURItoBlob(image.dataUrl);
                  const img = {
                    name: 'Image-' + new Date().toISOString(),
                    image: blobImg
                  }
                  this.attachments[1].push(img);
                  this.global.displayToastMessage('Image has uploaded successfully');
                })
                .catch(error => {
                  return false;
                });
            }
          },
          {
            text: 'Upload',
            icon: 'folder',
            cssClass: 'filesOptionAS',
            handler: () => {
              document.getElementById('fileUploadInput').click();
            }
          }
        ]
      }
    ).then(actionSheet => actionSheet.present());
  }

  onSelectFile(event) {
    const formData = event.target.files;
    for (let key in formData) {
      if (key !== 'length' && key !== 'item') {
        if (formData[key].size < 25000000) {
          this.attachments[0].push(formData[key]);
          console.log(this.attachments[0]);
          this.global.displayToastMessage('File has attached successfully');
        } else {
          this.global.displayToastMessage('File size is more than 25MB');
        }
      }
    }
    (<HTMLInputElement>document.getElementById('fileUploadInput')).value = '';
  }

  onRemoveAttachment(index, type) {
    if (type === 'file') this.attachments[0].splice(index, 1);
    else if (type === 'camera') this.attachments[1].splice(index, 1);
  }

  getBytesConversion(bytes) {
    const size = this.global.getBytesConversion(bytes, 2);
    return size;
  }

  downloadAttachment(file) {
    this.global.downloadFile(this.requestData.requestId, file)
  }

  onCollaborate(activityText) {
    const activity = this.global.getActivityByList(activityText, this.requestData.activities);
    if (this.requestData.status === '5') {
      this.alertController.create(
        {
          header: this.global.getLogTextBasedOnActivity(activity.description),
          backdropDismiss: false,
          inputs: [
            {
              type: 'text',
              name: 'comments',
              placeholder: 'Add Comments*'
            }
          ],
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel'
            },
            {
              text: 'Submit',
              handler: (data) => {
                if ((data.comments).trim().length > 0) {
                  const submitData = {
                    activities: [activity],
                    header: this.requestData.header,
                    comment: data.comments,
                    lineItems: this.requestData.lineItems,
                  }
                  this.loadingController.create(
                    {
                      message: 'Please wait while its getting initiated for ' + activity.description
                    }
                  ).then(loader => {
                    loader.present();
                    this.service.submitForCollaborate(submitData, null, this.attachments[0], this.attachments[1])
                      .subscribe(response => {
                        loader.dismiss();
                        this.alertController.create(
                          {
                            header: 'Message',
                            subHeader: 'Request ID: ' + this.requestData.requestId,
                            backdropDismiss: false,
                            message: this.global.generateSubmissionMessage(activity, (this.requestData.status === '5' ? this.requestData.collaborateUser : data.user)),
                            buttons: [
                              {
                                text: 'Ok',
                                handler: () => {
                                  loader.dismiss();
                                  this.router.navigate(['/main/inbox']);
                                }
                              }
                            ]
                          }
                        ).then(successAlert => {
                          loader.dismiss();
                          successAlert.present();
                        });
                      }, error => {
                        loader.dismiss();
                        this.global.displayToastMessage('Unable to collaborate request, Try after some time');
                      });
                  });
                } else this.global.displayToastMessage('Please Enter Comments.');
              }
            }
          ]
        }).then(alert => {
          alert.present();
        })
    } else {
      this.modalController.create(
        {
          component: CollaborateModalPage,
          componentProps: {
            data: this.requestData
          }
        }
      ).then(
        modal => {
          modal.present();
          modal.onDidDismiss().then((data: any) => {
            data = data.data;
            if (data && data.user !== '' && data.comment !== '') {
              this.loadingController.create(
                {
                  message: 'Please wait while its getting initiated for ' + activity.buttonText
                }
              ).then(
                loader => {
                  loader.present();
                  const submitData = {
                    activities: [activity],
                    header: this.requestData.header,
                    comment: data.comment,
                    lineItems: this.requestData.lineItems,
                  }
                  this.service.submitForCollaborate(submitData, data.user, this.attachments[0], this.attachments[1])
                    .subscribe(response => {
                      loader.dismiss();
                      this.alertController.create(
                        {
                          header: 'Message',
                          subHeader: 'Request ID: ' + this.requestData.requestId,
                          backdropDismiss: false,
                          message: this.global.generateSubmissionMessage(activity, (this.requestData.status === '5' ? this.requestData.collaborateUser : data.user)),
                          buttons: [
                            {
                              text: 'Ok',
                              handler: () => {
                                loader.dismiss();
                                this.router.navigate(['/main/inbox']);
                              }
                            }
                          ]
                        }
                      ).then(successAlert => {
                        loader.dismiss();
                        successAlert.present();
                      });
                    }, error => {
                      loader.dismiss();
                      this.global.displayToastMessage('Unable to collaborate request, Try after some time');
                    });
                }
              )
            }
          });
        }
      )
    }
  }

  submit(activity) {
    activity = this.global.getActivityByList(activity, this.requestData.activities);
    if (activity.buttonText == "Approve") {
      if (this.requestData.header.requesttype == 'NewMaterial') {
        if (this.requestData.header.MATNR && this.requestData.header.MATNR != "") {
          this.decision = true;
        } else this.decision = false;
      } else this.decision = true;
    } else this.decision = true;
    if (this.decision) {
      this.alertController.create(
        {
          header: activity.description,
          message: 'Request ID: ' + this.requestData.requestId,
          backdropDismiss: false,
          inputs: [
            {
              type: 'text',
              name: 'comments',
              placeholder: 'Add Comments'
            }
          ],
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel'
            },
            {
              text: 'Submit',
              handler: (data) => {
                // if (data.comments && (data.comments).trim().length > 0) {
                this.decision = true;
                // } else this.global.displayToastMessage('Comments are mandatory.');
                if (this.decision) {
                  const submitData = {
                    activities: [activity],
                    header: this.requestData.header,
                    comment: data.comments,
                    lineItems: this.requestData.lineItems,
                  }
                  this.loadingController.create(
                    {
                      message: 'Please wait while its getting initiated for ' + activity.buttonText
                    }
                  ).then(loader => {
                    loader.present();
                    this.service.submitWorkItem(submitData, this.attachments[0], this.attachments[1]).subscribe(response => {
                      this.alertController.create(
                        {
                          header: 'Message',
                          subHeader: 'Request ID: ' + this.requestData.requestId,
                          backdropDismiss: false,
                          message: this.global.generateSubmissionMessage(activity),
                          buttons: [
                            {
                              text: 'Ok',
                              handler: () => {
                                loader.dismiss();
                                this.router.navigate(['/main/inbox']);
                              }
                            }
                          ]
                        }
                      ).then(successAlert => {
                        loader.dismiss();
                        successAlert.present();
                      });
                    }, error => {
                      loader.dismiss();
                      this.global.displayToastMessage('Some problem occured while initiating for ' + activity.description);
                      this.router.navigate(['/main/inbox']);
                    });
                  });
                }
              }
            }
          ]
        }
      ).then(alert => alert.present());
    } else this.global.displayToastMessage('Please click on Generate Material Code to continue.');
  }

  changedDocumentHistory() {
    this.service.fetchChangedDocumentHistory(this.requestData.requestId)
      .subscribe((response: any) => {
        this.modalController.create({
          component: ChangeDocumentHistoryPage,
          componentProps: {
            data: response
          }
        }).then(modal => {
          modal.present();
        });
      });
  }
}
