import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MmgWorkitemsPageRoutingModule } from './mmg-workitems-routing.module';

import { MmgWorkitemsPage } from './mmg-workitems.page';
import { MatExpansionModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatExpansionModule,
    MmgWorkitemsPageRoutingModule
  ],
  declarations: [MmgWorkitemsPage]
})
export class MmgWorkitemsPageModule { }
