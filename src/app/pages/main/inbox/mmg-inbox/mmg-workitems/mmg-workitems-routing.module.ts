import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MmgWorkitemsPage } from './mmg-workitems.page';

const routes: Routes = [
  {
    path: '',
    component: MmgWorkitemsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MmgWorkitemsPageRoutingModule {}
