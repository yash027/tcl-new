import { MmgInboxService } from './mmg-inbox/mmg-inbox.service';
import { RebateInboxService } from './rebate-inbox/rebate-inbox.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InboxPage } from './inbox.page';
import { InboxService } from './inbox.service';
import { SorInboxService } from './sor-inbox/sor-inbox.service';
import { TimesheetInboxService } from './timesheet-inbox/timesheet-inbox.service';
import { AuthGuardService } from 'src/app/guards/auth.guard';
import { OtherScopeService } from './other-scopes/other-scopes.service';
import { GoodsIssueInboxService } from './goods-issue-inbox/goods-issue-inbox.service';
import { CapexBudgetInboxService } from './capex-budget-inbox/capex-budget-inbox.service';
import { PricingInboxService } from './pricing-inbox/pricing-inbox.service';

const routes: Routes = [
  {
    path: '',
    component: InboxPage,
    resolve: { data: InboxService },
    canActivate: [AuthGuardService],
  },
  {
    path: 'SOR_CLAIM',
    loadChildren: () => import('./sor-inbox/sor-inbox.module').then(m => m.SorInboxPageModule),
    resolve: { data: SorInboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'TIME_CLAIM',
    loadChildren: () => import('./timesheet-inbox/timesheet-inbox.module').then(m => m.TimesheetInboxPageModule),
    resolve: { data: TimesheetInboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'PO_PROCESS',
    loadChildren: () => import('./other-scopes/other-scopes.module').then(m => m.OtherScopesPageModule),
    resolve: { data: OtherScopeService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'PR_PROCESS',
    loadChildren: () => import('./other-scopes/other-scopes.module').then(m => m.OtherScopesPageModule),
    resolve: { data: OtherScopeService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'SA_PROCESS',
    loadChildren: () => import('./other-scopes/other-scopes.module').then(m => m.OtherScopesPageModule),
    resolve: { data: OtherScopeService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'CONTRACTS',
    loadChildren: () => import('./other-scopes/other-scopes.module').then(m => m.OtherScopesPageModule),
    resolve: { data: OtherScopeService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'SERVICE_ENTRY',
    loadChildren: () => import('./other-scopes/other-scopes.module').then(m => m.OtherScopesPageModule),
    resolve: { data: OtherScopeService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'GI_PROCESS',
    loadChildren: () => import('./goods-issue-inbox/goods-issue-inbox.module').then(m => m.GoodsIssueInboxPageModule),
    resolve: { data: GoodsIssueInboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'ABP',
    loadChildren: () => import('./capex-budget-inbox/capex-budget-inbox.module').then(m => m.CapexBudgetInboxPageModule),
    resolve: { data: CapexBudgetInboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'PRC_PROCESS',
    loadChildren: () => import('./pricing-inbox/pricing-inbox.module').then(m => m.PricingInboxPageModule),
    resolve: { data: PricingInboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'REB_PROCESS',
    loadChildren: () => import('./rebate-inbox/rebate-inbox.module').then(m => m.RebateInboxPageModule),
    resolve: { data: RebateInboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'REB_DRCT_A_NOTE',
    loadChildren: () => import('./rebate-inbox/rebate-inbox.module').then(m => m.RebateInboxPageModule),
    resolve: { data: RebateInboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'REB_DRCT_CM',
    loadChildren: () => import('./rebate-inbox/rebate-inbox.module').then(m => m.RebateInboxPageModule),
    resolve: { data: RebateInboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'REB_CRE_MEMO',
    loadChildren: () => import('./rebate-inbox/rebate-inbox.module').then(m => m.RebateInboxPageModule),
    resolve: { data: RebateInboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'MM_CREATE',
    loadChildren: () => import('./mmg-inbox/mmg-inbox.module').then(m => m.MmgInboxPageModule),
    resolve: { data: MmgInboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'MM_CHANGE',
    loadChildren: () => import('./mmg-inbox/mmg-inbox.module').then(m => m.MmgInboxPageModule),
    resolve: { data: MmgInboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'MM_EXTEND',
    loadChildren: () => import('./mmg-inbox/mmg-inbox.module').then(m => m.MmgInboxPageModule),
    resolve: { data: MmgInboxService },
    canActivate: [AuthGuardService]
  }



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InboxPageRoutingModule { }
