import { Component, OnInit, ViewChild } from '@angular/core';
import { OtherScopesWorkItemsService } from './other-scopes-work-items.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { ActionSheetController, AlertController, LoadingController, ModalController, IonSlides } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
import { CollaborateModalPage } from 'src/app/pages/modals/collaborate-modal/collaborate-modal.page';
import { LogsAndAttachmentsPage } from 'src/app/pages/modals/logs-and-attachments/logs-and-attachments.page';
import { Capacitor, CameraSource, CameraResultType, Plugins } from '@capacitor/core';

@Component({
  selector: 'app-other-scopes-work-items',
  templateUrl: './other-scopes-work-items.page.html',
  styleUrls: ['./other-scopes-work-items.page.scss'],
})
export class OtherScopesWorkItemsPage implements OnInit {

  requestID: any;

  requestData: any = {
    header: {},
    logs: []
  };

  attachments: any = [[], []];

  segment = 0;
  login: any;

  slideOpts = {
    allowTouchMove: false,
    autoHeight: true
  };

  decision = false;

  @ViewChild('slides', { static: false }) slider: IonSlides;

  constructor(private route: ActivatedRoute,
    private router: Router,
    public service: OtherScopesWorkItemsService,
    public global: GlobalService,
    private actionsheet: ActionSheetController,
    private sanitizer: DomSanitizer,
    private alertController: AlertController,
    private loadingController: LoadingController,
    private modalController: ModalController,
  ) { }

  ionViewWillEnter() {
    this.global.hideBottomTabs();
    this.route.data.subscribe(data => {
      if (data.data) {
        this.requestData = data.data;
      }
    });
    this.checkRequestTypeForHeader();
  }

  ngOnInit() {
  }

  checkRequestTypeForHeader() {
    switch (this.requestData.requestType) {
      case 'PO_PROCESS':
        this.requestID = `PO-${this.requestData.header.EBELN}`
        break;
      case 'PR_PROCESS':
        this.requestID = `PR-${this.requestData.header.BANFN}`
        break;
      case 'SERVICE_ENTRY':
        this.requestID = `SE-${this.requestData.header.SER_ET_NO}`
        break;
      default:
        this.requestID = (this.requestData.requestId).toUpperCase();
        break;
    }
  }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  onBack() {
    this.router.navigate(['/main/inbox/' + this.service.parentRequestId]);
  }

  getAttachmentUrl() {
    if (this.requestData.attachments[0].url) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(this.requestData.attachments[0].url);
    }
  }

  openActionSheet() {
    if (this.requestData.attachments.length > 0 && this.requestData.attachments[0].url !== null) {
      this.actionsheet.create(
        {
          header: 'Options',
          buttons: [
            {
              text: 'Preview',
              handler: () => {
                this.global.openDocumentInViewer(this.requestData.attachments[0].url);
              }
            },
            {
              text: 'Logs',
              handler: () => {
                this.modalController.create(
                  {
                    component: LogsAndAttachmentsPage,
                    componentProps: {
                      data: this.requestData
                    }
                  }
                ).then(modal => {
                  modal.present();
                });
              }
            }
          ]
        }
      ).then(actionSheet => actionSheet.present());
    } else {
      this.actionsheet.create(
        {
          header: 'Options',
          buttons: [
            {
              text: 'Logs',
              handler: () => {
                this.modalController.create(
                  {
                    component: LogsAndAttachmentsPage,
                    componentProps: {
                      data: this.requestData
                    }
                  }
                ).then(modal => {
                  modal.present();
                });
              }
            }
          ]
        }
      ).then(actionSheet => actionSheet.present());
    }

  }

  uploadAnAttachment() {
    this.actionsheet.create(
      {
        header: 'Options',
        cssClass: 'attachmentsAS',
        mode: 'md',
        buttons: [
          {
            text: 'Scan',
            icon: 'camera',
            cssClass: 'scanOptionAS',
            handler: () => {
              if (!Capacitor.isPluginAvailable('Camera')) {
                this.global.displayToastMessage(
                  'Unable To Open Camera');
                return;
              }
              Plugins.Camera.getPhoto({
                quality: 60,
                source: CameraSource.Camera,
                height: 400,
                width: 300,
                correctOrientation: true,
                resultType: CameraResultType.DataUrl
              })
                .then(image => {
                  const blobImg = this.global.dataURItoBlob(image.dataUrl);
                  const img = {
                    name: 'Image-' + new Date().toISOString(),
                    image: blobImg
                  }
                  this.attachments[1].push(img);
                  this.global.displayToastMessage('Image has uploaded successfully');
                })
                .catch(error => {
                  return false;
                });
            }
          },
          {
            text: 'Upload',
            icon: 'folder',
            cssClass: 'filesOptionAS',
            handler: () => {
              document.getElementById('fileUploadInput').click();
            }
          }
        ]
      }
    ).then(actionSheet => actionSheet.present());
  }

  onSelectFile(event) {
    const formData = event.target.files;
    for (let key in formData) {
      if (key !== 'length' && key !== 'item') {
        if (formData[key].size < 25000000) {
          this.attachments[0].push(formData[key]);
          console.log(this.attachments[0]);
          this.global.displayToastMessage('File has attached successfully');
        } else {
          this.global.displayToastMessage('File size is more than 25MB');
        }
      }
    }
    (<HTMLInputElement>document.getElementById('fileUploadInput')).value = '';
  }

  onRemoveAttachment(index, type) {
    if (type === 'file') this.attachments[0].splice(index, 1);
    else if (type === 'camera') this.attachments[1].splice(index, 1);
  }

  getBytesConversion(bytes) {
    const size = this.global.getBytesConversion(bytes, 2);
    return size;
  }

  downloadAttachment(file) {
    this.global.downloadFile(this.requestData.requestId, file)
  }

  onCollaborate(activityText) {
    const activity = this.global.getActivityByList(activityText, this.requestData.activities);
    if (this.requestData.status === '5') {
      this.alertController.create(
        {
          header: this.global.getLogTextBasedOnActivity(activity.description),
          backdropDismiss: false,
          inputs: [
            {
              type: 'text',
              name: 'comments',
              placeholder: 'Add Comments*'
            }
          ],
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel'
            },
            {
              text: 'Submit',
              handler: (data) => {
                if ((data.comments).trim().length > 0) {
                  const submitData = {
                    activities: [activity],
                    header: this.requestData.header,
                    comment: data.comments,
                    lineItems: this.requestData.lineItems,
                  }
                  this.loadingController.create(
                    {
                      message: 'Please wait while its getting initiated for ' + activity.description
                    }
                  ).then(loader => {
                    loader.present();
                    this.service.submitForCollaborate(submitData, null, this.attachments[0], this.attachments[1])
                      .subscribe(response => {
                        loader.dismiss();
                        this.alertController.create(
                          {
                            header: 'Message',
                            subHeader: 'Request ID: ' + this.requestData.requestId,
                            backdropDismiss: false,
                            message: this.global.generateSubmissionMessage(activity, (this.requestData.status === '5' ? this.requestData.collaborateUser : data.user)),
                            buttons: [
                              {
                                text: 'Ok',
                                handler: () => {
                                  loader.dismiss();
                                  this.router.navigate(['/main/inbox']);
                                }
                              }
                            ]
                          }
                        ).then(successAlert => {
                          loader.dismiss();
                          successAlert.present();
                        });
                      }, error => {
                        loader.dismiss();
                        this.global.displayToastMessage('Unable to collaborate request, Try after some time');
                      });
                  });
                } else this.global.displayToastMessage('Please Enter Comments.');
              }
            }
          ]
        }).then(alert => {
          alert.present();
        })
    } else {
      this.modalController.create(
        {
          component: CollaborateModalPage,
          componentProps: {
            data: this.requestData
          }
        }
      ).then(
        modal => {
          modal.present();
          modal.onDidDismiss().then((data: any) => {
            data = data.data;
            if (data && data.user !== '' && data.comment !== '') {
              this.loadingController.create(
                {
                  message: 'Please wait while its getting initiated for ' + activity.buttonText
                }
              ).then(
                loader => {
                  loader.present();
                  const submitData = {
                    activities: [activity],
                    header: this.requestData.header,
                    comment: data.comment,
                    lineItems: this.requestData.lineItems,
                  }
                  this.service.submitForCollaborate(submitData, data.user, this.attachments[0], this.attachments[1])
                    .subscribe(response => {
                      loader.dismiss();
                      this.alertController.create(
                        {
                          header: 'Message',
                          subHeader: 'Request ID: ' + this.requestData.requestId,
                          backdropDismiss: false,
                          message: this.global.generateSubmissionMessage(activity, (this.requestData.status === '5' ? this.requestData.collaborateUser : data.user)),
                          buttons: [
                            {
                              text: 'Ok',
                              handler: () => {
                                loader.dismiss();
                                this.router.navigate(['/main/inbox']);
                              }
                            }
                          ]
                        }
                      ).then(successAlert => {
                        loader.dismiss();
                        successAlert.present();
                      });
                    }, error => {
                      loader.dismiss();
                      this.global.displayToastMessage('Unable to collaborate request, Try after some time');
                    });
                }
              )
            }
          });
        }
      )
    }
  }

  submit(activity) {
    activity = this.global.getActivityByList(activity, this.requestData.activities);
    let inputs;
    if (this.requestData.requestType === 'SERVICE_ENTRY') {
      inputs = [
        {
          type: 'text',
          name: 'comments',
          placeholder: 'Add Comments'
        },
        {
          type: 'date',
          name: 'postingDate',
          placeholder: 'Select Date',
          max: this.global.getMaxDate(),
          value: this.global.getMaxDate()
        }
      ]
    } else {
      inputs = [
        {
          type: 'text',
          name: 'comments',
          placeholder: 'Add Comments'
        }
      ]
    }
    this.alertController.create(
      {
        header: activity.description,
        subHeader: this.requestData.requestType === 'SERVICE_ENTRY' ? '(Posting date is mandatory)' : '',
        message: 'Request ID: ' + this.requestData.requestId,
        backdropDismiss: false,
        inputs: inputs,
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel'
          },
          {
            text: 'Submit',
            handler: (data) => {
              // if (data.comments && (data.comments).trim().length > 0) {
                if (this.requestData.requestType === 'SERVICE_ENTRY') {
                  if (data.postingDate && data.postingDate != "") {
                    this.requestData.header.POSTINGDATE = this.global.getSAPDate(data.postingDate);
                    this.decision = true;
                  } else {
                    this.decision = false;
                    this.global.displayToastMessage('Posting Date is mandatory.');
                  }
                } else this.decision = true;
              // } else this.global.displayToastMessage('Comments are mandatory.');
              if (this.decision) {
                const submitData = {
                  activities: [activity],
                  header: this.requestData.header,
                  comment: data.comments,
                  lineItems: this.requestData.lineItems,
                }
                this.loadingController.create(
                  {
                    message: 'Please wait while its getting initiated for ' + activity.buttonText
                  }
                ).then(loader => {
                  loader.present();
                  this.service.submitWorkItem(submitData, this.attachments[0], this.attachments[1]).subscribe(response => {
                    this.alertController.create(
                      {
                        header: 'Message',
                        subHeader: 'Request ID: ' + this.requestData.requestId,
                        backdropDismiss: false,
                        message: this.global.generateSubmissionMessage(activity),
                        buttons: [
                          {
                            text: 'Ok',
                            handler: () => {
                              loader.dismiss();
                              this.router.navigate(['/main/inbox']);
                            }
                          }
                        ]
                      }
                    ).then(successAlert => {
                      loader.dismiss();
                      successAlert.present();
                    });
                  }, error => {
                    loader.dismiss();
                    this.global.displayToastMessage('Some problem occured while initiating for ' + activity.description);
                    this.router.navigate(['/main/inbox']);
                  });
                });
              }
            }
          }
        ]
      }
    ).then(alert => alert.present());
  }

}
