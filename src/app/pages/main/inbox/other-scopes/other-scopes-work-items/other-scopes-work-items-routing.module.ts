import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OtherScopesWorkItemsPage } from './other-scopes-work-items.page';

const routes: Routes = [
  {
    path: '',
    component: OtherScopesWorkItemsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OtherScopesWorkItemsPageRoutingModule {}
