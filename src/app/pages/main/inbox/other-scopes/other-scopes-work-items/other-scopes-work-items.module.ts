import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OtherScopesWorkItemsPageRoutingModule } from './other-scopes-work-items-routing.module';

import { OtherScopesWorkItemsPage } from './other-scopes-work-items.page';
import { MatExpansionModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatExpansionModule,
    OtherScopesWorkItemsPageRoutingModule
  ],
  declarations: [OtherScopesWorkItemsPage]
})
export class OtherScopesWorkItemsPageModule {}
