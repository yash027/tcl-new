import { Component, OnInit, ViewChild } from '@angular/core';
import { OtherScopeService } from './other-scopes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-other-scopes',
  templateUrl: './other-scopes.page.html',
  styleUrls: ['./other-scopes.page.scss'],
})
export class OtherScopesPage implements OnInit {

  @ViewChild('Searchbar', { static: false }) searchbar;

  requestData: any = {};
  selectedWorkItems: any = [];
  copyOfRequests: any = [];

  isAllSelected: boolean = false;
  selectAllClicked: boolean = false;
  selectOneClicked: boolean = false;

  searchBarLabel: any = 'Search';

  decision = false;

  bulkApprovalActivities = [{
    id: "5d1c602b9c73c510e0aae5b7",
    actvity: "510",
    description: "Approved",
    buttonText: "Approve",
    type: "SAP",
    url: "/approve",
    commentsRequired: true
  }, {
    id: "5d1c60469c73c510e0aae5b8",
    actvity: "511",
    description: "Rejected",
    buttonText: "Reject",
    type: "SAP",
    url: "/reject",
    commentsRequired: true
  }]

  constructor(
    public service: OtherScopeService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public global: GlobalService,
    private alertController: AlertController,
    private loadingController: LoadingController,
    private modalController: ModalController
  ) { }

  ionViewWillEnter() {
    this.global.hideBottomTabs();
    this.activatedRoute.data.subscribe(data => {
      if (data.data.content.length > 0) this.requestData = data.data.content[0];
      this.requestData.RequestList.forEach(element => {
        element['isSelected'] = false;
      });
      this.copyOfRequests = this.requestData.RequestList.slice();
      // this.setSearchBarLabel();
      // this.requestData.RequestList = this.sortingRequestList(this.requestData.RequestList);
    });
  }

  setSearchBarLabel() {
    switch (this.service.requestType) {
      case 'PO_PROCESS':
        this.searchBarLabel = 'Search by PO Number';
        break;
      case 'PR_PROCESS':
        this.searchBarLabel = 'Search by PR Number';
        break;
      case 'SERVICE_ENTRY':
        this.searchBarLabel = 'Search by SE Number';
        break;
      default:
        this.searchBarLabel = 'Search by Document No.';
        break;
    }
  }

  searchOnResult(searchTerm) {
    let filteredData = [];
    switch (this.service.requestType) {
      case 'PO_PROCESS':
        filteredData = this.copyOfRequests.filter(item => (item.header.EBELN).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 || (item.header.LIFNR).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 || (item.header.NAME1).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 || (item.header.GROSS_AMT).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 || (item.createdBy).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1);
        break;
      case 'PR_PROCESS':
        filteredData = this.copyOfRequests.filter(item =>
          (item.header.BANFN != undefined ? (item.header.BANFN).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 : '')
          || (item.header.EKORG != undefined ? (item.header.EKORG).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 : '')
          || (item.header.EKGRP != undefined ? (item.header.EKGRP).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 : '')
          || (item.lineItems.length != 0 ? (item.lineItems[0].lineItems.MATNR).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 : '')
          || (item.lineItems.length != 0 ? (item.lineItems[0].lineItems.MAKTX).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 : '')
          || (item.lineItems.length != 0 ? (item.lineItems[0].lineItems.RLWRT).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 : '')
          || (item.createdBy != undefined ? (item.createdBy).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 : ''));
        break;
      case 'SERVICE_ENTRY':
        filteredData = this.copyOfRequests.filter(item => (item.header.SER_ET_NO).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 || (item.header.LIFNR).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 || (item.header.NAME1).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 || (item.lineItems.length != 0 ? item.lineItems[0].lineItems.NETWR : '').toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 || (item.createdBy).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1);
        break;
      default:
        filteredData = this.copyOfRequests.filter(item => (item.requestId).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1);
        break;
    }
    this.requestData.RequestList = filteredData;
  }

  ngOnInit() {
  }

  onBack() {
    this.router.navigate(['/main/inbox']);
  }

  sortingRequestList(array) {
    let sortedArray: any;
    switch (this.service.requestType) {
      case 'PO_PROCESS':
        sortedArray = array.sort((a, b) => {
          return a.header.EBELN - b.header.EBELN;
        });
        break;
      case 'PR_PROCESS':
        sortedArray = array.sort((a, b) => {
          return a.header.BANFN - b.header.BANFN || a.lineItems[0].lineItems.ITEM_NO - b.lineItems[0].lineItems.ITEM_NO;
        });
        break;
      case 'SA_PROCESS':
      case 'CONTRACTS':
        sortedArray = array.sort((a, b) => {
          return a.header.EBELN - b.header.EBELN || a.lineItems[0].lineItems.ITEM_NO - b.lineItems[0].lineItems.ITEM_NO;
        });
        break;
      case 'SERVICE_ENTRY':
        sortedArray = array.sort((a, b) => {
          return a.header.SER_ET_NO - b.header.SER_ET_NO;
        });
        break;
      default:
        alert('Not an actual process');
    }
    return sortedArray;
  }

  onSelectWorkItem(request) {
    if (!this.selectAllClicked) {
      this.selectOneClicked = true;
      request.isSelected = !request.isSelected;
      if (request.isSelected) {
        this.selectedWorkItems.push(request);
      } else {
        this.selectedWorkItems = this.selectedWorkItems.filter(element => element.requestId !== request.requestId);
      }
      if (this.checkForAllSelected()) this.isAllSelected = true;
      else this.isAllSelected = false;
      setTimeout(() => this.selectOneClicked = false, 1000);
    }
  }

  onSubmitMultipleRequest() {
    console.log(this.selectedWorkItems);
  }

  checkForAllSelected() {
    let decision = true;
    for (let i = 0; i < this.requestData.RequestList.length; i++) {
      if (this.requestData.RequestList[i].isSelected === false) {
        decision = false;
        break;
      }
    }
    if (decision) this.isAllSelected = true;
    return decision;
  }

  onSelectAll(event) {
    if (!this.selectOneClicked) {
      this.selectAllClicked = true;
      if (event.detail.checked) {
        this.requestData.RequestList.forEach(element => {
          element.isSelected = true;
        });
        this.isAllSelected = true;
        this.addOrRemoveWorkItemsFromArray();
      } else {
        this.requestData.RequestList.forEach(element => {
          element.isSelected = false;
        });
        this.isAllSelected = false;
        this.selectedWorkItems = [];
      }
      setTimeout(() => this.selectAllClicked = false, 1000);
    }
  }

  addOrRemoveWorkItemsFromArray() {
    this.requestData.RequestList.forEach(element => {
      if (element.isSelected) {
        if (!this.checkIfWorkItemIsPresent(element)) this.selectedWorkItems.push(element);
      }
    });
  }

  checkIfWorkItemIsPresent(request) {
    let decision = false;
    let workitem = this.selectedWorkItems.find(element => element.requestId === request.requestId);
    if (workitem) decision = true;
    return decision;
  }

  ionViewDidLeave() {
    this.selectedWorkItems = [];
    this.searchOnResult('');
    this.searchbar.value = '';
  }

  checkIfAnyCollaborateWorkItemExist() {
    let decision = false;
    let workitem = this.selectedWorkItems.find(element => element.status === '5');
    if (workitem) {
      decision = true;
    } else {
      decision = workitem.requestId
    };
    return decision;
  }

  submit(activity) {
    let inputs;
    if (this.requestData.RequestList[0].requestType === 'SERVICE_ENTRY') {
      inputs = [
        {
          type: 'text',
          name: 'comments',
          placeholder: 'Add Comments'
        },
        {
          type: 'date',
          name: 'postingDate',
          max: this.global.getMaxDate(),
          value: this.global.getMaxDate()
        }
      ]
    } else {
      inputs = [
        {
          type: 'text',
          name: 'comments',
          placeholder: 'Add Comments'
        }
      ]
    }
    if (this.selectedWorkItems.length > 0) {
      this.alertController.create(
        {
          header: 'Confirmation',
          message: 'Are you sure you want to submit the selected workitem(s)?',
          subHeader: this.requestData.RequestList[0].requestType === 'SERVICE_ENTRY' ? '(Posting date is mandatory)' : '',
          inputs: inputs,
          backdropDismiss: false,
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel'
            },
            {
              text: 'Ok',
              handler: (inputData) => {
                // if (inputData.comments && (inputData.comments).trim().length > 0) {
                if (this.requestData.RequestList[0].requestType === 'SERVICE_ENTRY') {
                  if (inputData.postingDate && inputData.postingDate != "") {
                    let sapDate = this.global.getSAPDate(inputData.postingDate);
                    this.selectedWorkItems.forEach(item => {
                      item.header.POSTINGDATE = sapDate;
                    });
                    this.decision = true;
                  } else {
                    this.decision = false;
                    this.global.displayToastMessage('Posting Date is mandatory.');
                  }
                } else this.decision = true;
                // } else this.global.displayToastMessage('Comments are mandatory.');
                if (this.decision) {
                  this.loadingController.create(
                    {
                      message: 'Please wait...'
                    }
                  ).then(loader => {
                    loader.present();
                    let data = {
                      requestDetailsList: []
                    }
                    this.selectedWorkItems.forEach(element => {
                      let obj = {
                        activities: [(activity === 'Approve' ? this.bulkApprovalActivities[0] : this.bulkApprovalActivities[1])],
                        header: element.header,
                        lineItems: element.lineItems,
                        requestId: element.requestId,
                        comment: inputData.comments
                      }
                      data.requestDetailsList.push(obj);
                    });
                    this.service.submitWorkItem(data).subscribe(response => {
                      loader.dismiss();
                      this.alertController.create(
                        {
                          header: 'Message',
                          message: this.global.generateSubmissionMessage({ buttonText: activity }),
                          backdropDismiss: false,
                          buttons: [
                            {
                              text: 'Ok',
                              handler: () => {
                                data.requestDetailsList.forEach(element => {
                                  this.requestData.RequestList = this.requestData.RequestList.filter(subElement => subElement.requestId !== element.requestId);
                                });
                                this.selectedWorkItems = [];
                                this.global.displayToastMessage('List Refreshed');
                              }
                            }
                          ]
                        }
                      ).then(alert => alert.present());
                    }, error => {
                      loader.dismiss();
                      this.global.displayToastMessage('Some problem occurred while submitting request, Please try after some time.');
                    })
                  });
                }
              }
            }
          ]
        }
      ).then(alert => {
        alert.present();
      });
    } else {
      this.global.displayToastMessage('Atleast one workitem needs to be selected for ' + activity);
    }
  }

}
