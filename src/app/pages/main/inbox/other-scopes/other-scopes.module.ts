import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OtherScopesPageRoutingModule } from './other-scopes-routing.module';

import { OtherScopesPage } from './other-scopes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OtherScopesPageRoutingModule
  ],
  declarations: [OtherScopesPage]
})
export class OtherScopesPageModule {}
