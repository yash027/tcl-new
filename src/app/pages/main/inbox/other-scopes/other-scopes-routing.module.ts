import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OtherScopesPage } from './other-scopes.page';
import { OtherScopesWorkItemsService } from './other-scopes-work-items/other-scopes-work-items.service';

const routes: Routes = [
  {
    path: '',
    component: OtherScopesPage
  },
  {
    path: ':requestId',
    loadChildren: () => import('./other-scopes-work-items/other-scopes-work-items.module').then( m => m.OtherScopesWorkItemsPageModule),
    resolve: { data: OtherScopesWorkItemsService }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OtherScopesPageRoutingModule {}
