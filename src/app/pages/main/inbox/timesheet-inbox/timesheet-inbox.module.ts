import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TimesheetInboxPageRoutingModule } from './timesheet-inbox-routing.module';

import { TimesheetInboxPage } from './timesheet-inbox.page';

import { MatExpansionModule } from '@angular/material';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatExpansionModule,
    Ng2SearchPipeModule,
    TimesheetInboxPageRoutingModule
  ],
  declarations: [TimesheetInboxPage]
})
export class TimesheetInboxPageModule {}
