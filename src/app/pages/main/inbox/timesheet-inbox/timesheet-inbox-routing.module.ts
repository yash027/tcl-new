import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TimesheetInboxPage } from './timesheet-inbox.page';

const routes: Routes = [
  {
    path: '',
    component: TimesheetInboxPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TimesheetInboxPageRoutingModule {}
