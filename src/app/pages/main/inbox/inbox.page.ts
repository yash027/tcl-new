import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.page.html',
  styleUrls: ['./inbox.page.scss'],
})
export class InboxPage implements OnInit {

  requests: any = [];

  constructor(private activatedRoute: ActivatedRoute,
    public global: GlobalService,
    private router: Router) { }

  ionViewWillEnter() {
    this.global.showBottomTabs();
    this.activatedRoute.data.subscribe(data => {
      if (data.data.length > 0) {
        data = data.data;
        this.requests = data;
        this.sortRequests();
      }
    });
  }

  ionViewWillLeave() {
    this.global.hideBottomTabs();
  }

  ngOnInit() {
  }

  onSelectRequest(requestType) {
    this.router.navigate(['/main/inbox/' + requestType]);
  }

  // Showing Goods Issue Request on top of the list
  sortRequests() {
    let request, i;
    this.requests.forEach((element, index) => {
      if (element.displayName === 'Goods Issue') {
        request = element;
        i = index;
        element.displayName = 'Store Goods Issue';
        element.description = 'Store Goods Issue';
      }
    });
    if (request && i) {
      this.requests.splice(i, 1);
      this.requests.unshift(request);
    }
  }

}
