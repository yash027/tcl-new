import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SorInboxPage } from './sor-inbox.page';

const routes: Routes = [
  {
    path: '',
    component: SorInboxPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SorInboxPageRoutingModule {}
