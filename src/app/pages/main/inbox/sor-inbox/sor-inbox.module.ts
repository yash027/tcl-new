import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SorInboxPageRoutingModule } from './sor-inbox-routing.module';

import { SorInboxPage } from './sor-inbox.page';

import { MatExpansionModule } from '@angular/material';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ng2SearchPipeModule,
    MatExpansionModule,
    SorInboxPageRoutingModule
  ],
  declarations: [SorInboxPage]
})
export class SorInboxPageModule {}
