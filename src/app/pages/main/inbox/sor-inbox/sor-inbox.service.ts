import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from 'src/app/services/http.service';
import { GlobalService } from 'src/app/services/global.service';

@Injectable({
  providedIn: 'root'
})
export class SorInboxService {

  requestType: any;

  constructor(private http: HttpService, private global: GlobalService) { }

  resolve(route: ActivatedRoute) {
    if(route.routeConfig.path){
      this.requestType = route.routeConfig.path;
      return this.getRequestTypeData(route.routeConfig.path, this.setApprovalType());
    } else {
      this.global.gotoLogin(); 
    }
  }

  getRequestTypeData(requestType, approvalType) {
    let setApprovalTypeUrl;
    if (approvalType === 'undefined') {
      setApprovalTypeUrl = undefined;
    } else {
      setApprovalTypeUrl = 'isApprovalItems=' + approvalType;
    }
    const url = '/requests/inbox/requests/' + requestType + '/list?page=0&size=100&uniQueHeaderList=CONT_NAME%2CEMP_ENG_NAME%2CTG_DES&' + setApprovalTypeUrl;
    return this.http.call_GET(url);
  }

  getDocumentChangeHistory(requestId) {
    const url = '/changedocument/' + requestId;
    return this.http.call_GET(url);
  }

  setApprovalType(value?) {
    let approvalType = 'true';
    if(value){
      switch(value) {
        case 'Approved':
          approvalType = 'true';
          break;
        case 'Rejected':
          approvalType = 'false';
          break;
        case 'All':
          approvalType = 'undefined';
          break;
      }
    }
    return approvalType;
  }

  onSubmitWorkItems(workItems, attachments, images) {
    const url = '/requests/inbox/requestType/' + this.requestType + '/requests/submit';
    const form = new FormData();          
    for (let key in attachments) {
        form.append('files', attachments[key]);
    }
    images.forEach(element => {
      form.append('files', element.image, element.name);
    });
    form.append('requestData', JSON.stringify(workItems));
    return this.http.call_POST(url, form);
  }
}
