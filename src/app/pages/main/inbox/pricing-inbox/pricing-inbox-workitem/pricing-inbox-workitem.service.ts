import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { ActivatedRouteSnapshot } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';

@Injectable(
  {
    providedIn: 'root'
  }
)
export class PricingInboxWorkitemService {

  requestId: any;
  parentRequestId: any;

  constructor(private http: HttpService, private globalService: GlobalService) { }

  resolve(route: ActivatedRouteSnapshot) {
    if (route.parent.routeConfig.path && route.params.requestId) {
      this.requestId = route.params.requestId;
      this.parentRequestId = route.parent.routeConfig.path;
      return this.getRequestIdDetails(route.parent.routeConfig.path, this.requestId);
    }
  }

  getRequestIdDetails(parent, requestId) {
    const url = '/requests/inbox/requestType/' + parent + '/requests/' + requestId;
    return this.http.call_GET(url);
  }

  getUserData() {
    return this.http.call_GET('/users?page=0&size=9000');
  }

  submitWorkItem(data, attachments, images) {
    const url = '/requests/inbox/requestType/' + this.parentRequestId + '/requests/' + this.requestId + '/submit';
    const formData = new FormData();
    for (let key in attachments) {
      formData.append('files', attachments[key]);
    }
    images.forEach(element => {
      formData.append('files', element.image, element.name);
    });
    formData.append('requestData', JSON.stringify(data));
    return this.http.call_POST(url, formData);
  }

  submitForCollaborate(request, user, attachments, images) {
    const formData = new FormData();
    for (let key in attachments) {
      formData.append('files', attachments[key]);
    }
    images.forEach(element => {
      formData.append('files', element.image, element.name);
    });
    const appUrl = '/collaborate?requestType=' + this.parentRequestId + '&requestID=' + this.requestId + '&userName=' + user + '&requestData=' + encodeURIComponent(JSON.stringify(request));
    return this.http.call_POST(appUrl, formData);
  }

  // saveRequest(requestType, requestId, body) {
  //   const formData = new FormData();
  //   formData.append('requestData', JSON.stringify(body));
  //   return this.http.call_POST('/requests/inbox/requestType/' + requestType + '/requests/' + requestId + '/submit/draft', formData);
  // }
  getPricingValidationRecords(url, conditionType, keyCombination, requestType, lineitem, pageNumber, pageSize) {
    let fromDate: any;
    let toDate: any;
    fromDate = this.globalService.getValidationDate(lineitem.COND_VALID_FROM);
    fromDate = new Date(fromDate);
    (fromDate.setDate(fromDate.getDate() + 1));
    fromDate = fromDate.toISOString();
    toDate = this.globalService.getValidationDate(lineitem.COND_VALID_TO);
    toDate = new Date(toDate);
    (toDate.setDate(toDate.getDate() + 1));
    toDate = toDate.toISOString();

    let body = {
      "headerFields": {
        "KSCHL": conditionType,
        "KEY_COMB": keyCombination,
        "BONEM": lineitem.PAYER,
        "MATNR": lineitem.MATNR
      },
      "toDate": toDate,
      "fromDate": fromDate,
      "requestType": requestType
    }
    return this.http.call_POST(url + '?order=createDate?page=' + pageNumber + '&size=' + pageSize, body);
  }

  getDuplicateData(url, requestTypes, payer, material, fromDate, toDate, pageNumber, pageSize) {
    let body = {
      "headerFields": {
        "BONEM": payer,
        "MATNR": material,
      },
      "toDate": toDate,
      "fromDate": fromDate,
      "requestType": requestTypes
    }
    return this.http.call_POST('/' + url + '?order=createDate?page=' + pageNumber + '&size=' + pageSize, body);
  }
}