import { ValidationModalPage } from './../../../../modals/validation-modal/validation-modal.page';
import { Component, ViewChild } from '@angular/core';
import { PricingInboxWorkitemService } from './pricing-inbox-workitem.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { ActionSheetController, AlertController, LoadingController, ModalController, IonSlides } from '@ionic/angular';
import { Capacitor, Plugins, CameraSource, CameraResultType } from '@capacitor/core';
import { CollaborateModalPage } from 'src/app/pages/modals/collaborate-modal/collaborate-modal.page';
import { StorageService } from 'src/app/services/storage.service';
import { CostingModelPage } from 'src/app/pages/modals/costing-modal/costing-model.page';

@Component({
  selector: 'app-pricing-inbox-workitem',
  templateUrl: './pricing-inbox-workitem.page.html',
  styleUrls: ['./pricing-inbox-workitem.page.scss'],
})
export class PricingInboxWorkitemPage {

  @ViewChild('slides', { static: false }) slider: IonSlides;

  segment = 0;
  login: any;

  slideOpts = {
    allowTouchMove: false,
    autoHeight: true
  };

  requestData: any = {
    header: {},
    attachments: [],
    logs: []
  };

  attachments: any = {
    images: [],
    files: []
  };

  userList: any = [];

  lastActivity: any;

  currentStatus: any;
  currentStatusCssClass: any;

  validating: any = [];
  popupData: any = [];
  price_duplicate: any;
  selectedLineitem: any;
  lastCollabUserName: string;

  constructor(private route: ActivatedRoute,
    private router: Router,
    public service: PricingInboxWorkitemService,
    public global: GlobalService,
    private actionSheetController: ActionSheetController,
    private alertController: AlertController,
    private loadingController: LoadingController,
    private modalController: ModalController,
    private storage: StorageService) { }

  ionViewWillEnter() {
    this.global.hideBottomTabs();
    this.route.data.subscribe(response => {
      if (response.data) {
        this.requestData = response.data;
        if (this.requestData.logs) {
          this.currentStatus = this.global.getLogTextBasedOnActivity(this.requestData.logs[this.requestData.logs.length - 1].activityText);
          this.currentStatusCssClass = this.global.getColorCssClass(this.currentStatus);
          this.lastActivity = this.requestData.logs[this.requestData.logs.length - 1].endDate;
        }
      }
    });
  }

  getAmount(amount) {
    return (parseFloat(amount)).toFixed(2);
  }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  uploadAnAttachment() {
    this.actionSheetController.create(
      {
        header: 'Options',
        cssClass: 'attachmentsAS',
        mode: 'md',
        buttons: [
          {
            text: 'Scan',
            icon: 'camera',
            cssClass: 'scanOptionAS',
            handler: () => {
              if (!Capacitor.isPluginAvailable('Camera')) {
                this.global.displayToastMessage(
                  'Unable To Open Camera');
                return;
              }
              Plugins.Camera.getPhoto({
                quality: 60,
                source: CameraSource.Camera,
                height: 400,
                width: 300,
                correctOrientation: true,
                resultType: CameraResultType.DataUrl
              })
                .then(image => {
                  const blobImg = this.global.dataURItoBlob(image.dataUrl);
                  const img = {
                    name: 'Image-' + new Date().toISOString(),
                    image: blobImg
                  }
                  this.attachments.images.push(img);
                  this.global.displayToastMessage('Image has uploaded successfully');
                })
                .catch(error => {
                  return false;
                });
            }
          },
          {
            text: 'Upload',
            icon: 'folder',
            cssClass: 'filesOptionAS',
            handler: () => {
              document.getElementById('fileUploadInput').click();
            }
          }
        ]
      }
    ).then(actionSheet => actionSheet.present());
  }

  onSelectFile(event) {
    const formData = event.target.files;
    for (let key in formData) {
      if (key !== 'length' && key !== 'item') {
        if (formData[key].size < 25000000) {
          this.attachments.files.push(formData[key]);
          this.global.displayToastMessage('File has attached successfully');
        } else {
          this.global.displayToastMessage('File size is more than 25MB');
        }
      }
    }
    (<HTMLInputElement>document.getElementById('fileUploadInput')).value = '';
  }

  onRemoveAttachment(index, type) {
    if (type === 'file') {
      this.attachments.files.splice(index, 1);
    } else {
      this.attachments.images.splice(index, 1);
    }
  }

  onViewFile(file) {
    this.actionSheetController.create(
      {
        header: 'Options',
        cssClass: 'attachmentsAS',
        buttons: [
          {
            text: 'Preview Attachment',
            icon: 'eye',
            cssClass: 'scanOptionAS',
            handler: () => {
              this.global.downloadFile(this.requestData.requestId, file);
            }
          }
        ]
      }
    ).then(actionSheet => actionSheet.present());
  }

  truncateString(str, n) {
    if (str != undefined) return (str.length > n) ? str.substr(0, n - 1) + '...' : str;
    else str;
  }

  async showNotes(notes) {
    const alert = await this.alertController.create({
      header: 'Notes !!',
      message: notes,
      buttons: ['OK']
    });
    await alert.present();
  }

  async editNotes(lineitem) {
    const alert = await this.alertController.create({
      header: 'Edit Notes! (max 1500 characters)',
      inputs: [
        {
          name: 'notes',
          type: 'text',
          placeholder: 'Enter Notes here...',
          value: lineitem.Addnotes
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'Save',
          handler: (data) => {
            if (data.notes.length < 1500) lineitem.Addnotes = data.notes;
            else {
              this.global.displayToastMessage('Notes length exceeding 1500 Characters are removed.');
              lineitem.Addnotes = (data.notes).substring(0, 1500);
            }
          }
        }
      ]
    });
    await alert.present();
  }

  //Pricing Cost Evaluation modal for PR00
  async getPricingByCombination(ev, item) {
    const modal = await this.modalController.create({
      component: CostingModelPage,
      componentProps: { data: item }
    });
    return await modal.present();
  }

  // Pricing line Items Validation
  checkPricingDuplicate(index, lineitem, workitem) {
    this.price_duplicate = true;
    this.validating[index] = true;
    this.selectedLineitem = lineitem;
    let items = JSON.parse(JSON.stringify(lineitem));
    this.popupData = [];
    let all_data = [];
    if (items.lineItems.PAYER && items.lineItems.MATNR && items.lineItems.COND_VALID_FROM && items.lineItems.COND_VALID_TO) {
      this.service.getPricingValidationRecords('/requestSearch/tcl', workitem.header.KSCHL,
        workitem.header.KEY_COMB, ['PRC_PROCESS'], items.lineItems, 0, 1000).subscribe((response: any) => {
          workitem.lineItems[index].lineItems.isValidate = true;
          if (response) {
            (response).forEach(val => {
              if (val.requestId != workitem.requestId) all_data.push(val);
            });
          }

          let vm = new Set(all_data);
          this.popupData = Array.from(vm);
          this.showAvailableDataInDateRange();
          this.validating[index] = false;
        }, (error) => {
          this.validating[index] = false;
          this.global.displayToastMessage(error.description + ' , Please Try again later.');
        })
    } else {
      this.validating[index] = false;
      this.global.displayToastMessage('Payer, Material and date range is required for validation.');
    }
  }

  checkDuplicationOfRecords(index) {
    this.price_duplicate = false;
    this.validating[index] = true;
    this.popupData = [];

    let fromDate: any;
    let toDate: any;
    let new_data = [];

    fromDate = new Date(this.global.getValidationDate(this.requestData.header.DATAB));
    (fromDate.setDate(fromDate.getDate() + 1));
    fromDate = fromDate.toISOString();
    toDate = new Date(this.global.getValidationDate(this.requestData.header.DATBI));
    (toDate.setDate(toDate.getDate() + 1));
    toDate = toDate.toISOString();
    if (this.requestData.header.BONEM && this.requestData.header.MATNR && this.requestData.header.DATAB && this.requestData.header.DATBI) {
      this.service.getDuplicateData('requestSearch/tcl', ["PRC_PROCESS", "REB_PROCESS", "REB_DRCT_CM", "REB_DRCT_A_NOTE", "REB_CRE_MEMO"],
        this.requestData.header.BONEM, this.requestData.header.MATNR, fromDate, toDate, 0, 1000).subscribe((response: any) => {
          if (response) {
            (response).forEach((item) => {
              if (item.header.KSCHL == 'ZSD2') {
                if (item.requestId != this.requestData.requestId) new_data.push(item);
              }
            });
            var vm = new Set(new_data);
            this.popupData = Array.from(vm);
          }
          this.requestData.lineItems[0].lineItems.isValidate = true;
          this.validating[index] = false;
          if (this.requestData.lineItems[0].lineItems.isValidate) this.showAvailableDataInDateRange();
        },
          (error) => {
            this.validating[index] = false;
            this.global.displayToastMessage('Error !! Error Occured while getting the requests.');
          }
        )
    } else this.global.displayToastMessage('Error !! Payer, Material and date range are mandatory for validation.')
  }

  async showAvailableDataInDateRange() {
    if (this.popupData.length > 0) {
      const modal = await this.modalController.create({
        component: ValidationModalPage,
        backdropDismiss: false,
        componentProps: { data: this.popupData, isLineitem: this.price_duplicate, selectedItem: this.selectedLineitem }
      });
      return await modal.present();
    } else alert('INFO !! No Duplicate Data Found.')
  }

  getActivity(activityText) {
    const activity = this.global.getActivityByList(activityText, this.requestData.activities);
    let decision = false;
    if (this.requestData.header.KSCHL === 'YFRT' || this.requestData.header.KSCHL === 'YFRD') {
      if (activityText === 'Collaborate' || activityText === 'CollaborateBack') {
        this.onCollaborate(activity);
      } else this.submit(activity);
    } else {
      if (activityText === 'Collaborate' || activityText === 'CollaborateBack') {
        decision = this.requestData.lineItems.every(element => element.lineItems.isValidate === true);
        if (decision) {
          this.onCollaborate(activity);
        } else {
          this.global.displayToastMessage('Perform "Validate" activity in lineitems to proceed.');
        }
      } else {
        decision = this.requestData.lineItems.every(element => element.lineItems.isValidate === true);
        if (decision) {
          this.submit(activity);
        } else {
          this.global.displayToastMessage('Perform "Validate" activity in lineitems to proceed.');
        }
      }
    }
  }

  onCollaborate(activity) {
    if (activity.buttonText === 'Collaborate') {
      this.actionSheetController.create(
        {
          header: 'Collaborate with',
          cssClass: 'scanOptionAS',
          buttons: [
            {
              text: 'User',
              icon: 'person',
              cssClass: 'userIconAS',
              handler: () => {
                this.modalController.create(
                  {
                    component: CollaborateModalPage,
                    componentProps: {
                      data: this.requestData
                    }
                  }
                ).then(
                  modal => {
                    modal.present();
                    modal.onDidDismiss().then((data: any) => {
                      data = data.data;
                      if (data && data.user !== '') {
                        this.doCollaboration(activity, data);
                      }
                    });
                  }
                );
              }
            }
          ]
        }
      ).then(alert => alert.present());
    } else {
      if (activity.buttonText === 'CollaborateBack') {
        this.global.getUserDetailsById(this.requestData.collaborateUser).subscribe((res: any) => {
          this.lastCollabUserName = res.firstName + " " + res.lastName;
          this.alertController.create(
            {
              header: 'Collaborate Back',
              message: 'User: ' + this.lastCollabUserName,
              inputs: [
                {
                  type: 'text',
                  placeholder: 'Enter Comments (Optional)',
                  name: 'comment'
                }
              ],
              buttons: [
                {
                  text: 'Cancel',
                  role: 'cancel'
                },
                {
                  text: 'Collaborate',
                  handler: (data) => {
                    if (data) {
                      data['user'] = this.requestData.collaborateUser;
                      this.doCollaboration(activity, data);
                    }
                  }
                }
              ]
            }
          ).then(alert => alert.present());
        });
      } else {
        this.modalController.create(
          {
            component: CollaborateModalPage,
            componentProps: {
              data: this.requestData
            }
          }
        ).then(
          modal => {
            modal.present();
            modal.onDidDismiss().then((data: any) => {
              data = data.data;
              if (data && data.user !== '') {
                this.doCollaboration(activity, data);
              }
            });
          }
        );
      }
    }
  }

  doCollaboration(activity, data) {
    this.loadingController.create(
      {
        message: 'Please wait while your request is getting collaborated...'
      }
    ).then(
      loader => {
        loader.present();

        this.requestData.lineItems.forEach(element => {
          delete element.$$hashKey;
          delete element.lineItems.$$hashKey;
          delete element.lineItems.isValidate;
          if (element.lineItems.isPricing) delete element.lineItems.isPricing;
        });

        const submitData = {
          activities: [activity],
          header: this.requestData.header,
          comment: data.comment,
          lineItems: this.requestData.lineItems,
        }
        let user = data.user;
        if (activity.description === 'CollaborateBack') user = 'null';
        this.service.submitForCollaborate(submitData, user, this.attachments.files, this.attachments.images)
          .subscribe(response => {
            loader.dismiss();
            this.alertController.create(
              {
                header: 'Message',
                backdropDismiss: false,
                message: this.global.generateSubmissionMessage(activity, (this.requestData.status === '5' ? this.lastCollabUserName : data.name)),
                buttons: [
                  {
                    text: 'Ok',
                    handler: () => {
                      loader.dismiss();
                      this.router.navigate(['/main/inbox']);
                    }
                  }
                ]
              }
            ).then(successAlert => {
              loader.dismiss();
              successAlert.present();
            });
          }, error => {
            loader.dismiss();
            this.global.displayToastMessage('Unable to collaborate request, Try after some time');
          });
      }
    )
  }

  submit(activity) {
    this.alertController.create(
      {
        header: activity.description,
        subHeader: 'Request ID: ' + this.requestData.requestId,
        backdropDismiss: false,
        inputs: [
          {
            type: 'text',
            name: 'comments',
            placeholder: 'Add Comments',
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              // this.removeTemporarySelectionInCaseOfGoodsIssue('rejected');
            }
          },
          {
            text: 'Submit',
            handler: (data) => {
              this.requestData.lineItems.forEach(element => {
                delete element.$$hashKey;
                delete element.lineItems.$$hashKey;
                delete element.lineItems.isValidate;
                if (element.lineItems.isPricing) delete element.lineItems.isPricing;
              });

              const submitData = {
                activities: [activity],
                header: this.requestData.header,
                comment: data.comments,
                lineItems: this.requestData.lineItems,
              }
              this.loadingController.create(
                {
                  message: 'Please wait while its getting initiated for ' + activity.buttonText
                }
              ).then(loader => {
                loader.present();
                this.service.submitWorkItem(submitData, this.attachments.files, this.attachments.images).subscribe(response => {
                  let successMessage = '';
                  if (this.requestData.currentstep === '2' && this.requestData.sapDocId !== null) {
                    successMessage = 'Request submitted for Pricing Process.';
                  } else {
                    successMessage = this.global.generateSubmissionMessage(activity, (this.requestData.step === '5' ? this.requestData.collaborateUser : data.user));
                  }
                  this.alertController.create(
                    {
                      header: 'Message',
                      subHeader: 'Request ID: ' + this.requestData.requestId,
                      backdropDismiss: false,
                      message: successMessage,
                      buttons: [
                        {
                          text: 'Ok',
                          handler: () => {
                            loader.dismiss();
                            this.router.navigate(['/main/inbox']);
                          }
                        }
                      ]
                    }
                  ).then(successAlert => {
                    loader.dismiss();
                    successAlert.present();
                  });
                }, error => {
                  loader.dismiss();
                  this.global.displayToastMessage('Some problem occured while initiating for ' + activity.description);
                  this.router.navigate(['/main/inbox']);
                });
              });
            }
          }
        ]
      }
    ).then(alert => alert.present());
  }
}
