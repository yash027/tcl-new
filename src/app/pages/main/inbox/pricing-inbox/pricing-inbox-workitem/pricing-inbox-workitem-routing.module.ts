import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PricingInboxWorkitemPage } from './pricing-inbox-workitem.page';

const routes: Routes = [
  {
    path: '',
    component: PricingInboxWorkitemPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PricingInboxWorkitemPageRoutingModule {}
