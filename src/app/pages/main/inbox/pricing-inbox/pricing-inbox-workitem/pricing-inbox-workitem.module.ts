import { PricingInboxWorkitemPageRoutingModule } from './pricing-inbox-workitem-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PricingInboxWorkitemPage } from './pricing-inbox-workitem.page';
import { MatExpansionModule } from '@angular/material';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatExpansionModule,
    NgSelectModule,
    PricingInboxWorkitemPageRoutingModule
  ],
  declarations: [PricingInboxWorkitemPage]
})
export class PricingInboxWorkitemPageModule {}
