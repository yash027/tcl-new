import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PricingInboxPageRoutingModule } from './pricing-inbox-routing.module';

import { PricingInboxPage } from './pricing-inbox.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PricingInboxPageRoutingModule
  ],
  declarations: [PricingInboxPage]
})
export class PricingInboxPageModule {}
