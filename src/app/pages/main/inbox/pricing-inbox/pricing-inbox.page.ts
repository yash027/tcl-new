import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-pricing-inbox',
  templateUrl: './pricing-inbox.page.html',
  styleUrls: ['./pricing-inbox.page.scss'],
})
export class PricingInboxPage implements OnInit {

  @ViewChild('Searchbar', { static: false }) searchbar;
  
  requestData: any = [];
  copyOfRequests: any = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public global: GlobalService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.activatedRoute.data.subscribe((response) => {
      if (response.data.content.length > 0) this.requestData = response.data.content[0];
      this.copyOfRequests = this.requestData.RequestList.slice();
    },
      (error) => {
        console.log(error);
      });
  }

  searchOnResult(searchTerm) {
    let filteredData = [];
    filteredData = this.copyOfRequests.filter(item => (item.requestId).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1);
    this.requestData.RequestList = filteredData;
  }

  ionViewDidLeave() {
    this.searchOnResult('');
    this.searchbar.value = '';
  }

}
