import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/guards/auth.guard';
import { PricingInboxWorkitemService } from './pricing-inbox-workitem/pricing-inbox-workitem.service';

import { PricingInboxPage } from './pricing-inbox.page';

const routes: Routes = [
  {
    path: '',
    component: PricingInboxPage
  },
  {
    path: ':requestId',
    loadChildren: () => import('./pricing-inbox-workitem/pricing-inbox-workitem.module').then( m => m.PricingInboxWorkitemPageModule),
    resolve: { data: PricingInboxWorkitemService },
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PricingInboxPageRoutingModule {}
