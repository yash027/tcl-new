import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { ActionSheetController, IonSlides, ModalController } from '@ionic/angular';
import { LogsAndAttachmentsPage } from 'src/app/pages/modals/logs-and-attachments/logs-and-attachments.page';
import { OtherScopesWorkItemsService } from './other-scopes-work-items.service';

@Component({
  selector: 'app-other-scopes-work-items',
  templateUrl: './other-scopes-work-items.page.html',
  styleUrls: ['./other-scopes-work-items.page.scss'],
})
export class OtherScopesWorkItemsPage implements OnInit {

  requestID: any;

  requestData: any = {
    header: {},
    lineItems: [],
    logs: []
  };

  attachments: any = [[], []];

  segment = 0;
  login: any;

  slideOpts = {
    allowTouchMove: false,
    autoHeight: true
  };

  @ViewChild('slides', { static: false }) slider: IonSlides;

  constructor(private route: ActivatedRoute,
    private router: Router,
    public service: OtherScopesWorkItemsService,
    public global: GlobalService,
    private actionsheet: ActionSheetController,
    private modalController: ModalController) { }

  ionViewWillEnter() {
    this.route.data.subscribe(data => {
      if (data.data) {
        this.requestData = data.data;
      }
    });
    this.checkRequestTypeForHeader();
  }

  checkRequestTypeForHeader() {
    switch (this.requestData.requestType) {
      case 'PO_PROCESS':
        this.requestID = `PO-${this.requestData.header.EBELN}`
        break;
      case 'PR_PROCESS':
        this.requestID = `PR-${this.requestData.header.BANFN}`
        break;
      case 'SERVICE_ENTRY':
        this.requestID = `SE-${this.requestData.header.SER_ET_NO}`
        break;
      default:
        this.requestID = (this.requestData.requestId).toUpperCase();
        break;
    }
  }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  downloadAttachment(file) {
    this.global.downloadFile(this.requestData.requestId, file)
  }

  ngOnInit() {
  }

  onBack() {
    this.router.navigate(['/main/outbox/' + this.service.parentRequestId]);
  }

  openActionSheet() {
    if (this.requestData.attachments.length > 0 && this.requestData.attachments[0].url !== null) {
      this.actionsheet.create(
        {
          header: 'Options',
          buttons: [
            {
              text: 'Preview',
              handler: () => {
                this.global.openDocumentInViewer(this.requestData.attachments[0].url);
              }
            },
            {
              text: 'Logs',
              handler: () => {
                this.modalController.create(
                  {
                    component: LogsAndAttachmentsPage,
                    componentProps: {
                      data: this.requestData
                    }
                  }
                ).then(modal => {
                  modal.present();
                });
              }
            }
          ]
        }
      ).then(actionSheet => actionSheet.present());
    } else {
      this.actionsheet.create(
        {
          header: 'Options',
          buttons: [
            {
              text: 'Logs',
              handler: () => {
                this.modalController.create(
                  {
                    component: LogsAndAttachmentsPage,
                    componentProps: {
                      data: this.requestData
                    }
                  }
                ).then(modal => {
                  modal.present();
                });
              }
            }
          ]
        }
      ).then(actionSheet => actionSheet.present());
    }

  }

}
