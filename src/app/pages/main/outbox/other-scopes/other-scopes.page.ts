import { Component, OnInit, ViewChild } from '@angular/core';
import { OtherScopesService } from './other-scopes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-other-scopes',
  templateUrl: './other-scopes.page.html',
  styleUrls: ['./other-scopes.page.scss'],
})
export class OtherScopesPage implements OnInit {
  
  @ViewChild('Searchbar', { static: false }) searchbar;

  requestData: any = {};
  copyOfRequests: any = [];

  searchBarLabel: any = 'Search';

  constructor(public service: OtherScopesService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public global: GlobalService,
    private loadingController: LoadingController
  ) { }

  ionViewWillEnter() {
    this.activatedRoute.data.subscribe(data => {
      this.service.pageDetails = { ...data.data };
      delete this.service.pageDetails.content;
      this.requestData = data.data.content[0];
      // this.requestData.RequestList = this.sortingRequestList(this.requestData.RequestList);
      this.copyOfRequests = this.requestData.RequestList.slice();
      // this.setSearchBarLabel();
    });
  }

  ngOnInit() {
  }

  setSearchBarLabel() {
    switch (this.service.requestType) {
      case 'PO_PROCESS':
        this.searchBarLabel = 'Search by PO Number';
        break;
      case 'PR_PROCESS':
        this.searchBarLabel = 'Search by PR Number';
        break;
      case 'SERVICE_ENTRY':
        this.searchBarLabel = 'Search by SE Number';
        break;
      default:
        this.searchBarLabel = 'Search by Document No.';
        break;
    }
  }

  searchOnResult(searchTerm) {
    let filteredData = [];
    switch (this.service.requestType) {
      case 'PO_PROCESS':
        filteredData = this.copyOfRequests.filter(item => (item.header.EBELN).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 || (item.header.LIFNR).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 || (item.header.NAME1).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 || (item.header.GROSS_AMT).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 || (item.createdBy).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1);
        break;
      case 'PR_PROCESS':
        filteredData = this.copyOfRequests.filter(item =>
          (item.header.BANFN != undefined ? (item.header.BANFN).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 : '')
          || (item.header.EKORG != undefined ? (item.header.EKORG).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 : '')
          || (item.header.EKGRP != undefined ? (item.header.EKGRP).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 : '')
          || (item.lineItems.length != 0 ? (item.lineItems[0].lineItems.MATNR).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 : '')
          || (item.lineItems.length != 0 ? (item.lineItems[0].lineItems.MAKTX).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 : '')
          || (item.lineItems.length != 0 ? (item.lineItems[0].lineItems.RLWRT).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 : '')
          || (item.createdBy != undefined ? (item.createdBy).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 : ''));
        break;
      case 'SERVICE_ENTRY':
        filteredData = this.copyOfRequests.filter(item => (item.header.SER_ET_NO).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 || (item.header.LIFNR).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 || (item.header.NAME1).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 || (item.lineItems.length != 0 ? item.lineItems[0].lineItems.NETWR : '').toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 || (item.createdBy).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1);
        break;
      default:
        filteredData = this.copyOfRequests.filter(item => (item.requestId).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1);
        break;
    }
    this.requestData.RequestList = filteredData;
  }

  onBack() {
    this.router.navigate(['/main/outbox']);
  }

  changePage(type) {
    if (type == 'increment') {
      this.service.pageNumber++;
    } else {
      this.service.pageNumber--;
    }
    this.loadingController.create(
      {
        message: 'Please Wait...'
      }
    ).then(loader => {
      loader.present();
      this.service.getListData().subscribe((response: any) => {
        this.service.pageDetails = { ...response };
        delete this.service.pageDetails.content;
        if (response.data.content.length > 0) this.requestData = response.data.content[0];
        this.requestData.RequestList = this.sortingRequestList(this.requestData.RequestList);
        loader.dismiss();
      });
    })
  }

  sortingRequestList(array) {
    let sortedArray: any;
    switch (this.service.requestType) {
      case 'PO_PROCESS':
        sortedArray = array.sort((a, b) => {
          return b.header.EBELN - a.header.EBELN;
        });
        break;
      case 'PR_PROCESS':
        sortedArray = array.sort((a, b) => {
          return a.header.BANFN - b.header.BANFN || a.lineItems[0].lineItems.ITEM_NO - b.lineItems[0].lineItems.ITEM_NO;
        });
        break;
      case 'SA_PROCESS':
      case 'CONTRACTS':
        sortedArray = array.sort((a, b) => {
          return a.header.EBELN - b.header.EBELN || a.lineItems[0].lineItems.ITEM_NO - b.lineItems[0].lineItems.ITEM_NO;
        });
        break;
      case 'SERVICE_ENTRY':
        sortedArray = array.sort((a, b) => {
          return a.header.SER_ET_NO - b.header.SER_ET_NO;
        });
        break;
      default:
        alert('Not an actual process');
    }
    return sortedArray;
  }

  ionViewDidLeave() {
    this.service.pageNumber = 0;
    this.searchOnResult('');
    this.searchbar.value = '';
  }
}
