import { Injectable } from '@angular/core';
import { GlobalService } from 'src/app/services/global.service';
import { HttpService } from 'src/app/services/http.service';
import { ActivatedRouteSnapshot } from '@angular/router';

@Injectable(
    {
        providedIn: 'root'
    }
)
export class OtherScopesService {

    requestType: any;
    pageNumber = 0;
    pageDetails: any = {};

    constructor(private global: GlobalService,
        private http: HttpService) { }

    resolve(route: ActivatedRouteSnapshot) {
        if (route.routeConfig.path) {
            this.requestType = route.routeConfig.path;
            return this.getListData();
        } else {
            this.global.gotoLogin();
        }
    }

    getListData() {
        const url = '/requestsList/outbox/requestType/' + this.requestType + '?page=' + this.pageNumber + '&size=1000&uniQueHeaderList=CONT_NAME%2CEMP_ENG_NAME%2CTG_DES%2CDEPT_DES';
        return this.http.call_GET(url);
    }

}