import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class OutboxService {

  constructor(private http: HttpService) { }

  resolve() {
    return this.getOutboxRequests();
  }

  getOutboxRequests() {
    const url = '/requests/outbox';
    return this.http.call_GET(url);
  }
}
