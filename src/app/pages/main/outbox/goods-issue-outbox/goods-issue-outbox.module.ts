import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GoodsIssueOutboxPageRoutingModule } from './goods-issue-outbox-routing.module';

import { GoodsIssueOutboxPage } from './goods-issue-outbox.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GoodsIssueOutboxPageRoutingModule
  ],
  declarations: [GoodsIssueOutboxPage]
})
export class GoodsIssueOutboxPageModule {}
