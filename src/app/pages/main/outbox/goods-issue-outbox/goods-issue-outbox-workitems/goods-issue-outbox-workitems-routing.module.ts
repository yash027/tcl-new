import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GoodsIssueOutboxWorkitemsPage } from './goods-issue-outbox-workitems.page';

const routes: Routes = [
  {
    path: '',
    component: GoodsIssueOutboxWorkitemsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GoodsIssueOutboxWorkitemsPageRoutingModule {}
