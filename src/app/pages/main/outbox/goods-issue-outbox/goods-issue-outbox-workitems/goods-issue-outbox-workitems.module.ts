import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GoodsIssueOutboxWorkitemsPageRoutingModule } from './goods-issue-outbox-workitems-routing.module';

import { GoodsIssueOutboxWorkitemsPage } from './goods-issue-outbox-workitems.page';
import { MatExpansionModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatExpansionModule,
    GoodsIssueOutboxWorkitemsPageRoutingModule
  ],
  declarations: [GoodsIssueOutboxWorkitemsPage]
})
export class GoodsIssueOutboxWorkitemsPageModule {}
