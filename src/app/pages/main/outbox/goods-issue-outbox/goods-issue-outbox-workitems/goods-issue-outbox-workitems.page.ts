import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, ActionSheetController, AlertController, LoadingController, ModalController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { GoodsIssueOutboxWorkitemsService } from './goods-issue-outbox-workitems.service';
import { GlobalService } from 'src/app/services/global.service';
import { DomSanitizer } from '@angular/platform-browser';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-goods-issue-outbox-workitems',
  templateUrl: './goods-issue-outbox-workitems.page.html',
  styleUrls: ['./goods-issue-outbox-workitems.page.scss'],
})
export class GoodsIssueOutboxWorkitemsPage implements OnInit {

  @ViewChild('slides', { static: false }) slider: IonSlides;

  segment = 0;

  slideOpts = {
    allowTouchMove: false,
    autoHeight: true
  };

  requestData: any = {
    header: {},
    attachments: [],
    logs: []
  };

  attachments: any = {
    images: [],
    files: []
  };

  isSapUser: any = {
    isStoreIncharge: false
  }

  userList: any = [];

  selectedUser: any;

  currentStatus: any;
  currentStatusCssClass: any;

  constructor(private route: ActivatedRoute,
    public service: GoodsIssueOutboxWorkitemsService,
    public global: GlobalService,
    private actionSheetController: ActionSheetController,
    private alertController: AlertController,
    private storage: StorageService) { }

  ionViewWillEnter() {
    this.route.data.subscribe(data => {
      if (data.data) {
        this.requestData = data.data;
        this.currentStatus = this.global.getLogTextBasedOnActivity(this.requestData.logs[this.requestData.logs.length - 1].activityText);
        this.currentStatusCssClass = this.global.getColorCssClass(this.currentStatus);
      }
      this.detectSAPUsers();
    });
  }

  ngOnInit() { }



  detectSAPUsers() {
    if (this.requestData.sapDocId !== null) {
      this.storage.getUser().then(currentUser => {
        for (let key in this.requestData.steps) {
          if (this.requestData.steps[key].users[0].toLowerCase() === currentUser.login) {
            switch (this.requestData.steps[key].stepDescription) {
              case 'StoreIncharge':
                this.isSapUser.isStoreIncharge = true;
                break;
            }
          }
        }
      });
    }
  }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  showNoStockItemAlert(lineItem) {
    let msg = 'For material ' + lineItem.lineItems.MAT_DES + ' (Quantity ' + parseInt(lineItem.lineItems.ERFMG) + ') cannot be procedded for Goods Issue because of No Stock availiblity.';
    this.alertController.create(
      {
        header: 'Message',
        message: msg,
        buttons: [
          {
            text: 'Ok',
            role: 'ok'
          }
        ]
      }
    ).then(alert => alert.present());
  }

  onViewFile(file) {
    this.actionSheetController.create(
      {
        header: 'Options',
        buttons: [
          {
            text: 'Preview Attachment',
            handler: () => {
              if (file.url) {
                this.global.downloadFile(this.requestData.requestId, file);
              } else {
                this.global.displayToastMessage('File URL Missing for Preview');
              }
            }
          }
        ]
      }
    ).then(actionSheet => actionSheet.present());
  }

}
