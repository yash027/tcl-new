import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { StorageService } from 'src/app/services/storage.service';
import { GoodsIssueOutboxService } from './goods-issue-outbox.service';

@Component({
  selector: 'app-goods-issue-outbox',
  templateUrl: './goods-issue-outbox.page.html',
  styleUrls: ['./goods-issue-outbox.page.scss'],
})
export class GoodsIssueOutboxPage implements OnInit {

  requestData: any = {};

  constructor(public service: GoodsIssueOutboxService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public global: GlobalService,
    private storage: StorageService
  ) { }

  ionViewWillEnter() {
    this.activatedRoute.data.subscribe(response => {
      if (response.data.content.length > 0) this.requestData = response.data.content[0];
      this.requestData.RequestList.forEach(request => {
        this.detectSAPUsers(request);
        request['prepostApprovalDecider'] = this.global.goodsIssuePrePostApprovalDecider(request);
      });
    });
  }

  detectSAPUsers(request) {
    let isSapUser = {
      isStoreIncharge: false,
      isStoreKeeper: false,
      isFinalInitiator: false
    };
    if (request.sapDocId !== null) {
      this.storage.getUser().then(currentUser => {
        for (let key in request.steps) {
          if (request.steps[key].users[0].toLowerCase() === currentUser.login) {
            switch (request.steps[key].stepDescription) {
              case 'StoreIncharge':
                isSapUser.isStoreIncharge = true;
                break;
              case 'Storekeeper':
                isSapUser.isStoreKeeper = true;
                break;
              case 'GIInitiator':
                isSapUser.isFinalInitiator = true;
                break;
            }
            request['sapUsersForGoodsIssue'] = isSapUser;
          }
        }
      });
    }
  }

  ngOnInit() { }

  sortingResultInDescendingOrder(data) {
    let sortedArray = [];
    sortedArray = data.sort((a, b) => { return b.sapDocId - a.sapDocId });
    return sortedArray;
  }

}
