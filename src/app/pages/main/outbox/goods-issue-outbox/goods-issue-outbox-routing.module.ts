import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GoodsIssueOutboxPage } from './goods-issue-outbox.page';
import { GoodsIssueOutboxWorkitemsService } from './goods-issue-outbox-workitems/goods-issue-outbox-workitems.service';
import { AuthGuardService } from 'src/app/guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: GoodsIssueOutboxPage
  },
  {
    path: ':requestId',
    loadChildren: () => import('./goods-issue-outbox-workitems/goods-issue-outbox-workitems.module').then( m => m.GoodsIssueOutboxWorkitemsPageModule),
    resolve: { data: GoodsIssueOutboxWorkitemsService },
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GoodsIssueOutboxPageRoutingModule {}
