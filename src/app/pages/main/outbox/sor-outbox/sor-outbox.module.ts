import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SorOutboxPageRoutingModule } from './sor-outbox-routing.module';

import { SorOutboxPage } from './sor-outbox.page';
import { MatExpansionModule } from '@angular/material';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatExpansionModule,
    Ng2SearchPipeModule,
    SorOutboxPageRoutingModule
  ],
  declarations: [SorOutboxPage]
})
export class SorOutboxPageModule {}
