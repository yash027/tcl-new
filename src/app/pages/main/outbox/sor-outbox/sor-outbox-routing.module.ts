import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SorOutboxPage } from './sor-outbox.page';

const routes: Routes = [
  {
    path: '',
    component: SorOutboxPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SorOutboxPageRoutingModule {}
