import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from 'src/app/services/http.service';
import { GlobalService } from 'src/app/services/global.service';

@Injectable({
  providedIn: 'root'
})
export class SorOutboxService {

  requestType: any;

  pageNumber: number = 0;

  pageDetails: any = {};

  constructor(private http: HttpService, private global: GlobalService) { }

  resolve(route: ActivatedRoute) {
    if(route.routeConfig.path){
      this.requestType = route.routeConfig.path;
      return this.getRequestWorkitems();
    } else {
      this.global.gotoLogin(); 
    }
  }

  getRequestWorkitems() {
    const url = '/requestsList/outbox/requestType/' + this.requestType + '?page=' + this.pageNumber + '&size=10&uniQueHeaderList=CONT_NAME%2CEMP_ENG_NAME%2CTG_DES%2CDEPT_DES';
    return this.http.call_GET(url);
  }

  getDocumentChangeHistory(requestId) {
    const url = '/changedocument/' + requestId;
    return this.http.call_GET(url);
  }
  
}
