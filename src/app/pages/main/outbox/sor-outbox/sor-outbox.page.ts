import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SorOutboxService } from './sor-outbox.service';
import { GlobalService } from 'src/app/services/global.service';
import { AlertController, ModalController, LoadingController, IonContent } from '@ionic/angular';
import { AttachmentsModalPage } from 'src/app/pages/modals/attachments-modal/attachments-modal.page';
import { LogsModalPage } from 'src/app/pages/modals/logs-modal/logs-modal.page';
import { ChangeDocumentHistoryPage } from 'src/app/pages/modals/change-document-history/change-document-history.page';

@Component({
  selector: 'app-sor-outbox',
  templateUrl: './sor-outbox.page.html',
  styleUrls: ['./sor-outbox.page.scss'],
})
export class SorOutboxPage implements OnInit {

  @ViewChild('page', {static: false}) pageTop: IonContent;

  public pageScroller(){
    this.pageTop.scrollToTop();
  }

  requestData: any = {
    RequestList: []
  };

  filteredSearch: any = {
    CONT_NAME: '',
    EMP_ENG_NAME: '',
    TG_DES: ''
  }

  searchText: any;

  constructor(private route: ActivatedRoute,
              private router: Router,
              public service: SorOutboxService,
              public global: GlobalService,
              private modalController: ModalController,
              private loadingController: LoadingController,
              private alertController: AlertController
    ) { }

  ionViewWillEnter() {
    
    this.route.data.subscribe( data => {
      this.service.pageDetails = {...data.data};
      delete this.service.pageDetails.content;
      if(data.data.content.length > 0) this.requestData = data.data.content[0];
    }, error => {
      this.global.displayToastMessage('Unable to fetch data from server, Please try after some time');
      this.global.gotoLogin();
    });
  }

  ionViewWillLeave() {
    
  }

  getRequestWorkitems() {
    this.loadingController.create(
      {
        message: 'Please wait...',
      }
    ).then( loader => {
      loader.present();
      this.service.getRequestWorkitems().subscribe( (response: any) => {
        this.service.pageDetails = {...response};
        delete this.service.pageDetails.content;
        this.requestData = response.content[0];
        loader.dismiss();
        this.pageScroller();
      }, error => {
        loader.dismiss();
        this.global.displayToastMessage('Unable to fetch data, please try after sometime.');
        this.global.gotoLogin();
      });
    });
  }

  ngOnInit() {
  }

  onBack() {
    this.router.navigate(['/main/outbox']);
  }

  changePage(type) {
    if(type == 'increment') {
      this.service.pageNumber++;
    } else {
      this.service.pageNumber--;
    }
  }

  onViewAttachment(request) {
    this.modalController.create(
      {
        component: AttachmentsModalPage,
        componentProps: {
          data: {
            attachments: request.attachments,
            requestId: request.requestId
          }
        }
      }
    ).then( modal => {
      modal.present();
    });
  }

  onViewLogs(request) {
    this.modalController.create(
      {
        component: LogsModalPage,
        componentProps: {
          data: request.logs
        }
      }
    ).then( modal => {
      modal.present();
    });
  }

  onViewChangeDocument(request) {
    this.loadingController.create(
      {
        message: 'Please wait...'
      }
    ).then(loader => {
      loader.present();
      this.service.getDocumentChangeHistory(request.requestId).subscribe(response => {
        loader.dismiss();
        this.modalController.create(
          {
            component: ChangeDocumentHistoryPage,
            componentProps: {
              data: response
            }
          }
        ).then(modal => {
          modal.present();
        });
      }, error => {
        loader.dismiss();
        this.global.displayToastMessage('Unable to display Document Change History, Try After Some Time');
      });
    })
  }

  showSORDescription(description) {
    // this.global.displayToastMessage(description);
    this.alertController.create(
      {
        header: 'SOR Description',
        message: description,
        buttons: [
          {
            text: 'Ok',
            role: 'ok'
          }
        ]
      }
    ).then(alert => alert.present());
  }

  ionViewDidLeave() {
    this.service.pageNumber = 0;
  }

}
