import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MmgOutboxPageRoutingModule } from './mmg-outbox-routing.module';

import { MmgOutboxPage } from './mmg-outbox.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MmgOutboxPageRoutingModule
  ],
  declarations: [MmgOutboxPage]
})
export class MmgOutboxPageModule {}
