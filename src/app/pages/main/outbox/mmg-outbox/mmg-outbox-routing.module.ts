import { MmgOutboxWorkItemsService } from './mmg-outbox-workitems/mmg-outbox-workitems.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MmgOutboxPage } from './mmg-outbox.page';
import { AuthGuardService } from 'src/app/guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: MmgOutboxPage
  },
  {
    path: ':requestId',
    loadChildren: () => import('./mmg-outbox-workitems/mmg-outbox-workitems.module').then(m => m.MmgOutboxWorkitemsPageModule),
    resolve: { data: MmgOutboxWorkItemsService },
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MmgOutboxPageRoutingModule { }
