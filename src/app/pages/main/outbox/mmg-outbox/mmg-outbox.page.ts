import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { GlobalService } from 'src/app/services/global.service';
import { MmgOutboxService } from './mmg-outbox.service';

@Component({
  selector: 'app-mmg-outbox',
  templateUrl: './mmg-outbox.page.html',
  styleUrls: ['./mmg-outbox.page.scss'],
})
export class MmgOutboxPage implements OnInit {

  @ViewChild('Searchbar', { static: false }) searchbar;

  requestData: any = {};
  selectedWorkItems: any = [];
  copyOfRequests: any = [];

  searchBarLabel: any = 'Search';

  decision = false;

  bulkApprovalActivities = [{
    id: "5d1c602b9c73c510e0aae5b7",
    actvity: "510",
    description: "Approved",
    buttonText: "Approve",
    type: "SAP",
    url: "/approve",
    commentsRequired: true
  }, {
    id: "5d1c60469c73c510e0aae5b8",
    actvity: "511",
    description: "Rejected",
    buttonText: "Reject",
    type: "SAP",
    url: "/reject",
    commentsRequired: true
  }]

  constructor(
    public service: MmgOutboxService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public global: GlobalService
  ) { }

  ionViewWillEnter() {
    this.global.hideBottomTabs();
    this.activatedRoute.data.subscribe(data => {
      if (data.data.content.length > 0) this.requestData = data.data.content[0];
      this.copyOfRequests = this.requestData.RequestList.slice();
    });
  }

  searchOnResult(searchTerm) {
    let filteredData = [];
    filteredData = this.copyOfRequests.filter(item => (item.header.Department ? item.header.Department : '').toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 || (item.header.PLNT ? item.header.PLNT : '').toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 || (item.header.MATKL ? item.header.MATKL : '').toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1 || (item.createdBy ? item.createdBy : '').toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1);
    this.requestData.RequestList = filteredData;
  }

  ngOnInit() {
  }

  onBack() {
    this.router.navigate(['/main/outbox']);
  }

  ionViewDidLeave() {
    this.searchOnResult('');
    this.searchbar.value = '';
  }

  checkIfAnyCollaborateWorkItemExist() {
    let decision = false;
    let workitem = this.selectedWorkItems.find(element => element.status === '5');
    if (workitem) {
      decision = true;
    } else {
      decision = workitem.requestId
    };
    return decision;
  }

}
