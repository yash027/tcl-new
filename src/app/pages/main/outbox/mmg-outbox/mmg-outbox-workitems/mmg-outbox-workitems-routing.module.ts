import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MmgOutboxWorkitemsPage } from './mmg-outbox-workitems.page';

const routes: Routes = [
  {
    path: '',
    component: MmgOutboxWorkitemsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MmgOutboxWorkitemsPageRoutingModule {}
