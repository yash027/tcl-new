import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MmgOutboxWorkitemsPageRoutingModule } from './mmg-outbox-workitems-routing.module';

import { MmgOutboxWorkitemsPage } from './mmg-outbox-workitems.page';
import { MatExpansionModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatExpansionModule,
    MmgOutboxWorkitemsPageRoutingModule
  ],
  declarations: [MmgOutboxWorkitemsPage]
})
export class MmgOutboxWorkitemsPageModule { }
