import { Component, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { IonSlides, ActionSheetController, ModalController } from '@ionic/angular';
import { ChangeDocumentHistoryPage } from 'src/app/pages/modals/change-document-history/change-document-history.page';
import { LogsAndAttachmentsPage } from 'src/app/pages/modals/logs-and-attachments/logs-and-attachments.page';
import { GlobalService } from 'src/app/services/global.service';
import { MmgOutboxWorkItemsService } from './mmg-outbox-workitems.service';

@Component({
  selector: 'app-mmg-outbox-workitems',
  templateUrl: './mmg-outbox-workitems.page.html',
  styleUrls: ['./mmg-outbox-workitems.page.scss'],
})
export class MmgOutboxWorkitemsPage implements OnInit {

  requestID: any;

  requestData: any = {
    header: {},
    logs: []
  };

  attachments: any = [[], []];

  segment = 0;
  login: any;

  slideOpts = {
    allowTouchMove: false,
    autoHeight: true
  };

  lengthOfAttachment: number;
  decision = false;
  isFinalLevel = false;
  showMaterial = true;

  @ViewChild('slides', { static: false }) slider: IonSlides;

  constructor(private route: ActivatedRoute,
    private router: Router,
    public service: MmgOutboxWorkItemsService,
    public global: GlobalService,
    private actionsheet: ActionSheetController,
    private sanitizer: DomSanitizer,
    private modalController: ModalController
  ) { }

  ionViewWillEnter() {
    this.global.hideBottomTabs();
    this.route.data.subscribe(data => {
      if (data.data) {
        this.requestData = data.data;
        this.lengthOfAttachment = this.requestData.attachments.length;
        this.checkMaterialStatus();
      }
    });
    this.checkRequestTypeForHeader();
  }

  ngOnInit() {
  }

  checkForMark(data) {
    if (data == 'X') return true;
    else false;
  }

  checkMaterialStatus() {
    if (this.requestData.header.MTART == 'ROH' || this.requestData.header.MTART == 'VERP' || this.requestData.header.MTART == 'HALB'
      || this.requestData.header.MTART == 'ZHLB' || this.requestData.header.MTART == 'FERT' || this.requestData.header.MTART == 'ZFRT'
      || this.requestData.header.MTART == 'EPA') {
      switch (this.requestData.header.location) {
        case 'Mithapur':
        case 'Nellore':
        case 'Sri-Perumbdur':
        case 'Mambattu':
          this.showMaterial = false;
          break;
        default:
          this.showMaterial = true;
          break;
      }
    } else if (this.requestData.header.MTART == 'ZROH' || this.requestData.header.MTART == 'ZVRP' || this.requestData.header.MTART == 'ZRSA'
      || this.requestData.header.MTART == 'ZIBE' || this.requestData.header.MTART == 'ZNBW') {
      switch (this.requestData.header.location) {
        case 'Nellore':
          this.showMaterial = false;
          break;
        default:
          this.showMaterial = true;
          break;
      }
    } else this.showMaterial = true;
  }

  checkRequestTypeForHeader() {
    this.requestID = (this.requestData.requestId).toUpperCase();
  }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  onBack() {
    this.router.navigate(['/main/outbox/' + this.service.parentRequestId]);
  }

  getAttachmentUrl() {
    if (this.requestData.attachments[0].url) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(this.requestData.attachments[0].url);
    }
  }

  openActionSheet() {
    if (this.requestData.attachments.length > 0 && this.requestData.attachments[0].url !== null) {
      this.actionsheet.create(
        {
          header: 'Options',
          buttons: [
            {
              text: 'Preview',
              handler: () => {
                this.global.openDocumentInViewer(this.requestData.attachments[0].url);
              }
            },
            {
              text: 'Logs',
              handler: () => {
                this.modalController.create(
                  {
                    component: LogsAndAttachmentsPage,
                    componentProps: {
                      data: this.requestData
                    }
                  }
                ).then(modal => {
                  modal.present();
                });
              }
            }
          ]
        }
      ).then(actionSheet => actionSheet.present());
    } else {
      this.actionsheet.create(
        {
          header: 'Options',
          buttons: [
            {
              text: 'Logs',
              handler: () => {
                this.modalController.create(
                  {
                    component: LogsAndAttachmentsPage,
                    componentProps: {
                      data: this.requestData
                    }
                  }
                ).then(modal => {
                  modal.present();
                });
              }
            }
          ]
        }
      ).then(actionSheet => actionSheet.present());
    }

  }

  getBytesConversion(bytes) {
    const size = this.global.getBytesConversion(bytes, 2);
    return size;
  }

  downloadAttachment(file) {
    this.global.downloadFile(this.requestData.requestId, file)
  }

  changedDocumentHistory() {
    this.service.fetchChangedDocumentHistory(this.requestData.requestId)
      .subscribe((response: any) => {
        this.modalController.create({
          component: ChangeDocumentHistoryPage,
          componentProps: {
            data: response
          }
        }).then(modal => {
          modal.present();
        });
      });
  }
}
