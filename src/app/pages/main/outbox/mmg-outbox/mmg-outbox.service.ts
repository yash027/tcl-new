import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { HttpService } from 'src/app/services/http.service';

@Injectable({
    providedIn: "root"
})
export class MmgOutboxService {

    requestType: any;
    pageNumber = 0;
    pageDetails: any = {};

    constructor(private global: GlobalService,
        private http: HttpService) { }

    resolve(route: ActivatedRouteSnapshot) {
        if (route.routeConfig.path) {
            this.requestType = route.routeConfig.path;
            return this.getListData();
        } else {
            this.global.gotoLogin();
        }
    }

    getListData() {
        const url = '/requestsList/outbox/requestType/' + this.requestType + '?page=' + this.pageNumber + '&size=500&uniQueHeaderList=CONT_NAME%2CEMP_ENG_NAME%2CTG_DES%2CDEPT_DES';
        return this.http.call_GET(url);
    }

}