import { RebateOutboxService } from './rebate-outbox/rebate-outbox.service';
import { PricingOutboxService } from './pricing-outbox/pricing-outbox.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OutboxPage } from './outbox.page';
import { OutboxService } from './outbox.service';
import { SorOutboxService } from './sor-outbox/sor-outbox.service';
import { TimesheetOutboxService } from './timesheet-outbox/timesheet-outbox.service';
import { AuthGuardService } from 'src/app/guards/auth.guard';
import { OtherScopesService } from './other-scopes/other-scopes.service';
import { GoodsIssueOutboxService } from './goods-issue-outbox/goods-issue-outbox.service';
import { CapexBudgetOutboxService } from './capex-budget-outbox/capex-budget-outbox.service';
import { MmgOutboxService } from './mmg-outbox/mmg-outbox.service';

const routes: Routes = [
  {
    path: '',
    component: OutboxPage,
    resolve: { data: OutboxService }
  },
  {
    path: 'SOR_CLAIM',
    loadChildren: () => import('./sor-outbox/sor-outbox.module').then(m => m.SorOutboxPageModule),
    resolve: { data: SorOutboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'TIME_CLAIM',
    loadChildren: () => import('./timesheet-outbox/timesheet-outbox.module').then(m => m.TimesheetOutboxPageModule),
    resolve: { data: TimesheetOutboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'PO_PROCESS',
    loadChildren: () => import('./other-scopes/other-scopes.module').then(m => m.OtherScopesPageModule),
    resolve: { data: OtherScopesService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'PR_PROCESS',
    loadChildren: () => import('./other-scopes/other-scopes.module').then(m => m.OtherScopesPageModule),
    resolve: { data: OtherScopesService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'SA_PROCESS',
    loadChildren: () => import('./other-scopes/other-scopes.module').then(m => m.OtherScopesPageModule),
    resolve: { data: OtherScopesService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'CONTRACTS',
    loadChildren: () => import('./other-scopes/other-scopes.module').then(m => m.OtherScopesPageModule),
    resolve: { data: OtherScopesService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'SERVICE_ENTRY',
    loadChildren: () => import('./other-scopes/other-scopes.module').then(m => m.OtherScopesPageModule),
    resolve: { data: OtherScopesService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'GI_PROCESS',
    loadChildren: () => import('./goods-issue-outbox/goods-issue-outbox.module').then(m => m.GoodsIssueOutboxPageModule),
    resolve: { data: GoodsIssueOutboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'PRC_PROCESS',
    loadChildren: () => import('./pricing-outbox/pricing-outbox.module').then(m => m.PricingOutboxPageModule),
    resolve: { data: PricingOutboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'ABP',
    loadChildren: () => import('./capex-budget-outbox/capex-budget-outbox.module').then(m => m.CapexBudgetOutboxPageModule),
    resolve: { data: CapexBudgetOutboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'REB_PROCESS',
    loadChildren: () => import('./rebate-outbox/rebate-outbox.module').then(m => m.RebateOutboxPageModule),
    resolve: { data: RebateOutboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'REB_DRCT_A_NOTE',
    loadChildren: () => import('./rebate-outbox/rebate-outbox.module').then(m => m.RebateOutboxPageModule),
    resolve: { data: RebateOutboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'REB_DRCT_CM',
    loadChildren: () => import('./rebate-outbox/rebate-outbox.module').then(m => m.RebateOutboxPageModule),
    resolve: { data: RebateOutboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'REB_CRE_MEMO',
    loadChildren: () => import('./rebate-outbox/rebate-outbox.module').then(m => m.RebateOutboxPageModule),
    resolve: { data: RebateOutboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'MM_CREATE',
    loadChildren: () => import('./mmg-outbox/mmg-outbox.module').then(m => m.MmgOutboxPageModule),
    resolve: { data: MmgOutboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'MM_CHANGE',
    loadChildren: () => import('./mmg-outbox/mmg-outbox.module').then(m => m.MmgOutboxPageModule),
    resolve: { data: MmgOutboxService },
    canActivate: [AuthGuardService]
  },
  {
    path: 'MM_EXTEND',
    loadChildren: () => import('./mmg-outbox/mmg-outbox.module').then(m => m.MmgOutboxPageModule),
    resolve: { data: MmgOutboxService },
    canActivate: [AuthGuardService]
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OutboxPageRoutingModule { }
