import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-rebate-outbox',
  templateUrl: './rebate-outbox.page.html',
  styleUrls: ['./rebate-outbox.page.scss'],
})
export class RebateOutboxPage implements OnInit {

  @ViewChild('Searchbar', { static: false }) searchbar;
  
  requestData: any = [];
  copyOfRequests: any = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public global: GlobalService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.activatedRoute.data.subscribe((response) => {
      let requestData;
      if (response.data.content.length > 0) requestData = response.data.content[0];
      this.requestData = requestData.RequestList;
      this.copyOfRequests = requestData.RequestList.slice();
    },
      (error) => {
        this.global.displayToastMessage('Some Internal Error Occured, try again later.')
      });
  }

  searchOnResult(searchTerm) {
    let filteredData = [];
    filteredData = this.copyOfRequests.filter(item => (item.requestId).toString().toLowerCase().indexOf((searchTerm).toLowerCase()) !== -1);
    this.requestData = filteredData;
  }

  ionViewDidLeave() {
    this.searchOnResult('');
    this.searchbar.value = '';
  }
}
