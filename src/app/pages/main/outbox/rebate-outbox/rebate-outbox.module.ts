import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RebateOutboxPageRoutingModule } from './rebate-outbox-routing.module';

import { RebateOutboxPage } from './rebate-outbox.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RebateOutboxPageRoutingModule
  ],
  declarations: [RebateOutboxPage]
})
export class RebateOutboxPageModule {}
