import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RebateOutboxWorkitemPage } from './rebate-outbox-workitem.page';

const routes: Routes = [
  {
    path: '',
    component: RebateOutboxWorkitemPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RebateOutboxWorkitemPageRoutingModule {}
