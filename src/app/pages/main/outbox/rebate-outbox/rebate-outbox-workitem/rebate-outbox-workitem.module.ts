import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RebateOutboxWorkitemPageRoutingModule } from './rebate-outbox-workitem-routing.module';

import { RebateOutboxWorkitemPage } from './rebate-outbox-workitem.page';
import { MatExpansionModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatExpansionModule,
    RebateOutboxWorkitemPageRoutingModule
  ],
  declarations: [RebateOutboxWorkitemPage]
})
export class RebateOutboxWorkitemPageModule {}
