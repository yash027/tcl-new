import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IonSlides, ActionSheetController, ModalController } from '@ionic/angular';
import { ValidationModalPage } from 'src/app/pages/modals/validation-modal/validation-modal.page';
import { GlobalService } from 'src/app/services/global.service';
import { StorageService } from 'src/app/services/storage.service';
import { RebateOutboxWorkitemService } from './rebate-outbox-workitem.service';

@Component({
  selector: 'app-rebate-outbox-workitem',
  templateUrl: './rebate-outbox-workitem.page.html',
  styleUrls: ['./rebate-outbox-workitem.page.scss'],
})

export class RebateOutboxWorkitemPage {

  @ViewChild('slides', { static: false }) slider: IonSlides;

  segment = 0;
  login: any;

  slideOpts = {
    allowTouchMove: false,
    autoHeight: true
  };

  requestData: any = {
    header: {},
    attachments: [],
    logs: []
  };

  attachments: any = {
    images: [],
    files: []
  };

  userList: any = [];

  lastActivity: any;

  currentStatus: any;
  currentStatusCssClass: any;

  validating = false;
  isValidate = false;

  popupData: any = [];
  price_duplicate: any;
  selectedLineitem: any;
  rebateShiptoParty: any = [];
  refWorkitemInfo: any;

  constructor(
    private route: ActivatedRoute,
    public service: RebateOutboxWorkitemService,
    public global: GlobalService,
    private actionSheetController: ActionSheetController,
    private modalController: ModalController,
    private router: Router,
    private storage: StorageService) { }

  ionViewWillEnter() {
    this.global.hideBottomTabs();
    this.route.data.subscribe(response => {
      if (response.data) {
        this.requestData = response.data;
        if (this.requestData.logs) {
          this.currentStatus = this.global.getLogTextBasedOnActivity(this.requestData.logs[this.requestData.logs.length - 1].activityText);
          this.currentStatusCssClass = this.global.getColorCssClass(this.currentStatus);
          this.lastActivity = this.requestData.logs[this.requestData.logs.length - 1].endDate;
        }
        this.getShiptoParty();
        if (this.requestData.header.REF_WORKFLOW) this.getRequestIdLogs();
        this.isValidate = false;
      }
    });
  }

  onBack() {
    this.router.navigate(['/main/outbox/' + this.service.parentRequestId]);
  }

  //get Ref. workflow logs
  getRequestIdLogs() {
    this.global.getrequestsById(this.requestData.header.REF_WORKFLOW).subscribe((success) => {
      this.refWorkitemInfo = success;
    }, (error) => {
      this.global.displayToastMessage('Error Occured while fetching the Referenced request number.')
    }
    )
  }

  //get ship to party details
  getShiptoParty() {
    if (this.requestData.header.SHIP_TO == 'No' || this.requestData.header.SHIP_TO == 'N') {
      this.rebateShiptoParty = [];
      let obj_code = this.requestData.header.Ship_To_Code ? this.requestData.header.Ship_To_Code.split('~') : this.requestData.header.KUNNR.split('~');
      let obj_name = this.requestData.header.Ship_To_Desc.split('~');

      for (let k = 0; k < obj_code.length; k++) {
        let obj = { party_code: '', party_name: '' };
        obj.party_code = obj_code[k];
        obj.party_name = obj_name[k];
        this.rebateShiptoParty.push(obj);
      }
    }
  }

  //get ship to party name
  getShipToPartyname(code) {
    if (code && code != '') {
      let partyCode = this.requestData.header.Ship_To_Code ? this.requestData.header.Ship_To_Code.split('~') : this.requestData.header.KUNNR.split('~');
      let partyName = this.requestData.header.Ship_To_Desc.split('~');
      let STPname = '';
      for (let i = 0; i < partyCode.length; i++) {
        if (code == partyCode[i]) {
          STPname = partyName[i];
          break;
        }
      }
      return STPname;
    }
  }

  getAmount(amount) {
    return (parseFloat(amount)).toFixed(2);
  }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  onViewFile(file) {
    this.actionSheetController.create(
      {
        header: 'Options',
        cssClass: 'attachmentsAS',
        buttons: [
          {
            text: 'Preview Attachment',
            icon: 'eye',
            cssClass: 'scanOptionAS',
            handler: () => {
              this.global.downloadFile(this.requestData.requestId, file);
            }
          }
        ]
      }
    ).then(actionSheet => actionSheet.present());
  }

  checkDuplicationOfRecords() {
    this.validating = true;
    this.popupData = [];

    let fromDate: any;
    let toDate: any;
    let new_data = [];

    fromDate = new Date(this.global.getValidationDate(this.requestData.header.DATAB));
    (fromDate.setDate(fromDate.getDate() + 1));
    fromDate = fromDate.toISOString();
    toDate = new Date(this.global.getValidationDate(this.requestData.header.DATBI));
    (toDate.setDate(toDate.getDate() + 1));
    toDate = toDate.toISOString();
    if (this.requestData.header.BONEM && (this.requestData.header.MATNR || this.requestData.header.MAKTL) && this.requestData.header.DATAB && this.requestData.header.DATBI) {
      this.service.getDuplicateData('requestSearch/tcl', ["PRC_PROCESS", "REB_PROCESS", "REB_DRCT_CM", "REB_DRCT_A_NOTE", "REB_CRE_MEMO"],
        this.requestData.header.BONEM, (this.requestData.header.MATNR || this.requestData.header.MAKTL), fromDate, toDate, 0, 1000).subscribe((response: any) => {
          if (response) {
            (response).forEach((item) => {
              if (item.header.KSCHL == 'ZSD2') {
                if (item.requestId != this.requestData.requestId) new_data.push(item);
              }
            });
            var vm = new Set(new_data);
            this.popupData = Array.from(vm);
          }
          this.isValidate = true;
          this.validating = false;
          this.showAvailableDataInDateRange();
        },
          (error) => {
            this.validating = false;
            this.global.displayToastMessage('Error !! Error Occured while getting the requests.');
          }
        )
    } else {
      this.validating = false;
      this.global.displayToastMessage('Error !! Payer, Material and date range are mandatory for validation.')
    }
  }

  async showAvailableDataInDateRange() {
    if (this.popupData.length > 0) {
      const modal = await this.modalController.create({
        component: ValidationModalPage,
        backdropDismiss: false,
        componentProps: { data: this.popupData, isLineitem: this.price_duplicate, selectedItem: this.selectedLineitem }
      });
      return await modal.present();
    } else alert('INFO !! No Duplicate Data Found.')
  }

  isCancelled() {
    return ((this.requestData.requestType == 'REB_DRCT_CM' || this.requestData.requestType == 'REB_CRE_MEMO')
      && this.requestData.logs[this.requestData.logs.length - 1].activityText == 'Cancel'
      && this.requestData.header.CRE_MEMO) ? true : false;
  }
}
