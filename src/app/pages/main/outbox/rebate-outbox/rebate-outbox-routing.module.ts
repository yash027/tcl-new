import { RebateOutboxWorkitemService } from './rebate-outbox-workitem/rebate-outbox-workitem.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RebateOutboxPage } from './rebate-outbox.page';
import { AuthGuardService } from 'src/app/guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: RebateOutboxPage
  },
  {
    path: ':requestId',
    loadChildren: () => import('./rebate-outbox-workitem/rebate-outbox-workitem.module').then( m => m.RebateOutboxWorkitemPageModule),
    resolve: { data: RebateOutboxWorkitemService },
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RebateOutboxPageRoutingModule {}
