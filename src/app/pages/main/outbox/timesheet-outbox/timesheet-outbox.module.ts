import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TimesheetOutboxPageRoutingModule } from './timesheet-outbox-routing.module';

import { TimesheetOutboxPage } from './timesheet-outbox.page';
import { MatExpansionModule } from '@angular/material';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatExpansionModule,
    Ng2SearchPipeModule,
    TimesheetOutboxPageRoutingModule
  ],
  declarations: [TimesheetOutboxPage]
})
export class TimesheetOutboxPageModule {}
