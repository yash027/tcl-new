import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent, ModalController, LoadingController } from '@ionic/angular';
import { TimesheetOutboxService } from './timesheet-outbox.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { AttachmentsModalPage } from 'src/app/pages/modals/attachments-modal/attachments-modal.page';
import { LogsModalPage } from 'src/app/pages/modals/logs-modal/logs-modal.page';

@Component({
  selector: 'app-timesheet-outbox',
  templateUrl: './timesheet-outbox.page.html',
  styleUrls: ['./timesheet-outbox.page.scss'],
})
export class TimesheetOutboxPage implements OnInit {

  requestData: any = {
    RequestList: []
  };

  filteredSearch: any = {
    CONT_NAME: '',
    EMP_ENG_NAME: '',
    TG_DES: ''
  }

  @ViewChild('page', {static: false}) pageTop: IonContent;

  public pageScroller() {
    this.pageTop.scrollToTop();
  }

  searchText: any;

  lineItemLoading: boolean;

  constructor(private route: ActivatedRoute,
    private router: Router,
    public service: TimesheetOutboxService,
    public global: GlobalService,
    private modalController: ModalController,
    private loadingController: LoadingController
  ) { }

  ionViewWillEnter() {

    this.route.data.subscribe(data => {
      this.service.pageDetails = {...data.data};
      delete this.service.pageDetails.content;
      if(data.data.content.length > 0) this.requestData = data.data.content[0];
    }, error => {
      this.global.displayToastMessage('Unable to fetch data from server, Please try after some time');
      this.global.gotoLogin();
    });
  }

  ionViewWillLeave() {

  }

  ngOnInit() { }

  onBack() {
    this.router.navigate(['/main/outbox']);
  }

  changePage(type) {
    if (type == 'increment') {
      this.service.pageNumber++;
    } else {
      this.service.pageNumber--;
    }
  }

  onViewAttachment(request) {
    this.modalController.create(
      {
        component: AttachmentsModalPage,
        componentProps: {
          data: {
            attachments: request.attachments,
            requestId: request.requestId
          }
        }
      }
    ).then(modal => {
      modal.present();
    });
  }

  onViewLogs(request) {
    this.modalController.create(
      {
        component: LogsModalPage,
        componentProps: {
          data: request.logs
        }
      }
    ).then(modal => {
      modal.present();
    });
  }

  getRequestWorkitems() {
    this.loadingController.create(
      {
        message: 'Please wait...',
      }
    ).then(loader => {
      loader.present();
      this.service.getRequestWorkitems().subscribe((response: any) => {
        this.service.pageDetails = {...response};
        delete this.service.pageDetails.content;
        this.requestData = response.content[0];
        loader.dismiss();
        this.pageScroller();
      }, error => {
        loader.dismiss();
        this.global.displayToastMessage('Unable to fetch data, please try after sometime.');
        this.global.gotoLogin();
      });
    });
  }

  ionViewDidLeave() {
    this.service.pageNumber = 0;
  }

}
