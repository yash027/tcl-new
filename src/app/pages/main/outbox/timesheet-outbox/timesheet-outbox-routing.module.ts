import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TimesheetOutboxPage } from './timesheet-outbox.page';

const routes: Routes = [
  {
    path: '',
    component: TimesheetOutboxPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TimesheetOutboxPageRoutingModule {}
