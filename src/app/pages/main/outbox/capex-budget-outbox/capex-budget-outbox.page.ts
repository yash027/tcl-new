import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { CapexBudgetOutboxService } from './capex-budget-outbox.service';

@Component({
  selector: 'app-capex-budget-outbox',
  templateUrl: './capex-budget-outbox.page.html',
  styleUrls: ['./capex-budget-outbox.page.scss'],
})
export class CapexBudgetOutboxPage implements OnInit {

  requestData: any = {};

  constructor(public service: CapexBudgetOutboxService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public global: GlobalService
  ) { }

  ionViewWillEnter() {
    this.activatedRoute.data.subscribe(data => {
      if(data.data.content.length > 0) this.requestData = data.data.content[0];
      this.requestData.RequestList = this.sortingResultInDescendingOrder(this.requestData.RequestList);
    });
  }

  ngOnInit() {}

  sortingResultInDescendingOrder(data) {
    let sortedArray = [];
    sortedArray = data.sort((a, b) => {return b.sapDocId - a.sapDocId});
    return sortedArray;
  }
}
