import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IonSlides, ActionSheetController } from '@ionic/angular';
import { GlobalService } from 'src/app/services/global.service';
import { CapexBudgetOutboxWorkitemService } from './capex-budget-outbox-workitem.service';

@Component({
  selector: 'app-capex-budget-outbox-workitem',
  templateUrl: './capex-budget-outbox-workitem.page.html',
  styleUrls: ['./capex-budget-outbox-workitem.page.scss'],
})
export class CapexBudgetOutboxWorkitemPage implements OnInit {

  @ViewChild('slides', { static: false }) slider: IonSlides;

  segment = 0;
  login: any;

  slideOpts = {
    allowTouchMove: false,
    autoHeight: true
  };

  requestData: any = {
    header: {},
    attachments: [],
    logs: []
  };

  attachments: any = {
    images: [],
    files: []
  };
  currentStatus: any = '';
  currentStatusCssClass: any = '';

  constructor(
    public global: GlobalService,
    private route: ActivatedRoute,
    private actionSheetController: ActionSheetController,
    public service: CapexBudgetOutboxWorkitemService
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.global.hideBottomTabs();
    this.route.data.subscribe(data => {
      if (data.data) {
        this.requestData = data.data;
      }
    });
    this.currentStatus = this.global.getLogTextBasedOnActivity(this.requestData.logs[this.requestData.logs.length - 1].activityText);
    this.currentStatusCssClass = this.global.getColorCssClass(this.currentStatus);
  }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  onViewFile(file) {
    this.actionSheetController.create(
      {
        header: 'Options',
        cssClass: 'attachmentsAS',
        buttons: [
          {
            text: 'Preview Attachment',
            icon: 'eye',
            cssClass: 'scanOptionAS',
            handler: () => {
              this.global.downloadFile(this.requestData.requestId, file);
            }
          }
        ]
      }
    ).then(actionSheet => actionSheet.present());
  }

}
