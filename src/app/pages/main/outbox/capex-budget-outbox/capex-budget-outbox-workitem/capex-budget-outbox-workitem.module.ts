import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CapexBudgetOutboxWorkitemPageRoutingModule } from './capex-budget-outbox-workitem-routing.module';

import { CapexBudgetOutboxWorkitemPage } from './capex-budget-outbox-workitem.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CapexBudgetOutboxWorkitemPageRoutingModule
  ],
  declarations: [CapexBudgetOutboxWorkitemPage]
})
export class CapexBudgetOutboxWorkitemPageModule {}
