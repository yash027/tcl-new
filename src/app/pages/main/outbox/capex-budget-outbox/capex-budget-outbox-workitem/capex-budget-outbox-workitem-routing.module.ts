import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CapexBudgetOutboxWorkitemPage } from './capex-budget-outbox-workitem.page';

const routes: Routes = [
  {
    path: '',
    component: CapexBudgetOutboxWorkitemPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CapexBudgetOutboxWorkitemPageRoutingModule {}
