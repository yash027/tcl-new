import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { HttpService } from 'src/app/services/http.service';

@Injectable({ providedIn: "root"})
export class CapexBudgetOutboxWorkitemService {
    
    requestId: any;
    parentRequestId: any;
  
    constructor(private http: HttpService) { }
  
    resolve(route: ActivatedRouteSnapshot) {
      if (route.parent.routeConfig.path && route.params.requestId) {
        this.requestId = route.params.requestId;
        this.parentRequestId = route.parent.routeConfig.path;
        return this.getRequestIdDetails(route.parent.routeConfig.path, this.requestId);
      }
    }
  
    getRequestIdDetails(parent, requestId) {
      const url = '/requests/outbox/requestType/' + parent + '/requests/' + requestId;
      return this.http.call_GET(url);
    }

}