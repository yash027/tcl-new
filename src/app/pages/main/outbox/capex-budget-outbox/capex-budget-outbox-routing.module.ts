import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/guards/auth.guard';
import { CapexBudgetOutboxWorkitemService } from './capex-budget-outbox-workitem/capex-budget-outbox-workitem.service';

import { CapexBudgetOutboxPage } from './capex-budget-outbox.page';

const routes: Routes = [
  {
    path: '',
    component: CapexBudgetOutboxPage
  },
  {
    path: ':requestId',
    loadChildren: () => import('./capex-budget-outbox-workitem/capex-budget-outbox-workitem.module').then( m => m.CapexBudgetOutboxWorkitemPageModule),
    resolve: {data: CapexBudgetOutboxWorkitemService},
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CapexBudgetOutboxPageRoutingModule {}
