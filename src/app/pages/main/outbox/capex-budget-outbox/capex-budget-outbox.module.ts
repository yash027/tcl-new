import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CapexBudgetOutboxPageRoutingModule } from './capex-budget-outbox-routing.module';

import { CapexBudgetOutboxPage } from './capex-budget-outbox.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CapexBudgetOutboxPageRoutingModule
  ],
  declarations: [CapexBudgetOutboxPage]
})
export class CapexBudgetOutboxPageModule {}
