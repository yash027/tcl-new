import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PricingOutboxPageRoutingModule } from './pricing-outbox-routing.module';

import { PricingOutboxPage } from './pricing-outbox.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PricingOutboxPageRoutingModule
  ],
  declarations: [PricingOutboxPage]
})
export class PricingOutboxPageModule {}
