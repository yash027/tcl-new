import { PricingOutboxWorkitemService } from './pricing-outbox-workitems/pricing-outbox-workitem.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PricingOutboxPage } from './pricing-outbox.page';
import { AuthGuardService } from 'src/app/guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: PricingOutboxPage
  },
  {
    path: ':requestId',
    loadChildren: () => import('./pricing-outbox-workitems/pricing-outbox-workitems.module').then(m => m.PricingOutboxWorkitemsPageModule),
    resolve: { data: PricingOutboxWorkitemService },
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PricingOutboxPageRoutingModule { }
