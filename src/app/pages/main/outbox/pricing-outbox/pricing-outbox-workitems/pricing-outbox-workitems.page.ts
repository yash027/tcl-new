import { PricingOutboxWorkitemService } from './pricing-outbox-workitem.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, AlertController, ModalController, ActionSheetController } from '@ionic/angular';
import { GlobalService } from 'src/app/services/global.service';
import { ValidationModalPage } from 'src/app/pages/modals/validation-modal/validation-modal.page';
import { CostingModelPage } from 'src/app/pages/modals/costing-modal/costing-model.page';

@Component({
  selector: 'app-pricing-outbox-workitems',
  templateUrl: './pricing-outbox-workitems.page.html',
  styleUrls: ['./pricing-outbox-workitems.page.scss'],
})

export class PricingOutboxWorkitemsPage implements OnInit {

  @ViewChild('slides', { static: false }) slider: IonSlides;

  segment = 0;
  login: any;

  slideOpts = {
    allowTouchMove: false,
    autoHeight: true
  };

  requestData: any = {
    header: {},
    attachments: [],
    logs: []
  };

  attachments: any = {
    images: [],
    files: []
  };

  userList: any = [];

  lastActivity: any;

  currentStatus: any;
  currentStatusCssClass: any;

  validating: any = [];
  popupData: any = [];
  price_duplicate: any;
  selectedLineitem: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public service: PricingOutboxWorkitemService,
    public global: GlobalService,
    private alertController: AlertController,
    private modalController: ModalController,
    private actionSheetController: ActionSheetController) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.global.hideBottomTabs();
    this.route.data.subscribe(response => {
      if (response.data) {
        this.requestData = response.data;
        if (this.requestData.logs) {
          this.currentStatus = this.global.getLogTextBasedOnActivity(this.requestData.logs[this.requestData.logs.length - 1].activityText);
          this.currentStatusCssClass = this.global.getColorCssClass(this.currentStatus);
          this.lastActivity = this.requestData.logs[this.requestData.logs.length - 1].endDate;
        }
      }
    });
  }

  onBack() {
    this.router.navigate(['/main/outbox/' + this.service.parentRequestId]);
  }

  onViewFile(file) {
    this.actionSheetController.create(
      {
        header: 'Options',
        cssClass: 'attachmentsAS',
        buttons: [
          {
            text: 'Preview Attachment',
            icon: 'eye',
            cssClass: 'scanOptionAS',
            handler: () => {
              this.global.downloadFile(this.requestData.requestId, file);
            }
          }
        ]
      }
    ).then(actionSheet => actionSheet.present());
  }

  getAmount(amount) {
    return (parseFloat(amount)).toFixed(2);
  }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  truncateString(str, n) {
    if (str != undefined) return (str.length > n) ? str.substr(0, n - 1) + '...' : str;
    else str;
  }

  async showNotes(notes) {
    const alert = await this.alertController.create({
      header: 'Notes !!',
      message: notes,
      buttons: ['OK']
    });
    await alert.present();
  }

  //Pricing Cost Evaluation modal for PR00
  async getPricingByCombination(ev, item) {
    const modal = await this.modalController.create({
      component: CostingModelPage,
      componentProps: { data: item }
    });
    return await modal.present();
  }

  // Pricing line Items Validation
  checkPricingDuplicate(index, lineitem, workitem) {
    this.price_duplicate = true;
    this.validating[index] = true;
    this.selectedLineitem = lineitem;
    let items = JSON.parse(JSON.stringify(lineitem));
    this.popupData = [];
    let all_data = [];
    if (items.lineItems.PAYER && items.lineItems.MATNR && items.lineItems.COND_VALID_FROM && items.lineItems.COND_VALID_TO) {
      this.service.getPricingValidationRecords('/requestSearch/tcl', workitem.header.KSCHL,
        workitem.header.KEY_COMB, ['PRC_PROCESS'], items.lineItems, 0, 1000).subscribe((response: any) => {
          workitem.lineItems[index].lineItems.isValidate = true;
          if (response) {
            (response).forEach(val => {
              if (val.requestId != workitem.requestId) all_data.push(val);
            });
          }

          let vm = new Set(all_data);
          this.popupData = Array.from(vm);
          this.showAvailableDataInDateRange();
          this.validating[index] = false;
        }, (error) => {
          this.validating[index] = false;
          this.global.displayToastMessage(error.description + ' , Please Try again later.');
        })
    } else {
      this.validating[index] = false;
      this.global.displayToastMessage('Payer, Material and date range is required for validation.');
    }
  }

  checkDuplicationOfRecords(index) {
    this.price_duplicate = false;
    this.validating[index] = true;
    this.popupData = [];

    let fromDate: any;
    let toDate: any;
    let new_data = [];

    fromDate = new Date(this.global.getValidationDate(this.requestData.header.DATAB));
    (fromDate.setDate(fromDate.getDate() + 1));
    fromDate = fromDate.toISOString();
    toDate = new Date(this.global.getValidationDate(this.requestData.header.DATBI));
    (toDate.setDate(toDate.getDate() + 1));
    toDate = toDate.toISOString();
    if (this.requestData.header.BONEM && this.requestData.header.MATNR && this.requestData.header.DATAB && this.requestData.header.DATBI) {
      this.service.getDuplicateData('requestSearch/tcl', ["PRC_PROCESS", "REB_PROCESS", "REB_DRCT_CM", "REB_DRCT_A_NOTE", "REB_CRE_MEMO"],
        this.requestData.header.BONEM, this.requestData.header.MATNR, fromDate, toDate, 0, 1000).subscribe((response: any) => {
          if (response) {
            (response).forEach((item) => {
              if (item.header.KSCHL == 'ZSD2') {
                if (item.requestId != this.requestData.requestId) new_data.push(item);
              }
            });
            var vm = new Set(new_data);
            this.popupData = Array.from(vm);
          }
          this.requestData.lineItems[0].lineItems.isValidate = true;
          this.validating[index] = false;
          if (this.requestData.lineItems[0].lineItems.isValidate) this.showAvailableDataInDateRange();
        },
          (error) => {
            this.validating[index] = false;
            this.global.displayToastMessage('Error !! Error Occured while getting the requests.');
          }
        )
    } else this.global.displayToastMessage('Error !! Payer, Material and date range are mandatory for validation.')
  }

  async showAvailableDataInDateRange() {
    if (this.popupData.length > 0) {
      const modal = await this.modalController.create({
        component: ValidationModalPage,
        backdropDismiss: false,
        componentProps: { data: this.popupData, isLineitem: this.price_duplicate, selectedItem: this.selectedLineitem }
      });
      return await modal.present();
    } else alert('INFO !! No Duplicate Data Found.')
  }
}
