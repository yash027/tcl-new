import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PricingOutboxWorkitemsPageRoutingModule } from './pricing-outbox-workitems-routing.module';

import { PricingOutboxWorkitemsPage } from './pricing-outbox-workitems.page';
import { MatExpansionModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatExpansionModule,
    PricingOutboxWorkitemsPageRoutingModule
  ],
  declarations: [PricingOutboxWorkitemsPage]
})
export class PricingOutboxWorkitemsPageModule {}
