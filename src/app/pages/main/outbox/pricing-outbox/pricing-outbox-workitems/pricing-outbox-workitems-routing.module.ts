import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PricingOutboxWorkitemsPage } from './pricing-outbox-workitems.page';

const routes: Routes = [
  {
    path: '',
    component: PricingOutboxWorkitemsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PricingOutboxWorkitemsPageRoutingModule {}
