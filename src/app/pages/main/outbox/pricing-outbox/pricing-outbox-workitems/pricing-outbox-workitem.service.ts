import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { ActivatedRouteSnapshot } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';

@Injectable(
  {
    providedIn: 'root'
  }
)
export class PricingOutboxWorkitemService {

  requestId: any;
  parentRequestId: any;

  constructor(private http: HttpService, private globalService: GlobalService) { }

  resolve(route: ActivatedRouteSnapshot) {
    if (route.parent.routeConfig.path && route.params.requestId) {
      this.requestId = route.params.requestId;
      this.parentRequestId = route.parent.routeConfig.path;
      return this.getRequestIdDetails(route.parent.routeConfig.path, this.requestId);
    }
  }

  getRequestIdDetails(parent, requestId) {
    const url = '/requests/outbox/requestType/' + parent + '/requests/' + requestId;
    return this.http.call_GET(url);
  }

  getPricingValidationRecords(url, conditionType, keyCombination, requestType, lineitem, pageNumber, pageSize) {
    let fromDate: any;
    let toDate: any;
    fromDate = this.globalService.getValidationDate(lineitem.COND_VALID_FROM);
    fromDate = new Date(fromDate);
    (fromDate.setDate(fromDate.getDate() + 1));
    fromDate = fromDate.toISOString();
    toDate = this.globalService.getValidationDate(lineitem.COND_VALID_TO);
    toDate = new Date(toDate);
    (toDate.setDate(toDate.getDate() + 1));
    toDate = toDate.toISOString();

    let body = {
      "headerFields": {
        "KSCHL": conditionType,
        "KEY_COMB": keyCombination,
        "BONEM": lineitem.PAYER,
        "MATNR": lineitem.MATNR
      },
      "toDate": toDate,
      "fromDate": fromDate,
      "requestType": requestType
    }
    return this.http.call_POST(url + '?order=createDate?page=' + pageNumber + '&size=' + pageSize, body);
  }

  getDuplicateData(url, requestTypes, payer, material, fromDate, toDate, pageNumber, pageSize) {
    let body = {
      "headerFields": {
        "BONEM": payer,
        "MATNR": material,
      },
      "toDate": toDate,
      "fromDate": fromDate,
      "requestType": requestTypes
    }
    return this.http.call_POST('/' + url + '?order=createDate?page=' + pageNumber + '&size=' + pageSize, body);
  }
}