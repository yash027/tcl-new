import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { GlobalService } from 'src/app/services/global.service';
import { AlertController } from '@ionic/angular';
import { ignoreElements } from 'rxjs/operators';
import { GooglePlus } from '@ionic-native/google-plus/ngx';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  userDetails: any = {};

  constructor(
    private storage: StorageService, 
    public global: GlobalService, 
    private alertController: AlertController,
    private googlePlus: GooglePlus) { }

  ionViewWillEnter() {
    this.global.showBottomTabs();
    this.storage.getUser().then( user => {
      if(user && user != null){
        this.userDetails = user;
      } else {
        this.global.displayToastMessage('You need to login first.');
        this.global.gotoLogin();
      }
    });
  }

  onLogout() {
    this.alertController.create({
      header: 'Message',
      message: 'Are you sure, you want to logout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Ok',
          handler: () => {
            if(this.global.isGoogleSignIn) {
              this.googlePlus.logout();
            }
            this.global.clearUser();
            this.global.displayToastMessage('You have successfully logged out');
            this.global.gotoLogin();
          }
        }
      ]
    }).then( alert => alert.present());
  }

  ngOnInit() {
  }

}
