import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainPage } from './main.page';
import { AuthGuardService } from 'src/app/guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: MainPage,
    children: [
      {
        path: 'requests',
        loadChildren: () => import('./requests/requests.module').then(m => m.RequestsPageModule),
        canActivate: [AuthGuardService]
      },
      {
        path: 'inbox',
        loadChildren: () => import('./inbox/inbox.module').then(m => m.InboxPageModule),
        canActivate: [AuthGuardService]
      },
      {
        path: 'outbox',
        loadChildren: () => import('./outbox/outbox.module').then(m => m.OutboxPageModule),
        canActivate: [AuthGuardService]
      },
      {
        path: 'profile',
        loadChildren: () => import('./profile/profile.module').then(m => m.ProfilePageModule),
        canActivate: [AuthGuardService]
      },
      {
        path: 'reports',
        loadChildren: () => import('./reports/reports.module').then(m => m.ReportsPageModule),
        canActivate: [AuthGuardService]
      },
      {
        path: '',
        redirectTo: 'requests',
        pathMatch: 'full'
      }
    ],
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainPageRoutingModule { }
