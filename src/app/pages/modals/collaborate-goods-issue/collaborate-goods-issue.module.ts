import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CollaborateGoodsIssuePageRoutingModule } from './collaborate-goods-issue-routing.module';

import { CollaborateGoodsIssuePage } from './collaborate-goods-issue.page';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgSelectModule,
    CollaborateGoodsIssuePageRoutingModule
  ],
  declarations: [CollaborateGoodsIssuePage]
})
export class CollaborateGoodsIssuePageModule {}
