import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CollaborateGoodsIssuePage } from './collaborate-goods-issue.page';

const routes: Routes = [
  {
    path: '',
    component: CollaborateGoodsIssuePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CollaborateGoodsIssuePageRoutingModule {}
