import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { LoadingController, ModalController, NavParams } from '@ionic/angular';
import { GlobalService } from 'src/app/services/global.service';
import { GoodsIssueService } from '../../main/requests/goods-issue/goods-issue.service';

@Component({
  selector: 'app-stock-check-modal',
  templateUrl: './stock-check-modal.page.html',
  styleUrls: ['./stock-check-modal.page.scss'],
})
export class StockCheckModalPage implements OnInit {

  materialDetails: any = {};
  extractedTime: any = '';
  stockDetails: any = [];
  renderedDetails: any = [];

  sortedColumn = '';

  sortingDetails = {
    storageLocationQuantity: {
      ascending: false,
      descending: false
    },
    batchQuantity: {
      ascending: false,
      descending: false
    },
    specialStockQuantity: {
      ascending: false,
      descending: false
    }
  }

  constructor(
    public modalController: ModalController,
    private navParams: NavParams,
    private service: GoodsIssueService,
    private loadingController: LoadingController,
    private global: GlobalService
  ) { }

  ionViewWillEnter() {
    this.materialDetails = this.navParams.data.data;
    this.getStockDetails();
  }

  onSortColumn = function (columnName) {
    if (this.sortedColumn === columnName) {
      this.sortingDetails[this.sortedColumn].ascending = !this.sortingDetails[this.sortedColumn].ascending;
      this.sortingDetails[this.sortedColumn].descending = !this.sortingDetails[this.sortedColumn].descending;
    } else {
      if (this.sortedColumn === '') {
        this.sortedColumn = columnName;
        this.sortingDetails[this.sortedColumn].ascending = true;
      } else {
        this.sortingDetails[this.sortedColumn].ascending = false;
        this.sortingDetails[this.sortedColumn].descending = false;
        this.sortedColumn = columnName;
        this.sortingDetails[this.sortedColumn].ascending = true;
      }
    }
    switch (this.sortedColumn) {
      case 'storageLocationQuantity':
        if (this.sortingDetails.storageLocationQuantity.ascending) {
          this.renderedDetails = this.stockDetails.sort(function (a, b) {
            return a.SLOC_QTY - b.SLOC_QTY;
          });
        } else {
          this.renderedDetails = this.stockDetails.sort(function (a, b) {
            return b.SLOC_QTY - a.SLOC_QTY;
          });
        }
        break;
      case 'batchQuantity':
        if (this.sortingDetails.batchQuantity.ascending) {
          this.renderedDetails = this.stockDetails.sort(function (a, b) {
            return a.BATCH_QTY - b.BATCH_QTY;
          });
        } else {
          this.renderedDetails = this.stockDetails.sort(function (a, b) {
            return b.BATCH_QTY - a.BATCH_QTY;
          });
        }
        break;
      case 'specialStockQuantity':
        if (this.sortingDetails.specialStockQuantity.ascending) {
          this.renderedDetails = this.stockDetails.sort(function (a, b) {
            return a.MKOL_QTY - b.MKOL_QTY;
          });
        } else {
          this.renderedDetails = this.stockDetails.sort(function (a, b) {
            return b.MKOL_QTY - a.MKOL_QTY;
          });
        }
        break;
      default:
        console.log('Nothing Selected for Sort');
    }
  }

  getStockDetails() {
    this.loadingController.create(
      {
        message: 'Please wait while details are getting loaded...'
      }
    ).then(loader => {
      loader.present();
      this.service.getStockDetails(
        {
          plant: this.materialDetails.plant,
          mat_NO: this.materialDetails.materialNumber
        }
      ).subscribe((response: any) => {
        loader.dismiss();
        if (response.length > 0) {
          this.stockDetails = response;
          this.renderedDetails = response;
        } else {
          this.global.displayToastMessage('Stock details not available for this material.');
          loader.dismiss();
          this.modalController.dismiss();
        }
        this.extractedTime = new Date().toLocaleString();
      }, error => {
        this.global.displayToastMessage('Error occurred while fetching Stock Details, Please try after some time');
        loader.dismiss();
        this.modalController.dismiss();
      });
    });

  }

  ngOnInit() {
  }

}
