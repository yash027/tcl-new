import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StockCheckModalPage } from './stock-check-modal.page';

const routes: Routes = [
  {
    path: '',
    component: StockCheckModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StockCheckModalPageRoutingModule {}
