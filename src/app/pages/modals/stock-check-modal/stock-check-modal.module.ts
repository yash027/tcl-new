import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StockCheckModalPageRoutingModule } from './stock-check-modal-routing.module';

import { StockCheckModalPage } from './stock-check-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StockCheckModalPageRoutingModule
  ],
  declarations: [StockCheckModalPage]
})
export class StockCheckModalPageModule {}
