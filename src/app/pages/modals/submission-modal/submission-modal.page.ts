import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { Capacitor, Plugins, CameraSource, CameraResultType } from '@capacitor/core';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-submission-modal',
  templateUrl: './submission-modal.page.html',
  styleUrls: ['./submission-modal.page.scss'],
})
export class SubmissionModalPage implements OnInit {

  currentStep: any;
  totalAmount: any;
  requestType: any;
  showInvoice: boolean;

  submissionData: any = {
    description: '',
    attachments: [],
    images: [],
    INV_REF: '',
    INV_REF_DATE: ''
  };

  constructor(private navParams: NavParams, 
              public modalController: ModalController, 
              private global: GlobalService) { }

  ionViewWillEnter() {
    this.submissionData = {
      description: '',
      attachments: [],
      images: [],
      INV_REF: '',
      INV_REF_DATE: ''
    };
    this.totalAmount = this.navParams.get('data');
    this.requestType = this.navParams.get('requestType');
    this.currentStep = this.navParams.get('currentStep');
    const isReject = this.requestType.includes('Reject');
    if (this.currentStep === '2' && isReject === false) {
      this.showInvoice = true;
    }
    if (this.requestType.includes('Approved')) {
      this.requestType = this.requestType.replace('Approved', 'Approve');
    } else if (this.requestType.includes('Rejected')) {
      this.requestType = this.requestType.replace('Rejected', 'Reject');
    }
  }

  ngOnInit() {
  }

  chooseFiles() {
    document.getElementById('fileUpload').click();
  }

  onSelectFile(event) {
    const formData = event.target.files;
    for (let key in formData) {
      if (key !== 'length' && key !== 'item') {
        this.submissionData.attachments.push(formData[key]);
      }
    }
  }

  onTakeImage() {
    if (!Capacitor.isPluginAvailable('Camera')) {
      this.global.displayToastMessage(
        'Unable To Open Camera');
      return;
    }
    Plugins.Camera.getPhoto({
      quality: 60,
      source: CameraSource.Camera,
      height: 400,
      width: 300,
      correctOrientation: true,
      resultType: CameraResultType.DataUrl
    })
      .then(image => {
        const blobImg = this.global.dataURItoBlob(image.dataUrl);
        const img = {
          name: 'Image-' + new Date().toISOString(),
          image: blobImg
        }
        this.submissionData.images.push(img);
      })
      .catch(error => {
        return false;
      });
  }

  onRemoveAttachment(index) {
    this.submissionData.attachments.splice(index, 1);
  }

  onRemoveImage(index) {
    this.submissionData.images.splice(index, 1);
  }

  getBytesConversion(bytes) {
    const size = this.global.getBytesConversion(bytes, 2);
    return size;
  }

  onSubmit() {
    this.modalController.dismiss(this.submissionData);
  }

  getTotalAmount(amount) {
    if (amount) {
      return Number(amount).toFixed(2);
    }
  }

}
