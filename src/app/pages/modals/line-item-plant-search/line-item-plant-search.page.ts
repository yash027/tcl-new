import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource, MatTabBody } from '@angular/material';
import { NavParams, ModalController } from '@ionic/angular';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-line-item-plant-search',
  templateUrl: './line-item-plant-search.page.html',
  styleUrls: ['./line-item-plant-search.page.scss'],
})
export class LineItemPlantSearchPage implements OnInit {

  @ViewChild(MatSort, { static: false }) sort: MatSort;

  dataHeaders: any;
  data: any = [];
  tableData: MatTableDataSource<any> = new MatTableDataSource<any>();

  selector: any;

  searchValue: any = '';

  constructor(
    private navParams: NavParams,
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  applyFilter(filterValue: string) {
    let filterData = [];
    this.tableData = new MatTableDataSource([]);
    this.data.filter((v) => {
      switch(this.selector) {
        case 'plant' :
          if(((v.plant).toLowerCase()).includes((filterValue).toLowerCase()) || ((v.plantDescription).toLowerCase()).includes((filterValue).toLowerCase())) {
            filterData.push(v);
          }
          break;
        case 'storage' :
          if(((v.LGORT_D).toLowerCase()).includes((filterValue).toLowerCase()) || ((v.STRG).toLowerCase()).includes((filterValue).toLowerCase())) {
            filterData.push(v);
          }
          break;
        case 'batch' :
          if(((v.materialNumber).toLowerCase()).includes((filterValue).toLowerCase()) || ((v.batchNumber).toLowerCase()).includes((filterValue).toLowerCase())) {
            filterData.push(v);
          }
          break;
        case 'storageFromLineItem' :
          if(((v.storageLocation).toLowerCase()).includes((filterValue).toLowerCase()) || ((v.storageDescription).toLowerCase()).includes((filterValue).toLowerCase())) {
            filterData.push(v);
          }
          break;
        case 'specialStock' :
          if(((v.SOBKZ).toLowerCase()).includes((filterValue).toLowerCase()) || ((v.SOTXT).toLowerCase()).includes((filterValue).toLowerCase())) {
            filterData.push(v);
          }
          break;
      }
      
    });
    this.tableData = new MatTableDataSource(filterData);
  }

  ionViewWillEnter() {
    const data = this.navParams.get('data');
    if (data) {
      this.selector = data.selector;
      this.data = data.data;
      this.tableData = new MatTableDataSource(data.data);
    }
    switch(this.selector) {
      case 'plant' :
        this.dataHeaders = ['plant', 'plantDescription'];
        break;
      case 'storage' :
        this.dataHeaders = ['storageLocation', 'storageDescription'];
        break;
      case 'batch' :
        this.dataHeaders = ['materialNumber', 'batchNumber'];
        break;
      case 'storageFromLineItem' :
        this.dataHeaders = ['storageLocation', 'storageDescription'];
        break;
      case 'specialStock' :
        this.dataHeaders = ['SOBKZ', 'SOTXT'];
        break;
    }
  }

  getSelectedListItem(row) {
    switch(this.selector) {
      case 'plant':
        this.modalController.dismiss({
          data: {
            WERKS: row.plant,
            PLANT_DES: row.plantDescription
          }
        });
        break;
      case 'storage':
        this.modalController.dismiss({
          data: {
            storageLocation: row.LGORT_D,
            storageLocationDescription: row.STRG
          }
        });
        break;
      case 'batch':
        this.modalController.dismiss({
          data: {
            CHARG: row.batchNumber
          }
        });
        break;
      case 'storageFromLineItem':
        this.modalController.dismiss({
          data: {
            storageLocation: row.storageLocation,
            storageLocationDescription: row.storageDescription
          }
        });
        break;
      case 'specialStock':
        this.modalController.dismiss({
          data: {
            SOBKZ: row.SOBKZ,
            SOTXT: row.SOTXT
          }
        });
        break;
    }
  }

}
