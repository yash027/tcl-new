import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LineItemPlantSearchPage } from './line-item-plant-search.page';

const routes: Routes = [
  {
    path: '',
    component: LineItemPlantSearchPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LineItemPlantSearchPageRoutingModule {}
