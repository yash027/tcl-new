import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LineItemPlantSearchPageRoutingModule } from './line-item-plant-search-routing.module';

import { LineItemPlantSearchPage } from './line-item-plant-search.page';
import { MatSortModule, MatTableModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatSortModule,
    MatTableModule,
    LineItemPlantSearchPageRoutingModule
  ],
  declarations: [LineItemPlantSearchPage]
})
export class LineItemPlantSearchPageModule {}
