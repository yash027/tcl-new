import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DepartmentSearchPageRoutingModule } from './department-search-routing.module';

import { DepartmentSearchPage } from './department-search.page';
import { MatSortModule, MatTableModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatSortModule,
    MatTableModule,
    DepartmentSearchPageRoutingModule
  ],
  declarations: [DepartmentSearchPage]
})
export class DepartmentSearchPageModule {}
