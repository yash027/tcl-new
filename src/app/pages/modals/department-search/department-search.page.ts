import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { IonSearchbar, NavParams, ModalController } from '@ionic/angular';
import { GlobalService } from 'src/app/services/global.service';
import { GoodsIssueService } from '../../main/requests/goods-issue/goods-issue.service';

@Component({
  selector: 'app-department-search',
  templateUrl: './department-search.page.html',
  styleUrls: ['./department-search.page.scss'],
})
export class DepartmentSearchPage implements OnInit {

  @ViewChild(MatSort, { static: false }) sort: MatSort;

  @ViewChild('searchbar', { static: false }) searchBar: IonSearchbar;

  pageNumber = 0;
  pageSize = 100;
  searchValue = '';

  isLoadedFirstTime: boolean = false;
  selectedPlant: any = '';

  tableData: MatTableDataSource<any> = new MatTableDataSource<any>();
  dataHeaders: any = ['DEPID', 'DEPDS'];

  displayHeaderFields: any = {
    DEPID: 'Department',
    DEPDS: 'Department Description'
  }

  constructor(
    private navParams: NavParams,
    public modalController: ModalController,
    private service: GoodsIssueService,
    private global: GlobalService
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.selectedPlant = this.navParams.get('data');
    this.getDataForInstance();
  }

  applyFilter(filterValue: string) {
    this.searchValue = filterValue.trim().toLowerCase();
    this.tableData.filter = filterValue;
  }

  getSelectedListItem(row) {
    this.modalController.dismiss({ data: row, instance: 'Departments' });
  }

  getDataForInstance() {
    this.service.getDynamicInstanceData(
      'Departments', ['WERKS'], this.selectedPlant, this.pageNumber, this.pageSize
    ).subscribe((allDepartments: any) => {
      if(allDepartments.Content.length > 0) {
        this.isLoadedFirstTime = true;
        this.tableData = new MatTableDataSource(allDepartments.Content);
        this.tableData.sort = this.sort;
        if (this.isLoadedFirstTime) {
          this.searchBar.setFocus();
        }
      } else {
        this.global.displayToastMessage('No departments found for selected Plant');
        this.modalController.dismiss();
      }
    }, error => {
      this.global.displayToastMessage('Problem occurred while fetching Departments, Please try after some time');
      this.modalController.dismiss();
    });
  }

}
