import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SearchTasklistPage } from './search-tasklist.page';
import { MatSortModule, MatTableModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: SearchTasklistPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatSortModule,
    MatTableModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SearchTasklistPage]
})
export class SearchTasklistPageModule {}
