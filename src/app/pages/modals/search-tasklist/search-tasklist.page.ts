import { Component, OnInit, ViewChild } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-search-tasklist',
  templateUrl: './search-tasklist.page.html',
  styleUrls: ['./search-tasklist.page.scss'],
})
export class SearchTasklistPage implements OnInit {

  @ViewChild(MatSort, { static: false }) sort: MatSort;

  tableDataSource: any = [];
  displayedColumns: any = [];
  fieldType: any;

  constructor(private navParams: NavParams, public modalController: ModalController) { }

  ngOnInit() {
  }

  applyFilter(filterValue: string) {
    this.tableDataSource.filter = filterValue.trim().toLowerCase();
  }

  ionViewWillEnter() {
    if (this.navParams.get('data')) {
      this.fieldType = this.navParams.get('field');
      if (this.fieldType === 'taskGroup') {
        this.displayedColumns = ['TG_CODE', 'TG_DES'];
      } else if (this.fieldType === 'sor') {
        this.displayedColumns = ['SOR', 'SOR_DES', 'UNIT', 'PRICE'];
      }
      this.tableDataSource = new MatTableDataSource(this.navParams.get('data'));
      this.tableDataSource.sort = this.sort;
    }
  }

  getSelectedListItem(value) {
    this.modalController.dismiss(value);
  }

}
