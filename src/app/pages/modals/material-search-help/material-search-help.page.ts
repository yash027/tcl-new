import { Component, OnInit, ViewChild } from '@angular/core';
import { GoodsIssueService } from '../../main/requests/goods-issue/goods-issue.service';
import { MatTableDataSource, MatSort } from '@angular/material';
import { IonSearchbar, ModalController, NavParams, AlertController } from '@ionic/angular';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-material-search-help',
  templateUrl: './material-search-help.page.html',
  styleUrls: ['./material-search-help.page.scss'],
})
export class MaterialSearchHelpPage implements OnInit {

  @ViewChild(MatSort, { static: false }) sort: MatSort;

  @ViewChild('searchbar', { static: false }) searchBar: IonSearchbar;

  dataHeaders: any = [];
  tableData: MatTableDataSource<any> = new MatTableDataSource<any>();

  pageNumber = 0;
  pageSize = 100;
  searchValue = '';
  materialCode = '';
  materialDescription = '';
  isLoadedFirstTime: boolean = false;

  searchCategory: any = '';

  searchParam: any = 'materialCode';

  constructor(
    private service: GoodsIssueService,
    public modalController: ModalController,
    private navParams: NavParams,
    private global: GlobalService,
    private alertController: AlertController) { }

  ionViewWillEnter() {
    if (this.navParams.get('data')) {
      this.searchCategory = this.navParams.get('data');
    }
    if (this.searchCategory === 'Material') {
      this.dataHeaders = ['materailCode', 'materialDescription'];
      this.getMaterialData();
    } else if (this.searchCategory === 'Order Number') {
      this.dataHeaders = ['orderNumber', 'orderDescription'];
      this.getOrderDetails();
    }
  }

  ngOnInit() {
  }

  onSelectSearchParam() {
    this.alertController.create(
      {
        header: 'Search by',
        inputs: [
          {
            label: 'Material Code',
            type: 'radio',
            name: 'radioSelection',
            value: 'materialCode'
          },
          {
            label: 'Material Desc.',
            type: 'radio',
            name: 'radioSelection',
            value: 'materialDescription'
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel'
          },
          {
            text: 'Ok',
            handler: (inputData) => {
              this.searchParam = inputData;
              this.onGetFocused(inputData);
            }
          }
        ]
      }
    ).then(alert => alert.present());
  }

  applyFilter(filterValue: string) {
    this.searchValue = filterValue.trim().toLowerCase();
    if (this.searchCategory === 'Material') {
      this.getMaterialData();
    } else if (this.searchCategory === 'Order Number') {
      this.getOrderDetails();
    }
  }

  applyFilterForMaterial(filterValue: string, type) {
    if (type === 'materialCode') {
      this.materialCode = filterValue.trim().toLowerCase();
    } else if (type === 'materialDescription') {
      this.materialDescription = filterValue.trim().toLowerCase();
    }
    this.getMaterialData();
  }

  onGetFocused(type) {
    if (this.searchCategory === 'Material') {
      if (type === 'materialCode') {
        this.materialDescription = '';
      } else if (type === 'materialDescription') {
        this.materialCode = '';
      }
    }
  }

  getMaterialData() {
    this.service.getMaterialData(this.materialDescription, this.materialCode).subscribe((response: any) => {
      this.isLoadedFirstTime = true;
      this.tableData = new MatTableDataSource(response);
      this.tableData.sort = this.sort;
    });
  }

  getOrderDetails() {
    this.service.getDynamicInstanceData('PlantBasedCompanyCode', ['Plant'], this.service.plantBasedMaterialValue, 0, 100)
      .subscribe((response: any) => {
        if (response.Content.length > 0) {
          let companyCode = response.Content[0].CompanyCode;
          this.service.getOrderData(this.searchValue, companyCode).subscribe((response: any) => {
            this.isLoadedFirstTime = true;
            this.tableData = new MatTableDataSource(response);
            this.tableData.sort = this.sort;
            if (this.isLoadedFirstTime) {
              this.searchBar.setFocus();
            }
          },
            error => {
              this.global.displayToastMessage('Error occured while fetching Order details. Please try after some time');
            });
        } else {
          this.global.displayToastMessage('Company Code not available for selected Plant');
        }
      }, error => {
        this.global.displayToastMessage('Error occurred while fetching company code. Please try after some time');
      });
  }

  getSelectedListItem(row) {
    if (this.searchCategory === 'Material') {
      if (this.service.movementTypeinHeader === '221') {
        this.service.getDynamicInstanceData('Msprvalues', ['MATNR'], row.materailCode, 0, 100).subscribe((response: any) => {
          var sortedWBSDetails = {}, isWBSAvailable = false;
          if (response.Content.length > 0) {
            for (var i = 0; i < response.Content.length; i++) {
              if (response.Content[i].POSID === this.service.wbsElementInHeader) {
                sortedWBSDetails = response.Content[i];
                isWBSAvailable = true;
                break;
              }
            }
            if (isWBSAvailable) {
              let unitPrice, toAllow = false;
              let plantDetails = row.plantDetails.find(element => element.plant === this.service.plantBasedMaterialValue);
              if (plantDetails.priceControl === 'V') {
                unitPrice = parseFloat(plantDetails.movPrice);
                toAllow = true;
              } else if (plantDetails.priceControl === 'S') {
                unitPrice = parseFloat(plantDetails.standardPrice);
                toAllow = true;
              } else {
                // change toAllow to false and remove unitPrice = 0 to show price Control logic
                toAllow = true;
                unitPrice = 0;
              }
              if (toAllow) {
                this.modalController.dismiss({
                  data: {
                    MATNR: row.materailCode,
                    MAT_DES: row.materialDescription,
                    MEINS: row.umo,
                    plantDetails: row.plantDetails,
                    storage: row.storage,
                    unitPrice: unitPrice
                  }
                });
              } else {
                this.alertController.create(
                  {
                    header: 'Message',
                    message: 'Price Control value is unavailable. Please re-input material having Price Control',
                    buttons: [{
                      text: 'Ok',
                      role: 'ok'
                    }]
                  }
                ).then(alert => alert.present());
              }
            } else {
              this.global.displayToastMessage('No stock is available for selected material');
            }
          } else {
            this.global.displayToastMessage('No stock is available for selected material');
          }
        },
          error => {
            this.global.displayToastMessage('Error occurred while fetching data, Please try again later.');
          });
      } else {
        let unitPrice, toAllow = false;
        let plantDetails = row.plantDetails.find(element => element.plant === this.service.plantBasedMaterialValue);
        if (plantDetails.priceControl === 'V') {
          unitPrice = parseFloat(plantDetails.movPrice);
          toAllow = true;
        } else if (plantDetails.priceControl === 'S') {
          unitPrice = parseFloat(plantDetails.standardPrice);
          toAllow = true;
        } else {
          // change toAllow to false and remove unitPrice = 0 to show price Control logic
          toAllow = true;
          unitPrice = 0;
        }
        if (toAllow) {
          this.modalController.dismiss({
            data: {
              MATNR: row.materailCode,
              MAT_DES: row.materialDescription,
              MEINS: row.umo,
              plantDetails: row.plantDetails,
              storage: row.storage,
              unitPrice: unitPrice
            }
          });
        } else {
          this.alertController.create(
            {
              header: 'Message',
              message: 'Price Control value is unavailable. Please re-input material having Price Control',
              buttons: [{
                text: 'Ok',
                role: 'ok'
              }]
            }
          ).then(alert => alert.present());
        }
      }
    } else if (this.searchCategory === 'Order Number') {
      this.modalController.dismiss({
        data: {
          AUFNR: row.orderNumber,
          ORDER_DES: row.orderDescription
        }
      });
    }
  }

}
