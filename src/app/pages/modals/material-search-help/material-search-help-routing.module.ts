import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MaterialSearchHelpPage } from './material-search-help.page';

const routes: Routes = [
  {
    path: '',
    component: MaterialSearchHelpPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MaterialSearchHelpPageRoutingModule {}
