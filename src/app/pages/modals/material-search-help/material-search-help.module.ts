import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MaterialSearchHelpPageRoutingModule } from './material-search-help-routing.module';

import { MaterialSearchHelpPage } from './material-search-help.page';
import { MatSortModule, MatTableModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatSortModule,
    MatTableModule,
    MaterialSearchHelpPageRoutingModule
  ],
  declarations: [MaterialSearchHelpPage]
})
export class MaterialSearchHelpPageModule {}
