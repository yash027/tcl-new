import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-image',
  templateUrl: './image.page.html',
  styleUrls: ['./image.page.scss'],
})
export class ImagePage implements OnInit {

  imageUrl: any;
  fileUrl: SafeResourceUrl;
  isImage: any;

  constructor(private navParams: NavParams, public modalController: ModalController, public sanitizer: DomSanitizer) { }

  ionViewWillEnter() {
    if(this.navParams.get('data')) {
      this.isImage = this.navParams.get('isImage');
      if(this.isImage) {
        this.imageUrl = this.navParams.get('data');
      } else {
        // const encodedURI = encodeURIComponent(this.navParams.get('data'));
        // this.fileUrl = 'https://docs.google.com/gview?url=' + encodedURI +' &embedded=true';
        this.fileUrl = this.navParams.get('data');
      }
    }
  }

  ngOnInit() {
  }

}
