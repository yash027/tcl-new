import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { ImagePage } from './image/image.page';
import { DomSanitizer } from '@angular/platform-browser';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-attachments-modal',
  templateUrl: './attachments-modal.page.html',
  styleUrls: ['./attachments-modal.page.scss'],
})
export class AttachmentsModalPage implements OnInit {

  attachments: any = [];
  requestId: any;

  constructor(public modalController: ModalController, 
              private navParams: NavParams,
              public global: GlobalService,
              private sanitizer: DomSanitizer) { }

  ionViewWillEnter() {
    if (this.navParams.get('data')) {
      const data = this.navParams.get('data');
      this.attachments = data.attachments;
      this.requestId = data.requestId;
    }
  }

  ngOnInit() {
  }

  onAttachmentOpen(attachment) {
    const token = (sessionStorage.getItem('accessToken')).trim();
    let isImage;
    const url = this.sanitizer.bypassSecurityTrustResourceUrl('https://tatachemicalsdev.smartdocsnow.com/smartportal-server/api/requests/inbox/filedownload?archiveId=' + attachment.archiveId + '&requestId=' + this.requestId + '&archiveDocId=' + attachment.archiveDocId + '&attachment=false&jwtToken=' + token);
    if(attachment.fileType == 'jpg' || attachment.fileType == 'jpeg' || attachment.fileType == 'png') {
      isImage = true;
    } else {
      isImage = false;
    }
    this.modalController.create({
      component: ImagePage,
      componentProps: {
        data: url,
        isImage: isImage
      }
    }).then(
      modal => modal.present()
    )
  }

}