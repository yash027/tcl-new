import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ValidationModalPage } from './validation-modal.page';

const routes: Routes = [
  {
    path: '',
    component: ValidationModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ValidationModalPageRoutingModule {}
