import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ValidationModalPageRoutingModule } from './validation-modal-routing.module';

import { ValidationModalPage } from './validation-modal.page';
import { MatSortModule } from '@angular/material/sort';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatSortModule,
    ValidationModalPageRoutingModule
  ],
  declarations: [ValidationModalPage]
})
export class ValidationModalPageModule { }
