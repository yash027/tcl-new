import { GlobalService } from './../../../services/global.service';
import { ModalController } from '@ionic/angular';
import { Component, Input, OnInit } from '@angular/core';
import { Sort } from '@angular/material/sort';

@Component({
  selector: 'app-validation-modal',
  templateUrl: './validation-modal.page.html',
  styleUrls: ['./validation-modal.page.scss'],
})
export class ValidationModalPage implements OnInit {

  @Input() data: any;
  @Input() isLineitem: any;
  @Input() selectedItem: any;

  showDatabyPayer = [];
  constructor(private modalCtrl: ModalController, public global: GlobalService) { }

  ngOnInit() {
  }

  closeModal() {
    this.modalCtrl.dismiss();
  }

  ionViewWillEnter() {
    this.data = this.data.slice();
    if (this.selectedItem) this.getSelectedLineitems();
  }

  getSelectedLineitems() {
    for (let i = 0; i < this.data.length; i++) {
      for (let j = 0; j < this.data[i].lineItems.length; j++) {
        if (this.selectedItem.lineItems.PAYER === this.data[i].lineItems[j].lineItems.PAYER) {
          this.showDatabyPayer.push(this.data[i].lineItems[j].lineItems);
          break;
        }
      }
    }
  }

  getAmount(amount) {
    return (parseFloat(amount)).toFixed(2);
  }

  sortData(sort: Sort) {
    const newdata = this.data.slice();
    if (!sort.active || sort.direction === '') {
      this.data = newdata;
      return;
    }

    this.data = newdata.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'createdDate': return this.compare(a.createdDate, b.createdDate, isAsc);
        default: return 0;
      }
    });
  }

  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

}
