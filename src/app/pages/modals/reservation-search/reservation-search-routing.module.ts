import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReservationSearchPage } from './reservation-search.page';

const routes: Routes = [
  {
    path: '',
    component: ReservationSearchPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReservationSearchPageRoutingModule {}
