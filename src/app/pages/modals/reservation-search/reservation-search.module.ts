import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReservationSearchPageRoutingModule } from './reservation-search-routing.module';

import { ReservationSearchPage } from './reservation-search.page';
import { MatSortModule, MatTableModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatSortModule,
    MatTableModule,
    ReservationSearchPageRoutingModule
  ],
  declarations: [ReservationSearchPage]
})
export class ReservationSearchPageModule {}
