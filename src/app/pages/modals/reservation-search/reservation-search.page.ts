import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, IonSearchbar } from '@ionic/angular';
import { GoodsIssueService } from '../../main/requests/goods-issue/goods-issue.service';
import { GlobalService } from 'src/app/services/global.service';
import { MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-reservation-search',
  templateUrl: './reservation-search.page.html',
  styleUrls: ['./reservation-search.page.scss'],
})
export class ReservationSearchPage implements OnInit {

  @ViewChild(MatSort, { static: false }) sort: MatSort;

  @ViewChild('searchbar', { static: false }) searchBar: IonSearchbar;

  dataHeaders: any = ['reservationNumber', 'mvtType'];
  tableData: MatTableDataSource<any> = new MatTableDataSource<any>();

  searchValue: any = '';

  constructor(
    public modalController: ModalController,
    private service: GoodsIssueService,
    private global: GlobalService
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getReservationData();
  }

  applyFilter(filterValue: string) {
    this.searchValue = filterValue.trim().toLowerCase();
    this.getReservationData();
  }

  getReservationData() {
    this.service.getReservationData(this.searchValue).subscribe((response: any) => {
      this.tableData = new MatTableDataSource(response);
    }, error => {
      this.global.displayToastMessage('Unable to fetch Reservation details');
    });
  }

  getSelectedListItem(row) {
    this.modalController.dismiss({data: row});
  }

}
