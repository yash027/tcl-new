import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { SearchHelpPage } from '../search-help/search-help.page';

@Component({
  selector: 'app-gifilter-module',
  templateUrl: './gifilter-module.page.html',
  styleUrls: ['./gifilter-module.page.scss'],
})
export class GIFilterModulePage implements OnInit {

  searchParams: any = {
    creator: '',
    requestType: ["GI_PROCESS"],
    requestId: '',
    createdDate: '',
    reservationNumber: '',
    workflowStatus: '',
    movementType: '',
    movementTypeDescription: '',
    plant: '',
    plantDescription: '',
    department: '',
    departmentDescription: ''
  };

  constructor(
    public modalController: ModalController,
    private navParams: NavParams
  ) { }

  ionViewWillEnter() {
    this.searchParams = this.navParams.data;
  }

  ngOnInit() {
  }

  openSearchHelp(instanceType) {
    this.modalController.create(
      {
        component: SearchHelpPage,
        componentProps: {
          data: instanceType
        }
      }
    ).then(modal => {
      modal.present();
      modal.onDidDismiss().then(data => {
        data = data.data;
        if (data !== undefined) {
          this.setDataToHeader(data);
        }
      });
    });
  }

  setDataToHeader(data) {
    switch (data.instance) {
      case 'MovementTypes':
        this.searchParams.movementType = data.data.BWART;
        this.searchParams.movementTypeDescription = data.data.BTEXT;
        break;
      case 'Plant':
        this.searchParams.plant = data.data.WERKS_D;
        this.searchParams.plantDescription = data.data.NAME1;
        break;
      case 'Departments':
        this.searchParams.department = data.data.DEPID;
        this.searchParams.departmentDescription = data.data.DEPDS;
        break;
    }
  }

  onSave() {
    this.modalController.dismiss({ data: this.searchParams });
  }

}
