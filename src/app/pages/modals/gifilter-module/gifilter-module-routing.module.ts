import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GIFilterModulePage } from './gifilter-module.page';

const routes: Routes = [
  {
    path: '',
    component: GIFilterModulePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GIFilterModulePageRoutingModule {}
