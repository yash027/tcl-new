import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GIFilterModulePageRoutingModule } from './gifilter-module-routing.module';

import { GIFilterModulePage } from './gifilter-module.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GIFilterModulePageRoutingModule
  ],
  declarations: [GIFilterModulePage]
})
export class GIFilterModulePageModule {}
