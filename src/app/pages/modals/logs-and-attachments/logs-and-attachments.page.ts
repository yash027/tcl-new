import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-logs-and-attachments',
  templateUrl: './logs-and-attachments.page.html',
  styleUrls: ['./logs-and-attachments.page.scss'],
})
export class LogsAndAttachmentsPage implements OnInit {

  requestData: any = {};
  segmentValue: any = 'logs';

  attachments: any = [];
  images: any = [];

  constructor(public modalController: ModalController,
              private navParams: NavParams,
              public global: GlobalService) { }

  ionViewWillEnter() {
    this.requestData =  this.navParams.get('data');
  }

  onAttachmentOpen(attachment) {
    if(attachment.url) {
      this.global.openDocumentInViewer(attachment.url); 
    } else {
      this.global.displayToastMessage('URL Not Available');
    }
  }
  
  ngOnInit() {
  }

}
