import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-change-document-history',
  templateUrl: './change-document-history.page.html',
  styleUrls: ['./change-document-history.page.scss'],
})
export class ChangeDocumentHistoryPage implements OnInit {

  data: any = {};

  filedDetails: any = {
    property: [],
    values: []
  };

  displayedColumns: any = [];
  isMMG = false;

  constructor(private navParams: NavParams, public modalController: ModalController) { }

  ionViewWillEnter() {
    this.data = this.navParams.get('data');
    if (this.data.requestType == 'MM_CHANGE') { this.displayedColumns = ['order', 'olddata', 'changeddata', 'by']; this.isMMG = true; }
    else this.displayedColumns = ['order', 'to', 'from', 'by'];
    let alldata = this.data.filedDetails;
    for (let key in alldata) {
      if (alldata[key]) {
        this.filedDetails.property.push(key);
        this.filedDetails.values.push(alldata[key]);
      }
    }
    if (this.isMMG) {
      this.filedDetails.property.forEach((element, index) => {
        if (element == 'MATKL') this.filedDetails.property[index] = 'MaterialGroup';
        if (element == 'SPART') this.filedDetails.property[index] = 'Division';
        if (element == 'MSEHL1') this.filedDetails.property[index] = 'UnitOfMeasureDescription';
        if (element == 'MBBEZ') this.filedDetails.property[index] = 'IndustrySectorDesc';
        if (element == 'MAKTX') this.filedDetails.property[index] = 'MaterialDescription';
        if (element == 'MTART') this.filedDetails.property[index] = 'MaterailType';
        if (element == 'TXZ01') this.filedDetails.property[index] = 'MaterialGroupDescription';
        if (element == 'MEINS') this.filedDetails.property[index] = 'UnitOfMeasure';
        if (element == 'VTEXT1') this.filedDetails.property[index] = 'DivisionDesc';
        if (element == 'LBTXT') this.filedDetails.property[index] = 'laboffice';
        if (element == 'MATKL') this.filedDetails.property[index] = 'LabourDescription';
        if (element == 'BASIC_TEXT') this.filedDetails.property[index] = 'BasicText';
        if (element == 'BRGEW') this.filedDetails.property[index] = 'GrossWeight';
        if (element == 'NTGEW') this.filedDetails.property[index] = 'NetWeight';
        if (element == 'PRODH_D') this.filedDetails.property[index] = 'ProductHireachy';
        if (element == 'TAXKM') this.filedDetails.property[index] = 'TaxClassificationCode';
        if (element == 'TAXIB') this.filedDetails.property[index] = 'TaxIndicator';
        if (element == 'MSEHL') this.filedDetails.property[index] = 'SalesText';
        if (element == 'STGMA') this.filedDetails.property[index] = 'MaterailStatisticsGroup';
        if (element == 'MTPOS_MARA') this.filedDetails.property[index] = 'ItemCategoryGroup';
        if (element == 'BEZEI') this.filedDetails.property[index] = 'ItemCategoryGroupDesc';
        if (element == 'GEN_ITEM') this.filedDetails.property[index] = 'GeneralItemCategoryGroup';
        if (element == 'CHCK_GROUP') this.filedDetails.property[index] = 'AvailabilityCheck';
        if (element == 'TRAGR') this.filedDetails.property[index] = 'TransferGroup';
        if (element == 'VTEXT5') this.filedDetails.property[index] = 'TransferGroupDesc';
        if (element == 'LADGR') this.filedDetails.property[index] = 'LoadingGroup';
        if (element == 'VTEXT6') this.filedDetails.property[index] = 'LoadingGroupDesc';
        if (element == 'PRCTR') this.filedDetails.property[index] = 'ProfitCenter';
        if (element == 'PRC_TXT') this.filedDetails.property[index] = 'ProfitCenterDesc';
        if (element == 'PURCHAS_GROUP') this.filedDetails.property[index] = 'PurchaseGroup';
        if (element == 'EKNAM') this.filedDetails.property[index] = 'PurchaseGroupDesc';
        if (element == 'EKWSL') this.filedDetails.property[index] = 'PurchaseValuationKey';
        if (element == 'DISMM') this.filedDetails.property[index] = 'MrpType';
        if (element == 'DIBEZ') this.filedDetails.property[index] = 'MrpTypeDesc';
        if (element == 'MHDHB') this.filedDetails.property[index] = 'TotalShelfLife';
        if (element == 'MHDRZ') this.filedDetails.property[index] = 'MinReminderShelfLife';
        if (element == 'LGTKZ') this.filedDetails.property[index] = 'stockPlacement';
        if (element == 'V_QPART') this.filedDetails.property[index] = 'inceptionSetup';
        if (element == 'QSSPUR') this.filedDetails.property[index] = 'QmControlKey';
        if (element == 'KURZTEXT') this.filedDetails.property[index] = 'QmControlKeyDesc';
        if (element == 'VERPR') this.filedDetails.property[index] = 'MovPrice';
        if (element == 'BKLAS') this.filedDetails.property[index] = 'ValuationClass';
        if (element == 'BKBEZ') this.filedDetails.property[index] = 'valuationClassDesc';
        if (element == 'PLNT') this.filedDetails.property[index] = 'PlantDescription';
        if (element == 'WERKS_D') this.filedDetails.property[index] = 'Plant';
        if (element == 'LGORT_D') this.filedDetails.property[index] = 'StorageLocation';
        if (element == 'MBRSH') this.filedDetails.property[index] = 'IndustrySector';
        if (element == 'WGBEZ') this.filedDetails.property[index] = 'MaterialGroupDesc';
        if (element == 'KLASSENART') this.filedDetails.property[index] = 'ClassType';
        if (element == 'ARTXT') this.filedDetails.property[index] = 'ClassTypeDescription';
        if (element == 'KLASSE_D') this.filedDetails.property[index] = 'Class';
        if (element == 'KSCHL') this.filedDetails.property[index] = 'ClassDescription';
        if (element == 'TAXKM') this.filedDetails.property[index] = 'Classification';
        if (element == 'VTEXT2') this.filedDetails.property[index] = 'ClassificationDescription';
        if (element == 'VKORG') this.filedDetails.property[index] = 'SalesOrganization';
        if (element == 'VTEXTV') this.filedDetails.property[index] = 'SalesOrganizationDescription';
        if (element == 'VTWEG') this.filedDetails.property[index] = 'DistibutionChannel';
        if (element == 'VTEXTD') this.filedDetails.property[index] = 'DistibutionChannelDescription';
        if (element == 'PURCHASE_TEXT') this.filedDetails.property[index] = 'PurchaseText';
        if (element == 'LGBKZ') this.filedDetails.property[index] = 'StorageSection';
        if (element == 'LBKZT') this.filedDetails.property[index] = 'StorageDescription';
        if (element == 'KURZTEXT2') this.filedDetails.property[index] = 'InspectionDescription';
        if (element == 'KTGRM') this.filedDetails.property[index] = 'AccountAssignment';
        if (element == 'VTEXT3') this.filedDetails.property[index] = 'AccountAssignmentDescription';
        if (element == 'QMPUR') this.filedDetails.property[index] = 'QMProactive';
        if (element == 'TAXIM1') this.filedDetails.property[index] = 'TaxIndicator';
        if (element == 'XCHPF') this.filedDetails.property[index] = 'Batchmanagement';
      });
    }
  }

  ngOnInit() {
  }

}
