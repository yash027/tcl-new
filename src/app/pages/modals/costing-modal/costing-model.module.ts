import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CostingModelPageRoutingModule } from './costing-model-routing.module';

import { CostingModelPage } from './costing-model.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CostingModelPageRoutingModule
  ],
  declarations: [CostingModelPage]
})
export class CostingModelPageModule {}
