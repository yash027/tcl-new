import { ModalController } from '@ionic/angular';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-costing-model',
  templateUrl: './costing-model.page.html',
  styleUrls: ['./costing-model.page.scss'],
})
export class CostingModelPage implements OnInit {

  @Input() data: any;
  
  constructor(private modalController: ModalController) { }

  ngOnInit() {
  }

  ionViewWillEnter(){

  }

  getAmount(prop1, prop2, prop3, prop4, type){
    if(type == 'price') return ((parseFloat(prop1) + parseFloat(prop2) + parseFloat(prop3)).toFixed(2));
    else return ((parseFloat(prop1) + parseFloat(prop2) + parseFloat(prop3)) * parseFloat(prop4)).toFixed(2);
  }

  closeModal(){
    this.modalController.dismiss();
  }
}
