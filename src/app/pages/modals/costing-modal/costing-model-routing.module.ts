import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CostingModelPage } from './costing-model.page';

const routes: Routes = [
  {
    path: '',
    component: CostingModelPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CostingModelPageRoutingModule {}
