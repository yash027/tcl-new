import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NgSelectModule } from '@ng-select/ng-select';

import { CollaborateModalPage } from './collaborate-modal.page';

const routes: Routes = [
  {
    path: '',
    component: CollaborateModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgSelectModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CollaborateModalPage]
})
export class CollaborateModalPageModule {}
