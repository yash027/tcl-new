import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, LoadingController, AlertController } from '@ionic/angular';
import { HttpService } from 'src/app/services/http.service';
import { GlobalService } from 'src/app/services/global.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-collaborate-modal',
  templateUrl: './collaborate-modal.page.html',
  styleUrls: ['./collaborate-modal.page.scss'],
})
export class CollaborateModalPage {

  segmentValue: any = 'user';
  login: any;

  userList: any = [];
  processDetails: any = {};

  selectedUser: any = {};

  comments: any = {
    userComment: '',
    initiatorComment: ''
  }

  currentUser: any;

  initiator: any;

  toShowCollaborateWithInitiator: boolean = true;

  constructor(
    public modalController: ModalController,
    private loadingController: LoadingController,
    private alertController: AlertController,
    private navParams: NavParams,
    private http: HttpService,
    private global: GlobalService,
    private storage: StorageService
  ) { }

  ionViewWillEnter() {
    if (this.navParams.get('data')) {
      this.processDetails = this.navParams.get('data');
      if (this.processDetails.requestType === 'GI_PROCESS') {
        // Check if there is one approver and is it the same initiator
        let countOfUsers = 0;
        for (let key in this.processDetails.approvalMatrix) {
          if (key.includes('level')) countOfUsers++;
        }
        if (countOfUsers === 1) {
          if (this.processDetails.approvalMatrix['level_1_Approvers'] === this.processDetails.header.RECP_ID) {
            this.alertController.create(
              {
                header: 'Message',
                message: 'You cannot collaborate becaues you are the only approver and the initiator.',
                backdropDismiss: false,
                buttons: [
                  {
                    text: 'Ok',
                    handler: () => {
                      this.modalController.dismiss();
                    }
                  }
                ]
              }
            ).then(alert => alert.present());
          } else {
            this.getUsersBasedOnDOA();
            this.initiator = this.processDetails.header.RECP_ID;
          }
        } else {
          this.getUsersBasedOnDOA();
          this.initiator = this.processDetails.header.RECP_ID;
        }
      } else {
        this.getUserList();
        this.initiator = this.processDetails.createdBy;
      }
    }
  }

  getUsersBasedOnDOA() {
    this.loadingController.create(
      {
        message: 'Loading Users',
        backdropDismiss: false
      }
    ).then(
      loader => {
        loader.present();
        if (this.processDetails.sapDocId !== null) {
          this.http.call_GET('/requests/requestType/' + this.processDetails.requestType + '/requests/' + this.processDetails.requestId + '/getlogusers').subscribe(response => {
            this.userList = response;
            if (this.userList.length === 0) this.segmentValue = 'initiator';
            this.userList.push({
              firstName: this.processDetails.header.RECP_NAME,
              lastName: '',
              login: this.processDetails.header.RECP_ID
            });
            this.storage.getUser().then(currentUser => {
              this.userList = this.userList.filter(user => user.login !== currentUser.login);
              if (currentUser.login === this.processDetails.header.RECP_ID) this.toShowCollaborateWithInitiator = false;
              if (this.toShowCollaborateWithInitiator) this.userList = this.userList.filter(user => user.login !== this.processDetails.header.RECP_ID);
              loader.dismiss();
            });
          });
        } else {
          let users = [], selectedUser;
          users.push(this.processDetails.header.RECP_ID);
          for (let key in this.processDetails.approvalMatrix) {
            if (!(key.includes('approversDate') || key.includes('approversDay') || key.includes('_name') || key.includes('actualApprovalLevels'))) users.push(this.processDetails.approvalMatrix[key]);
          }
          this.http.call_GET('/users?page=0&size=1000').subscribe((usersList: any) => {
            users.forEach(user => {
              selectedUser = usersList.filter(element => element.login === user);
              this.userList.push(selectedUser[0]);
            });
            this.storage.getUser().then(currentUser => {
              this.userList = this.userList.filter(user => user.login !== currentUser.login);
              if (currentUser.login === this.processDetails.header.RECP_ID) this.toShowCollaborateWithInitiator = false;
              if (this.toShowCollaborateWithInitiator) this.userList = this.userList.filter(user => user.login !== this.processDetails.header.RECP_ID);
              if (this.userList.length === 0) this.segmentValue = 'initiator';
              loader.dismiss();
            });
          });
        }
      });
  }

  //search by firstname and login ID
  customSearchFn(term: string, item: any) {
    term = term.toLocaleLowerCase();
    if (term === item.login) console.log(item);
    let value: any;
    if (item.lastName !== null) {
      value = item.login.toLocaleLowerCase().indexOf(term) > -1 || item.firstName.toLocaleLowerCase().indexOf(term) > -1 || item.lastName.toLocaleLowerCase().indexOf(term) > -1 || (item.firstName + " " + item.lastName).toLocaleLowerCase().indexOf(term) > -1;
    } else {
      value = item.login.toLocaleLowerCase().indexOf(term) > -1 ||
        item.firstName.toLocaleLowerCase().indexOf(term) > -1 ||
        (item.login).toLocaleLowerCase().indexOf(term) > -1;
    }
    return value;
  }

  getUserList() {
    this.loadingController.create(
      {
        message: 'Loading Users',
        backdropDismiss: false
      }
    ).then(
      loader => {
        loader.present();
        this.http.call_GET('/users?page=0&size=1000').subscribe((users: any) => {
          loader.dismiss();
          this.storage.getUser().then(storedUser => {
            this.userList = users.filter(user => user.login !== storedUser.login);
          });
        }, error => {
          loader.dismiss();
          this.global.displayToastMessage('Unable to fetch users');
        });
      }
    )
  }

  onCollaborate() {
    if (this.processDetails.status === '5') {
      this.selectedUser.login = this.processDetails.collaborateUser;
    }
    if (this.segmentValue === 'user') {
      if (this.selectedUser && JSON.stringify(this.selectedUser) !== '{}' && this.selectedUser !== null) {
        if ((this.comments.userComment).trim().length > 0) {
          const data = {
            user: this.selectedUser.login,
            name: this.selectedUser.firstName + ' ' + this.selectedUser.lastName,
            comment: (this.comments.userComment !== '') ? this.comments.userComment : ''
          }
          this.modalController.dismiss(data);
        } else {
          this.global.displayToastMessage('Please Enter comments.');
        }
      } else {
        this.global.displayToastMessage('Please select the user with whom you want to collaborate.');
      }
    } else if (this.segmentValue === 'initiator') {
      if ((this.comments.initiatorComment).trim().length > 0) {
        const data = {
          user: this.processDetails.header.RECP_ID || this.initiator,
          comment: (this.comments.initiatorComment !== '') ? this.comments.initiatorComment : ''
        }
        this.modalController.dismiss(data);
      } else {
        this.global.displayToastMessage('Please Enter comments.');
      }
    }
  }
}
