import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-search-engineer',
  templateUrl: './search-engineer.page.html',
  styleUrls: ['./search-engineer.page.scss'],
})
export class SearchEngineerPage implements OnInit {

  engineersList: any = [];
  displayList: any = [];

  constructor(public modalController: ModalController, private navParams: NavParams) { }

  ionViewWillEnter() {
    if (this.navParams.get('data')) {
      this.engineersList = this.navParams.get('data');
      this.displayList = this.engineersList;
    }
  }

  ngOnInit() {
  }

  onSearch(searchValue) {
    this.displayList = [];
    this.engineersList.filter((v) => {
      if (v.EMP_ENG.includes(searchValue) || ((v.EMP_ENG_NAME).toLowerCase()).includes((searchValue).toLowerCase())) {
        this.displayList.push(v);
      }
    });
  }

  onSelectEngineer(engineer) {
    this.modalController.dismiss(engineer);
  }

  onCancel() {
    this.onSearch('');
  }

}
