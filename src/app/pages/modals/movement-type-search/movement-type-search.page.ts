import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { IonSearchbar, LoadingController, ModalController, NavParams } from '@ionic/angular';
import { GoodsIssueService } from '../../main/requests/goods-issue/goods-issue.service';

@Component({
  selector: 'app-movement-type-search',
  templateUrl: './movement-type-search.page.html',
  styleUrls: ['./movement-type-search.page.scss'],
})
export class MovementTypeSearchPage implements OnInit {

  @ViewChild(MatSort, { static: false }) sort: MatSort;

  @ViewChild('searchbar', { static: false }) searchBar: IonSearchbar;

  pageNumber = 0;
  pageSize = 100;
  searchValue = '';

  isLoadedFirstTime: boolean = false;
  selectedPlant: any = '';

  tableData: MatTableDataSource<any> = new MatTableDataSource<any>();
  dataHeaders: any = ['BWART', 'BTEXT'];

  displayHeaderFields: any = {
    BWART: 'Mvt Type',
    BTEXT: 'Mvt Type Description'
  }

  constructor(
    private navParams: NavParams,
    public modalController: ModalController,
    private service: GoodsIssueService
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.selectedPlant = this.navParams.get('data');
    this.getDataForInstance();
  }

  applyFilter(filterValue: string) {
    this.searchValue = filterValue.trim().toLowerCase();
    this.tableData.filter = filterValue;
  }

  getSelectedListItem(row) {
    this.modalController.dismiss({ data: row, instance: 'MovementTypes' });
  }

  getDataForInstance() {
    let finalMovementTypes: any = [];
    this.service.getDynamicInstanceData(
      'MovementTypes', [], '', this.pageNumber, this.pageSize
    ).subscribe((allMovementTypes: any) => {
      this.service.getDynamicInstanceData(
        'MovementTypeBasedOnPlant', ['Plant'], this.selectedPlant, this.pageNumber, this.pageSize
      ).subscribe((plantBasedMvtTypes: any) => {
        if(plantBasedMvtTypes.TotalCount === 1) {
          let sortedMovementTypes = plantBasedMvtTypes.Content[0].MovementTypes;
          sortedMovementTypes = sortedMovementTypes.split(" ");
          sortedMovementTypes.forEach(mvtType => {
            finalMovementTypes.push(allMovementTypes.find(obj => obj.BWART === mvtType));
          });
        } else {
          finalMovementTypes = allMovementTypes;
        }
        this.isLoadedFirstTime = true;
        this.tableData = new MatTableDataSource(finalMovementTypes);
        this.tableData.sort = this.sort;
        if (this.isLoadedFirstTime) {
          this.searchBar.setFocus();
        }
      });
    });
  }

}
