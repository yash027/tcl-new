import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MovementTypeSearchPageRoutingModule } from './movement-type-search-routing.module';

import { MovementTypeSearchPage } from './movement-type-search.page';
import { MatSortModule, MatTableModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatSortModule,
    MatTableModule,
    MovementTypeSearchPageRoutingModule
  ],
  declarations: [MovementTypeSearchPage]
})
export class MovementTypeSearchPageModule {}
