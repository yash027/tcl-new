import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MovementTypeSearchPage } from './movement-type-search.page';

const routes: Routes = [
  {
    path: '',
    component: MovementTypeSearchPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MovementTypeSearchPageRoutingModule {}
