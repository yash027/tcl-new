import { Component, OnInit, ViewChild } from '@angular/core';
import { NavParams, ModalController, IonSearchbar } from '@ionic/angular';
import { GoodsIssueService } from '../../main/requests/goods-issue/goods-issue.service';
import { MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-search-help',
  templateUrl: './search-help.page.html',
  styleUrls: ['./search-help.page.scss'],
})
export class SearchHelpPage implements OnInit {

  @ViewChild(MatSort, { static: false }) sort: MatSort;

  @ViewChild('searchbar', { static: false }) searchBar: IonSearchbar;

  pageNumber = 0;
  pageSize = 100;
  searchValue = '';

  instanceType: any;
  toolbarHeader: any;

  tableData: MatTableDataSource<any> = new MatTableDataSource<any>();
  dataHeaders: any = [];

  isLoadedFirstTime: boolean = false;

  searchHelpHeader: any = {
    MovementTypes: 'Movement Type',
    GLaccount: 'GL Account',
    Profitcenter: 'Profit Center',
    CostCenter: 'Cost Center',
    Businessarea: 'Business Area',
    Fundscenter: 'Funds Center',
    Commitmentitem: 'Commitment Item',
    Departments: 'Department',
    Plant: 'Plant',
    OrderTypes: 'Order Type',
    StorageLocations: 'Storage Location',
    WBSElements: 'WBS Element',
    Vendors: 'Vendor Code',
    WorkOrders: 'Order Number',
    VendorsGI: 'Vendor Code'
  }

  displayHeaderFields: any = {
    BWART: 'Mvt Type',
    BTEXT: 'Mvt Type Description',
    DEPID: 'Department',
    DEPDS: 'Department Description',
    WERKS_D: 'Plant',
    NAME1: 'Plant Description',
    KOSTL: 'Cost Center',
    LTEXT: 'Cost Center Description',
    AUART: 'Order Type',
    TXT: 'Order Type Description',
    MEINS: 'Unit',
    MSEHL: 'Unit Description',
    LGORT_D: 'S.LOC/BIN',
    LGOBE: 'S.LOC/BIN Description',
    SAKNR: 'GLAccount',
    GLACNT_TXT: 'GLAccount Description',
    GSBER: 'Business Area',
    GTEXT: 'Business Area Description',
    PRCTR: 'Profit Center',
    PRC_TXT: 'Profit Center Description',
    FIPEX: 'Commitment Item',
    COMIT_ITEM_DES: 'Commitment Item Description',
    FICTR: 'Funds Center',
    BESCHR: 'Funds Center Description',
    POSID: 'WBS Element',
    WBS_DES: 'WBS Element Description',
    LIFNR: 'Vendor Code',
    VENDOR_NAME: 'Vendor Code Description',
    WRK_ORDER: 'Order No.',
    WRK_ORDER_DES: 'Order Description',
  }

  constructor(
    private navParams: NavParams,
    private service: GoodsIssueService,
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.instanceType = this.navParams.get('data');
    this.getDataForInstance();
  }

  applyFilter(filterValue: string) {
    this.searchValue = filterValue.trim().toLowerCase();
    this.getDataForInstance();
  }

  getSelectedListItem(row) {
    this.modalController.dismiss({ data: row, instance: this.instanceType });
  }

  getDataForInstance() {
    if (this.instanceType === 'ReceivingPlant') {
      this.toolbarHeader = 'Plant';
    } else if (this.instanceType === 'ReceivingStorageLocation') {
      this.toolbarHeader = 'StorageLocations';
    } else {
      this.toolbarHeader = this.instanceType;
    }
    this.service.getDynamicInstanceData(
      this.toolbarHeader, this.dataHeaders, this.searchValue, this.pageNumber, this.pageSize
    ).subscribe((response: any) => {
      this.isLoadedFirstTime = true;
      if (response.Content) {
        this.tableData = new MatTableDataSource(response.Content)
      } else {
        this.tableData = new MatTableDataSource(response);
      }
      this.tableData.sort = this.sort;
      if (response[0]) {
        let headers = Object.keys(response[0]);
        headers.shift();
        this.dataHeaders = headers;
      }
      if (this.isLoadedFirstTime) {
        this.searchBar.setFocus();
      }
    }
    )
  }

  onPaginate(dirType) {
    if (dirType === 'next') {
      this.pageNumber++;
    } else {
      this.pageNumber--;
    }
    this.getDataForInstance();
  }

}
