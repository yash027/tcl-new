import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SearchHelpPageRoutingModule } from './search-help-routing.module';

import { SearchHelpPage } from './search-help.page';
import { MatSortModule, MatTableModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatSortModule,
    MatTableModule,
    SearchHelpPageRoutingModule
  ],
  declarations: [SearchHelpPage]
})
export class SearchHelpPageModule {}
