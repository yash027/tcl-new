import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchHelpPage } from './search-help.page';

const routes: Routes = [
  {
    path: '',
    component: SearchHelpPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchHelpPageRoutingModule {}
