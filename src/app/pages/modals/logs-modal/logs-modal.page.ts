import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-logs-modal',
  templateUrl: './logs-modal.page.html',
  styleUrls: ['./logs-modal.page.scss'],
})
export class LogsModalPage implements OnInit {

  logs: any =[];

  constructor(public modalController: ModalController, 
              private navParams: NavParams, 
              public global: GlobalService) { }

  ionViewWillEnter() {
    if (this.navParams.get('data')) {
      this.logs = this.navParams.get('data');
    }
  }

  ngOnInit() {
  }

}
