import { Injectable } from '@angular/core';
import {
  HttpClient,
} from '@angular/common/http';
import { StorageService } from './storage.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  baseUrl: string = '';

  constructor(private http: HttpClient, private storage: StorageService) {
    this.setBaseUrl();
  }

  call_GET(url) {
    if (!this.baseUrl) {
      this.setBaseUrl();
    }
    return this.http.get(this.baseUrl + '/smartportal-server/api' + url);
  }

  call_POST(url, data?) {
    if (!this.baseUrl) {
      this.setBaseUrl();
    }
    return this.http.post(this.baseUrl + '/smartportal-server/api' + url, data);
  }

  call_PUT(url, data) {
    if (!this.baseUrl) {
      this.setBaseUrl();
    }
    return this.http.put(this.baseUrl + '/smartportal-server/api' + url, data);
  }

  call_DELETE(url) {
    if (!this.baseUrl) {
      this.setBaseUrl();
    }
    return this.http.delete(this.baseUrl + '/smartportal-server/api' + url);
  }

  call_GET_with_rfc(url) {
    if (!this.baseUrl) {
      this.setBaseUrl();
    }
    return this.http.get(this.baseUrl + '/smartportal-saprfc/api' + url);   
  }

  call_POST_with_rfc(url, body) {
    if (!this.baseUrl) {
      this.setBaseUrl();
    }
    return this.http.post(this.baseUrl + '/smartportal-saprfc/api' + url, body);   
  }

  callWithUserAgent(url: string, userDetails) {
    if (!this.baseUrl) {
      this.setBaseUrl();
    }
    const config = {
      headers: {
        "user-agent": "Mozilla/4.0 (compatible; MSIE 6.0; " + "Windows NT 5.2; .NET CLR 1.0.3705;)"
      }
    }
    return this.http.post(this.baseUrl + '/smartportal-server' + url, userDetails, config);
  }

  getCompanyCode(companyCode: number) {
    return this.http.get('https://smartdocs-mobile-login.appspot.com/rest/api/getCompanyURL/' + companyCode);
  }

  downloadAttachment(url) {
    return this.http.get(this.baseUrl + '/smartportal-server/api' + url, {
      responseType: 'blob'
    });
  }

  googleLoginFromBackend(idToken) {
    return this.http.get(this.baseUrl + '/smartportal-server/google/new/tokensignin?token=' + idToken, {headers: {responseType: 'text'}});
    // return this.http.get(this.baseUrl + '/google/tokensignin?token=' + idToken, {headers: {responseType: 'text'}});
  }

  setBaseUrl() {
    this.baseUrl = environment.baseUrl;
  }

  newGoogleLogin(accessToken, refreshToken, expiresIn) {
    return this.http.get(this.baseUrl + '/smartportal-server/mobile/google/signin?accessToken=' + accessToken + '&refreshToken=' + refreshToken + '&expiryTime=' + expiresIn);
  }
}
