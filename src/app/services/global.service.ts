import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { StorageService } from './storage.service';
import { HttpService } from './http.service';
import { FileOpener } from "@ionic-native/file-opener/ngx";
import { File } from "@ionic-native/file/ngx";

@Injectable({ providedIn: 'root' })
export class GlobalService {

    colors: any = ['primary', 'success', 'tertiary'];
    icons: any = ['analytics', 'speedometer', 'filing', 'flask', 'pie'];

    isIOSPlatform: boolean;

    animationClass: string = 'animated fadeInUp';

    isGoogleSignIn: boolean;

    googleSignInConfiguration = {
        'scopes': 'profile email',
        'webClientId': '588881357501-oed3b5etr3gta6mih74c6j8b5uooe5qm.apps.googleusercontent.com',
        'offline': true
    }

    userDetails: any = {};

    constructor(
        private toastController: ToastController,
        private loadingController: LoadingController,
        private router: Router,
        private storage: StorageService,
        private http: HttpService,
        private fileOpener: FileOpener,
        private file: File
    ) {
        if (!this.userDetails.user) {
            this.setUpUser();
        }
    }

    setUpUser() {
        this.storage.getUser().then(user => this.userDetails = user);
    }

    getrequestsById(requestId) {
        return this.http.call_GET('/requests/inbox/requests/' + requestId);
    }

    getMaxDate() {
        let date = new Date();
        return `${date.getFullYear()}-${((date.getMonth() + 1) >= 1 && (date.getMonth() + 1) < 10) ? '0' + (date.getMonth() + 1) : date.getMonth() + 1}-${((date.getDate()) >= 1 && (date.getDate()) < 10) ? '0' + (date.getDate()) : date.getDate()}`
    }

    getColorClass(index) {
        if (index >= this.colors.length) {
            while (index >= this.colors.length) {
                index = index - this.colors.length;
            }
        }
        return this.colors[index];
    }

    getIconName(requestName) {
        let assetUrl = 'assets/svg/';
        switch (requestName) {
            case 'SOR Claim':
            case 'SOR Cliam':
                assetUrl = assetUrl + 'sor.svg';
                break;
            case 'Timesheet Claim':
            case 'Time Claim':
                assetUrl = assetUrl + 'timesheet-claim.svg';
                break;
            case 'Store Goods Issue':
                assetUrl = assetUrl + 'goods-issue.svg';
                break;
            case 'PO Process':
            case 'Purchase Order':
                assetUrl = assetUrl + 'purchase-order.svg';
                break;
            case 'PR Process':
            case 'Pr Process':
            case 'Purchase Requisition':
                assetUrl = assetUrl + 'purchase-requisition.svg';
                break;
            case 'Contracts':
            case 'CONTRACTS':
                assetUrl = assetUrl + 'contracts.svg';
                break;
            case 'SA Process':
            case 'SA_PROCESS':
                assetUrl = assetUrl + 'schedule-agreement.svg';
                break;
            case 'Service Entry':
                assetUrl = assetUrl + 'service-entry.svg';
                break;
            case 'Capex Budget':
            case 'Capital Expenditure Proposal':
                assetUrl = assetUrl + 'capex.svg';
                break;
            case 'Pricing Process':
                assetUrl = assetUrl + 'pricing.svg';
                break;
            case 'Rebate Process':
            case 'Rebate Direct Approval Note':
            case 'Rebate Credit Memo':
            case 'Rebate Settlement':
            case 'Approval Note for Price & Rebate':
                assetUrl = assetUrl + 'rebate-agreement.svg';
                break;
            case 'Direct Settlement':
                assetUrl = assetUrl + 'direct-settlement.svg';
                break;
            case 'Material Master Creation':
            case 'Material Master Extension':
            case 'Material Master Change':
                assetUrl = assetUrl + 'MMG.svg';
                break;
            case 'CREDITAPPROVAL':
            case 'CRED_MANAGE':
            case 'Credit approval process':
                assetUrl = assetUrl + 'credit.svg';
                break;
            default:
                assetUrl = '';
                break;
        }
        return assetUrl;
    }

    displayToastMessage(message) {
        this.toastController
            .create({
                message: message,
                duration: 2000
            })
            .then(toast => toast.present());
    }

    gotoLogin() {
        this.router.navigate(['login']);
    }

    dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        let byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0) {
            byteString = atob(dataURI.split(',')[1]);
        } else {
            byteString = unescape(dataURI.split(',')[1]);
        }
        // separate out the mime component
        let mimeString = dataURI
            .split(',')[0]
            .split(':')[1]
            .split(';')[0];

        // write the bytes of the string to a typed array
        let ia = new Uint8Array(byteString.length);
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], { type: mimeString });
    }

    getSAPDate(date) {
        let selectedDate: any = new Date(date);
        let month: any = selectedDate.getMonth();
        if (month == 9) {
            month = month + 1;
        } else if (month < 9) {
            month = month + 1;
            month = "0" + (month.toString());
        } else if (month > 9) month = month + 1;
        let newDate: any = selectedDate.getDate();
        if (newDate <= 9) {
            newDate = "0" + (newDate.toString());
        }
        return selectedDate = selectedDate.getFullYear().toString() + month + newDate;
    }

    getPortalDate(date) {
        if (date) {
            const year = date.slice(0, 4);
            const month = date.slice(4, 6);
            const day = date.slice(6, 8);
            const newDate = day + '-' + month + '-' + year;
            return newDate;
        }
    }

    getValidationDate(date) {
        if (date) {
            const year = date.slice(0, 4);
            const month = date.slice(4, 6);
            const day = date.slice(6, 8);
            const newDate = month + '-' + day + '-' + year;
            return newDate;
        }
    }

    getPortalTime(time) {
        if (time) {
            const hour = time.slice(0, 2);
            const min = time.slice(2, 4);
            const sec = time.slice(4, 6);
            const newTime = hour + ':' + min + ':' + sec;
            return newTime;
        }
    }

    getRandomNumber() {
        return Math.floor(10000000 + Math.random() * 90000000);
    }

    getBytesConversion(a, b) {
        if (0 === a) {
            return '0 Bytes';
        }
        const c = 1024,
            d = b || 2,
            e = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            f = Math.floor(Math.log(a) / Math.log(c));
        return parseFloat((a / Math.pow(c, f)).toFixed(d)) + ' ' + e[f];
    }

    getAttachmentIcon(type) {
        let iconName;
        switch (type) {
            case 'jpg':
            case 'jpeg':
            case 'image/jpg':
            case 'image/jpeg':
            case 'image/png':
            case 'png': {
                iconName = 'image';
                break;
            }
            case 'html':
            case 'text/html': {
                iconName = 'logo-html5';
                break;
            }
            case 'css':
            case 'text/css': {
                iconName = 'logo-css3';
                break;
            }
            case 'txt':
            case 'text/plain': {
                iconName = 'text';
                break;
            }
            default: {
                iconName = 'document';
                break;
            }
        }
        return iconName;
    }

    hideBottomTabs() {
        const elem = <HTMLElement>document.querySelector('#tabs');
        if (elem != null) {
            elem.style.display = 'none';
        }
    }

    showBottomTabs() {
        const elem = <HTMLElement>document.querySelector('#tabs');
        if (elem != null) {
            elem.style.display = 'flex';
        }
    }

    // Other Scopes Type value check
    checkForType(request: any) {
        let value: any;
        if (request.status === '5') {
            value = 'Collaborated';
        } else {
            switch (request.header.WORKFLOW_REPEAT) {
                case 'X':
                    value = 'Release';
                    break;
                case 'Y':
                    value = 'Re-Release';
                    break;
                default:
                    value = '';
                    break;
            }
        }
        return value;
    }

    clearUser() {
        sessionStorage.clear();
        localStorage.clear();
        this.storage.deleteUser();
    }

    openDocumentInViewer(attachmentUrl: any) {
        const url = 'https://docs.google.com/viewer?url=' + encodeURIComponent(attachmentUrl);
        let browser = window.open(url, '_blank', 'location=no');
    }

    getActivityByList(activityText, activityList) {
        return activityList.find(element => element.buttonText === activityText);
    }

    downloadFile(reqId, attachment) {
        this.loadingController.create(
            {
                message: 'Please wait while file is getting ready for preview...'
            }
        ).then(loader => {
            loader.present();
            const url = '/requests/inbox/filedownload?archiveId=' + attachment.archiveId + '&requestId=' + reqId + '&archiveDocId=' + attachment.archiveDocId + '&attachment=true';
            this.http.downloadAttachment(url).subscribe(blobFile => {
                this.file.writeFile(this.file.externalRootDirectory + '/Download', attachment.fileName, blobFile, { replace: true }).then(fileEntry => {
                    this.fileOpener.open(fileEntry.nativeURL, attachment.mimeType).then(success => {
                        loader.dismiss();
                        console.log('File Opened');
                    }).catch(error => {
                        loader.dismiss();
                        this.displayToastMessage('File is not getting opened from device.');
                        console.log('File Not Opened');
                    });
                }).catch(error => {
                    console.log(error);
                    this.displayToastMessage('Error occurred while fetching file for preview. Please try after some time.');
                    loader.dismiss();
                });
            }, error => {
                loader.dismiss();
                this.displayToastMessage('Error occurred while fetching file from server');
            });
        })
    }

    generateSubmissionMessage(activity, user?) {
        let message;
        switch (activity.buttonText) {
            case 'Approve':
                message = 'Request has been successfully Approved';
                break;
            case 'Reject':
            case 'RejectComplete':
                message = 'Request has been successfully Rejected';
                break;
            case 'Collaborate':
                message = 'Collaboration successfully done with User : ' + user;
                break;
            case 'CollaborateBack':
                message = 'Collaborated Back successfully done with User : ' + user;
                break;
            case 'Cancel':
                message = 'Request has been successfully Cancelled';
                break;
            case 'Resubmit':
                message = 'Request has been successfully Resubmitted';
                break;
            default:
                message = activity.buttonText;
                break;
        }
        return message;
    }

    getLogTextBasedOnActivity(activity) {
        let text = '';
        switch (activity) {
            case 'Submit':
            case 'Initiated':
            case 'CreateGoodsIssue':
                text = 'Initiated';
                break;
            case 'Approve':
                text = 'Approved';
                break;
            case 'Reject':
            case 'RejectComplete':
            case 'POReject':
                text = 'Rejected';
                break;
            case 'Collaborate':
                text = 'Collaborated';
                break;
            case 'Cancel':
                text = 'Cancelled';
                break;
            case 'CollaborateBack':
                text = 'Collaborated Back';
                break;
            case 'SubmitForApproval':
                text = 'Submitted';
                break;
            case 'GIBGActivity':
            case 'CreateReservation':
                text = 'Reservation Created';
                break;
            default:
                text = activity;
                break;
        }
        return text;
    }

    getWorkflowStatusForGoodsIssue(workItem, user?) {
        let status = '';
        if (workItem.sapDocId === null || (workItem.steps === null || Object.keys(workItem.steps).length === 0)) {
            if (workItem.logs && workItem.logs.length > 0) {
                switch (workItem.logs[workItem.logs.length - 1].activityText) {
                    case 'Initiated':
                    case 'Submit':
                        if (workItem.prepostApprovalDecider.isPostApprovalPlant) status = 'Reservation Created';
                        else status = 'Initiated';
                        break;
                    case 'Approve':
                        status = 'Approved';
                        break;
                    case 'RejectComplete':
                        status = 'Rejected';
                        break;
                    case 'Cancel':
                        status = 'Cancelled';
                        status;
                    case 'Collaborate':
                        status = 'Collaborated';
                        break;
                    case 'CollaborateBack':
                        status = 'Collaborated Back';
                        break;
                    case 'Resubmit':
                        status = 'Re-Submitted';
                        break;
                    default:
                        if (workItem.logs && workItem.logs.length > 0) status = workItem.logs[workItem.logs.length - 1].activityText;
                        else status = '';
                        break;
                }
            } else {
                status = '';
            }
        } else {
            let loggedInUsername;
            if (user) loggedInUsername = user.login;
            else loggedInUsername = this.userDetails.login;
            if (workItem.logs && workItem.logs.length > 0) {
                if (workItem.logs[workItem.logs.length - 1].activityText !== 'Collaborate') {
                    if (workItem.logs[workItem.logs.length - 1].activityText !== 'CollaborateBack') {
                        if (workItem.header.FL_STATUS && workItem.header.FL_STATUS === 'Completed') {
                            status = 'Completed';
                        } else {
                            for (let key in workItem.steps) {
                                let stepUser = workItem.steps[key].users[0].toLowerCase();
                                if (stepUser === loggedInUsername) {
                                    switch (workItem.steps[key].stepDescription) {
                                        case 'StoreIncharge':
                                            status = 'Reservation Created';
                                            break;
                                        case 'Storekeeper':
                                            status = 'Approved';
                                            break;
                                        case 'GIInitiator':
                                            status = 'Delivered';
                                            break;
                                    }
                                }
                            }
                        }
                    } else {
                        status = 'Collaborated Back';
                    }
                } else {
                    status = 'Collaborated';
                }
            }
        }
        return status;
    }

    goodsIssuePrePostApprovalDecider(workItem) {
        let decider = {
            isPreApprovalPlant: false,
            isPostApprovalPlant: false
        }
        switch (workItem.header.WERKS) {
            case 'MMGD':
            case 'MKAJ':
            case 'MMSA':
                decider.isPostApprovalPlant = true;
                break;
            default:
                decider.isPreApprovalPlant = true;
                break;
        }
        return decider;
    }

    getColorCssClass(text) {
        let colorClass = '';
        switch (text) {
            case 'Initiated':
                colorClass = 'initiatedChip';
                break;
            case 'Approved':
            case 'Resubmit':
            case 'Reservation Created':
                colorClass = 'approvedChip';
                break;
            case 'Rejected':
            case 'Cancelled':
                colorClass = 'rejectedChip';
                break;
            case 'Collaborated':
            case 'Collaborated Back':
                colorClass = 'collaboratedChip';
                break;
            default:
                colorClass = 'outForDeliveryChip';
                break;
        }
        return colorClass;
    }

    getColorTextClass(text) {
        let colorClass = '';
        switch (text) {
            case 'Initiated':
                colorClass = 'initiatedText';
                break;
            case 'Approved':
            case 'Resubmit':
                colorClass = 'approvedText';
                break;
            case 'Rejected':
            case 'Canceled':
                colorClass = 'rejectedText';
                break;
            case 'Collaborated':
            case 'Collaborated Back':
                colorClass = 'collaboratedText';
                break;
            case 'Delivered':
            case 'Completed':
                colorClass = 'outForDeliveryText';
                break;
            default:
                colorClass = 'outForDeliveryText';
                break;
        }
        return colorClass;
    }

    getProperOtherScopeName(text) {
        let properName = '';
        switch (text) {
            case 'PO_PROCESS':
            case 'Po Process':
                properName = 'PO Process';
                break;
            case 'PR_PROCESS':
            case 'Pr Process':
                properName = 'PR Process';
                break;
            case 'SERVICE_ENTRY':
            case 'Service Entry':
                properName = 'Service Entry';
                break;
            case 'CONTRACTS':
                properName = 'Contracts';
                break;
            case 'SA_PROCESS':
            case 'Sa Process':
                properName = 'SA Process';
                break;
            default:
                properName = text;
                break;
        }
        return properName;
    }

    setAttachmentName(attachmentName) {
        let name = attachmentName;
        let format = '';
        if (name.length > 10) {
            for (let i = name.length - 1; i >= 0; i--) {
                if (name[i] === '.') {
                    format += name[i];
                    break;
                } else {
                    format += name[i];
                }
            }
            format = format.split("").reverse().join("");
            name = name.substring(0, 15) + '... ' + format;
        }
        return name;
    }

    generateRandomNumber() {
        let maximum = 1, minimum = 9999;
        return Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
    }

    convertToLocalDate(date) {
        return new Date(date).toLocaleDateString();
    }

    convertZonalDate(date) {
        let month, n_date, year, new_date;
        new_date = new Date(date);
        month = (new_date.getMonth() + 1).toString();
        if (month.length == 1) month = '0' + month;
        year = new_date.getFullYear();
        n_date = new_date.getDate().toString();
        if (n_date.length == 1) n_date = '0' + n_date;
        new_date = `${n_date}-${month}-${year}`;
        return new_date;
    }

    goodsIssueInputFieldsDecider(workItem) {
        var fields = {
            checkboxSelection: false,
            storeKeeperSelection: false,
            movementReasonSelection: false,
            grgiSlipSelection: false,
            issueQtySelection: false,
            stockBeforeSelection: false,
            remarksSelection: false,
            deliveredToSelection: false,
            storageLocationSelection: false,
            batchSelection: false,
            reserveQuantitySelection: false,
            specialStockSelection: false,
            vendorCodeSelection: false,
            finalInitiatorCollaborateButtonText: 'Collaborate With Store Keeper'
        }
        switch (workItem.header.WERKS) {
            case 'MMGD':
            case 'MKAJ':
            case 'MMSA':
                if (Object.keys(workItem.steps).length === 0) {
                    fields.checkboxSelection = true;
                } else {
                    if (workItem.sapUsersForGoodsIssue.isStoreIncharge) {
                        fields.movementReasonSelection = true;
                        fields.issueQtySelection = true;
                        fields.stockBeforeSelection = true;
                        fields.remarksSelection = true;
                        fields.deliveredToSelection = true;
                    } else if (workItem.sapUsersForGoodsIssue.isFinalInitiator) {
                        fields.finalInitiatorCollaborateButtonText = 'Collaborate With Store Incharge';
                    }
                }
                break;
            case 'TMPC':
            case 'TMCT':
            case 'TMSA':
            case 'TMCC':
            case 'TMPH':
            case 'TMRV':
            case 'TMFS':
            case 'TMSM':
            case 'TMST':
            case 'TMBC':
                if (workItem.sapDocId === null) {
                    fields.checkboxSelection = true;
                } else {
                    if (workItem.sapUsersForGoodsIssue.isStoreIncharge) {
                        fields.movementReasonSelection = true;
                        fields.grgiSlipSelection = true;
                        fields.storeKeeperSelection = true;
                        fields.storageLocationSelection = true;
                        fields.batchSelection = true;
                        fields.reserveQuantitySelection = true;
                        fields.specialStockSelection = true;
                        fields.vendorCodeSelection = true;
                    } else if (workItem.sapUsersForGoodsIssue.isStoreKeeper) {
                        fields.issueQtySelection = true;
                        fields.stockBeforeSelection = true;
                        fields.remarksSelection = true;
                        fields.deliveredToSelection = true;
                    }
                }
                break;
            case 'NELR':
                if (workItem.sapDocId === null) {
                    fields.checkboxSelection = true;
                } else {
                    if (workItem.sapUsersForGoodsIssue.isStoreIncharge) {
                        fields.movementReasonSelection = true;
                        fields.grgiSlipSelection = true;
                        fields.storeKeeperSelection = true;
                        fields.storageLocationSelection = true;
                        fields.batchSelection = true;
                        fields.reserveQuantitySelection = true;
                        fields.specialStockSelection = true;
                        fields.vendorCodeSelection = true;
                    } else if (workItem.sapUsersForGoodsIssue.isStoreKeeper) {
                        fields.issueQtySelection = true;
                        fields.stockBeforeSelection = true;
                        fields.remarksSelection = true;
                        fields.deliveredToSelection = true;
                    }
                }
            default:
                if (workItem.sapDocId === null) {
                    fields.checkboxSelection = true;
                } else {
                    if (workItem.sapUsersForGoodsIssue.isStoreIncharge) {
                        fields.movementReasonSelection = true;
                        fields.grgiSlipSelection = true;
                        fields.storeKeeperSelection = true;
                    } else if (workItem.sapUsersForGoodsIssue.isStoreKeeper) {
                        fields.issueQtySelection = true;
                        fields.stockBeforeSelection = true;
                        fields.remarksSelection = true;
                        fields.deliveredToSelection = true;
                    }
                }
                break;
        }
        return fields;
    }

    getUserDetailsById(loginId) {
        return this.http.call_GET('/users/' + loginId);
    }

}