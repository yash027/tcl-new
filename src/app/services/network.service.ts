import { Injectable } from "@angular/core";
import { Network } from '@ionic-native/network/ngx';
import { ToastController } from "@ionic/angular";

@Injectable({
    providedIn: 'root'
})
export class NetworkService {

    connectionMessage = '';
    networkConnection: any;

    constructor(private network: Network, private toastController: ToastController) {
    }

    startWatchingNetworkConnection() {
        this.networkConnection = this.network.onConnect().subscribe(connected => {
            if (this.connectionMessage !== 'connected') {
                this.toastController.create(
                    {
                        color: 'success',
                        message: 'Connected',
                        duration: 2000
                    }
                ).then(toast => toast.present());
                this.connectionMessage = 'connected';
            }
        });
        this.network.onDisconnect().subscribe(disconnected => {
            if (this.connectionMessage !== 'notConnected') {
                this.toastController.create(
                    {
                        color: 'danger',
                        message: 'Not Connected To Internet, Please Check Your Connection',
                        duration: 2000
                    }
                ).then(toast => toast.present());
                this.connectionMessage = 'notConnected';
            }
        })
    }

    stopWatchingNetworkConnection() {
        this.networkConnection.unsubscribe();
    }

}