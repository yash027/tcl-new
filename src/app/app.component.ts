import { Component, QueryList, ViewChildren } from '@angular/core';
import { Platform, IonRouterOutlet, AlertController, ActionSheetController, PopoverController, ModalController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { GlobalService } from './services/global.service';
import { Location } from '@angular/common';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError, NavigationCancel, ActivatedRoute } from '@angular/router';
import { NetworkService } from './services/network.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  lastTimeBackPress = 0;
  timePeriodToExit = 2000;

  loading = true;

  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private global: GlobalService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private networkService: NetworkService,
    private alertController: AlertController,
    private actionSheetController: ActionSheetController,
    private popoverController: PopoverController,
    private modalController: ModalController,
    private location: Location,
  ) {
    this.initializeApp();
    this.backButtonEvent();
    router.events.subscribe((routerEvent: Event) => {
      this.checkRouterEvent(routerEvent);
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.networkService.startWatchingNetworkConnection();
    });
  }

  ionViewDidLeave() {
    this.networkService.stopWatchingNetworkConnection();
  }

  checkRouterEvent(routerEvent: Event): void {
    if (routerEvent instanceof NavigationStart) {
      this.loading = true;
    }
    if (routerEvent instanceof NavigationEnd ||
      routerEvent instanceof NavigationCancel ||
      routerEvent instanceof NavigationError) {
      this.loading = false;
    }
    if (routerEvent instanceof NavigationError) {
      this.loading = false;
      this.alertController.create(
        {
          header: 'Message',
          message: 'Some problem occurred, please try after some time.',
          buttons: [
            {
              text: 'Ok',
              role: 'ok'
            }
          ]
        }
      ).then(alert => alert.present());
    }
  }

  backButtonEvent() {
    this.platform.backButton.subscribeWithPriority(9999, async () => {
      try {
        const element = await this.actionSheetController.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
        console.log(error);
      }
      try {
        const element = await this.popoverController.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
        console.log(error);
      }
      try {
        const element = await this.modalController.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
        console.log(error);
      }
      try {
        const element = await this.alertController.getTop();
        if (element) {
          element.dismiss();
          return
        }
      } catch (error) {
        console.log(error);
      }
      if (this.router.url === '/main/requests' || this.router.url === '/main/inbox' || this.router.url === '/main/outbox' || this.router.url === '/main/profile') {
        if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
          navigator['app'].exitApp();
        } else {
          this.global.displayToastMessage('Press back button again to exit');
          this.lastTimeBackPress = new Date().getTime();
          document.addEventListener('backbutton', function (event) {
            event.preventDefault();
            event.stopPropagation();
          }, false);
        }
      } else if (this.router.url === '/main/requests/goods-issue') {
        this.alertController.create(
          {
            header: 'Warning',
            message: 'Are you sure you want to go back?',
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel'
              },
              {
                text: 'Ok',
                handler: () => {
                  this.router.navigate(['/main/requests']);
                }
              }
            ]
          }
        ).then(alert => alert.present());
      } else if(this.router.url === '/main/inbox/GI_PROCESS') {
        this.router.navigate(['/main/inbox']);
      } else if (this.router.url.includes('inbox/GI_PROCESS/')) {
        this.alertController.create(
          {
            header: 'Warning',
            message: 'Are you sure you want to go back?',
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel'
              },
              {
                text: 'Ok',
                handler: () => {
                  this.router.navigate(['/main/inbox/GI_PROCESS']);
                }
              }
            ]
          }
        ).then(alert => alert.present());
      } else if(this.router.url.includes('login')) {
        this.alertController.create(
          {
            header: 'Confirmation',
            message: 'Are you sure you want to exit the app?',
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel'
              },
              {
                text: 'Ok',
                handler: () => {
                  navigator['app'].exitApp();
                }
              }
            ]
          }
        ).then(alert => alert.present());
        // if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
        //   navigator['app'].exitApp();
        // } else {
        //   this.global.displayToastMessage('Press back button again to exit');
        //   this.lastTimeBackPress = new Date().getTime();
        //   document.addEventListener('backbutton', function (event) {
        //     event.preventDefault();
        //     event.stopPropagation();
        //   }, false);
        // }
      } else {
        this.location.back();
      }
    });
  }
}
