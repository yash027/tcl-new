import { Component, ViewChild, ElementRef, Renderer2, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-custom-line-item',
  templateUrl: './custom-line-item.component.html',
  styleUrls: ['./custom-line-item.component.scss'],
})
export class CustomLineItemComponent implements AfterViewInit {

  @ViewChild('collapsedArea', { static: false }) collapsedArea: ElementRef;
  @Input('lineItemObj') lineItemObj: any = {};
  @Input('lineItemId') lineItemId: number = 0;
  @Input('isReservationSynced') isReservationSynced: boolean;
  @Output() removeLineItemById = new EventEmitter();
  @Output() editLineItem = new EventEmitter();
  @Output() checkLineItem = new EventEmitter();
  @Output() stockCheckLineItem = new EventEmitter();

  isCollapsed: boolean;

  constructor(private renderer: Renderer2) { }

  ngAfterViewInit() {
    this.onExpand();
  }

  onExpand() {
    let elementId = 'collapsedContent' + this.lineItemId;
    if (this.isCollapsed) {
      document.getElementById(elementId).style.display = 'block';
      this.renderer.setStyle(this.collapsedArea.nativeElement, 'height', document.getElementById(elementId).clientHeight + 'px');
    } else {
      document.getElementById(elementId).style.display = 'none';
      this.renderer.setStyle(this.collapsedArea.nativeElement, 'height', '0px');
    }
    this.isCollapsed = !this.isCollapsed;
  }

  onRemoveLineItem() {
    this.removeLineItemById.emit(this.lineItemId);
  }

  onEditLineItem() {
    this.editLineItem.emit({
      id: this.lineItemId,
      lineItem: this.lineItemObj
    });
  }

  onSelectLineItem(event) {
    if (event.detail.checked) {
      this.checkLineItem.emit({
        id: this.lineItemId,
        isChecked: 'yes'
      });
    } else {
      this.checkLineItem.emit({
        id: this.lineItemId,
        isChecked: 'no'
      });
    }
  }

  openStockCheckModal() {
    this.stockCheckLineItem.emit({
      id: this.lineItemId,
      lineItem: this.lineItemObj
    });
  }


}
