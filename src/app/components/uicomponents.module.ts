import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { CustomLineItemComponent } from './custom-line-item/custom-line-item.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  declarations: [CustomLineItemComponent],
  exports: [CustomLineItemComponent]
})
export class UIComponentsModule {}