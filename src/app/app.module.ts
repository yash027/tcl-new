import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { Market } from '@ionic-native/market/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { ValidationModalPageModule } from './pages/modals/validation-modal/validation-modal.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Network } from '@ionic-native/network/ngx';
import { FileOpener } from "@ionic-native/file-opener/ngx";
import { File } from "@ionic-native/file/ngx";
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { IonicStorageModule } from '@ionic/storage';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HTTPInterceptor } from './guards/http.interceptor';
import { AuthGuardService } from './guards/auth.guard';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SearchEngineerPageModule } from './pages/modals/search-engineer/search-engineer.module';
import { SearchTasklistPageModule } from './pages/modals/search-tasklist/search-tasklist.module';
import { AttachmentsModalPageModule } from './pages/modals/attachments-modal/attachments-modal.module';
import { LogsModalPageModule } from './pages/modals/logs-modal/logs-modal.module';
import { ImagePageModule } from './pages/modals/attachments-modal/image/image.module';
import { SubmissionModalPageModule } from './pages/modals/submission-modal/submission-modal.module';
import { ChangeDocumentHistoryPageModule } from './pages/modals/change-document-history/change-document-history.module';
import { LogsAndAttachmentsPageModule } from './pages/modals/logs-and-attachments/logs-and-attachments.module';
import { CollaborateModalPageModule } from './pages/modals/collaborate-modal/collaborate-modal.module';
import { SearchHelpPageModule } from './pages/modals/search-help/search-help.module';
import { MaterialSearchHelpPageModule } from './pages/modals/material-search-help/material-search-help.module';
import { LineItemPlantSearchPageModule } from './pages/modals/line-item-plant-search/line-item-plant-search.module';
import { ReservationSearchPageModule } from './pages/modals/reservation-search/reservation-search.module';
import { GIFilterModulePageModule } from './pages/modals/gifilter-module/gifilter-module.module';
import { CostingModelPageModule } from './pages/modals/costing-modal/costing-model.module';
import { MovementTypeSearchPageModule } from './pages/modals/movement-type-search/movement-type-search.module';
import { DepartmentSearchPageModule } from './pages/modals/department-search/department-search.module';
import { CollaborateGoodsIssuePageModule } from './pages/modals/collaborate-goods-issue/collaborate-goods-issue.module';
import { StockCheckModalPageModule } from './pages/modals/stock-check-modal/stock-check-modal.module';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule, 
    HttpClientModule,
    IonicStorageModule.forRoot(),
    BrowserAnimationsModule,
    SearchEngineerPageModule,
    SearchTasklistPageModule,
    AttachmentsModalPageModule,
    LogsModalPageModule,
    ImagePageModule,
    SubmissionModalPageModule,
    ChangeDocumentHistoryPageModule,
    LogsAndAttachmentsPageModule,
    CollaborateModalPageModule,
    SearchHelpPageModule,
    MaterialSearchHelpPageModule,
    LineItemPlantSearchPageModule,
    ReservationSearchPageModule,
    GIFilterModulePageModule,
    CostingModelPageModule,
    ValidationModalPageModule,
    MovementTypeSearchPageModule,
    DepartmentSearchPageModule,
    CollaborateGoodsIssuePageModule,
    StockCheckModalPageModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthGuardService,
    Network,
    GooglePlus,
    FileOpener,
    AppVersion,
    Market,
    BackgroundMode,
    File,
    { 
      provide: RouteReuseStrategy, 
      useClass: IonicRouteStrategy 
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HTTPInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
