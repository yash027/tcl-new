import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Router } from '@angular/router';
import { GlobalService } from '../services/global.service';

@Injectable()
export class HTTPInterceptor implements HttpInterceptor {

  constructor(
    private router: Router,
    private global: GlobalService
    ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // Add authorization header with JWT token if available.
    let accessToken;
    if (!(request.url.includes('/authenticate'))) {
      accessToken = sessionStorage.getItem('accessToken');
    }
    if (accessToken) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${accessToken}`,
        }
      });
    } else {
      request = request.clone({
        setHeaders: {
          "user-agent": "Mozilla/4.0 (compatible; MSIE 6.0; " + "Windows NT 5.2; .NET CLR 1.0.3705;)"
        }
      });
    }
    // Next, hit's this block for every HTTP request. So that we can catch the error here only.
    return next.handle(request).pipe(
      // Retry the failed request for 1 more time.
      retry(1),
      catchError((error: HttpErrorResponse) => {
        // this.router.navigate(['/login']);
        // this.global.displayToastMessage('Some problem occurred while communicating from server, Please try after some time.');
        return throwError(error);
      })
    );
  }
}
